<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>@lang('backend.login_page_title', ['name' => $setting->backend_name], env('BACKEND_LOCALE'))</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <link rel="shortcut icon" href="{{asset('images/app_icon/favicon.ico')}}" type="image/x-icon">
  <link rel="apple-touch-icon" sizes="57x57" href="{{asset('images/apple-touch-icon-57x57.png')}}">
  <link rel="apple-touch-icon" sizes="60x60" href="{{asset('images/apple-touch-icon-60x60.png')}}">
  <link rel="apple-touch-icon" sizes="72x72" href="{{asset('images/apple-touch-icon-72x72.png')}}">
  <link rel="apple-touch-icon" sizes="76x76" href="{{asset('images/apple-touch-icon-76x76.png')}}">
  <link rel="apple-touch-icon" sizes="114x114" href="{{asset('images/apple-touch-icon-114x114.png')}}">
  <link rel="apple-touch-icon" sizes="120x120" href="{{asset('images/apple-touch-icon-120x120.png')}}">
  <link rel="apple-touch-icon" sizes="144x144" href="{{asset('images/apple-touch-icon-144x144.png')}}">
  <link rel="apple-touch-icon" sizes="152x152" href="{{asset('images/apple-touch-icon-152x152.png')}}">
  <link rel="apple-touch-icon" sizes="180x180" href="{{asset('images/apple-touch-icon-180x180.png')}}">
  <!-- <link rel="icon" type="image/png" href="{{asset('images/favicon-16x16.png')}}" sizes="16x16">
  <link rel="icon" type="image/png" href="{{asset('images/favicon-32x32.png')}}" sizes="32x32">
  <link rel="icon" type="image/png" href="{{asset('images/favicon-96x96.png')}}" sizes="96x96">
  <link rel="icon" type="image/png" href="{{asset('images/android-chrome-192x192.png')}}" sizes="192x192"> -->
  <meta name="msapplication-square70x70logo" content="{{asset('images/smalltile.png')}}" />
  <meta name="msapplication-square150x150logo" content="{{asset('images/mediumtile.png')}}" />
  <meta name="msapplication-wide310x150logo" content="{{asset('images/widetile.png')}}" />
  <meta name="msapplication-square310x310logo" content="{{asset('images/largetile.png')}}" />

  <link rel="stylesheet" href="{{asset('AdminLTE/bootstrap/css/bootstrap.min.css')}}">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <link rel="stylesheet" href="{{asset('AdminLTE/dist/css/AdminLTE.min.css')}}">
  <link rel="stylesheet" href="{{asset('AdminLTE/dist/css/custom.css')}}">

  <script src='https://www.google.com/recaptcha/api.js'></script>

  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <b>{{ $setting->backend_name }}</b></br>@lang('backend.cms', [], env('BACKEND_LOCALE'))</a>
  </div>
  <div class="login-box-body">
      @include('backend.layouts.message')
    <form action="{{ route('backend.login.submit') }}" method="post">
      {{ csrf_field() }}
      <div class="form-group has-feedback{{ ($errors->first('account')) ? ' has-error' : '' }}">
        <label for="account">@lang('validation.attributes.admin_account', [], env('BACKEND_LOCALE'))</label>
        <input type="text" id="account" name="account" class="form-control" placeholder="Email" value="{{old('account')}}">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback{{ ($errors->first('password')) ? ' has-error' : '' }}">
        <label for="password">@lang('validation.attributes.admin_password', [], env('BACKEND_LOCALE'))</label>
        <input type="password" id="password" name="password"  class="form-control" placeholder="Password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="form-group captcha">
        <div class="captcha-box">
            <div class="g-recaptcha" data-sitekey="{{ env('RE_CAP_SITE') }}"></div>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12">
          <button type="submit" class="btn btn-primary btn-block btn-flat">@lang('backend.login', [], env('BACKEND_LOCALE'))</button>
        </div>
      </div>
    </form>

  </div>
</div>

<script src="{{asset('AdminLTE/plugins/jQuery/jquery-2.2.3.min.js')}}"></script>
<script src="{{asset('AdminLTE/bootstrap/js/bootstrap.min.js')}}"></script>
</body>
</html>
