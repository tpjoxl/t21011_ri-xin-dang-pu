  @include('backend.common.image.index_btns', ['wrap_class'=>'box-header with-border', 'datas' => $datas])
  {{-- @include('backend.layouts.pager', ['wrap_class'=>'box-header with-border']) --}}
  <div class="box-body">
    @if (count($datas)<1)
      <div class="alert alert-primary text-center" role="alert">@lang('backend.img_max_size', ['mb'=>8], env('BACKEND_LOCALE'))</div>
      <div class="no-data text-center">@lang('backend.no_img', [], env('BACKEND_LOCALE'))</div>
    @else
      <div class="alert alert-primary text-center" role="alert">@lang('backend.img_max_size', ['mb'=>8], env('BACKEND_LOCALE'))<br>@lang('backend.img_rank_hint', [], env('BACKEND_LOCALE'))</div>
      <div id="img-list" class="row">
        @foreach ($datas as $img)
          <div class="col-lg-2 col-md-3 col-sm-4 col-xs-6" data-id="{{ $img->id }}">
            <div class="img-item">
              <img src="{{asset($img->thumb_path)}}" alt="">
              @foreach ($lang_datas as $lang_code => $lang_name)
                <div class="form-group">
                  <label class="control-label">@lang('backend.img_desc', [], env('BACKEND_LOCALE'))({{$lang_name}})</label>
                  <input class="form-control" type="text" name="lang[{{$lang_code}}][imgdesc][]" data-id="{{$img->id}}" data-lang="{{$lang_code}}" placeholder="@lang('backend.please_input_img_desc', [], env('BACKEND_LOCALE'))" value="{{$img->translate($lang_code)?$img->translate($lang_code)->description:''}}">
                </div>
              @endforeach
              <label class="delete-checkbox"><input class="selectedItem" type="checkbox" name="selectedItem[]" value="{{ $img->id }}"></label>
              @if ($img->is_cover)
                <div class="cover-tag">@lang('backend.cover', [], env('BACKEND_LOCALE'))</div>
              @endif
            </div>
          </div>
        @endforeach
      </div>
    @endif
  </div>
  <form id="rankUpdate" method="POST" action="{{route('backend.'.$prefix.'.rank.update')}}" class="form">
      {{ csrf_field() }}
      <input type="hidden" id="sortNum" name="sortNum" value="">
  </form>
  <form id="imageUpload" class="form-horizontal" method="post" action="{{route('backend.'.$prefix.'.store')}}" accept-charset="UTF-8" enctype="multipart/form-data" style="display: none">
    {{ csrf_field() }}
    <input type="hidden" name="parent" value="{{$parent->id}}">
    <input type="file" name="images[]" data-max-size="8192" multiple="multiple">
  </form>
  {{-- @include('backend.layouts.pager', ['wrap_class'=>'box-footer']) --}}
  @include('backend.common.image.index_btns', ['wrap_class'=>'box-footer', 'datas' => $datas])
