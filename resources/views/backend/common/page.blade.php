@extends('backend.layouts.master')

@section('content')
  <form class="form-horizontal" method="post" action="" accept-charset="UTF-8" enctype="multipart/form-data">
    @include('backend.common._form', ['btns_view' => 'backend.common.page_btns'])
  </form>
@endsection
