<div class="{{$wrap_class}} text-center">
	<a class="btn btn-default" href="{{route('backend.'.$prefix.'.index')}}"><i class="fa fa-list-ul"></i> @lang('backend.back', [], env('BACKEND_LOCALE'))</a>
	@if (Route::currentRouteName() == 'backend.'.$prefix.'.edit')
			@if ($auth_admin->group->checkPowerByName($prefix.'.destroy'))
				<a class="btn btn-default btn-delete-row del-cate" href="{{ route('backend.'.$prefix.'.destroy', ['id' => $data->id]) }}"><span class="fa fa-trash-o"></span> @lang('backend.delete', [], env('BACKEND_LOCALE'))</a>
			@endif
			@if ($data->level<$data->getLimitLevel() && $auth_admin->group->checkPowerByName($prefix.'.create'))
					<a href="{{route('backend.'.$prefix.'.create')}}?parent_id={{$data->id}}" class="btn btn-success"><i class="fa fa-plus"></i> @lang('backend.add_child', [], env('BACKEND_LOCALE'))</a>
			@endif
			<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> @lang('backend.save', [], env('BACKEND_LOCALE'))</button>
	@else
			<button type="submit" class="btn btn-primary">@lang('backend.submit', [], env('BACKEND_LOCALE'))</button>
	@endif
</div>
