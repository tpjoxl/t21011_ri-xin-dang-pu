<div class="{{$wrap_class}} text-center hidden-print">
		@if (count($datas)>0)
				<label class="btn btn-default"><input type="checkbox" name="chkAll" class="chkAll"> @lang('backend.select_all', [], env('BACKEND_LOCALE'))</label>
				@if ($model->getLimitLevel()>1)
					<button class="btn btn-default dd-control" type="button" data-action="expand-all">@lang('backend.expand_all', [], env('BACKEND_LOCALE'))</button>
					<button class="btn btn-default dd-control" type="button" data-action="collapse-all">@lang('backend.collapse_all', [], env('BACKEND_LOCALE'))</button>
				@endif
				@if (Route::has('backend.'.$prefix.'.destroy') && $auth_admin->group->checkPowerByName($prefix.'.destroy'))
					<a href="{{route('backend.'.$prefix.'.destroy')}}" class="btn btn-default del_select_all"><span class="fa fa-trash-o"></span> @lang('backend.delete_selected', [], env('BACKEND_LOCALE'))</a>
				@endif
				@if (Route::has('backend.'.$prefix.'.status') && $auth_admin->group->checkPowerByName($prefix.'.edit'))
				<span class="btn-menu dropdown">
					<button class="btn btn-default" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						@lang('backend.change_status', [], env('BACKEND_LOCALE')) <span class="name"></span>
						<span class="caret"></span>
					</button>
					<ul class="dropdown-menu">
						@foreach ($menu_status['options'] as $key => $value)
							<li><a href="{{route('backend.'.$prefix.'.status', ['val' => $key])}}" class="setstatus_select_all">{!!$value!!}</a></li>
						@endforeach
					</ul>
				</span>
				@endif
				<button type="button" class="btn btn-primary save-rank"><i class="fa fa-save"></i> @lang('backend.save_sort_level', [], env('BACKEND_LOCALE'))</button>
		@endif
		@if (Route::has('backend.'.$prefix.'.create') && $auth_admin->group->checkPowerByName($prefix.'.create'))
			<a href="{{route('backend.'.$prefix.'.create')}}" class="btn btn-success"><i class="fa fa-plus"></i> @lang('backend.add', [], env('BACKEND_LOCALE'))</a>
		@endif
</div>
