<ol class="dd-list">
@foreach ($parents as $parent)
	<li class="dd-item" data-id="{{$parent['id']}}">
			<div class="dd-handle"><i class="ic fa fa-arrows"></i></div>
			<div class="dd-content">
					<label><input class="selectedItem" type="checkbox" name="selectedItem[]" value="{{ $parent['id'] }}"></label>
						<a href="{{ route('backend.'.$prefix.'.edit', ['id' => $parent['id']]) }}">{{ $parent['title'] }}</a>
					<div class="btn-holder">
							@if ($auth_admin->group->checkPowerByName($prefix.'.create') && $model->getLimitLevel() > $parent['level'])
								<a href="{{ route('backend.'.$prefix.'.create')}}?parent_id={{$parent['id']}}" class="btn btn-circle" data-toggle="tooltip" title="@lang('backend.add_child', [], env('BACKEND_LOCALE'))"><i class="fa fa-plus-circle"></i><i class="fa fa-level-down"></i></a>
							@endif
							<span data-toggle="tooltip" title="{{array_key_exists($menu_status['name'], $valid_attrs)?$valid_attrs[$menu_status['name']]:__('validation.attributes.'.$menu_status['name'], [], env('BACKEND_LOCALE'))}}">{!! $menu_status['options'][$parent[$menu_status['name']]] !!}</span>
							@if ($auth_admin->group->checkPowerByName($prefix.'.edit'))
								<a href="{{ route('backend.'.$prefix.'.edit', ['id' => $parent['id']]) }}" class="btn btn-circle" data-toggle="tooltip" title="@lang('backend.edit', [], env('BACKEND_LOCALE'))"><span class="glyphicon glyphicon-pencil"></span></a>
							@endif
							@if ($auth_admin->group->checkPowerByName($prefix.'.destroy'))
								<a href="{{ route('backend.'.$prefix.'.destroy')}}?id={{$parent['id']}}" class="btn btn-circle btn-delete-row del-cate" data-toggle="tooltip" title="@lang('backend.delete', [], env('BACKEND_LOCALE'))"><span class="glyphicon glyphicon-trash"></span></a>
							@endif
					</div>
			</div>
			@if (count($parent['children']))
				@include('backend.common.category._menu', ['parents'=>$parent['children']])
			@endif
	</li>
@endforeach
</ol>
