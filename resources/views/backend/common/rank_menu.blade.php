@foreach ($parents as $parent)
	<li
	@if (!is_null($category))
		class="{{($parent['id']==$category->id)?'active':''}}"
	@endif
	>
			<a href="{{route('backend.'.$prefix.'.rank.index', $parent['id'])}}">{{$parent['full_path']}}</a>
	</li>

	@if (count($parent['children']))
      @include('backend.common.rank_menu', ['parents'=>$parent['children']])
  @endif
@endforeach
