@extends('backend.layouts.master')

@section('content')
  <form class="form-horizontal" method="post" action="" accept-charset="UTF-8" enctype="multipart/form-data">
    @include('backend.common.page_btns', ['wrap_class'=>'box-header with-border'])
    {{ csrf_field() }}
    <div class="box-body">
      <div class="alert alert-primary text-center" role="alert">@lang('backend.required_txt', [], env('BACKEND_LOCALE'))</div>
    </div>
    <div class="nav-tabs-custom">
      @if (count($form_fields) > count(array_diff(array_keys($form_fields), $transAttrs)))
        <ul class="nav nav-tabs">
          @foreach ($lang_datas as $lang_code => $lang_name)
            <li class="{{($lang_code==config('app.locale'))?'active':''}}"><a href="#{{$lang_code}}" data-toggle="tab">{{$lang_name}}</a></li>
          @endforeach
          @if (count(array_diff(array_keys($form_fields), $transAttrs)))
            <li><a href="#common" data-toggle="tab">@lang('backend.common', [], env('BACKEND_LOCALE'))</a></li>
          @endif
        </ul>
      @endif
      <div class="tab-content">
        @if (count($form_fields) > count(array_diff(array_keys($form_fields), $transAttrs)))
          @foreach ($lang_datas as $lang_code => $lang_name)
            <div class="tab-pane {{($lang_code==config('app.locale'))?'active':''}}" id="{{$lang_code}}">
              @foreach ($form_fields as $form_label => $form_field)
                @if (in_array(is_array($form_field['name'])?$form_field['name'][0]:$form_field['name'], $transAttrs))
                  @php
                    if (old('lang')[$lang_code][$form_field['name']]) {
                      $value = old('lang')[$lang_code][$form_field['name']];
                    } elseif (array_key_exists($lang_code, $data) && array_key_exists($form_field['name'], $data[$lang_code]) && isset($data[$lang_code][$form_field['name']])) {
                      $value = $data[$lang_code][$form_field['name']];
                    } elseif (array_key_exists('default', $form_field)) {
                      $value = $form_field['default'];
                    } else {
                      $value = '';
                    }
                    if (array_key_exists('label', $form_field)) {
                      $label = $form_field['label'];
                    } elseif (array_key_exists($form_label, $valid_attrs)) {
                      $label = $valid_attrs[$form_label];
                    } else {
                      $label = __('validation.attributes.'.$form_label, [], env('BACKEND_LOCALE'));
                    }
                  @endphp
                  @if ($form_field['type'] == 'include')
                    @include($form_field['view'])
                  @else
                    <div class="form-group{{ ($errors->first("lang.$lang_code.".$form_field['name'])) ? ' has-error' : '' }}">
                      <label for="lang[{{$lang_code}}][{{$form_field['name']}}]" class="col-md-2 col-sm-3 control-label">{{$form_field['required']?'* ':''}}{{$label}}</label>
                      <div class="col-sm-9">
                        @if ($form_field['type'] == 'text_input')
                          <input type="text" class="form-control" name="lang[{{$lang_code}}][{{$form_field['name']}}]" value="{{$value}}">
                        @elseif ($form_field['type'] == 'text_area')
                          <textarea class="form-control" name="lang[{{$lang_code}}][{{$form_field['name']}}]" rows="{{array_key_exists('rows', $form_field)?$form_field['rows']:'3'}}">{{$value}}</textarea>
                        @elseif ($form_field['type'] == 'text_editor')
                          <textarea class="ckeditor" name="lang[{{$lang_code}}][{{$form_field['name']}}]">{{$value}}</textarea>
                        @elseif ($form_field['type'] == 'radio')
                          @foreach ($form_field['options'] as $key => $val)
                            <label class="radio-inline">
                                <input type="radio" name="lang[{{$lang_code}}][{{$form_field['name']}}]" value="{{ $key }}" {{ ($key == $value) ? 'checked' : '' }}> {!! $val !!}
                            </label>
                          @endforeach
                        @endif
                      </div>
                      @if (array_key_exists('hint', $form_field))
                        <div class="col-sm-9 col-md-offset-2 col-sm-offset-3">
                          <p class="help-block">{!!$form_field['hint']!!}</p>
                        </div>
                      @endif
                    </div>
                  @endif
                @endif
              @endforeach
            </div>
          @endforeach
        @endif
        <div id="common" class="tab-pane {{count($form_fields) == count(array_diff(array_keys($form_fields), $transAttrs))?'active':''}}">
          @foreach ($form_fields as $form_label => $form_field)
            @if (!in_array(is_array($form_field['name'])?$form_field['name'][0]:$form_field['name'], $transAttrs))
              @php
                if (is_array($form_field['name'])) {
                  $value = array();
                  foreach ($form_field['name'] as $key => $name) {
                    if (old($name)) {
                      $value[$key] = old($name);
                    } elseif (!is_null($data[$name])) {
                      $value[$key] = $data[$name];
                    } elseif (array_key_exists('default', $form_field)) {
                      $value[$key] = $form_field['default'][$key];
                    } else {
                      $value[$key] = '';
                    }
                  }
                } else {
                  $value = '';
                  if (array_key_exists('value', $form_field)) {
                    $value = $form_field['value'];
                  } elseif (old($form_field['name'])) {
                    $value = old($form_field['name']);
                  } elseif (array_key_exists('common', $data) && array_key_exists($form_field['name'], $data['common']) && isset($data['common'][$form_field['name']])) {
                    $value = $data['common'][$form_field['name']];
                  } elseif (array_key_exists('default', $form_field)) {
                    $value = $form_field['default'];
                  } else {
                    $value = '';
                  }
                }
                if (array_key_exists('label', $form_field)) {
                  $label = $form_field['label'];
                } elseif (array_key_exists($form_label, $valid_attrs)) {
                  $label = $valid_attrs[$form_label];
                } else {
                  $label = __('validation.attributes.'.$form_label, [], env('BACKEND_LOCALE'));
                }
              @endphp
              @if ($form_field['type'] == 'include')
                @include($form_field['view'])
              @else
                <div class="form-group{{ ($errors->first(array_key_exists('error_name', $form_field)?$form_field['error_name']:$form_field['name'])) ? ' has-error' : '' }}">
                  <label for="{{$form_field['name']}}" class="col-md-2 col-sm-3 control-label">{{$form_field['required']?'* ':''}}{{$label}}</label>
                  <div class="col-sm-9">
                    @if ($form_field['type'] == 'text_input')
                      <input type="text" class="form-control" name="{{$form_field['name']}}" value="{{$value}}" placeholder="{{array_key_exists('placeholder', $form_field)?$form_field['placeholder']:''}}">
                    @elseif ($form_field['type'] == 'date_input')
                      <input type="text" class="form-control datepicker" name="{{$form_field['name']}}" value="{{$value}}" placeholder="{{__('backend.select_date', [], env('BACKEND_LOCALE'))}}">
                    @elseif ($form_field['type'] == 'text')
                      <p class="form-control-static">{!! $value !!}</p>
                    @elseif ($form_field['type'] == 'text_area')
                      <textarea class="form-control" name="{{$form_field['name']}}" rows="{{array_key_exists('rows', $form_field)?$form_field['rows']:'3'}}">{{$value}}</textarea>
                    @elseif ($form_field['type'] == 'text_editor')
                      <textarea class="ckeditor" name="{{$form_field['name']}}">{{$value}}</textarea>
                    @elseif ($form_field['type'] == 'password_input')
                      <input type="password" class="form-control" name="{{$form_field['name']}}" value="{{$value}}">
                    @elseif ($form_field['type'] == 'img')
                      <div class="input-group">
                        <span class="input-group-btn">
                            <a data-input="{{$form_field['name']}}_thumbnail" data-preview="{{$form_field['name']}}_holder" class="btn btn-primary lfm-img" data-folder="{{array_key_exists('folder', $form_field)?$form_field['folder']:''}}">
                              <i class="fa fa-picture-o"></i> @lang('backend.select_img', [], env('BACKEND_LOCALE'))
                            </a>
                        </span>
                        <input id="{{$form_field['name']}}_thumbnail" class="form-control" type="text" name="{{$form_field['name']}}" value="{{$value}}">
                        <a href="#" class="btn btn-default btn-deletePic input-group-addon">@lang('backend.clear', [], env('BACKEND_LOCALE'))</a>
                      </div>
                      <img id="{{$form_field['name']}}_holder" class="pic-holder" src="{{$value}}">
                      @if (!is_null($form_field['w']))
                        <p class="help-block">@lang('backend.img_size', ['w'=>$form_field['w'], 'h'=>$form_field['h']], env('BACKEND_LOCALE'))</p>
                      @endif
                    @elseif ($form_field['type'] == 'radio')
                      @foreach ($form_field['options'] as $key => $val)
                        <label class="radio-inline">
                            <input type="radio" name="{{$form_field['name']}}" value="{{ $key }}" {{ ($key == $value) ? 'checked' : '' }}> {!! $val !!}
                        </label>
                      @endforeach
                    @elseif ($form_field['type'] == 'set_top')
                      <div class="checkbox">
                        <label><input type="checkbox" name="rank" id="rank" value="0" {{ (isset($value)&&$value==0)? 'checked':'' }}> @lang('backend.set_top', [], env('BACKEND_LOCALE'))</label>
                      </div>
                    @elseif ($form_field['type'] == 'select')
                      @if (array_key_exists('add_first', $form_field) && count($form_field['options']) < 1)
                        <a href="{{$form_field['add_first']['url']}}" class="btn btn-success"><i class="fa fa-plus"></i> @lang('backend.add_first', ['title'=>array_key_exists('title', $form_field['add_first'])?$form_field['add_first']['title']:$label], env('BACKEND_LOCALE'))</a>
                      @else
                        <select class="form-control show-tick" name="{{$form_field['name']}}"
                        @if (array_key_exists('live_search', $form_field))
                          data-live-search="{{$form_field['live_search']}}"
                        @endif>
                          <option value="">{{array_key_exists('placeholder', $form_field)?$form_field['placeholder']:__('backend.please_select', [], env('BACKEND_LOCALE'))}}</option>
                          @include('backend.common._select', ['parents'=>$form_field['options'], 'field'=>$form_field])
                        </select>
                      @endif
                    @endif
                    @if (array_key_exists('hint', $form_field))
                      <p class="help-block">{!!$form_field['hint']!!}</p>
                    @endif
                  </div>
                </div>
              @endif
            @endif
          @endforeach
        </div>
      </div>
    </div>

    @include('backend.common.page_btns', ['wrap_class'=>'box-footer'])
  </form>
@endsection
