@if (count($list_datas)<1)
	<div class="no-data text-center">@lang('backend.no_data', [], env('BACKEND_LOCALE'))</div>
@else
	<div class="table-responsive">
		<table class="table no-margin">
				<thead>
				<tr>
					@if (!isset($noCheckbox))
						<th class="hidden-print"><input type="checkbox" class="chkAll" name="chkAll" onclick=""></th>
					@endif
					@foreach (reset($list_datas) as $list_label => $list_field)
						<th class="text-{{$list_field['align']}}">
							@if(array_key_exists('label', $list_field))
	              {{$list_field['label']}}
	            @elseif(array_key_exists($list_label, $valid_attrs))
	              {{$valid_attrs[$list_label]}}
	            @else
	              {{__('validation.attributes.'.$list_label, [], env('BACKEND_LOCALE'))}}
	            @endif
						</th>
					@endforeach
				</tr>
				</thead>
				<tbody>
						@foreach ($list_datas as $list_data_id => $list_data)
							<tr>
								@if (!isset($noCheckbox))
									<td class="hidden-print"><input class="selectedItem" type="checkbox" name="selectedItem[]" value="{{ $list_data_id }}"></td>
								@endif
								@foreach ($list_data as $field)
									@if ($field['type'] == 'link')
										<td class="text-{{$field['align']}}"><a href="{{ $field['url'] }}"
											@if (array_key_exists('download', $field))
												download="{{ $field['download'] }}"
											@endif>{!! $field['data'] !!}</a></td>
									@elseif ($field['type'] == 'text')
										<td class="text-{{$field['align']}}">{{ $field['data'] }}</td>
									@elseif ($field['type'] == 'html')
										<td class="text-{{$field['align']}}">{!! $field['data'] !!}</td>
									@elseif ($field['type'] == 'btn')
										<td class="text-{{$field['align']}}"><a href="{{ $field['url'] }}" class="btn {{ array_key_exists('class', $field)?$field['class']:'btn-default' }}"
											@if (array_key_exists('download', $field))
												download="{{ $field['download'] }}"
											@endif>{!! $field['data'] !!}</a></td>
									@endif
								@endforeach
							</tr>
						@endforeach
				</tbody>
		</table>
	</div>
@endif
