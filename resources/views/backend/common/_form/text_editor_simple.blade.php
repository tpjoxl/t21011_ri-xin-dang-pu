@if (count($form_fields) > count(array_diff(array_keys($form_fields), $transAttrs)))
  <div class="form-group{{ ($errors->first("lang.$lang_code.".$form_field['name'])) ? ' has-error' : '' }}">
    <label for="lang[{{$lang_code}}][{{$form_field['name']}}]" class="col-md-2 col-sm-3 control-label">{{$form_field['required']?'* ':''}}{{$label}}</label>
    <div class="col-sm-9">
      <textarea id="lang[{{$lang_code}}][{{$form_field['name']}}]" class="form-control" name="lang[{{$lang_code}}][{{$form_field['name']}}]">{{$value}}</textarea>
      @if (array_key_exists('hint', $form_field))
        <p class="help-block">{!!$form_field['hint']!!}</p>
      @endif
    </div>
  </div>
  <script>
    CKEDITOR.replace('lang[{{$lang_code}}][{{$form_field['name']}}]', {
      toolbar : 'simple'
    });
  </script>
@else
  <div class="form-group{{ ($errors->first(array_key_exists('error_name', $form_field)?$form_field['error_name']:$form_field['name'])) ? ' has-error' : '' }}">
    <label for="{{$form_field['name']}}" class="col-md-2 col-sm-3 control-label">{{$form_field['required']?'* ':''}}{{$label}}</label>
    <div class="col-sm-9">
      <textarea id="{{$form_field['name']}}" class="form-control" name="{{$form_field['name']}}">{{$value}}</textarea>
      @if (array_key_exists('hint', $form_field))
        <p class="help-block">{!!$form_field['hint']!!}</p>
      @endif
    </div>
  </div>
  <script>
    CKEDITOR.replace('{{$form_field['name']}}', {
      toolbar : 'simple'
    });
  </script>
@endif
