@include($btns_view, ['wrap_class'=>'box-header with-border'])
{{ csrf_field() }}
<div class="box-body">
  <div class="alert alert-primary text-center" role="alert">@lang('backend.required_txt', [], env('BACKEND_LOCALE'))</div>
</div>
<div class="nav-tabs-custom">
  @if (count(array_diff(array_keys($form_fields), array_diff(array_keys($form_fields), $data->getTransAttrs())))>0)
    <ul class="nav nav-tabs">
      @foreach ($lang_datas as $lang_code => $lang_name)
        <li class="{{($lang_code==config('app.locale'))?'active':''}}"><a href="#{{$lang_code}}" data-toggle="tab">{{$lang_name}}</a></li>
      @endforeach
      @if (count(array_diff(array_keys($form_fields), $data->getTransAttrs()))>0)
        <li><a href="#common" data-toggle="tab">@lang('backend.common', [], env('BACKEND_LOCALE'))</a></li>
      @endif
    </ul>
  @endif
  <div class="tab-content">
    {{-- {{dd($data->getTransAttrs())}} --}}
    @if (count(array_diff(array_keys($form_fields), array_diff(array_keys($form_fields), $data->getTransAttrs())))>0)
      @foreach ($lang_datas as $lang_code => $lang_name)
        <div class="tab-pane {{($lang_code==config('app.locale'))?'active':''}}" id="{{$lang_code}}">
          @foreach ($form_fields as $form_label => $form_field)
            @if (in_array(is_array($form_field['name'])?$form_field['name'][0]:$form_field['name'], $data->getTransAttrs()))
              @php
                if (isset(old('lang')[$lang_code][$form_field['name']])) {
                  $value = old('lang')[$lang_code][$form_field['name']];
                } elseif ($data->translate($lang_code)) {
                  $value = $data->translate($lang_code)[$form_field['name']];
                } elseif (array_key_exists('default', $form_field)) {
                  $value = $form_field['default'];
                } else {
                  $value = '';
                }
                if (array_key_exists('label', $form_field)) {
                  $label = $form_field['label'];
                } elseif (array_key_exists($form_label, $valid_attrs)) {
                  $label = $valid_attrs[$form_label];
                } else {
                  $label = __('validation.attributes.'.$form_label, [], env('BACKEND_LOCALE'));
                }
              @endphp
              <div class="form-group{{ ($errors->first("lang.$lang_code.".$form_field['name'])) ? ' has-error' : '' }}">
                <label for="lang[{{$lang_code}}][{{$form_field['name']}}]" class="col-md-2 col-sm-3 control-label">{{$form_field['required']?'* ':''}}{{$label}}</label>
                <div class="col-sm-9">
                  @if ($form_field['type'] == 'text_input')
                    <input type="text" class="form-control" name="lang[{{$lang_code}}][{{$form_field['name']}}]" value="{{$value}}">
                  @elseif ($form_field['type'] == 'text')
                    <p class="form-control-static">{!! $value !!}</p>
                  @elseif ($form_field['type'] == 'text_area')
                    <textarea class="form-control" name="lang[{{$lang_code}}][{{$form_field['name']}}]" rows="{{array_key_exists('rows', $form_field)?$form_field['rows']:'3'}}">{{$value}}</textarea>
                  @elseif ($form_field['type'] == 'text_editor')
                    <textarea class="ckeditor" name="lang[{{$lang_code}}][{{$form_field['name']}}]">{{$value}}</textarea>
                  @elseif ($form_field['type'] == 'radio')
                    @foreach ($form_field['options'] as $key => $val)
                      <label class="radio-inline">
                          <input type="radio" name="lang[{{$lang_code}}][{{$form_field['name']}}]" value="{{ $key }}" {{ ($key == $value) ? 'checked' : '' }}> {!! $val !!}
                      </label>
                    @endforeach
                  @elseif ($form_field['type'] == 'file')
                    <div class="input-group">
                      <span class="input-group-btn">
                          <a data-input="lang_{{$lang_code}}_{{$form_field['name']}}" class="btn btn-primary lfm-file" data-folder="{{$form_field['folder']}}">
                            <i class="fa fa-file-o"></i> @lang('backend.select_file', [], env('BACKEND_LOCALE'))
                          </a>
                      </span>
                      <input id="lang_{{$lang_code}}_{{$form_field['name']}}" class="form-control" type="text" name="lang[{{$lang_code}}][{{$form_field['name']}}]" value="{{$value}}">
                      <a href="#" class="btn btn-default btn-deletePic input-group-addon">@lang('backend.clear', [], env('BACKEND_LOCALE'))</a>
                    </div>
                  @endif
                </div>
                @if (array_key_exists('hint', $form_field))
                  <div class="col-sm-9 col-md-offset-2 col-sm-offset-3">
                    <p class="help-block">{!!$form_field['hint']!!}</p>
                  </div>
                @endif
              </div>
            @endif
          @endforeach
        </div>
      @endforeach
    @endif
    <div id="common" class="tab-pane {{count(array_diff(array_keys($form_fields), array_diff(array_keys($form_fields), $data->getTransAttrs())))==0?'active':''}}">
      @foreach ($form_fields as $form_label => $form_field)
        @if (!in_array(is_array($form_field['name'])?$form_field['name'][0]:$form_field['name'], $data->getTransAttrs()))
          @php
            if (is_array($form_field['name'])) {
              $value = array();
              foreach ($form_field['name'] as $key => $name) {
                if (old($name)) {
                  $value[$key] = old($name);
                } elseif (!is_null($data[$name])) {
                  $value[$key] = $data[$name];
                } elseif (array_key_exists('default', $form_field)) {
                  $value[$key] = $form_field['default'][$key];
                } else {
                  $value[$key] = '';
                }
              }
            } else {
              $value = '';
              if (array_key_exists('value', $form_field)) {
                $value = $form_field['value'];
              } elseif (old($form_field['name'])) {
                $value = old($form_field['name']);
              } elseif (!is_null($data[$form_field['name']])) {
                $value = $data[$form_field['name']];
              } elseif (array_key_exists('default', $form_field)) {
                $value = $form_field['default'];
              } else {
                $value = '';
              }
            }
            if (array_key_exists('label', $form_field)) {
              $label = $form_field['label'];
            } elseif (array_key_exists($form_label, $valid_attrs)) {
              $label = $valid_attrs[$form_label];
            } else {
              $label = __('validation.attributes.'.$form_label, [], env('BACKEND_LOCALE'));
            }
          @endphp

          @if ($form_field['type'] == 'include')
            @include($form_field['view'])
          @elseif ($form_field['type'] == 'hidden')
            <input type="hidden" name="{{$form_field['name']}}" value="{{$value}}">
          @elseif ($form_field['type'] == 'date_range' || $form_field['type'] == 'datetime_range')
            <div class="form-group{{ ($errors->first($form_field['name'][0])||$errors->first($form_field['name'][1])) ? ' has-error' : '' }}">
              <label for="{{$form_field['name'][0]}}" class="col-md-2 col-sm-3 control-label">{{$form_field['required']?'* ':''}}{{$label}}</label>
              <div class="col-md-3 col-sm-4">
                  <input type="text" class="form-control {{$form_field['type'] == 'date_range'?'datepicker':'datetimepicker'}}" id="{{$form_field['name'][0]}}" name="{{$form_field['name'][0]}}" placeholder="{{$form_field['placeholder'][0]}}" value="{{$value[0]}}">
              </div>
              <div class="col-md-3 col-sm-4 date-off-holder">
                  <input type="text" class="form-control {{$form_field['type'] == 'date_range'?'datepicker':'datetimepicker'}}" id="{{$form_field['name'][1]}}" name="{{$form_field['name'][1]}}" placeholder="{{$form_field['placeholder'][1]}}" value="{{$value[1]}}">
              </div>
              @if (array_key_exists('hint', $form_field))
                <div class="col-sm-9 col-sm-9 col-md-offset-2 col-sm-offset-3">
                  <p class="help-block">{!!$form_field['hint']!!}</p>
                </div>
              @endif
            </div>
          @elseif ($form_field['type'] == 'multi_select')
            <div class="form-group{{ ($errors->first(array_key_exists('error_name', $form_field)?$form_field['error_name'].'.*':$form_field['name'].'.*'))||$errors->first($form_field['name']) ? ' has-error' : '' }}">
              <label for="{{$form_field['name']}}[]" class="col-md-2 col-sm-3 control-label">{{$form_field['required']?'* ':''}}{{$label}}</label>
              <div class="col-sm-9">
                @if (!$form_field['required'])
                  <input type="hidden" name="{{$form_field['name']}}[]" value="">
                @endif
                @if (array_key_exists('add_first', $form_field) && count($form_field['options']) < 1)
                  <a href="{{$form_field['add_first']['url']}}" class="btn btn-success"><i class="fa fa-plus"></i> @lang('backend.add_first', ['title'=>array_key_exists('title', $form_field['add_first'])?$form_field['add_first']['title']:$label], env('BACKEND_LOCALE'))</a>
                @else
                  <select class="form-control show-tick select-level" name="{{$form_field['name']}}[]" title="{{array_key_exists('placeholder', $form_field)?$form_field['placeholder']:__('backend.please_select', [], env('BACKEND_LOCALE'))}}" multiple
                  @if (array_key_exists('max', $form_field))
                    data-max-options="{{$form_field['max']}}"
                  @endif
                  @if (array_key_exists('actions', $form_field))
                    data-actions-box="{{$form_field['actions']}}"
                  @endif
                  @if (array_key_exists('live_search', $form_field))
                    data-live-search="{{$form_field['live_search']}}"
                  @endif>
                    {{-- <option value="">{{array_key_exists('placeholder', $form_field)?$form_field['placeholder']:__('backend.please_select', [], env('BACKEND_LOCALE'))}}</option> --}}
                    @include('backend.common._select', ['parents'=>$form_field['options'], 'field'=>$form_field])
                  </select>
                @endif
              </div>
              @if (array_key_exists('hint', $form_field))
                <div class="col-sm-9 col-sm-9 col-md-offset-2 col-sm-offset-3">
                  <p class="help-block">{!!$form_field['hint']!!}</p>
                </div>
              @endif
            </div>
          @else
            <div class="form-group{{ ($errors->first(array_key_exists('error_name', $form_field)?$form_field['error_name']:$form_field['name'])) ? ' has-error' : '' }}">
              <label for="{{$form_field['name']}}" class="col-md-2 col-sm-3 control-label">{{$form_field['required']?'* ':''}}{{$label}}</label>
              <div class="col-sm-9">
                @if ($form_field['type'] == 'text_input')
                  <input type="text" class="form-control" name="{{$form_field['name']}}" value="{{$value}}" placeholder="{{array_key_exists('placeholder', $form_field)?$form_field['placeholder']:''}}">
                @elseif ($form_field['type'] == 'date_input')
                  <input type="text" class="form-control datepicker" name="{{$form_field['name']}}" value="{{$value}}" placeholder="{{__('backend.select_date', [], env('BACKEND_LOCALE'))}}">
                @elseif ($form_field['type'] == 'datetime_input')
                  <input type="text" class="form-control datetimepicker" name="{{$form_field['name']}}" value="{{$value}}" placeholder="{{__('backend.select_datetime', [], env('BACKEND_LOCALE'))}}">
                @elseif ($form_field['type'] == 'text')
                  <p class="form-control-static">{!! $value !!}</p>
                @elseif ($form_field['type'] == 'text_option')
                  <p class="form-control-static">{!! array_key_exists($value, $form_field['options'])?$form_field['options'][$value]:'' !!}</p>
                @elseif ($form_field['type'] == 'text_options')
                  <p class="form-control-static">
                    @foreach ($value as $key => $val)
                      {{($key != 0)?'、':''}}
                      {!! $form_field['options'][$val] !!}
                    @endforeach
                  </p>
                @elseif ($form_field['type'] == 'text_nl2br')
                  <p class="form-control-static">{!! nl2br($value) !!}</p>
                @elseif ($form_field['type'] == 'text_area')
                  <textarea class="form-control" name="{{$form_field['name']}}" rows="{{array_key_exists('rows', $form_field)?$form_field['rows']:'3'}}">{{$value}}</textarea>
                @elseif ($form_field['type'] == 'text_editor')
                  <textarea class="ckeditor" name="{{$form_field['name']}}">{{$value}}</textarea>
                @elseif ($form_field['type'] == 'password_input')
                  <input type="password" class="form-control" name="{{$form_field['name']}}" value="{{$value}}">
                @elseif ($form_field['type'] == 'img')
                  <div class="input-group">
                    <span class="input-group-btn">
                        <a data-input="{{$form_field['name']}}_thumbnail" data-preview="{{$form_field['name']}}_holder" class="btn btn-primary lfm-img" data-folder="{{$form_field['folder']}}">
                          <i class="fa fa-picture-o"></i> @lang('backend.select_img', [], env('BACKEND_LOCALE'))
                        </a>
                    </span>
                    <input id="{{$form_field['name']}}_thumbnail" class="form-control" type="text" name="{{$form_field['name']}}" value="{{$value}}">
                    <a href="#" class="btn btn-default btn-deletePic input-group-addon">@lang('backend.clear', [], env('BACKEND_LOCALE'))</a>
                  </div>
                  <img id="{{$form_field['name']}}_holder" class="pic-holder" src="{{$value?asset($value):''}}">
                  @if (!is_null($form_field['w']))
                    <p class="help-block">@lang('backend.img_size', ['w'=>$form_field['w'], 'h'=>$form_field['h']], env('BACKEND_LOCALE'))</p>
                  @endif
                @elseif ($form_field['type'] == 'file')
                  <div class="input-group">
                    <span class="input-group-btn">
                        <a data-input="{{$form_field['name']}}_thumbnail" class="btn btn-primary lfm-file" data-folder="{{$form_field['folder']}}">
                          <i class="fa fa-file-o"></i> @lang('backend.select_file', [], env('BACKEND_LOCALE'))
                        </a>
                    </span>
                    <input id="{{$form_field['name']}}_thumbnail" class="form-control" type="text" name="{{$form_field['name']}}" value="{{$value}}">
                    <a href="#" class="btn btn-default btn-deletePic input-group-addon">@lang('backend.clear', [], env('BACKEND_LOCALE'))</a>
                  </div>
                @elseif ($form_field['type'] == 'radio')
                  @foreach ($form_field['options'] as $key => $val)
                    <label class="radio-inline">
                        <input type="radio" name="{{$form_field['name']}}" value="{{ $key }}" {{ ($key == $value) ? 'checked' : '' }}> {!! $val !!}
                    </label>
                  @endforeach
                @elseif ($form_field['type'] == 'set_top')
                  <div class="checkbox">
                    <label><input type="checkbox" name="rank" id="rank" value="0" {{ (isset($value)&&$value===0)? 'checked':'' }}> @lang('backend.set_top', [], env('BACKEND_LOCALE'))</label>
                  </div>
                @elseif ($form_field['type'] == 'select')
                  @if (array_key_exists('add_first', $form_field) && count($form_field['options']) < 1)
                    <a href="{{$form_field['add_first']['url']}}" class="btn btn-success"><i class="fa fa-plus"></i> @lang('backend.add_first', ['title'=>array_key_exists('title', $form_field['add_first'])?$form_field['add_first']['title']:$label], env('BACKEND_LOCALE'))</a>
                  @else
                    <select class="form-control show-tick" name="{{$form_field['name']}}"
                      title="{{array_key_exists('placeholder', $form_field)?$form_field['placeholder']:__('backend.please_select', [], env('BACKEND_LOCALE'))}}"
                    @if (array_key_exists('live_search', $form_field))
                      data-live-search="{{$form_field['live_search']}}"
                    @endif>
                      <option value="">{{array_key_exists('placeholder', $form_field)?$form_field['placeholder']:__('backend.please_select', [], env('BACKEND_LOCALE'))}}</option>
                      @include('backend.common._select', ['parents'=>$form_field['options'], 'field'=>$form_field])
                    </select>
                  @endif
                @endif
                @if (array_key_exists('hint', $form_field))
                  <p class="help-block">{!!$form_field['hint']!!}</p>
                @endif
              </div>
            </div>
          @endif
        @endif
      @endforeach
    </div>
  </div>
</div>
@include($btns_view, ['wrap_class'=>'box-footer'])
