<div class="{{$wrap_class}} text-center">
	<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> @lang('backend.save', [], env('BACKEND_LOCALE'))</button>
</div>
