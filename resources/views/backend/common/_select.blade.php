@foreach ($parents as $parent)
	@unless (array_key_exists('ignore', $field) && $field['ignore'] && $parent['id'] == $field['ignore'])
		<option value="{{$parent['id']}}"
			@if (array_key_exists('full_path_id', $parent))
				data-parents="{{$parent['full_path_id']}}"
			@endif
			@if (array_key_exists('children_id', $parent))
				data-children="{{$parent['children_id']}}"
			@endif
			@if (request()->has($field['name']))
				{{ ($parent['id']==request()->input($field['name'])) ? 'selected' : '' }}
			@elseif ($field['type'] == 'multi_select')
				{{in_array($parent['id'], old($field['name'], (isset($data)?$data->{$field['name']}->pluck('id')->toArray():array())))?'selected':''}}
			@elseif ($field['type'] == 'select')
				{{ (old($field['name'], (isset($data)?$data[$field['name']]:''))==$parent['id']) ? 'selected' : '' }}
			@endif
			@if (array_key_exists('full_path', $parent))
				title="{{$parent['full_path']}}"
			@else
				title="{{$parent[array_key_exists('key', $field)?$field['key']:'title']}}"
			@endif
			>
				@if (array_key_exists('full_path', $parent))
					{{$parent['full_path']}}
				@else
					{{$parent[array_key_exists('key', $field)?$field['key']:'title']}}
				@endif
		</option>

		@if (!empty($parent['children']))
	    @include('backend.common._select', ['parents'=>$parent['children']])
	  @endif
	@endunless
@endforeach
