<div class="{{$wrap_class}} text-center">
	<a class="btn btn-default" href="{{route('backend.'.$prefix.'.index', request()->all())}}"><i class="fa fa-list-ul"></i> @lang('backend.back', [], env('BACKEND_LOCALE'))</a>
	@if (Route::currentRouteName() == 'backend.'.$prefix.'.edit')
		@if (Route::has('backend.'.$prefix.'.destroy') && $auth_admin->group->checkPowerByName($prefix.'.destroy'))
			<a class="btn btn-default" href="{{ route('backend.'.$prefix.'.destroy', ['id' => $data->id]) }}" onclick="if(confirm('{{__('backend.delete_item_confirm', [], env('BACKEND_LOCALE'))}}')==false) return false;"><span class="fa fa-trash-o"></span> @lang('backend.delete', [], env('BACKEND_LOCALE'))</a>
		@endif
		<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i>
			@if (!empty($btn_txt))
				{{$btn_txt}}
			@else
				@lang('backend.save', [], env('BACKEND_LOCALE'))
			@endif
		</button>
	@elseif (Route::currentRouteName() == 'backend.'.$prefix.'.create')
		<button type="submit" class="btn btn-primary">
			@if (!empty($btn_txt))
				{{$btn_txt}}
			@else
				@lang('backend.add', [], env('BACKEND_LOCALE'))
			@endif
		</button>
	@else
		<button type="submit" class="btn btn-primary">
			@if (!empty($btn_txt))
				{{$btn_txt}}
			@else
				@lang('backend.submit', [], env('BACKEND_LOCALE'))
			@endif
		</button>
	@endif
</div>
