@foreach ($parents as $parent)
	<li
	@if (request()->has($relationCategoryKey))
		class="{{($parent['id']==request()->input($relationCategoryKey))?'active':''}}"
	@endif
	>
			<a href="{{route('backend.'.$prefix.'.index', [$relationCategoryKey=>$parent['id']])}}">{{$parent['full_path']}}</a>
	</li>

	@if (count($parent['children']))
      @include('backend.common.index_category_menu', ['parents'=>$parent['children']])
  @endif
@endforeach
