<div class="{{$wrap_class}} text-center hidden-print">
	<a class="btn btn-default" href="{{route('backend.'.$prefix.'.index', request()->id?['categories'=>request()->id]:[])}}"><i class="fa fa-list-ul"></i> @lang('backend.back', [], env('BACKEND_LOCALE'))</a>
	{{-- @if (isset($categories) && !is_null($categories))
		<div class="btn-menu category-menu dropdown">
			<button class="btn btn-info" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				{{array_key_exists('categories', $valid_attrs)?$valid_attrs['categories']:__('validation.attributes.categories', [], env('BACKEND_LOCALE'))}}： <span class="name"></span>
				<span class="caret"></span>
			</button>
			<ul class="dropdown-menu">
				@if ($rank_all)
					<li class="{{is_null($category)?'active':''}}">
						<a href="{{route('backend.'.$prefix.'.rank.index')}}">@lang('backend.all_item', [], env('BACKEND_LOCALE'))</a>
					</li>
				@endif
				@include('backend.common.rank_menu', ['parents'=>$categories])
			</ul>
		</div>
	@endif --}}
	@if (count($datas)>1)
		<button type="submit" class="btn btn-primary btn-save-rank"><i class="fa fa-save"></i> @lang('backend.save_sort', [], env('BACKEND_LOCALE'))</button>
	@endif
</div>
