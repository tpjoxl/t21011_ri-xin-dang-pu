@extends('backend.layouts.master')

@section('content')
  @include('backend.'.$prefix.'.rank_btns', ['wrap_class'=>'box-header with-boder'])
  <div class="box-body">
    @if (count($datas)>0)
      <ol class="rank-list ui-sortable row">
          @foreach ($datas as $data)
              <li
                @if (count($datas)<50)
                  class="col-xs-12"
                @elseif (count($datas)<100)
                  class="col-md-6 col-xs-12"
                @elseif (count($datas)<500)
                  class="col-md-4 col-sm-6 col-xs-12"
                @endif
              data-id="{{ $data->id }}">
                <div class="txt">
                  <span class="fa fa-arrows"></span>
                  @foreach ($rank_fields as $key => $rank_field)
                    @if ($rank_field['type'] == 'img')
                      <img class="list-pic" src="{{asset($data[$rank_field['name']])}}" alt="">
                    @elseif ($rank_field['type'] == 'text')
                      {{$data[$rank_field['name']]}}
                    @endif
                  @endforeach
                  {{-- {{ is_array($rank_field)?implode(' ', array_intersect_key($data->toArray(),array_flip($rank_field))):$data[$rank_field] }} --}}
                </div>
              </li>
          @endforeach
      </ol>
    @else
      <div class="no-data text-center">
        @lang('backend.no_data', [], env('BACKEND_LOCALE'))<br>
        {{-- @if (count($categories)>0)
          <br><a href="{{route('backend.'.$prefix.'.create')}}" class="btn btn-success"><i class="fa fa-plus"></i> Add</a>
        @endif --}}
      </div>
    @endif
    <form id="rankUpdate" method="POST" action="" class="form">
        {{ csrf_field() }}
        <input type="hidden" id="sortNum" name="sortNum" value="">
        {{-- <input type="hidden" id="category" name="category" value="{{(!is_null($category))?$category->id:''}}"> --}}
    </form>
  </div>
  @include('backend.'.$prefix.'.rank_btns', ['wrap_class'=>'box-footer'])
@endsection

@section('script')
  <script src="{{asset('AdminLTE/plugins/jquery-ui-interactions/jquery-ui.min.js')}}"></script>
  <script src="{{asset('AdminLTE/plugins/jQueryUI/jquery.ui.touch-punch.min.js')}}"></script>
  <script src="{{asset('AdminLTE/dist/js/rank.js')}}"></script>
@endsection
