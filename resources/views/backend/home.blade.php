@extends('backend.layouts.master')

{{-- @section('content-header')
  Dashboard
  <small>Version 2.0</small>
@endsection --}}
@section('content-top')
  <div class="row">
    <div class="col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-yellow"><i class="ion ion-ios-person-outline"></i></span>
        <div class="info-box-content">
          <span class="info-box-number">@lang('backend.account_info')</span>
          <span class="info-box-text">
              @lang('backend.login_id')：{{ $auth_admin->account }}<br>
              @lang('backend.login_time')：{{ $auth_admin->login_at }}
          </span>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('content-box-class', 'hidden')

@section('content-bottom')
  <div class="row">
    @php
      $contactPower = Auth::guard('admin')->user()->group->checkPower(\App\Power::where('name', 'contact.index')->first()->id);
    @endphp
    @if ($contactPower)
      <div class="col-sm-12">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">聯絡我們表單通知</h3>
          </div>
          <div class="box-body">
              <div class="alert alert-primary text-center" role="alert">
                  您目前有<span style="color: #e8cf0d;font-size: 18px; padding: 0 3px; font-weight: bold;"> {{$contacts->has(0)?count($contacts[0]):0}} </span>筆聯絡我們表單未處理<br>
                  ※此頁面僅顯示最新的
                  @if ($contacts->has(0))
                    {{(count($contacts[0])<=$listLimit)?count($contacts[0]):$listLimit}}
                  @else
                    0
                  @endif
                  則通知
              </div>
            <div class="table-responsive">
                @if (!$contacts->has(0) || $contacts[0]->isEmpty())
                    <p class="text-center">@lang('backend.no_data')</p>
                @else
                    <table class="table no-margin">
                        <thead>
                        <tr>
                          <th></th>
                          <th>@lang('validation.attributes.name')</th>
                          <th>@lang('validation.attributes.sex')</th>
                          <th>填表時間</th>
                          <th>@lang('backend.edit')</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach ($contacts[0] as $key => $contact)
                              @if ($key < $listLimit)
                                <tr>
                                    <td><i class="fa fa-star text-yellow"></i></td>
                                    <td><a href="{{ route('backend.contact.edit', ['id' => $contact->id]) }}">{{ $contact->name }}</a></td>
                                    <td>{{ !is_null($contact->sex)?$contact->present()->sex($contact->sex):'' }}</td>
                                    <td>{{ $contact->created_at }}</td>
                                    <td><a href="{{route('backend.contact.edit', array_merge(request()->all(), ['id' => $contact->id]))}}" class="btn btn-default"><span class="glyphicon glyphicon-pencil"></span></a></td>
                                </tr>
                              @endif
                            @endforeach
                        </tbody>
                    </table>
                @endif
            </div>
          </div>
          <div class="box-footer clearfix">
              <div class="text-center hidden-print">
                  <a href="{{route('backend.contact.index')}}" class="btn btn-default"><span class="fa fa-search"></span> 瀏覽全部表單</a>
              </div>
          </div>
        </div>
      </div>
    @endif
  </div>
@endsection
