<ul class="sidebar-menu">
    @php
      $navpower = $auth_admin->group->powers()->with('children')->where('status', 1)->ordered()->get()->groupBy('parent_id');
    @endphp
    @if (count($navpower))
      @foreach ($navpower[''] as $navroot)
        <li class="treeview {{$navroot->children->where('name', str_replace('backend.', '', Route::currentRouteName()))->count()>0 ? 'active' : ''}}">
          <a href="#">
            <i class="{{$navroot->icon}}"></i> <span>{{$navroot->title}}</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          @if ($navpower->has($navroot->id) && count($children = $navpower[$navroot->id]))
            <ul class="treeview-menu">
                @foreach ($children as $child)
                    <li class="{{ in_array(Route::currentRouteName(), explode(',', preg_replace('/\s(?=)/', '', $child->route_name))) ? 'active' : '' }}">
                      <a href="{{Route::has(explode(',', $child->route_name)[0])? route(explode(',', $child->route_name)[0]) : ''}}{{$child->route_param}}">
                        <i class="fa fa-circle-o"></i> {{$child->title}}
                      </a>
                    </li>
                @endforeach
            </ul>
          @endif
        </li>
        @if ($navroot->divider_after == 1)
        <li class="treeview_line"></li>
        @endif
      @endforeach
    @endif
</ul>
