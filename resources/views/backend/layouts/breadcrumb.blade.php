<ol class="breadcrumb">
  <li><a href="{{route('admin.home')}}"><i class="fa fa-dashboard"></i> 首頁</a></li>
  @if (Route::currentRouteName()!='admin.home')
      <li>{{\App\Item::getPageParent(Route::currentRouteName())->name}}</li>
      <li class="active">{{\App\Item::getPage(Route::currentRouteName())->name}}</li>
  @endif
</ol>
