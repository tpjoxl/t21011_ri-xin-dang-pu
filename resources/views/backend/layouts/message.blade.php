@if (Session::has('success') && Session::get('success'))
    <div class="alert alert-success alert-dismissible hidden-print">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <i class="icon fa fa-check"></i> {{ Session::get('success') }}
    </div>
@endif

@if (Session::has('error') && Session::get('error'))
    <div class="alert alert-danger alert-dismissible hidden-print">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <i class="icon fa fa-ban"></i> {{ Session::get('error') }}
    </div>
@endif

@if (count($errors))
    <div class="alert alert-danger hidden-print" role="alert">
      <ul>
          @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
          @endforeach
      </ul>
    </div>
@endif
