<link rel="stylesheet" type="text/css" href="{{asset('AdminLTE/plugins/lou-multi-select/css/multi-select.css')}}" />

<div class="form-group{{ ($errors->first($form_field['name'])) ? ' has-error' : '' }}">
    <label for="name" class="col-md-2 col-sm-3 control-label">{{$form_field['required']?'* ':''}}{{$label}}</label>
  	<div class="col-sm-9">
      {{-- <div class="row item-group">
        <div class="col-md-12 item-root">
          <label><input type="checkbox" id="checkAll"> 全選</label>
        </div>
      </div>
      @foreach ($form_field['options'] as $group)
        <div class="row item-group">
            <div class="col-md-12 item-root">
                <label><input type="checkbox" name="group[]" value="{{$group['id']}}"
                  {{old($form_field['name'].'_group')&&in_array($group['id'], old($form_field['name'].'_group'))?'checked':''}}>{{ $group['title'] }}</label>
            </div>
            @if (count($group['faqs']))
              @foreach ($group['faqs'] as $child)
                <div class="col-lg-3 col-md-4 col-xs-6 item-child">
                    <label><input type="checkbox" name="{{$form_field['name']}}[]" value="{{$child['id']}}"
                      {{in_array($child['id'], old($form_field['name'], (isset($data)?$data->{$form_field['name']}->pluck('id')->toArray():array())))?'checked':''}}
                     >{{ $child['title'] }}</label>
                </div>
              @endforeach
            @endif
        </div>
      @endforeach --}}

      <div class="row item-group">
        <div class="col-md-12 item-root">
          <label><input type="checkbox" id="checkAll_"> 全選</label>
        </div>
      </div>
      <select multiple class="searchable" name="{{$form_field['name']}}[]">
        @foreach ($form_field['options'] as $group)
        <optgroup label="{{ $group['title'] }}">
          @if (count($group["faqs"]))
            @foreach ($group["faqs"] as $child)
          <option value="{{$child['id']}}"
            {{in_array($child['id'], old($form_field['name'], (isset($form_field['selected'])?$form_field['selected']:array())))?'selected':''}}
            >{{ $child['question'] }}</option>
            @endforeach
          @endif
        </optgroup>
        @endforeach
      </select>
  		@if (array_key_exists('hint', $form_field))
  			<p class="help-block">{{$form_field['hint']}}</p>
  		@endif
  	</div>
</div>


@section('script')
  <script src="{{asset('AdminLTE/plugins/lou-multi-select/js/jquery.multi-select.js')}}" type="text/javascript"></script>
  <script src="{{asset('AdminLTE/plugins/lou-multi-select/js/jquery.quicksearch.js')}}" type="text/javascript"></script>
  <script type="text/javascript">
		// http://loudev.com/
    $(document).ready(function() {
      $('.searchable').multiSelect({
        selectableOptgroup: true,
        selectableHeader: "<input type='text' class='form-control search-input' autocomplete='off' placeholder='所有項目 '>",
        selectionHeader: "<input type='text' class='form-control search-input' autocomplete='off' placeholder='已選擇項目 '>",
        afterInit: function(ms){
          var that = this,
          $selectableSearch = that.$selectableUl.prev(),
          $selectionSearch = that.$selectionUl.prev(),
          selectableSearchString = '#'+that.$container.attr('id')+' .ms-elem-selectable:not(.ms-selected)',
          selectionSearchString = '#'+that.$container.attr('id')+' .ms-elem-selection.ms-selected';

          that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
          .on('keydown', function(e){
            console.log(1);
            if (e.which === 40){
              that.$selectableUl.focus();
              return false;
            }
          });

          that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
          .on('keydown', function(e){
            console.log(2);
            if (e.which == 40){
              that.$selectionUl.focus();
              return false;
            }
          });
        },
        afterSelect: function(){
          this.qs1.cache();
          this.qs2.cache();
        },
        afterDeselect: function(){
          this.qs1.cache();
          this.qs2.cache();
        }
      });

      $('#checkAll_').click(function(){
        if ($(this).prop('checked')==true) {
          $('.searchable').multiSelect('select_all');
        } else {
          $('.searchable').multiSelect('deselect_all');
        }
      });
    });
  </script>
@endsection
