@foreach ($parents as $parent)
	<option value="{{$parent['id']}}"
		@if (request()->group_id)
			{{ (request()->group_id==$parent['id']) ? 'selected' : '' }}
		@else
			{{ (old('group_id', (isset($data)?$data->group_id:''))==$parent['id']) ? 'selected' : '' }}
		@endif
		>
			{{$parent['full_path']}}
	</option>

	@if (count($parent['children']))
      @include('backend.'.$prefix.'._select', ['parents'=>$parent['children']])
  @endif
@endforeach
