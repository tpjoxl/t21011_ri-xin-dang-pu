@include('backend.common.category._form_btns', ['wrap_class' => 'box-header with-border'])
<div class="box-body">
  <div class="alert alert-primary text-center" role="alert">@lang('backend.required_txt', [], env('BACKEND_LOCALE'))</div>
    {{ csrf_field() }}
    @if ($data->getLimitLevel() > 1)
    <div class="form-group{{ ($errors->first('parent_id')) ? ' has-error' : '' }}">
        <label for="parent_id" class="col-md-2 col-sm-3 control-label">@lang('validation.attributes.parent_id', [], env('BACKEND_LOCALE'))</label>
        <div class="col-sm-9">
            <select class="form-control" name="parent_id">
                <option value="">@lang('backend.top_level', [], env('BACKEND_LOCALE'))</option>
                @include('backend.'.$prefix.'._select', ['parents'=>$parents])
            </select>
            {{-- <input type="text" class="form-control" name="parent_id" value="{{ old('parent_id', $data->parent_id) }}"> --}}
        </div>
    </div>
    @endif
    <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
        <label for="title" class="col-md-2 col-sm-3 control-label">* @lang('validation.attributes.group_name', [], env('BACKEND_LOCALE'))</label>
        <div class="col-sm-9">
            <input type="text" class="form-control" name="title" value="{{ old('title', $data->title) }}" autofocus>
        </div>
    </div>
    <div class="form-group{{ ($errors->first('power_list')) ? ' has-error' : '' }}">
        <label for="name" class="col-md-2 col-sm-3 control-label">* @lang('validation.attributes.power_option', [], env('BACKEND_LOCALE'))</label>
        <div class="col-sm-9">
          @foreach ($powers as $power)
            <div class="row item-group">
                <div class="col-md-12 item-root">
                    <label><input type="checkbox" name="power_list[]" value="{{$power['id']}}"
                      @if (old('power_list'))
                        {{(in_array($power['id'], old('power_list')))? 'checked' : ''}}
                      @else
                        {{($data->checkPower($power['id']))? 'checked' : ''}}
                      @endif
                      >{{ $power['title'] }}</label>
                </div>
                @if (count($power['children']))
                  @foreach ($power['children'] as $child)
                    <div class="col-lg-3 col-md-4 col-xs-6 item-child">
                        <label><input type="checkbox" name="power_list[]" value="{{$child['id']}}"
                          @if (old('power_list'))
                            {{(in_array($child['id'], old('power_list')))? 'checked' : ''}}
                          @else
                            {{($data->checkPower($child['id']))? 'checked' : ''}}
                          @endif
                         >{{ $child['title'] }}</label>
                    </div>
                  @endforeach
                @endif
            </div>
          @endforeach
        </div>
    </div>
</div>
@include('backend.common.category._form_btns', ['wrap_class' => 'box-footer'])
