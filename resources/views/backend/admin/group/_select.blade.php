@foreach ($parents as $parent)
	@if ($parent['id'] != $data->id)
		<option value="{{$parent['id']}}"
			@if (request()->parent_id)
				{{ (request()->parent_id==$parent['id']) ? 'selected' : '' }}
			@else
				{{ (old('parent_id', $data->parent_id)==$parent['id']) ? 'selected' : '' }}
			@endif
			>
				{{$parent['full_path']}}
		</option>
	@endif

	@if (count($parent['children']) && $data->getLimitLevel()-1 > $parent['level'])
      @include('backend.'.$prefix.'._select', ['parents'=>$parent['children']])
  @endif
@endforeach
