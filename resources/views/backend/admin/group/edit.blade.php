@extends('backend.layouts.master')

@section('content')
  <form class="form-horizontal" method="post" action="" accept-charset="UTF-8" enctype="multipart/form-data">
      {{ method_field('PATCH') }}
      @include('backend.'.$prefix.'._form')
  </form>
@endsection
