<div class="form-group">
	<label for="group" class="col-md-2 col-sm-3 control-label">@lang('validation.attributes.power_group', [], env('BACKEND_LOCALE'))</label>
	<div class="col-sm-9">
			<select class="form-control" name="group_id">
					<option value="">@lang('backend.please_select', [], env('BACKEND_LOCALE'))</option>
					@include('backend.admin._select', ['parents'=>$categories])
			</select>
	</div>
</div>
