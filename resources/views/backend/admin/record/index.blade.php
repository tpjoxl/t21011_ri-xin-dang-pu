@extends('backend.layouts.master')

@section('content-top')
  @include('backend.common._search')
@endsection

@section('content')
  @include('backend.common.pager', ['wrap_class'=>'box-header with-border'])
  <div class="box-body no-padding">
    @include('backend.common.index_table')
  </div>
  @include('backend.common.pager', ['wrap_class'=>'box-footer'])
@endsection
