<div class="{{$wrap_class}} text-center hidden-print">
	@if (isset($frontContent))
		{!!$frontContent!!}
	@endif
	@if (!$rank_all && isset($categories) && !is_null($categories))
		<div class="btn-menu category-menu dropdown">
			<button class="btn btn-info" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				{{array_key_exists('categories', $valid_attrs)?$valid_attrs['categories']:__('validation.attributes.categories', [], env('BACKEND_LOCALE'))}}： <span class="name"></span>
				<span class="caret"></span>
			</button>
			<ul class="dropdown-menu">
				@include('backend.common.index_category_menu', ['parents'=>$categories])
			</ul>
		</div>
	@endif
	@if (count($list_datas)>0)
		{{-- <a href="javascript:window.print()" class="btn btn-default btn-print"><span class="glyphicon glyphicon-print"></span> @lang('backend.print', [], env('BACKEND_LOCALE'))</a> --}}
		@if (Route::has('backend.'.$prefix.'.destroy') && $auth_admin->group->checkPowerByName($prefix.'.destroy'))
			<a href="{{route('backend.'.$prefix.'.destroy')}}" class="btn btn-default btn-del" id="del_select_all"><span class="fa fa-trash-o"></span> @lang('backend.delete_selected', [], env('BACKEND_LOCALE'))</a>
		@endif
		@if (Route::has('backend.'.$prefix.'.status') && $auth_admin->group->checkPowerByName($prefix.'.edit'))
			<span class="btn-menu dropdown">
				<button class="btn btn-default" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					@lang('backend.change_status', [], env('BACKEND_LOCALE')) <span class="name"></span>
					<span class="caret"></span>
				</button>
				<ul class="dropdown-menu">
					@foreach ($search_fields['status']['options'] as $key => $value)
						<li><a href="{{route('backend.'.$prefix.'.status', ['val' => $key])}}" class="setstatus_select_all">{!!$value!!}</a></li>
					@endforeach
				</ul>
			</span>
		@endif
	@endif
	@if ($auth_admin->group->checkPowerByName($prefix.'.rank') && Route::has('backend.'.$prefix.'.rank.index'))
		<a href="{{route('backend.'.$prefix.'.rank.index', request()->only('categories'))}}" class="btn bg-purple"><i class="fa fa-sort-numeric-asc"></i> @lang('backend.rank', [], env('BACKEND_LOCALE'))</a>
	@endif
	@if (count($list_datas)>0)
		@if (Route::has('backend.'.$prefix.'.export'))
			<a href="{{route('backend.'.$prefix.'.export', request()->all())}}" class="btn btn-default"><span class="fa fa-file-excel-o"></span> @lang('backend.export', [], env('BACKEND_LOCALE'))</a>
		@endif
	@endif
	@if ($auth_admin->group->checkPowerByName($prefix.'.create'))
		@if (Route::has('backend.'.$prefix.'.import'))
			<a href="{{route('backend.'.$prefix.'.import')}}" class="btn btn-primary btn-import-trigger"><i class="fa fa-upload"></i> @lang('backend.import', [], env('BACKEND_LOCALE'))</a>
		@endif
		@if (Route::has('backend.'.$prefix.'.create'))
			<a href="{{route('backend.'.$prefix.'.create', request()->all())}}" class="btn btn-success"><i class="fa fa-plus"></i> @lang('backend.add', [], env('BACKEND_LOCALE'))</a>
		@endif
	@endif
	@if (isset($endContent))
		{!!$endContent!!}
	@endif
</div>
