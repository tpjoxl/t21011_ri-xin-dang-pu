@if ($paginator->hasPages())
<div class="col-sm-12 text-center">
  <div class="with_padding small_padding content-justify vertical-center cs">
    @if(!$paginator->onFirstPage())
      <a class="small-text big text-left" href="{{ $paginator->previousPageUrl() }}" @if (!$paginator->onFirstPage()) tabindex="-1" aria-disabled="true" @endif title="上一頁"> prev <span class="big">上一頁</span></a>
    @else
      <a class="small-text big text-left" href= "">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</a>
    @endif
    <ul class="pagination ">
      @foreach ($elements as $element)
        {{-- "Three Dots" Separator --}}
        @if (is_string($element))
            <li class="disabled"><span>{{ $element }}</span></li>
        @endif

        {{-- Array Of Links --}}
        @if (is_array($element))
            @foreach ($element as $page => $url)
                @if ($page == $paginator->currentPage())
                    <li class="active" aria-current="page"><a href="{{ $url }}">{{ $page }}</a></li>
                @else
                    <li><a href="{{ $url }}">{{ $page }}</a></li>
                @endif
            @endforeach
        @endif
      @endforeach
    </ul>
    @if($paginator->hasMorePages())
      <a class="small-text big text-right" href="{{ $paginator->nextPageUrl() }}" @if ($paginator->hasMorePages()) tabindex="-1" aria-disabled="true" @endif title="下一頁">next <span class="big">下一頁</span></a>
    @else
      <a class="small-text big text-right" href= "">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</a>
    @endif
  </div>
</div>
@endif

