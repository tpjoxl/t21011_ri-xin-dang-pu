@extends('frontend.layouts.master')

@section('content')
<section class="intro_section page_mainslider ds">
  <div class="flexslider vertical-nav" data-dots="true" data-nav="false">
    <ul class="slides">
      @foreach ($banners as $key => $banner)
        <li>
          <img src="{{asset($banner->pic1)}}" alt="">
          <div class="container">
            <div class="row">
              <div class="col-sm-12 text-center">
                <div class="slide_description_wrapper">
                  <div class="slide_description">
                    @if($banner->text2)
                      <div class="intro-layer" data-animation="fadeInUp">
                        <h4 class="text-uppercase">
                          {{$banner->text2}}
                        </h4>
                      </div>
                    @endif
                    <div class="intro-layer" data-animation="fadeInUp">
                      <h2 class="text-uppercase thin">
                        {!!$banner->text!!}
                      </h2>
                    </div>
                    @if ($banner->url && $banner->btn_txt)
                      <div class="intro-layer" data-animation="fadeInUp">
                        <a href="{{$banner->url}}" target="{{$banner->target}}" title="{{$banner->tag_title}}" class="theme_button color3">
                          {{$banner->btn_txt}}
                        </a>
                      </div>
                    @endif
                  </div>
                </div>
              </div>
            </div>
          </div>
        </li>
      @endforeach
    </ul>
  </div>
</section>
@if (!empty($setting->home_editor1))
  <section class="ls section_padding_top_100 section_padding_bottom_150">
    <div class="container">
      <div class="row">
        {!!$setting->home_editor1!!}
        {{-- <div class="col-sm-12 text-center">
          <h2 class="section_header">
            <span class="small">SERVICES</span>
            服務項目
          </h2>
          <p class="fontsize_20">
            因應現今社會的科技發達及物資潮流生活提高，許多人為解手頭一時不便之需，將手上擁有的汽機車、3C產品、黃金、名錶、鑽石等，甚至是不動產皆能來日鑫辦理借款。此外，舉凡小額借款、支票貼現等，以上也皆可在台中日鑫當鋪辦理借款。
          </p>
        </div> --}}
      </div>
      <div class="row">
        @foreach ($menu_service_categories as $key => $menu_service_category)
        @php
          if($menu_service_category->title == $menu_service_categories[1]->title || $menu_service_category->title == $menu_service_categories[4]->title)
          {
            $color = "color3_border";
          }else{
            $color = "color2_border";
          }
        @endphp
          <div class="col-md-4 col-sm-6">
            <div class="vertical-item content-padding with_border {{$color}} text-center">
              <div class="item-media">
                <a href="{{route('service.index', ['slug'=>rawurlencode($menu_service_category->slug)])}}" title="{{$menu_service_category->title}}"><img src="{{asset($menu_service_category->picPath)}}" alt="{{$menu_service_category->title}}"></a>
              </div>
              <div class="item-content">
                <h4 class="entry-title  hover-color2">
                  <a href="{{route('service.index', ['slug'=>rawurlencode($menu_service_category->slug)])}}" title="{{$menu_service_category->title}}">{{$menu_service_category->title}}</a>
                </h4>
                <p>{!!nl2br($menu_service_category->description)!!}</p>
              </div>
            </div>
          </div>
        @endforeach
      </div>
    </div>
  </section>
@endif
@if (!empty($setting->home_editor2))
  <section id="features" class="cs light-gray background_cover section_padding_top_150 section_padding_bottom_150"
    style="background-image: url('{{asset($setting->home_editor2_pic)}}');">
    <div class="container">
      {!!$setting->home_editor2!!}
      {{-- <div class="row">
        <div class="col-md-6 col-md-offset-6">
          <h2 class="section_header">
            <span class="small">PROCESSING FLOW</span>
            日鑫當舖借款流程
          </h2>
          <div class="teaser media topmargin_10">
            <div class="media-left media-middle">
              <div class="teaser_icon main_bg_color2 small_icon rounded">
                <b>1</b>
              </div>
            </div>
            <div class="media-body media-middle">
              <p class="fontsize_20">
                <strong>24H免費諮詢</strong><br>不分晝夜，只要您有疑慮，樂意為您服務
              </p>
            </div>
          </div>
          <div class="teaser media topmargin_10">
            <div class="media-left media-middle">
              <div class="teaser_icon main_bg_color3 small_icon rounded">
                <b>2</b>
              </div>
            </div>
            <div class="media-body media-middle">
              <p class="fontsize_20">
                <strong>了解需求</strong><br>聯絡當鋪告知借款需求
              </p>
            </div>
          </div>
          <div class="teaser media topmargin_10">
            <div class="media-left media-middle">
              <div class="teaser_icon main_bg_color small_icon rounded">
                <b>3</b>
              </div>
            </div>
            <div class="media-body media-middle">
              <p class="fontsize_20">
                <strong>準備資料</strong><br>攜帶借款相關資料至當鋪
              </p>
            </div>
          </div>
          <div class="teaser media topmargin_10">
            <div class="media-left media-middle">
              <div class="teaser_icon main_bg_color2 small_icon rounded">
                <b>4</b>
              </div>
            </div>
            <div class="media-body media-middle">
              <p class="fontsize_20">
                <strong>接洽審核</strong><br>由當鋪專業人員親自與您一對一詳談並審核解說借貸契約
              </p>
            </div>
          </div>
          <div class="teaser media topmargin_10">
            <div class="media-left media-middle">
              <div class="teaser_icon main_bg_color3 small_icon rounded">
                <b>5</b>
              </div>
            </div>
            <div class="media-body media-middle">
              <p class="fontsize_20">
                <strong>當日撥款</strong><br>審核流程快速，免等待現金之煎熬
              </p>
            </div>
          </div>
          <div class="teaser media topmargin_10">
            <div class="media-left media-middle">
              <div class="teaser_icon main_bg_color small_icon rounded">
                <b>6</b>
              </div>
            </div>
            <div class="media-body media-middle">
              <p class="fontsize_20">
                <strong>輕鬆生活</strong><br>為您解決現金危機，無壓力生活
              </p>
            </div>
          </div>
        </div>
      </div> --}}
    </div>
  </section>
@endif
@if (!empty($setting->home_editor3))
  <section id="pricing" class="ls section_padding_top_130 section_padding_bottom_150">
    <div class="container">
      {!!$setting->home_editor3!!}
      {{-- <div class="row">
        <div class="col-sm-12 text-center">
          <h2 class="section_header">
            <span class="small">Why Choose Us</span>
            我們的優勢
          </h2>
          <p class="fontsize_20">日鑫當舖是政府立案合法當舖，通過溝通交流，為您解決經濟上的危機。信用有瑕疵或者有任何債務都沒有關係，當鋪會告訴您如何借款，經由快速借款流程讓您快速解決用錢之危機。
          </p>
          <p class="topmargin_50">
            <a href="about.html" class="theme_button color1 wide_button">了解更多</a>
          </p>
        </div>
      </div> --}}
    </div>
  </section>
@endif
@if (!empty($setting->home_editor4) && count($faqs)>0)
  <section class="ls bg-light section_padding_top_150 section_padding_bottom_130">
    <div class="container">
      <div class="row">
        {!!$setting->home_editor4!!}
        {{-- <div class="col-sm-12 text-center">
          <h2 class="section_header">
            <span class="small">FAQ</span>
            精選常見Q&amp;A
          </h2>
          <p class="fontsize_20">簡述簡述簡述簡述簡述簡述簡述簡述簡述簡述簡述簡述簡述簡述簡述簡述簡述簡述簡述簡述簡述簡述簡述簡述簡述簡述簡述簡述簡述簡述簡述簡述簡述簡述簡述簡</p>
        </div> --}}
      </div>
      <div class="row">
        @php
          $faqdatas=$faqs->split(2);
        @endphp
          <div class="col-sm-6">
            <div class="panel-group" id="accordion1">
              @foreach ($faqdatas[0] as $key => $faq)
                @php
                    $firstdata = $faqdatas[0]->first();
                    if($faq->id == $firstdata->id){
                        $status1 = '';
                        $status2 = "in";
                    }
                    else{
                        $status1 = "collapsed";
                        $status2 = '';
                    }  
                @endphp
                <div class="panel panel-default panel-color2">
                  <div class="panel-heading">
                    <h4 class="panel-title">
                      <a data-toggle="collapse" data-parent="#accordion1" href="#collapse{{$faq->id}}" @if($status1 == "collapsed") class={{$status1}} @endif>
                        {{$faq->question}}
                      </a>
                    </h4>
                  </div>
                  <div id="collapse{{$faq->id}}" class="panel-collapse collapse {{$status2}}">
                    <div class="panel-body">
                      {!!$faq->answer!!}
                    </div>
                  </div>
                </div>
              @endforeach
            </div>
          </div>
          <div class="col-sm-6">
            <div class="panel-group" id="accordion2">
              @foreach ($faqdatas[1] as $key => $faq)
                @php
                  $firstdata = $faqdatas[1]->first();
                    if($faq->id == $firstdata->id){
                        $status1 = '';
                        $status2 = "in";
                    }
                    else{
                        $status1 = "collapsed";
                        $status2 = '';
                    }  
                @endphp
                <div class="panel panel-default panel-color2">
                  <div class="panel-heading">
                    <h4 class="panel-title">
                      <a data-toggle="collapse" data-parent="#accordion2" href="#collapse2{{$faq->id}}" @if($status1 == "collapsed") class={{$status1}} @endif>
                        {{$faq->question}}
                      </a>
                    </h4>
                  </div>
                  <div id="collapse2{{$faq->id}}" class="panel-collapse collapse {{$status2}}">
                    <div class="panel-body">
                      {!!$faq->answer!!}
                    </div>
                  </div>
                </div>
              @endforeach
            </div>
          </div>
      </div>
    </div>
  </section>
@endif
@if ($blogs->count() > 0 && $setting->home_editor5)
  <section class="ls section_padding_top_150 section_padding_bottom_130 columns_margin_bottom_30">
    <div class="container">
      <div class="row">
        <div class="col-lg-3 col-md-4">
          {!!$setting->home_editor5!!}
          {{-- <h2 class="section_header">
            <span class="small">Blogs</span>
            精選文章
          </h2>
          <p>At vero eos et accusam et justo duo dolores et ea rebum. Stclita gubergren no sea takimata sanctus
            eipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing.</p>
          <p class="topmargin_30">
            <a href="blog.html" class="theme_button color2 wide_button">更多文章</a>
          </p>  --}}
        </div>
        <div class="col-lg-9 col-md-8">
          <div class="owl-carousel" data-nav="false" data-dots="false" data-responsive-lg="3"
            data-responsive-md="2">
            @foreach ($blogs as $item)
              <article class="vertical-item content-padding with_border color3_border rounded text-center">
                <div class="item-media rounded overflow_hidden bottommargin_30">
                  <img img src="{{asset($item->picPath)}}" alt="{{$item->title}}">
                  <div class="media-links color3">
                    <a href="{{$item->url?:route('blog.detail', ['slug'=>rawurlencode($item->slug)])}}" target="{{$item->url?'_blank':''}}" title="{{$item->title}}" class="abs-link"></a>
                  </div>
                  <div class="cs main_color3 entry-meta media-meta text-center pos-static">
                    <div>
                      <span class="date">{{$item->created_at->format('d')}}</span>
                      <span class="small-text">{{$item->created_at->format('M')}}</span>
                    </div>
                    @php
                        $category = $item->categories->first();
                    @endphp
                    <div class="side-media-links">
                      <a class="w-auto" href="{{route('blog.category', ['category'=>$category->slug])}}">
                        <i class="fa fa-star" aria-hidden="true"></i> {{$category->title}}
                      </a>
                    </div>
                  </div>
                </div>
                <div class="item-content">
                  <h4 class="entry-title hover-color3">
                    <a href="{{$item->url?:route('blog.detail', ['slug'=>rawurlencode($item->slug)])}}" target="{{$item->url?'_blank':''}}" title="{{$item->title}}">{{$item->title}}</a>
                  </h4>
                  <div class="entry-content">
                    <p class="multi-line-3">{!!$item->description!!}</p>
                  </div>
                </div>
                <footer class="highlight3links small-text semibold">
                  <a href="{{$item->url?:route('blog.detail', ['slug'=>rawurlencode($item->slug)])}}" target="{{$item->url?'_blank':''}}" title="{{$item->title}}">read more</a>
                </footer>
              </article>
            @endforeach
          </div>
        </div>
        <div class="col-lg-12 col-md-12">
          <div class="owl-carousel" data-nav="false" data-dots="false" data-responsive-lg="4"
            data-responsive-md="2">
            @foreach ($bloggs as $item)
              <article class="vertical-item content-padding with_border color3_border rounded text-center">
                <div class="item-media rounded overflow_hidden bottommargin_30">
                  <img img src="{{asset($item->picPath)}}" alt="{{$item->title}}">
                  <div class="media-links color3">
                    <a href="{{$item->url?:route('blog.detail', ['slug'=>rawurlencode($item->slug)])}}" target="{{$item->url?'_blank':''}}" title="{{$item->title}}" class="abs-link"></a>
                  </div>
                  <div class="cs main_color3 entry-meta media-meta text-center pos-static">
                    <div>
                      <span class="date">{{$item->created_at->format('d')}}</span>
                      <span class="small-text">{{$item->created_at->format('M')}}</span>
                    </div>
                    @php
                        $category = $item->categories->first();
                    @endphp
                    <div class="side-media-links">
                      <a class="w-auto" href="{{route('blog.category', ['category'=>$category->slug])}}">
                        <i class="fa fa-star" aria-hidden="true"></i> {{$category->title}}
                      </a>
                    </div>
                  </div>
                </div>
                <div class="item-content">
                  <h4 class="entry-title hover-color3">
                    <a href="{{$item->url?:route('blog.detail', ['slug'=>rawurlencode($item->slug)])}}" target="{{$item->url?'_blank':''}}" title="{{$item->title}}">{{$item->title}}</a>
                  </h4>
                  <div class="entry-content">
                    <p class="multi-line-3">{!!$item->description!!}</p>
                  </div>
                </div>
                <footer class="highlight3links small-text semibold">
                  <a href="{{$item->url?:route('blog.detail', ['slug'=>rawurlencode($item->slug)])}}" target="{{$item->url?'_blank':''}}" title="{{$item->title}}">read more</a>
                </footer>
              </article>
            @endforeach
          </div>
        </div>
      </div>
    </div>
  </section>
@endif
@include('frontend.layouts.footer_form')
@endsection
