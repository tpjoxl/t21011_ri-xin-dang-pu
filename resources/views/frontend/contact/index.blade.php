@extends('frontend.layouts.master')


@section('content')
<section class="page_breadcrumbs ds color parallax section_padding_top_75 section_padding_bottom_75" 
        style="background-image: @if (!empty($data->banner)) url('{{asset($data->banner)}}') @endif;">
  <div class="container">
    <div class="row">
      <div class="col-sm-12 text-center">
        <h1 class="page-heading text-uppercase">{{$data->h1?:($data->title?:__('text.'.$prefix))}}</h1>
        @include('frontend.layouts.breadcrumb')
      </div>
    </div>
  </div>
</section>

          
<section class="ls columns_padding_25 section_padding_top_100 section_padding_bottom_100">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <h3 class="module-header text-center">立即諮詢</h3>
      </div>
    </div>
    <div class="row">
      <div class="col-md-8 to_animate" data-animation="scaleAppear">
        @include('frontend.contact._form')
      </div>
      <div class="col-md-4 to_animate" data-animation="scaleAppear">
        @if($data->text_top)
          {!!$data->text_top!!}
        @endif
        {{-- <ul class="list1 no-bullets no-top-border no-bottom-border">
          <li>
            <div class="media">
              <div class="media-left">
                <i class="rt-icon2-shop highlight2 fontsize_18"></i>
              </div>
              <div class="media-body">
                <h5 class="media-heading grey">地址:</h5>
                <a class="color-inherit" href="#" target="_blank">台中市北屯區北屯路469之6號</a>
              </div>
            </div>
          </li>
          <li>
            <div class="media">
              <div class="media-left">
                <i class="rt-icon2-phone5 highlight2 fontsize_18"></i>
              </div>
              <div class="media-body">
                <h5 class="media-heading grey">電話:</h5>
                <a class="color-inherit" href="tel:0966-423-333">0966-423-333</a>
              </div>
            </div>
          </li>
          <li>
            <div class="media">
              <div class="media-left">
                <i class="rt-icon2-mail highlight2 fontsize_18"></i>
              </div>
              <div class="media-body">
                <h5 class="media-heading grey">Email:</h5>
                <a class="color-inherit" href="mailto:aa0966423333@gmail.com">aa0966423333@gmail.com</a>
              </div>
            </div>
          </li>
          <li>
            <div class="media">
              <div class="media-left">
                <i class="rt-icon2-clock4 highlight2 fontsize_18"></i>
              </div>
              <div class="media-body greylinks">
                <h5 class="media-heading grey">營業時間:</h5>
                9:00~19:00
              </div>
            </div>
          </li>
        </ul> --}}
      </div>
    </div>
  </div>
</section>
@endsection
