<script src='https://www.google.com/recaptcha/api.js'></script>
<form class="contact-form columns_padding_5" action="{{route('contact.submit')}}" method="post" id="contact_form">
{{ csrf_field() }}
<input type="hidden" name="url" value="{{rawurldecode(url()->full())}}">
@php
  $device = [];
  if ($agent->isMobile()) {
    array_push($device, 'mobile');
  }
  if ($agent->isTablet()) {
    array_push($device, 'tablet');
  }
  if ($agent->isDesktop()) {
    array_push($device, 'desktop');
  }
@endphp
<input type="hidden" name="device" value="{{implode(', ', $device)}}">
  <div class="col-sm-6">
    <div class="form-group">
      <label for="name">姓名
        <span class="required">*</span>
      </label>
      <i class="fa fa-user highlight2" aria-hidden="true"></i>
      <input type="text" aria-required="true" size="30" value="{{old('name')}}" name="name" id="name"
        class="form-control @if (isset($errors) && $errors->first('name'))invalid @endif" placeholder="姓名 *">
        @if((isset($errors) && $errors->first('name')))
          <div class="error-msg">{{$errors->first('name')}}</div>
        @endif
    </div>
  </div>
  <div class="col-sm-6">
    <div class="form-group">
      <label for="sex">稱謂
        <span class="required">*</span>
      </label>
      <i class="fa fa-info-circle highlight2" aria-hidden="true"></i>
      <select aria-required="true" name="sex" id="sex" class="form-control  @if (isset($errors) && $errors->first('sex')) invalid @endif">
        <option value="">稱謂 *</option>
        @foreach ($common->present()->sex_title as $key => $val)
          <option value="{{$key}}" {{old('sex')&&old('sex')==$key?'selected':''}} required>{!!$val!!}</option>
        @endforeach
      </select>
      @if((isset($errors) && $errors->first('sex')))
        <div class="error-msg">{{$errors->first('sex')}}</div>
      @endif
    </div>
  </div>
  <div class="col-sm-6">
    <div class="form-group">
      <label for="service_category">詢問類型
        <span class="required">*</span>
      </label>
      <i class="fa fa-folder-open highlight2" aria-hidden="true"></i>
      <select aria-required="true" name="service_category" id="service_category" class="form-control  @if (isset($errors) && $errors->first('service_category')) invalid @endif">
        <option value="">詢問類型 *</option>
          @foreach ($menu_service_categories as $menu_service_category)
            <option value="{{$menu_service_category->title}}" {{old('service_category')==$menu_service_category->title?'selected':''}} required>{{$menu_service_category->title}}</option>
          @endforeach
      </select>
      @if((isset($errors) && $errors->first('service_category')))
        <div class="error-msg">{{$errors->first('service_category')}}</div>
      @endif
    </div>
  </div>
  <div class="col-sm-6">
    <div class="form-group">
      <label for="phone">電話
        <span class="required">*</span>
      </label>
      <i class="fa fa-phone highlight2" aria-hidden="true"></i>
      <input type="text" aria-required="true" size="30" value="{{old('phone')}}" name="phone" id="phone"
        class="form-control  @if (isset($errors) && $errors->first('phone')) invalid @endif" placeholder="電話 *">
      @if((isset($errors) && $errors->first('phone')))
        <div class="error-msg">{{$errors->first('phone')}}</div>
      @endif
    </div>
  </div>
  <div class="col-sm-6">
    <div class="form-group">
      <label for="email">LINE ID</label>
      <i class="fa fa-envelope highlight2" aria-hidden="true"></i>
      <input type="text" aria-required="false" size="30" value="{{old('line_id')}}" name="line_id" id="line_id"
        class="form-control" placeholder="LINE ID">
    </div>
  </div>
  <div class="col-sm-6">
    <div class="form-group">
      <label for="subject">居住縣市</label>
      <i class="fa fa-home highlight2" aria-hidden="true"></i>
      <select aria-required="false" name="city" id="city" class="form-control">
        <option value="">居住縣市</option>
        @foreach ($cities as $city)
          <option value="{{$city->name}}" {{old('city')==$city->name?'selected':''}}>{{$city->name}}</option>
        @endforeach
      </select>
    </div>
  </div>
  <div class="col-sm-12">
    <div class="contact-form-message form-group">
      <label for="message">詢問內容
        <span class="required">*</span>
      </label>
      <i class="fa fa-comment highlight2" aria-hidden="true"></i>
      <textarea aria-required="true" rows="3" cols="45" name="message" id="message"
        class="form-control  @if (isset($errors) && $errors->first('message')) invalid @endif" placeholder="詢問內容 *"></textarea>
      @if((isset($errors) && $errors->first('message')))
        <div class="error-msg">{{$errors->first('message')}}</div>
      @endif
    </div>
  </div>

  <div class="col-sm-8 bottommargin_0">
    <div class="captcha @if (isset($errors) && $errors->first('g-recaptcha-response')) invalid @endif">
      <div class="g-recaptcha" data-sitekey="{{env('RE_CAP_SITE')}}"></div>
    </div>
    @if((isset($errors) && $errors->first('g-recaptcha-response')))
      <div class="error-msg">{{$errors->first('g-recaptcha-response')}}</div>
    @endif
  </div>

  <div class="col-sm-4 bottommargin_0">
    <div class="contact-form-submit topmargin_10 text-sm-right bottommargin_30">
      <button type="submit" id="contact_form_submit" name="contact_submit"
        class="theme_button color2 wide_button margin_0">確定送出</button>
    </div>
  </div>
</form>

  
