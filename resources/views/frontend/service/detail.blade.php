@extends('frontend.layouts.master')

@section('content')
<section class="page_breadcrumbs ds color parallax section_padding_top_75 section_padding_bottom_75" 
        style="background-image: @if (!empty($data->banner)) url('{{asset($data->banner)}}') @endif;">
  <div class="container">
    <div class="row">
      <div class="col-sm-12 text-center">
        <h1 class="page-heading text-uppercase">{{$data->h1?:($data->title?:__('text.'.$prefix))}}</h1>
        @include('frontend.layouts.breadcrumb')
      </div>
    </div>
  </div>
</section>

@if (!empty($data->text))
    <section class="ls section_padding_top_130 section_padding_bottom_150 columns_padding_25 table_section table_section_md">
        <div class="container">
            {!!$data->text!!}
            {{-- <div class="editor">
                <div class="row">
                    <div class="col-lg-7 col-md-6 col-lg-push-5 col-md-push-6">
                        <img src="https://fakeimg.pl/900x500/ddd/fff/" alt="">
                    </div>
                    <div class="col-lg-5 col-md-6 col-lg-pull-7 col-md-pull-6">
                        <h3 class="text-uppercase txt-color1">日鑫汽車借款</h3>
                        <p>顧名思義就是以汽車為抵押品借款，當鋪借款最大的特色是可以免留車借貸，如果想繼續使用汽車也可以，此外，第一次汽車貸款的借款人只需要進行動保設定即可申請更優惠借貸額度，汽車借款流程相當便捷迅速，針對您的借款需求，只要您名下有車且年滿20歲，核對身分、車輛鑑價、簽訂合約、即可等待撥款。日鑫當鋪汽車借款可當日快速撥款，無綁約隨借隨還，不限車種、車齡，以日計息，讓您借款免煩惱。</p>
                    </div>
                </div>
            </div> --}}
        </div>
    </section>
@endif

@if (!empty($data->category->text4))
    <section class="ls section_padding_top_130 section_padding_bottom_150 columns_padding_25 table_section table_section_md">
        <div class="container">
            {!!$data->category->text4!!}
            {{-- <div class="editor">
            <div class="row topmargin_50">
            <div class="col-sm-12">
            <h3 class="text-center">汽車借款 - 六大服務特點</h3>

            <div class="row topmargin_30">

                <div class="col-sm-6 col-md-4">
                    <div class="vertical-item product text-center">
                    <div class="item-media muted_background rounded overflow_hidden">
                        <a href="shop-product-right.html">
                        <img src="https://fakeimg.pl/400x300/ddd/fff/" alt="">
                        </a>
                        <div class="cs main_color entry-meta media-meta jcc vertical-center text-center">
                        <div class="price weight-black fontsize_20">
                            <span>快速審核 申辦容易</span>
                        </div>
                        </div>
                    </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-4">
                    <div class="vertical-item product text-center">
                    <div class="item-media muted_background rounded overflow_hidden">
                        <a href="shop-product-right.html">
                        <img src="https://fakeimg.pl/400x300/ddd/fff/" alt="">
                        </a>
                        <div class="cs main_color4 entry-meta media-meta jcc vertical-center text-center">
                        <div class="price weight-black fontsize_20">
                            <span>不限車齡  免留車</span>
                        </div>
                        </div>
                    </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-4">
                    <div class="vertical-item product text-center">
                    <div class="item-media muted_background rounded overflow_hidden">
                        <a href="shop-product-right.html">
                        <img src="https://fakeimg.pl/400x300/ddd/fff/" alt="">
                        </a>
                        <div class="cs main_color3 entry-meta media-meta jcc vertical-center text-center">
                        <div class="price weight-black fontsize_20">
                            <span>安全保密 手續簡便</span>
                        </div>
                        </div>
                    </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-4">
                    <div class="vertical-item product text-center">
                    <div class="item-media muted_background rounded overflow_hidden">
                        <a href="shop-product-right.html">
                        <img src="https://fakeimg.pl/400x300/ddd/fff/" alt="">
                        </a>
                        <div class="cs main_color5 entry-meta media-meta jcc vertical-center text-center">
                        <div class="price weight-black fontsize_20">
                            <span>額度高  利率低  免保人</span>
                        </div>
                        </div>
                    </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-4">
                    <div class="vertical-item product text-center">
                    <div class="item-media muted_background rounded overflow_hidden">
                        <a href="shop-product-right.html">
                        <img src="https://fakeimg.pl/400x300/ddd/fff/" alt="">
                        </a>
                        <div class="cs main_color2 entry-meta media-meta jcc vertical-center text-center">
                        <div class="price weight-black fontsize_20">
                            <span>不綁約   彈性還款</span>
                        </div>
                        </div>
                    </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-4">
                    <div class="vertical-item product text-center">
                    <div class="item-media muted_background rounded overflow_hidden">
                        <a href="shop-product-right.html">
                        <img src="https://fakeimg.pl/400x300/ddd/fff/" alt="">
                        </a>
                        <div class="cs main_color6 entry-meta media-meta jcc vertical-center text-center">
                        <div class="price weight-black fontsize_20">
                            <span>信用瑕疵、協商 均可辦</span>
                        </div>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
            </div>
            </div>
            </div> --}}
        </div>
    </section>
@endif

@if (!empty($data->category->text5))
    <section id="pricing" class="ls bg-light section_padding_top_130 section_padding_bottom_150">
        <div class="container">
            {!!$data->category->text5!!}
            {{-- <div class="row">
                <div class="col-sm-12 text-center">
                    <h3 class="txt-color3 fontsize_30">汽車借款如何辦理？</h3>
                </div>
            </div>
            <div class="row">
            <div class="col-sm-6">
            <div class="teaser media ">
                <div class="media-left">
                <div class="teaser_icon highlight3 size_small">
                    <i class="fa fa-star-o"></i>
                </div>
                </div>
                <div class="media-body">
                <h3 class="text-uppercase bottommargin_5">
                    汽車借款申請條件：
                </h3>
                <p>年滿20歲至75歲，名下有汽車者。</p>
                </div>
            </div>
            <div class="teaser media ">
                <div class="media-left">
                <div class="teaser_icon highlight3 size_small">
                    <i class="fa fa-star-o"></i>
                </div>
                </div>
                <div class="media-body">
                <h3 class="text-uppercase bottommargin_5">
                    汽車借款額度：
                </h3>
                <p>依據權威汽車雜誌鑑價，最低1萬元~最高300萬元。</p>
                </div>
            </div>
            <div class="teaser media ">
                <div class="media-left">
                <div class="teaser_icon highlight3 size_small">
                    <i class="fa fa-star-o"></i>
                </div>
                </div>
                <div class="media-body">
                <h3 class="text-uppercase bottommargin_5">
                    汽車借款準備資料：
                </h3>
                <p>1.&nbsp;車主身份證正反面影本（公司戶為營利事業登記證影本）<br />
                    2.&nbsp;行車執照影本<br />
                    3.&nbsp;第二證件影本（公司戶為負責人身份證影本）<br />
                    4.&nbsp;銀行或郵局封面影本（撥款專用）</p>
                </div>
            </div>
            </div>
            <div class="col-sm-6">
            <div class="teaser media ">
                <div class="media-left">
                <div class="teaser_icon highlight3 size_small">
                    <i class="fa fa-star-o"></i>
                </div>
                </div>
                <div class="media-body">
                <h3 class="text-uppercase bottommargin_5">
                    汽車借款利率：
                </h3>
                <p>汽車借款按月計息，每萬元250元。（實際利率依個人申請條件核定）。</p>
                </div>
            </div>
            <div class="teaser media ">
                <div class="media-left">
                <div class="teaser_icon highlight3 size_small">
                    <i class="fa fa-star-o"></i>
                </div>
                </div>
                <div class="media-body">
                <h3 class="text-uppercase bottommargin_5">
                    汽車借款還款方式：
                </h3>
                <p>1. 按月繳息：每月只繳利息，依您本身的經濟狀況而定，隨時可償還部分本金，利息按剩餘本金計算。<br />2. 分期償還：每月需繳金額＝期數平均分攤本金＋利息，每月繳付金額固定。</p>
                </div>
            </div>
            </div>
            </div> --}}
        </div>
    </section>
@endif

@if ($data->category->faqs->count() > 0)
    <section class="ls section_padding_top_150 section_padding_bottom_130">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 text-center">
                    <h3 class="section_header fontsize_30">
                        {{$data->category->title}} - 常見問答
                    </h3>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">

                    <div class="panel-group" id="accordion1">
                        @foreach ($data->category->faqs as $key => $faq)
                            @php
                                $firstdata = $data->category->faqs->first();
                                if($faq->id == $firstdata->id){
                                    $status1 = '';
                                    $status2 = "in";
                                }
                                else{
                                    $status1 = "collapsed";
                                    $status2 = '';
                                }  
                            @endphp
                            <div class="panel panel-default panel-color2">
                                <div class="panel-heading">
                                  <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion1" href="#collapse{{$faq->id}}" @if($status1 == "collapsed") class={{$status1}} @endif>
                                      {{$faq->question}}
                                    </a>
                                  </h4>
                                </div>
                                <div id="collapse{{$faq->id}}" class="panel-collapse collapse {{$status2}}">
                                  <div class="panel-body">
                                    {!!$faq->answer!!}
                                  </div>
                                </div>
                              </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
@endif

@if ($data->category->recommends->count() > 0)
    <section class="ls bg-gray section_padding_top_150 section_padding_bottom_130 columns_margin_bottom_30">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-4">
                    <h2 class="section_header fontsize_36">
                        <span class="small">CASES</span>
                        {{$data->category->title}}<br>精選案例
                    </h2>
                    @if ($data->category->blog_category)
                        <p class="topmargin_30">
                            <a href="{{route('blog.category', ['category'=>$data->category->blog_category->slug])}}" class="theme_button color2 wide_button">更多文章</a>
                        </p>
                    @endif
                </div>
                <div class="col-lg-9 col-md-8">
                    <div class="owl-carousel" data-nav="false" data-dots="false" data-responsive-lg="3"
                        data-responsive-md="2">
                        @foreach ($data->category->recommends->take(3) as $item)
                            <article class="vertical-item content-padding with_border color3_border rounded text-center">
                                <div class="item-media rounded overflow_hidden bottommargin_30">
                                    <img src="{{asset($item->picPath)}}" alt="{{$item->title}}">
                                    <div class="media-links color3">
                                        <a href="{{$item->url?:route('blog.detail', ['slug'=>rawurlencode($item->slug)])}}" target="{{$item->url?'_blank':''}}" title="{{$item->title}}" class="abs-link"></a>
                                    </div>
                                    @php
                                        $category = $item->categories->first();
                                    @endphp
                                    <div class="cs main_color3 entry-meta media-meta text-center pos-static">
                                        <div>
                                            <span class="date">{{$item->created_at->format('d')}}</span>
                                            <span class="small-text">{{$item->created_at->format('M')}}</span>
                                        </div>
                                        <div class="side-media-links">
                                            <a class="w-auto" href="{{route('blog.category', ['category'=>$category->slug])}}">
                                                <i class="fa fa-star" aria-hidden="true"></i> {{$category->title}}
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="item-content">
                                    <h4 class="entry-title hover-color3">
                                        <a href="{{$item->url?:route('blog.detail', ['slug'=>rawurlencode($item->slug)])}}" target="{{$item->url?'_blank':''}}" title="{{$item->title}}">{{$item->title}}</a>
                                    </h4>
                                    <div class="entry-content">
                                        <p class="multi-line-3">{!!$item->description!!}</p>
                                    </div>
                                  </div>
                                <footer class="highlight3links small-text semibold">
                                    <a href="{{$item->url?:route('blog.detail', ['slug'=>rawurlencode($item->slug)])}}" target="{{$item->url?'_blank':''}}" title="{{$item->title}}">read more</a>
                                </footer>
                            </article>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
@endif

@if ($data->category->blog_category&&$data->category->blogs->count() > 0)
    <section class="ls section_padding_top_150 section_padding_bottom_130 columns_margin_bottom_30">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-4">
                    <h2 class="section_header fontsize_36">
                        <span class="small">BLOGS</span>
                        {{$data->category->title}}<br>最新文章
                    </h2>
                    @if ($data->category->blog_category)
                        <p class="topmargin_30">
                            <a href="{{route('blog.category', ['category'=>$data->category->blog_category->slug])}}" class="theme_button color2 wide_button">更多文章</a>
                        </p>
                    @endif
                </div>
                <div class="col-lg-9 col-md-8">
                    <div class="owl-carousel" data-nav="false" data-dots="false" data-responsive-lg="3"
                        data-responsive-md="2">
                        @foreach ($data->category->blogs->take(3) as $item)
                           <article class="vertical-item content-padding with_border color2_border rounded text-center">
                                <div class="item-media rounded overflow_hidden bottommargin_30">
                                    <img src="{{asset($item->picPath)}}" alt="{{$item->title}}">
                                    <div class="media-links color2">
                                        <a href="{{$item->url?:route('blog.detail', ['slug'=>rawurlencode($item->slug)])}}" target="{{$item->url?'_blank':''}}" title="{{$item->title}}" class="abs-link"></a>
                                    </div>
                                    @php
                                        $category = $item->categories->first();
                                    @endphp
                                    <div class="cs main_color2 entry-meta media-meta text-center pos-static">
                                        <div>
                                            <span class="date">{{$item->created_at->format('d')}}</span>
                                            <span class="small-text">{{$item->created_at->format('M')}}</span>
                                        </div>
                                        <div class="side-media-links">
                                            <a class="w-auto" href="{{route('blog.category', ['category'=>$category->slug])}}">
                                                <i class="fa fa-star" aria-hidden="true"></i> {{$category->title}}
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="item-content">
                                    <h4 class="entry-title hover-color2">
                                        <a href="{{$item->url?:route('blog.detail', ['slug'=>rawurlencode($item->slug)])}}" target="{{$item->url?'_blank':''}}" title="{{$item->title}}">{{$item->title}}</a>
                                    </h4>
                                    <div class="entry-content">
                                        <p class="multi-line-3">{!!$item->description!!}</p>
                                    </div>
                                  </div>
                                <footer class="highlight2links small-text semibold">
                                    <a href="{{$item->url?:route('blog.detail', ['slug'=>rawurlencode($item->slug)])}}" target="{{$item->url?'_blank':''}}" title="{{$item->title}}">read more</a>
                                </footer>
                            </article>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
@endif
@include('frontend.layouts.footer_form')
@endsection
