<div class="single-sidebar-widget fix">
  @if ($data->service_id)
    <div class="sidebar-widget-title">
      <h5>{{$data->service->title}}最新案例</h5>
    </div>  
    <ul class="categories">
      @foreach ($data->service->cases->take(5) as $item)
        @php
          if ($item->url) {
            $link = $item->url;
          } elseif ($item->service_id) {
            $link = route('service.case.detail', ['category'=>$item->category->slug, 'service'=>$item->service->slug, 'slug'=>rawurlencode($item->slug)]);
          } else {
            $link = route('service.case.detail2', ['category'=>$item->category->slug, 'slug'=>rawurlencode($item->slug)]);
          }
        @endphp
        <li><a href="{{$link}}" title="{{$item->title}}">{{$item->title}}</a></li>
      @endforeach
    </ul>
  @else
    <div class="sidebar-widget-title">
      <h5>{{$data->category->title}}最新案例</h5>
    </div>  
    <ul class="categories">
      @foreach ($data->category->cases->take(5) as $item)
        @php
          if ($item->url) {
            $link = $item->url;
          } elseif ($item->service_id) {
            $link = route('service.case.detail', ['category'=>$item->category->slug, 'service'=>$item->service->slug, 'slug'=>rawurlencode($item->slug)]);
          } else {
            $link = route('service.case.detail2', ['category'=>$item->category->slug, 'slug'=>rawurlencode($item->slug)]);
          }
        @endphp
        <li><a href="{{$link}}" title="{{$item->title}}">{{$item->title}}</a></li>
      @endforeach
    </ul>
  @endif
</div>

<div class="single-sidebar-widget fix">
  <div class="sidebar-widget-title">
      <h5>最新文章</h5>
  </div>
  @foreach ($latest_blogs as $latest_blog)
    <div class="single-post-widget fix">
        <div class="post-img">
            <a href="{{$latest_blog->url?:route('blog.detail', ['slug'=>rawurlencode($latest_blog->slug)])}}" target="{{$latest_blog->url?'_blank':''}}" title="{{$latest_blog->title}}"><img src="{{asset($latest_blog->picPath)}}" alt=""></a>
        </div>
        <div class="post-texts">
            <h5><a href="{{$latest_blog->url?:route('blog.detail', ['slug'=>rawurlencode($latest_blog->slug)])}}" target="{{$latest_blog->url?'_blank':''}}" title="{{$latest_blog->title}}">{{$latest_blog->title}}</a></h5>
            <div class="post-info"><span class="post-date">{{$latest_blog->created_at->format('d M, Y')}}</span></div>
            <p class="post-desc">{!!nl2br($latest_blog->description)!!}</p>
        </div>
    </div>
  @endforeach
</div>