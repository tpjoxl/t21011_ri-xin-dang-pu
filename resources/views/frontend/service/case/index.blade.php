@extends('frontend.layouts.master')

@section('head')
  @include('frontend.layouts.page_link', ['datas' => $items])
@endsection

@section('content')
<div class="breadcrumbs-area bg-overlay-dark bg-6" @if (!empty($data->banner)) style="background-image: url('{{asset($data->banner)}}')" @endif>	
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="breadcrumbs-text text-left">
          <h1 class="heading">{{$data->title}}案例文章</h1>
          @include('frontend.layouts.breadcrumb')
        </div>
      </div>
    </div>
  </div>
</div>
<div class="blog-section pt-120 pb-115 sidebar right-side bb-lightgray">
  <div class="container">
    @if (count($items)>0)
      <div class="row">
        @foreach ($items as $item)
          @php
            if ($item->url) {
              $link = $item->url;
            } elseif ($item->service_id) {
              $link = route('service.case.detail', ['category'=>$item->category->slug, 'service'=>$item->service->slug, 'slug'=>rawurlencode($item->slug)]);
            } else {
              $link = route('service.case.detail2', ['category'=>$item->category->slug, 'slug'=>rawurlencode($item->slug)]);
            }
          @endphp
          <div class="col-xl-4 col-lg-6 col-md-6">
              <div class="single-blog">
                  <div class="blog-image">
                      <a href="{{$link}}" target="{{$item->url?'_blank':''}}" title="{{$item->title}}"><img src="img/blog/5.jpg" alt=""></a>
                      <span>{{$item->created_at->format('d M, Y')}}</span>
                  </div>
                  <div class="blog-text">
                      <h4><a href="{{$link}}" target="{{$item->url?'_blank':''}}" title="{{$item->title}}">{{$item->title}}</a></h4>
                      <p class="desc">{!!$item->description!!}</p>
                      <a href="{{$link}}" target="{{$item->url?'_blank':''}}" title="{{$item->title}}">Read More</a>
                  </div>
              </div>
          </div>
        @endforeach
      </div>
      {{ $items->appends(request()->input())->links('vendor.pagination.default_front') }}
    @else
      <div class="no-data">@lang('text.no_data')</div>
    @endif
  </div>
</div>
@include('frontend.layouts.footer_form')
@endsection