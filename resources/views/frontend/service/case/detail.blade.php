@extends('frontend.layouts.master')

@section('content')
<div class="breadcrumbs-area bg-overlay-dark bg-6" @if (!empty($data->banner)) style="background-image: url('{{asset($data->banner)}}')" @endif>	
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="breadcrumbs-text text-left">
          <div class="heading">{{$data->service_id?$data->service->title:$data->category->title}}案例文章</div>
          @include('frontend.layouts.breadcrumb')
        </div>
      </div>
    </div>
  </div>
</div>
<div class="blog-section ptb-120 sidebar">
  <div class="container">
      <div class="row">
          <div class="col-xl-9 col-lg-8">
              <div class="blog-image mr-lg-5">
                  @if ($data->pic)
                    <img src="{{asset($data->pic)}}" alt="">
                  @endif
                  <span>{{$data->created_at->format('d M, Y')}}</span>
              </div>
              <div class="blog-details-text mr-lg-5">
                  <div class="like-share">
                    @if ($data->service_id)
                      <a href="{{route('service.detail', ['category'=>$data->slug, 'slug'=>rawurlencode($data->service->slug)])}}"><i class="fa fa-tags"></i>
                        {{$data->service->title}}
                      </a>
                    @else
                      <a href="{{route('service.case.index', ['category'=>$data->slug, 'service'=>rawurlencode($data->category->slug)])}}}"><i class="fa fa-tags"></i>
                        {{$data->category->title}}
                      </a>
                    @endif
                  </div>
                  <h1 class="heading">{{$data->h1?:$data->title}}</h1>
                  <div class="editor">{!!$data->text!!}</div>
                  <hr class="my-5">
                  <div class="clearfix">
                    <div class="float-left">
                        <span>更新日期：</span> {{$data->updated_at->format('Y-m-d')}}
                    </div>
                    <div class="float-right">
                      <div class="a2a_kit a2a_kit_size_32 a2a_default_style">
                          <a class="a2a_button_facebook"></a>
                          <a class="a2a_button_line"></a>
                          <script async src="https://static.addtoany.com/menu/page.js"></script>
                      </div>
                    </div>
                </div>
              </div>
          </div>  
          <div class="col-xl-3 col-lg-4 fix">
            @include('frontend.'.$prefix.'.side')
          </div>
      </div>
  </div>
</div>
@include('frontend.layouts.footer_form')
@endsection
