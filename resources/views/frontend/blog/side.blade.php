<div class="widget widget_search">
    <h3 class="widget-title">文章搜尋</h3>
    <form method="get" class="searchform" action="{{route('blog.search')}}">

        <div class="form-group">
            <label class="sr-only" for="widget-search">搜尋關鍵字:</label>
            <input id="widget-search" type="search" value="{{request()->q}}" name="q" class="form-control"
                placeholder="請輸入關鍵字...">
        </div>
        <button type="submit" class="theme_button color1">搜尋</button>

    </form>
</div>

<div class="widget widget_categories">

    <h3 class="widget-title">文章分類</h3>
    <ul class="greylinks list2 checklist">
        <li class="active">
            <a href="{{route($prefix.'.index')}}" title="@lang('text.all_blog')" class="content-justify">
                <span>全部文章</span>
                <span>/ {{ $all_blogs }}</span>
            </a>
        </li>
        @foreach ($menu_blog_categories as $menu_blog_category)
            <li>
                <a href="{{route($prefix.'.category', ['category'=>rawurlencode($menu_blog_category->slug)])}}" title="{{$menu_blog_category->title}}" class="content-justify">
                    <span>{{$menu_blog_category->title}}</span>
                    <span>/ {{$menu_blog_category->blogs_count}}</span>
                </a>
            </li>
        @endforeach
    </ul>
</div>

<div class="widget widget_recent_posts">

    <h3 class="widget-title">最新文章</h3>
    <ul class="media-list">
        @foreach ($latest_blogs as $latest_blog)
            <li class="media">
                <div class="media-left media-middle">
                    <a href="{{$latest_blog->url?:route('blog.detail', ['slug'=>rawurlencode($latest_blog->slug)])}}" target="{{$latest_blog->url?'_blank':''}}" title="{{$latest_blog->title}}">
                        <img src="{{asset($latest_blog->picPath)}}" alt="{{$latest_blog->title}}" />
                    </a>
                </div>
                <div class="media-body media-middle">
                    <h4 class="entry-title">
                        <a href="{{$latest_blog->url?:route('blog.detail', ['slug'=>rawurlencode($latest_blog->slug)])}}" target="{{$latest_blog->url?'_blank':''}}" title="{{$latest_blog->title}}" class="text-inherit">
                            {{$latest_blog->title}}
                        </a>
                    </h4>
                    <p class="small-text medium txt-color2">
                        {{$latest_blog->created_at->format('Y-m-d')}}
                    </p>
                </div>
            </li>
        @endforeach
    </ul>
</div>