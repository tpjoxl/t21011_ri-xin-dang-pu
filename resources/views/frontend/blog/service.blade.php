@extends('frontend.layouts.master')

@section('head')
  @include('frontend.layouts.page_link', ['datas' => $items])
@endsection

@section('content')
<div class="breadcrumb-area shadow dark bg-fixed text-center text-light" @if (!empty($main->banner)) style="background-image: url('{{asset($main->banner)}}')" @endif>
  <div class="container">
      <div class="row">
          <div class="col-lg-12 col-md-12">
              <h1 class="heading">{{$main->h1?:($main->title?:__('text.'.$prefix))}}</h1>
              @include('frontend.layouts.breadcrumb')
          </div>
      </div>
  </div>
</div>
<div class="blog-area full-blog right-sidebar full-blog default-padding">
  <div class="container">
      <div class="row">
        <div class="blog-items">
          <div class="blog-content col-md-8">
            <div class="site-heading text-center">
                <h2>{{$data->title}}貸款知識文章</h2>
            </div>
            @if (count($items)>0)
              <div class="row">
                @foreach ($items as $item)
                  <div class="single-item col-sm-6 col-md-6">
                    <div class="item">
                        <div class="thumb">
                            <a href="{{$item->url?:route('blog.detail', ['slug'=>rawurlencode($item->slug)])}}" target="{{$item->url?'_blank':''}}" title="{{$item->title}}"><img src="{{asset($item->picPath)}}" alt="{{$item->title}}"></a>
                        </div>
                        <div class="info">
                            <div class="cats">
                                @php
                                    $category = $item->categories->first();
                                @endphp
                                <a href="{{route('blog.category', ['category'=>$category->slug])}}">{{$category->title}}</a>
                            </div>
                            <h4>
                                <a href="{{$item->url?:route('blog.detail', ['slug'=>rawurlencode($item->slug)])}}" target="{{$item->url?'_blank':''}}" title="{{$item->title}}">{{$item->title}}</a>
                            </h4>
                            <p class="desc">{!!$item->description!!}</p>
                            <div class="meta">
                                <ul>
                                    <li><i class="fas fa-calendar-alt"></i> {{$item->created_at->format('d M, Y')}}</li>
                                </ul>
                                <a href="{{$item->url?:route('blog.detail', ['slug'=>rawurlencode($item->slug)])}}" target="{{$item->url?'_blank':''}}" title="{{$item->title}}">Read More</a>
                            </div>
                        </div>
                    </div>
                  </div>
                @endforeach
              </div>
              <div class="row">
                  <div class="col-md-12 pagi-area">
                    {{ $items->appends(request()->input())->links('vendor.pagination.default_front') }}
                  </div>
              </div>
            @else
              <div class="no-data">@lang('text.no_data')</div>
            @endif
          </div>
          <div class="sidebar col-md-4 pl-md-40">
            @include('frontend.'.$prefix.'.side')
          </div>
      </div>
  </div>
</div>
@endsection
