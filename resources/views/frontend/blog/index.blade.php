@extends('frontend.layouts.master')

@section('head')
  @include('frontend.layouts.page_link', ['datas' => $items])
@endsection

@section('content')
<section class="page_breadcrumbs ds color parallax section_padding_top_75 section_padding_bottom_75" 
        style="background-image: @if (!empty($data->banner)) url('{{asset($data->banner)}}') @endif;">
  <div class="container">
    <div class="row">
      <div class="col-sm-12 text-center">
        <h1 class="page-heading text-uppercase">{{$data->h1?:($data->title?:__('text.'.$prefix))}}</h1>
        @include('frontend.layouts.breadcrumb')
      </div>
    </div>
  </div>
</section>

<section class="ls section_padding_top_130 section_padding_bottom_130 columns_padding_25">
  <div class="container">

    <div class="row">

      <div class="col-sm-7 col-md-8 col-lg-8">
        @if (Route::currentRouteName()=='blog.search')
          <div class="site-heading text-center">
              <h2>@lang('text.search_keyword')：{{request()->q}}</h2>
          </div>
        @endif
        @if (count($items)>0)
          @foreach ($items as $item)
            <article class="post format-small-image">

              <div class="side-item side-md content-padding big-padding with_border color3_border">

                <div class="row">

                  <div class="col-md-5">
                    <div class="item-media entry-thumbnail rounded overflow_hidden">
                      <img src="{{asset($item->picPath)}}" alt="{{$item->title}}">
                      <div class="media-links color3">
                        <a href="{{$item->url?:route('blog.detail', ['slug'=>rawurlencode($item->slug)])}}" target="{{$item->url?'_blank':''}}" title="{{$item->title}}" class="abs-link"></a>
                      </div>
                      <div class="cs main_color3 entry-meta media-meta text-center pos-static">
                        <div>
                          <span class="date">{{$item->created_at->format('d')}}</span>
                          <span class="small-text big-spacing">{{$item->created_at->format('M')}}</span>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="col-md-7">
                    <div class="item-content">
                      @php
                        $category = $item->categories->first();
                      @endphp
                      <header class="entry-header">
                        <div class="entry-meta small-text medium content-justify">
                          <span class="highlight3links">
                            <a href="{{route('blog.category', ['category'=>$category->slug])}}">{{$category->title}}</a>
                          </span>
                        </div>
                        <h4 class="entry-title hover-color3">
                          <a href="{{$item->url?:route('blog.detail', ['slug'=>rawurlencode($item->slug)])}}" target="{{$item->url?'_blank':''}}" title="{{$item->title}}" rel="bookmark">
                            {{$item->title}}</a>
                        </h4>
                      </header>

                      <div class="entry-content">
                        <p class="multi-line-3">{!!$item->description!!}</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </article>
          @endforeach
        @else
          <div class="no-data">@lang('text.no_data')</div>
        @endif
        <div class="row topmargin_60">
          {{ $items->appends(request()->input())->links('vendor.pagination.default_front') }}
        </div>
      </div>
      <aside class="col-sm-5 col-md-4 col-lg-4">
        @include('frontend.'.$prefix.'.side')
      </aside>
    </div>
  </div>
</section>              
@endsection
