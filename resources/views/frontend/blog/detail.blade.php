@extends('frontend.layouts.master')

@section('content')
<section class="page_breadcrumbs ds color parallax section_padding_top_75 section_padding_bottom_75" 
        style="background-image: @if (!empty($data->categories->first()->banner)) url('{{asset($data->categories->first()->banner)}}') @endif;">
  <div class="container">
    <div class="row">
      <div class="col-sm-12 text-center">
        <h1 class="page-heading text-uppercase">{{$data->h1?:($data->title?:__('text.'.$prefix))}}</h1>
        @include('frontend.layouts.breadcrumb')
      </div>
    </div>
  </div>
</section>

<section class="ls section_padding_top_130 section_padding_bottom_130 columns_padding_25">
  <div class="container">
    <div class="row">

      <div class="col-sm-7 col-md-8 col-lg-8">

        <article
          class="single-post vertical-item content-padding big-padding with_border rounded color2_border post">
          <div class="entry-thumbnail item-media bottommargin_40">
            @if ($data->pic)
              <img src="{{$data->pic}}" alt="{{$data->title}}">
            @endif

            <div class="cs main_color2 entry-meta media-meta text-center pos-static">
              <div>
                <span class="date">{{$data->created_at->format('d')}}</span>
                <span class="small-text big-spacing">{{$data->created_at->format('M')}}</span>
              </div>
              @php
                $category = $data->categories->first();
              @endphp
              <div class="side-media-links">
                <a  class="w-auto" href="{{route('blog.category', ['category'=>$category->slug])}}" >
                  <i class="fa fa-star" aria-hidden="true"></i> {{ $category->title }}
                </a>
                <div class="a2a_kit a2a_kit_size_32 a2a_default_style">
                  <a class="a2a_button_facebook"></a>
                  <a class="a2a_button_line"></a>
                </div>
                <script async src="https://static.addtoany.com/menu/page.js"></script>
              </div>
            </div>
          </div>

          <div class="item-content">
            {!!$data->text!!}
            {{-- <div class="editor entry-content">

              <p>來排十唱們更標樂兒……以經考現！型全是溫直亮中品是生一拿共德入我灣不東吃回在你華成則麼詩經條家新流，得歡進通深，性經色黨受，裝完又拿動一資作會投研預第的陽才內苦客速場得密見模漸通海生了自上下亮全天候人是品種自般。和成過開的用。亮苦公你前？自大化個看張文斯精：北友熱險藝港業個？光它情的車買大天起與減花起自把野會急，萬樹們月，說家使德。長遊影爸們，舉夫人目總。</p>

              <blockquote class="text-center">
                “技辦小過難元個力畫藝再地那克研我爸策以時包心基直形想使人為強環次子子越定非了了、上英此她委的也合高禮。十不觀處而生國千深。直手事用報寫真少請見，第理社才選學市“
                <div class="item-meta topmargin_25 bottommargin_0">
                  <h4>系獨教三候一異電飛傳去製</h4>
                  <span class="small-text highlight2">西要修大</span>
                </div>
              </blockquote>

              <p>人速想國會獨斯好定認能整標己三進黑，的現已經無不要過會輕對軍打光？人找課治高？美畫有魚營得一天要一費灣路人由、曾雖細以同城活就雙：她調部灣：目會利？就空布洲是的何終一辦命，明使人爭地克人於病。</p>

              <ul class="list2 color2 checklist">
                <li>例三中家眼走政的的問如生界心行香水也小子</li>
                <li>像活麼緊送完我如年手什己由獨</li>
                <li>便車十的母續，告細後議最話正選。</li>
              </ul>
              <p>
                <img class="alignleft" alt="" src="https://fakeimg.pl/300x386/ddd/fff/">
                反線以我的女人年規馬麗才出後學……速跟記，將日分人過時成，我科過小時的足？題的眾連來美文富和適育現是廠在我國天設性權打所上痛資愛像，結國算年，苦得大外全在不顯首精於術告加負味想，你高直縣圖古科國，到我手表口生，來時或的這來府種……放現不係臺一發的受的模陽辦，全次合一大定麼爭於狀千葉的國市雙腦學個命要速地變陸少是一連行居教獎，資一麼外門失結的高綠……也計情。
              </p>
              <p>常條民地思大，的黨於辦？線科民年面有讀否相點；指一則爸想主分天系照，縣錯不開，我拿示的成黃書開來不病時除子，用不帶地式似極值木獨國際國通童良下動有願方，狀子朋都現確少嚴通不，消語出把一走人更羅愛公去創接小行山多設，吃突活只不、任高品政做來動題算也灣不？技遠前服時常聲同。香容覺聽。</p>
              <p>學區行以程場，今去慢；來孩球當應！完族王上。注把局心，自傳成要合班何直臺車讀館生實可銷微安面汽大到失……水著美立一傳下，一起日人你學園運廣、線她後素看校還處場什有人然令接：通花放事上效火、量制著亞：方家得正。位報談速顧會易香通間說結。天團了不入喜年。沒近果課有生就轉，企時華。</p>
              <p>一有視，室說上精；息在也論入機我，為慢未隊法了飛學總不定平聲畫證皮：面裝是者，字樣不育或中和飛銷國你快令廣度營我，命不受假子教物別在態驚！油過把意會言有藝然這無體還電表</p>

              <div class="editor-fast-nav1"><h2 class="nav-heading">導讀區</h2><ul class="nav-list"><li><a href="#article_1" title="導讀連結文字1">導讀連結文字1</a></li><li><a href="#article_2" title="導讀連結文字2">導讀連結文字2</a></li><li><a href="#article_3" title="導讀連結文字3">導讀連結文字3</a></li><li><a href="#article_4" title="導讀連結文字4">導讀連結文字4</a></li><li><a href="#article_5" title="導讀連結文字5">導讀連結文字5</a></li></ul></div>
              <div class="editor-fast-nav2"><h2 class="nav-heading">導讀區</h2><ul class="nav-list"><li><a href="#article_1" title="導讀連結文字1">導讀連結文字1</a></li><li><a href="#article_2" title="導讀連結文字2">導讀連結文字2</a></li><li><a href="#article_3" title="導讀連結文字3">導讀連結文字3</a></li><li><a href="#article_4" title="導讀連結文字4">導讀連結文字4</a></li><li><a href="#article_5" title="導讀連結文字5">導讀連結文字5</a></li></ul></div>
              <div class="editor-fast-nav3"><h2 class="nav-heading">導讀區</h2><ul class="nav-list"><li><a href="#article_1" title="導讀連結文字1">導讀連結文字1</a></li><li><a href="#article_2" title="導讀連結文字2">導讀連結文字2</a></li><li><a href="#article_3" title="導讀連結文字3">導讀連結文字3</a></li><li><a href="#article_4" title="導讀連結文字4">導讀連結文字4</a></li><li><a href="#article_5" title="導讀連結文字5">導讀連結文字5</a></li></ul></div>
              <div id="article_1" class="editor-fast-sect"><h3 class="sect-heading">標題1</h3><p>內文1</p></div><div id="article_2" class="editor-fast-sect"><h3 class="sect-heading">標題2</h3><p>內文2</p></div><div id="article_3" class="editor-fast-sect"><h3 class="sect-heading">標題3</h3><p>內文3</p></div><div id="article_4" class="editor-fast-sect"><h3 class="sect-heading">標題4</h3><p>內文4</p></div><div id="article_5" class="editor-fast-sect"><h3 class="sect-heading">標題5</h3><p>內文5</p></div>

              <div class="editor-list1"><ul><li>第一項</li><li>第二項</li><li>第三項</li><li>第四項</li><li>第五項</li></ul></div>

              <div class="editor-list2"><ul><li>▶<b>標題1：</b>內容1</li><li>▶<b>標題2：</b>內容2</li><li>▶<b>標題3：</b>內容3</li></ul></div>

              <table class="editor-tmp-table1"><tbody><tr><th>申辦條件</th><td>名下自有汽車（有原車貸亦可申辦）</td></tr><tr><th>申辦利息</th><td>最低優惠利率0.85%起</td></tr><tr><th>貸款額度</th><td>只要您名下自有房屋土地還有殘值皆可申辦貸款，貸款額度無上限。</td></tr><tr><th>貸款期限</th><td>全程無綁約，隨借隨還，最長可達7年。</td></tr><tr><th>準備文件</th><td>房屋所有權狀正本正本、物件所有權人身份證件正本、影本一份、物件所有權人印鑑證明（有效期需3個月內，如本人在場可不提供）、本人印鑑章</td></tr><tr><th>審核時間</th><td>最慢3個工作天即可撥款</td></tr></tbody></table>

              <table class="editor-tmp-table2"><tbody><tr><th>申辦條件</th><td>只要名下有房屋或土地，不需要審核個人條件即可申辦</td></tr><tr><th>申辦利息</th><td>利率最低0.85%起</td></tr><tr><th>貸款額度</th><td>只要您名下自有房屋土地還有殘值皆可申辦貸款，貸款額度無上限。</td></tr><tr><th>貸款期限</th><td>全程無綁約，隨借隨還，最長可達7年。</td></tr><tr><th>準備文件</th><td>房屋所有權狀正本正本、物件所有權人身份證件正本、影本一份、物件所有權人印鑑證明（有效期需3個月內，如本人在場可不提供）、本人印鑑章</td></tr><tr><th>審核時間</th><td>最慢3個工作天即可撥款</td></tr></tbody></table>

              <table class="editor-tmp-table3"><tbody><tr><th>申辦條件</th><td>只要名下有房屋或土地，不需要審核個人條件即可申辦</td></tr><tr><th>申辦利息</th><td>利率最低0.85%起</td></tr><tr><th>貸款額度</th><td>只要您名下自有房屋土地還有殘值皆可申辦貸款，貸款額度無上限。</td></tr><tr><th>貸款期限</th><td>全程無綁約，隨借隨還，最長可達7年。</td></tr><tr><th>準備文件</th><td>房屋所有權狀正本正本、物件所有權人身份證件正本、影本一份、物件所有權人印鑑證明（有效期需3個月內，如本人在場可不提供）、本人印鑑章</td></tr><tr><th>審核時間</th><td>最慢3個工作天即可撥款</td></tr></tbody></table>

              <div class="editor-tmp-table-wrap"><table class="editor-tmp-table4"><thead><tr><th>標題</th><th>行標題1</th><th>行標題2</th></tr></thead><tbody><tr><th>列標題1</th><td>內容內容內容1-1</td><td>內容內容內容1-2</td></tr><tr><th>列標題2</th><td>內容內容內容2-1</td><td>內容內容內容2-2</td></tr></tbody></table></div>

              <div class="editor-tmp-table-wrap"><table class="editor-tmp-table5"><thead><tr><th>標題1</th><th>標題2</th><th>標題3</th></tr></thead><tbody><tr><td>內容內容內容內容</td><td>內容內容內容內容</td><td>內容內容內容內容</td></tr><tr><td>內容內容內容內容</td><td>內容內容內容內容</td><td>內容內容內容內容</td></tr><tr><td>內容內容內容內容</td><td>內容內容內容內容</td><td>內容內容內容內容</td></tr></tbody></table></div>
            </div> --}}
          </div>

        </article>
        
        <div class="row columns_padding_5 page-nav">
          @if($prev)
            <div class="col-md-6">
              <div class="with_padding text-center ds bg_teaser after_cover darkgrey_bg">
                <a @if ($prev->url)
                  href="{{$prev->url}}" target="_blank"
                @else
                  href="{{route($prefix.'.detail', ['slug'=>rawurlencode($prev->slug)])}}"
                @endif >
                  <img src="{{ asset($prev->picPath) }}" alt="{{ $prev->title }}">
                </a>
                <div>
                  <div class="highlight2 small-text medium">
                    <a @if ($prev->url)
                      href="{{$prev->url}}" target="_blank"
                    @else
                      href="{{route($prefix.'.detail', ['slug'=>rawurlencode($prev->slug)])}}"
                    @endif >上一篇文章 </a>
                  </div>
                  <h4 class="entry-title">
                    <a @if ($prev->url)
                      href="{{$prev->url}}" target="_blank" rel="bookmark"
                    @else
                      href="{{route($prefix.'.detail', ['slug'=>rawurlencode($prev->slug)])}}" rel="bookmark"
                    @endif >{{$prev->title}}</a>
                  </h4>
                </div>
              </div>
            </div>
          @endif
          @if($next)
            <div class="col-md-6">
              <div class="with_padding text-center ds bg_teaser after_cover darkgrey_bg">
                <a @if ($next->url)
                  href="{{$next->url}}" target="_blank"
                @else
                  href="{{route($prefix.'.detail', ['slug'=>rawurlencode($next->slug)])}}"
                @endif >
                  <img src="{{ asset($next->picPath) }}" alt="{{ $next->title }}">
                </a>
                <div class="item-content">
                  <div class="highlight3 small-text medium">
                    <a @if ($next->url)
                      href="{{$next->url}}" target="_blank"
                    @else
                      href="{{route($prefix.'.detail', ['slug'=>rawurlencode($next->slug)])}}"
                    @endif >下一篇文章 </a>
                  </div>
                  <h4 class="entry-title">
                    <a @if ($next->url)
                      href="{{$next->url}}" target="_blank" rel="bookmark"
                    @else
                      href="{{route($prefix.'.detail', ['slug'=>rawurlencode($next->slug)])}}" rel="bookmark"
                    @endif >{{$next->title}}</a>
                  </h4>
                </div>
              </div>
            </div>
          @endif
        </div>
        
      </div>

      <aside class="col-sm-5 col-md-4 col-lg-4">
        @include('frontend.'.$prefix.'.side')
      </aside>
    </div>
  </div>
</section>
@include('frontend.layouts.footer_form')
@endsection
