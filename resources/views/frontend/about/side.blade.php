<aside class="col-sm-5 col-md-4 col-lg-4">
  <div class="widget widget_recent_posts">
    <h3 class="widget-title">最新文章</h3>
    <ul class="media-list">
      @foreach ($latest_blogs as $latest_blog)
        <li class="media">
          <div class="media-left media-middle">
            <a href="{{$latest_blog->url?:route('blog.detail', ['slug'=>rawurlencode($latest_blog->slug)])}}" target="{{$latest_blog->url?'_blank':''}}" title="{{$latest_blog->title}}">
              <img src="{{asset($latest_blog->picPath)}}" alt="{{$latest_blog->title}}" />
            </a>
          </div>
          <div class="media-body media-middle">
            <h4 class="entry-title">
              <a href="{{$latest_blog->url?:route('blog.detail', ['slug'=>rawurlencode($latest_blog->slug)])}}" target="{{$latest_blog->url?'_blank':''}}" title="{{$latest_blog->title}}">
                {{$latest_blog->title}}
              </a>
            </h4>
            <p class="small-text medium txt-color2">
              {{$latest_blog->created_at->format('Y-m-d')}}
            </p>
          </div>
        </li>
      @endforeach
    </ul>
  </div>
</aside>
