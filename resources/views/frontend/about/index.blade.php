@extends('frontend.layouts.master')

@section('content')
<section class="page_breadcrumbs ds color parallax section_padding_top_75 section_padding_bottom_75" 
        style="background-image: @if (!empty($data->banner)) url('{{asset($data->banner)}}') @endif;">
  <div class="container">
    <div class="row">
      <div class="col-sm-12 text-center">
        <h1 class="page-heading text-uppercase">{{$data->h1?:($data->title?:__('text.'.$prefix))}}</h1>
        @include('frontend.layouts.breadcrumb')
      </div>
    </div>
  </div>
</section>

<!-- content start -->
<section class="ls section_padding_top_130 section_padding_bottom_130 columns_padding_25">
  <div class="container">
    <div class="row">
      @if($data->text)
        <div class="col-sm-7 col-md-8 col-lg-8">
          {!!$data->text!!}
          {{-- <article
            class="single-post vertical-item content-padding big-padding with_border rounded color2_border post">
            <div class="item-content">

              <div class="editor entry-content">

                <div class="item-meta text-center">
                  <h4>日鑫當舖</h4>
                  <span class="fontsize_18 highlight2">人性化的當鋪，站在您的立場為您考量。</span>
                </div>
                <p>
                  沒有人的人生是一路順遂，路途中難免會遇到緊急的時候需要資金周轉，選擇日鑫台中當鋪，讓您借貸快速還款無壓力。<br>日鑫台中當鋪除了實體店面營業，是政府立案的合法當舖，借貸符合當鋪業法規範，我們的專業人員鑑定物品精準快速，台中日鑫當鋪不僅不會巧立名目收取各種費用，站在借款人的立場上設想，以專業的親切態度服務借款人，讓借款人解決現金週轉的問題！不論職業，只要年滿20歲，有借款的需求都可找我們服務，幫助借款人規劃彈性還款的方案攤還本金利息，秉持著與您一起解決用錢的需求，讓您借款免煩惱。
                </p>

                <div class="row">
                  <div class="col-lg-4 col-sm-6">
                    <div class="teaser main_bg_color text-center rounded">
                      <div class="teaser_icon size_small">
                        <a href="service.html" title="汽車借款"><img src="https://fakeimg.pl/40x40/ddd/fff/" alt=""></a>
                      </div>
                      <h3 class="numbered bottommargin_0">
                        <a href="service.html" title="汽車借款">汽車借款</a>
                      </h3>
                    </div>
                  </div>

                  <div class="col-lg-4 col-sm-6">
                    <div class="teaser main_bg_color2 text-center rounded">
                      <div class="teaser_icon size_small">
                        <a href="service.html" title="機車借款"><img src="https://fakeimg.pl/40x40/ddd/fff/" alt=""></a>
                      </div>
                      <h3 class="numbered bottommargin_0">
                        <a href="service.html" title="機車借款">機車借款</a>
                      </h3>
                    </div>
                  </div>

                  <div class="col-lg-4 col-sm-6">
                    <div class="teaser main_bg_color3 text-center rounded">
                      <div class="teaser_icon size_small">
                        <a href="service.html" title="黃金借款"><img src="https://fakeimg.pl/40x40/ddd/fff/" alt=""></a>
                      </div>
                      <h3 class="numbered bottommargin_0">
                        <a href="service.html" title="黃金借款">黃金借款</a>
                      </h3>
                    </div>
                  </div>

                  <div class="col-lg-4 col-sm-6">
                    <div class="teaser main_bg_color4 text-center rounded">
                      <div class="teaser_icon size_small">
                        <a href="service.html" title="汽車借款"><img src="https://fakeimg.pl/40x40/ddd/fff/" alt=""></a>
                      </div>
                      <h3 class="numbered bottommargin_0">
                        <a href="service.html" title="汽車借款">汽車借款</a>
                      </h3>
                    </div>
                  </div>

                  <div class="col-lg-4 col-sm-6">
                    <div class="teaser main_bg_color5 text-center rounded">
                      <div class="teaser_icon size_small">
                        <a href="service.html" title="機車借款"><img src="https://fakeimg.pl/40x40/ddd/fff/" alt=""></a>
                      </div>
                      <h3 class="numbered bottommargin_0">
                        <a href="service.html" title="機車借款">機車借款</a>
                      </h3>
                    </div>
                  </div>

                  <div class="col-lg-4 col-sm-6">
                    <div class="teaser main_bg_color6 text-center rounded">
                      <div class="teaser_icon size_small">
                        <a href="service.html" title="黃金借款"><img src="https://fakeimg.pl/40x40/ddd/fff/" alt=""></a>
                      </div>
                      <h3 class="numbered bottommargin_0">
                        <a href="service.html" title="黃金借款">黃金借款</a>
                      </h3>
                    </div>
                  </div>
                </div>
                <div class="row topmargin_30">
                  <div class="col-md-6">
                    <img src="https://fakeimg.pl/400x400/ddd/fff/" alt="">
                  </div>
                  <div class="col-md-6">
                    <h4 class="text-uppercase">日鑫當鋪-快速貸款專線</h4>
                    <p>日鑫當鋪(大台中地區)<br>全年無休、24HR專人服務</p>
                    <div class="topmargin_40">
                      <div class="teaser media ">
                        <div class="media-left">
                          <div class="teaser_icon highlight size_small">
                            <i class="fa fa-phone" aria-hidden="true"></i>
                          </div>
                        </div>
                        <div class="media-body">
                          <h3 class="text-uppercase bottommargin_0">
                            24H服務專線
                          </h3>
                          <p>0966-423333</p>
                        </div>
                      </div>
                      <div class="teaser media ">
                        <div class="media-left">
                          <div class="teaser_icon highlight size_small">
                            <i class="social-icon soc-line" aria-hidden="true"></i>
                          </div>
                        </div>
                        <div class="media-body">
                          <h3 class="text-uppercase bottommargin_0">
                            LINE ID
                          </h3>
                          <p>0966423333</p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <!-- .entry-content -->
            </div>
            <!-- .item-content -->
          </article> --}}
        </div>
      @endif
      @include('frontend.about.side')
    </div>
  </div>
</section>
@include('frontend.layouts.footer_form')
@endsection
