<div class="mobile-nav__wrapper">
  <div class="mobile-nav__overlay mobile-nav__toggler"></div>
  <div class="mobile-nav__content">
      <span class="mobile-nav__close mobile-nav__toggler"><i class="far fa-times"></i></span>

      <div class="logo-box">
          <a href="{{route('home')}}" title="{{$setting->name}}" aria-label="logo image"><img src="{{asset('assets/images/logo-light.png')}}" alt="{{$setting->name}}" /></a>
      </div>
      <div class="mobile-nav__container"></div>

      <ul class="mobile-nav__contact list-unstyled">
          <li>
              <i class="img-icon"><img src="{{asset('assets/images/icon_phone.svg')}}" alt=""></i>
              <a href="tel:{{$setting->phone}}" title="免費諮詢">{{$setting->phone}}</a>
          </li>
          <li>
              <i class="img-icon"><img src="{{asset('assets/images/icon_fb.svg')}}" alt=""></i>
              <a href="{{$setting->fb}}" target="_blank" title="Facebook 臉書聯繫">Facebook 臉書聯繫</a>
          </li>
          <li>
              <i class="img-icon"><img src="{{asset('assets/images/icon_line.svg')}}" alt=""></i>
              <a href="{{$setting->line}}" target="_blank" title="LINE 聯繫我們">LINE 聯繫我們</a>
          </li>
      </ul>
  </div>
</div>