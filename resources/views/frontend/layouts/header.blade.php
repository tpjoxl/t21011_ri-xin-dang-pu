<header class="page_header header_white toggler_xs_right section_padding_30">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12 display_table">
        <div class="header_left_logo display_table_cell">
          <a  href="{{route('home')}}" class="logo top_logo">
            <img src="{{asset('assets/images/logo.svg')}}" alt="日鑫當鋪">
          </a>
        </div>

        <div class="header_mainmenu display_table_cell text-center">
          <nav class="mainmenu_wrapper">
            <ul class="mainmenu nav sf-menu">
              <li class="{{(strpos(Route::currentRouteName(), 'about') !== false)? 'active' : ''}}">
                <a href="{{route('about.index')}}">@lang('text.about')</a>
              </li>
              @foreach ($menu_service_categories->where('menu_status', 1) as $menu_service_category)
                @php
                  $linkClass = '';
                  if ((Route::currentRouteName()=='service.index'&&!empty($data)&&$menu_service_category->id==$data->id) || (Route::currentRouteName()=='service.detail'&&!empty($data)&&$menu_service_category->id==$data->service_category_id)) {
                    $linkClass .= 'active';
                  }
                  if ($menu_service_category->services->count()>0) {
                    $linkClass .= ' dropdown-toggle';
                  }
                @endphp
                @if ($menu_service_category->services->count()>0)
                  <li class="{{$linkClass}}">
                    <a href="{{route('service.index', ['slug'=>rawurlencode($menu_service_category->slug)])}}" title="{{$menu_service_category->title}}">{{$menu_service_category->title}}</a>
                @else
                  <li class="{{$linkClass}}">
                    <a href="{{route('service.index', ['slug'=>rawurlencode($menu_service_category->slug)])}}" title="{{$menu_service_category->title}}">{{$menu_service_category->title}}</a>
                @endif
                    @if ($menu_service_category->services->count()>0)
                      @foreach ($menu_service_category->services as $service)
                      <ul>
                        <li class="{{Route::currentRouteName()=='service.detail'&&!empty($data)&&$service->id==$data->id?'active':''}}">
                          <a href="{{route('service.detail', ['category'=>rawurlencode($menu_service_category->slug), 'slug'=>rawurlencode($service->slug)])}}" title="{{$service->title}}">{{$service->title}}</a>
                        </li>
                      </ul>
                      @endforeach
                    @endif
                  </li>
              @endforeach
              <li>
                <a href="#">其他服務</a>
                <ul>
                  @foreach ($menu_service_categories->where('menu_status', 0) as $menu_service_category)
<<<<<<< HEAD
                    <li>
                      <a
                        @php
                          $linkClass = '';
                          if ((Route::currentRouteName()=='service.index'&&!empty($data)&&$menu_service_category->id==$data->id) || (Route::currentRouteName()=='service.detail'&&!empty($data)&&$menu_service_category->id==$data->service_category_id)) {
                            $linkClass .= ' active';
                          }
                        @endphp
                        class="{{$linkClass}}"
                        href="{{route('service.index', ['slug'=>rawurlencode($menu_service_category->slug)])}}"
                        title="{{$menu_service_category->title}}">
                        {{$menu_service_category->title}}
                      </a>
                        @if ($menu_service_category->services->count()>0)
                          <ul>
                            @foreach ($menu_service_category->services as $service)
                              <li><a href="{{route('service.detail', ['category'=>rawurlencode($menu_service_category->slug), 'slug'=>rawurlencode($service->slug)])}}" class="{{Route::currentRouteName()=='service.detail'&&!empty($data)&&$service->id==$data->id?'active':''}}" title="{{$service->title}}">{{$service->title}}</a></li>
                            @endforeach
                          </ul>
                        @endif
=======
                    @php
                      $linkClass = '';
                      if ((Route::currentRouteName()=='service.index'&&!empty($data)&&$menu_service_category->id==$data->id) || (Route::currentRouteName()=='service.detail'&&!empty($data)&&$menu_service_category->id==$data->service_category_id)) {
                        $linkClass .= 'active';
                      }
                      if ($menu_service_category->services->count()) {
                        $linkClass .= ' dropdown-toggle';
                      }
                    @endphp
                    <li class="{{$linkClass}}">
                      <a href="{{route('service.index', ['slug'=>rawurlencode($menu_service_category->slug)])}}"
                        title="{{$menu_service_category->title}}">{{$menu_service_category->title}}
                      </a>
>>>>>>> origin/master
                    </li>
                  @endforeach
                </ul>
              </li>
              <li class="{{(strpos(Route::currentRouteName(), 'blog') !== false)? 'active' : ''}}">
                <a href="{{route('blog.index')}}">@lang('text.blog')</a>
              </li>
              <li class="{{(strpos(Route::currentRouteName(), 'contact') !== false)? 'active' : ''}}">
                <a href="{{route('contact.index')}}">聯絡我們</a>
              </li>
            </ul>
          </nav>
          <!-- header toggler -->
          <span class="toggle_menu">
            <span></span>
          </span>
        </div>

        <div class="header_right_buttons display_table_cell text-right hidden-xs">
          <p class="header-meta grey">
            @if($setting->phone)
              <a href="tel:{{$setting->phone}}" title="免費諮詢"><img src="{{asset('assets/images/phone2.png')}}" alt="電話"></a>
            @endif
            @if($setting->line)
              <a href="{{$setting->line}}" target="_blank" title="線上諮詢"><img src="{{asset('assets/images/line.png')}}" alt="LINE"></a>
            @endif
            @if($setting->fb)
              <a href="{{$setting->fb}}" target="_blank" title="FB諮詢"><img src="{{asset('assets/images/facebook3.png')}}" alt="Facebook"></a>
            @endif
            @if($setting->address_url)
              <a href="{{$setting->address_url}}" target="_blank" title="google_map"><img src="{{asset('assets/images/map.png')}}" alt="google_map"></a>
            @endif
          </p>
        </div>
      </div>
    </div>
  </div>
</header>

<script type="application/ld+json">
{
  "@context": "http://schema.org/",
  "@type":"SiteNavigationElement",
  "headline": "headline-string",
  "@id":"#nav",
  "@graph":
  [
    {
      "@context": "https://schema.org",
      "@type":"SiteNavigationElement",
      "@id":"#nav",
      "name": "@lang('text.home')",
      "url":"{{Route::has('home')? route('home') : ''}}"
    },
    @foreach ($menu_service_categories as $menu_service_category)
    {
      "@context": "https://schema.org",
      "@type":"SiteNavigationElement",
      "@id":"#nav",
      "name": "{{$menu_service_category->title}}",
      "url":"{{route('service.index', ['slug'=>rawurlencode($menu_service_category->slug)])}}"
    },
    @endforeach
    {
      "@context": "https://schema.org",
      "@type":"SiteNavigationElement",
      "@id":"#nav",
      "name": "@lang('text.blog')",
      "url":"{{Route::has('blog.index')? route('blog.index') : ''}}"
    },
    {
      "@context": "https://schema.org",
      "@type":"SiteNavigationElement",
      "@id":"#nav",
      "name": "@lang('text.contact')",
      "url":"{{Route::has('contact.index')? route('contact.index') : ''}}"
    }
  ]
}
</script>
