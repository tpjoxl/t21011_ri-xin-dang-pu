@if (Session::has('success'))
<script type="text/javascript">
  swal({
    title: "{{ Session::get('success') }}",
    @if (Session::has('successText'))
      text: "{{ Session::get('successText') }}",
    @endif
    type: "success",
    confirmButtonText: "確定"
  });
</script>
@endif

@if (Session::has('error'))
<script type="text/javascript">
  swal({
    title: "{{ Session::get('error') }}",
    @if (Session::has('errorText'))
      text: "{{ Session::get('errorText') }}",
    @endif
    type: "error",
    confirmButtonText: "關閉"
  });
</script>
@endif
@if (isset($errors) && count($errors))
  <script type="text/javascript">
    $(window).on('load', function(){
      swal({
        title: "{{ $errors->all()[0] }}",
        type: "error",
        confirmButtonText: "關閉"
      }, function() {
        if ($('.has-error').length>0) {
          var errorPos = $('.has-error').eq(0).offset().top - ($('.navbar').outerHeight() + 15);
          $('html, body').scrollTop(errorPos);
        }
      });
    });
  </script>
@endif
{{-- @if (Session::has('success'))
    <div class="alert alert-success text-center hidden-print">
        <i class="icon fa fa-check"></i> {{ Session::get('success') }}
    </div>
@endif

@if (Session::has('error'))
    <div class="alert alert-danger text-center hidden-print">
        <i class="icon fa fa-ban"></i> {{ Session::get('error') }}
    </div>
@endif --}}

{{-- @if (count($errors))
    <div class="alert alert-danger hidden-print" role="alert">
      <ul>
          @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
          @endforeach
      </ul>
    </div>
@endif --}}
