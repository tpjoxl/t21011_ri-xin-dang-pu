<footer class="page_footer ds ms section_padding_50">
  <div class="container">
    <div class="row">
      <div class="col-md-4 col-sm-12 to_animate">
        <div class="widget_text bottommargin_40">
          <h3 class="widget-title">服務項目</h3>
          <div class="row">
            @foreach ($menu_service_categories as $menu_service_category) 
              <div class="col-xs-6 col-sm-4 col-md-6">
                <div class="media small-teaser">
                  <div class="media-left">
                    <i class="rt-icon2-checkmark highlight2 fontsize_24"></i>
                  </div>
                  <div class="media-body">
                    <a href="{{route('service.index', ['slug'=>rawurlencode($menu_service_category->slug)])}}" title="{{$menu_service_category->title}}">{{$menu_service_category->title}}</a>
                  </div>
                </div>
              </div>
            @endforeach
          </div>
          <p class="topmargin_30">
            <a href="{{route('about.index')}}" class="theme_button color2">@lang('text.about')</a>
          </p>
        </div>
      </div>

      <div class="col-md-5 col-sm-7 to_animate">
        <div class="widget widget_recent_entries rightmargin_30">
          <h3 class="widget-title">最新文章</h3>
          <ul class="media-list">
            @foreach ($footer_latest_blogs as $item)
              <li class="media">
                <div class="media-left media-middle">
                  <a href="{{$item->url?:route('blog.detail', ['slug'=>rawurlencode($item->slug)])}}" target="{{$item->url?'_blank':''}}" title="{{$item->title}}">
                    <img src="{{asset($item->picPath)}}" alt="{{$item->title}}" />
                  </a>
                </div>
                <div class="media-body media-middle">
                  <p class="small-text medium color-white margin_0">
                    {{$item->created_at->format('Y-m-d')}}
                  </p>
                  <h4 class="entry-title">
                    <a href="{{$item->url?:route('blog.detail', ['slug'=>rawurlencode($item->slug)])}}" target="{{$item->url?'_blank':''}}" 
                        title="{{$item->title}}">
                      {{$item->title}}
                    </a>
                  </h4>
                </div>
              </li>
            @endforeach
          </ul>
        </div>
      </div>

      <div class="col-md-3 col-sm-5 to_animate">
        <div class="widget_text">
          <h3 class="widget-title">聯絡我們</h3>
          @if ($setting->address)
            <p><a @if ($setting->address_url) href="{{$setting->address_url}}" target="_blank" @endif><i class="rt-icon2-home2 highlight2"></i> {{$setting->address}}</a></p>
          @endif
          <ul class="list1 no-bullets">
            @if ($setting->phone)
              <li>
                <a href="tel:{{$setting->phone}}"><i class="rt-icon2-device-phone highlight2"></i> {{$setting->phone}}</a>
              </li>
            @endif
            @if ($setting->email)
              <li>
                <a href="mailto:{{$setting->email}}"><i class="rt-icon2-mail2 highlight2"></i> {{$setting->email}}</a>
              </li>
            @endif
            @if ($setting->business_hours)
              <li>
                <i class="rt-icon2-clock3 highlight2"></i> {{$setting->business_hours}}
              </li>
            @endif
          </ul>
        </div>
      </div>
    </div>
  </div>
</footer>

<section class="ds ms page_copyright section_padding_15 with_top_border_container">
  <div class="container">
    <div class="row">
      <div class="col-sm-12 text-center">
        <p>&copy; Copyright {{Carbon::now()->year}}. {{$setting->name}}All Rights Reserved.</p>
      </div>
    </div>
  </div>
</section>