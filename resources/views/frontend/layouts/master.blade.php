<!DOCTYPE html>
<html lang="{{ config('app.locale') }}" class="no-js">
<head>
    @include('frontend.layouts.head')

    @yield('head')

    @yield('style')

    @yield('head_script')

    {!! $setting->head_code !!}
</head>
<body>
  <div id="canvas">
		<div id="box_wrapper">
    @include('frontend.layouts.header')
    @yield('content')
    @include('frontend.layouts.footer')
    {{-- @include('frontend.layouts.mobile_nav') --}}
    @include('frontend.layouts.floating')
    @include('frontend.layouts.foot')
    @include('frontend.layouts.message')

    @yield('script')
    </div>
  </div>
  {!! $setting->body_code !!}
</body>
</html>
