<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<meta name="csrf-token" content="{{ csrf_token() }}">
@php
  $page_title=$page_description=$page_keyword=$page_pic=$page_og_title=$page_og_description='';
  // SEO Title
  if (isset($custom_page_title) && !empty($custom_page_title)) {
    $page_title .= $custom_page_title.' - ';
  } elseif (isset($data->seo_title) && !empty($data->seo_title)) {
    $page_title .= $data->seo_title.' - ';
  } elseif (isset($data->title) && !empty($data->title)) {
    $page_title .= $data->title.' - ';
  } elseif (isset($setting->seo_title) && !empty($setting->seo_title)) {
    $page_title .= $setting->seo_title.' - ';
  }
  $page_title .= $setting->name;
  // SEO Description
  if (isset($data->seo_description) && !empty($data->seo_description)) {
    $page_description .= $data->seo_description;
  } elseif (isset($data->description) && !empty($data->description)) {
    $page_description .= $data->description;
  } elseif (isset($setting->seo_description) && !empty($setting->seo_description)) {
    $page_description .= $setting->seo_description;
  }
  // SEO Keyword
  if (isset($data->seo_keyword) && !empty($data->seo_keyword)) {
    $page_keyword .= $data->seo_keyword;
  } elseif (isset($setting->seo_keyword) && !empty($setting->seo_keyword)) {
    $page_keyword .= $setting->seo_keyword;
  }
  // og image
  if (isset($data->og_image) && !empty($data->og_image)) {
    $page_pic .= $data->og_image;
  } elseif (isset($data->pic) && !empty($data->pic)) {
    $page_pic .= $data->pic;
  } elseif (isset($setting->og_image) && !empty($setting->og_image)) {
    $page_pic .= $setting->og_image;
  }
  // og title
  if (isset($data->og_title) && !empty($data->og_title)) {
    $page_og_title .= $data->og_title.' - ';
  } else if (isset($custom_page_title) && !empty($custom_page_title)) {
    $page_og_title .= $custom_page_title.' - ';
  } else if (isset($data->seo_title) && !empty($data->seo_title)) {
    $page_og_title .= $data->seo_title.' - ';
  } else if (isset($data->title) && !empty($data->title)) {
    $page_og_title .= $data->title.' - ';
  } else if (isset($setting->og_title) && !empty($setting->og_title)) {
    $page_og_title .= $setting->og_title.' - ';
  } else if (isset($setting->seo_title) && !empty($setting->seo_title)) {
    $page_og_title .= $setting->seo_title.' - ';
  }
  $page_og_title .= $setting->name;
  // og description
  if (isset($data->og_description) && !empty($data->og_description)) {
    $page_og_description .= $data->og_description;
  } else if (isset($data->seo_description) && !empty($data->seo_description)) {
    $page_og_description .= $data->seo_description;
  } else if (isset($data->description) && !empty($data->description)) {
    $page_og_description .= $data->description;
  } else if (isset($setting->og_description) && !empty($setting->og_description)) {
    $page_og_description .= $setting->og_description;
  } else if (isset($setting->seo_description) && !empty($setting->seo_description)) {
    $page_og_description .= $setting->seo_description;
  }
@endphp
<title>{{$page_title}}</title>

@if(isset($data->meta_robots))
  <meta name="robots" content="{{$data->meta_robots}}">
@elseif(isset($setting->meta_robots))
  <meta name="robots" content="{{$setting->meta_robots}}">
@endif

<meta name="keywords" content="{{$page_keyword}}">
<meta name="description" content="{{$page_description}}">
<meta name="copyright" content="Copyrights © {{ $setting->name }} All Rights Reserved">
<meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE">

<meta property="og:title" content="{{$page_og_title}}">
<meta property="og:type" content="website">
@if ($page_pic)
  <meta property="og:image" content="{{asset($page_pic)}}">
@endif
<meta property="og:url" content="{{url()->full()}}">
<meta property="og:description" content="{{$page_og_description}}">

<meta property="og:site_name" content="{{ $setting->name }}">

<link rel="canonical" href="{{request()->url()}}">

@include('_app_icon')

@if (count($lang_datas)>1)
  @foreach ($common->present()->langUrl(isset($data) && !is_null($data) ? $data : null) as $lang_code => $url)
    <link rel="alternate" href="{{$url}}" hreflang="{{$lang_code}}">
  @endforeach
@endif

<link rel="stylesheet" href="{{asset('assets/css/bootstrap.min.css')}}">
<link rel="stylesheet" href="{{asset('assets/css/animations.css')}}">
<link rel="stylesheet" href="{{asset('assets/css/fonts.css')}}">
<link rel="stylesheet" href="{{asset('assets/css/main.css')}}" class="color-switcher-link">
<link rel="stylesheet" href="{{asset('assets/css/shop.css')}}">
<link rel="stylesheet" href="{{asset('assets/css/custom.css')}}">
<link rel="stylesheet" href="{{asset('assets/css/custom_editor.css')}}">
<script src="{{asset('assets/js/vendor/modernizr-2.6.2.min.js')}}"></script>
<link rel="stylesheet" href="{{asset('js/sweetalert/dist/sweetalert.css')}}">



@php
    try {
        $main_css = mix('css/main.css');
    } catch(Exception $e) {
        $main_css = asset('css/main.css');
    }
@endphp
<link href="{{ $main_css }}" rel="stylesheet"> 

<script>
    window.App = {!! json_encode([
      'csrfToken' => csrf_token(),
    ]) !!};
</script>
<!--[if lt IE 9]>
  <script src="{{asset('assets/js/html5/html5shiv.min.js')}}"></script>
  <script src="{{asset('assets/js/html5/respond.min.js')}}"></script>
<![endif]-->