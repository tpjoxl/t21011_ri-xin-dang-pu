@if ($parent->categories->count() > 0)
  <ul class="sub-menu">
    @php
      $count = 0;
    @endphp
    @foreach ($parent->categories as $category)
      @php
        $service = $category->items->where('brand_id', $parent->id)->first();
      @endphp
      @if (!empty($service) && $count < 5)
        @php
          $count++;
        @endphp
        <li class="menu-item {{$category->children->count()?'menu-item-has-children':''}}">
          <a href="{{route('service.detail', ['brand'=> $parent->slug, 'slug'=>$service->slug])}}">{{$category->title}}</a>
          @if ($category->children->count())
            <ul class="sub-menu">
              @foreach ($category->children as $category_child)
                @php
                  $service_child = $category_child->items->where('brand_id', $parent->id)->first();
                @endphp
                @if (!empty($service_child))
                <li class="menu-item">
                  <a href="{{route('service.detail', ['brand'=> $parent->slug, 'slug'=>$service_child->slug])}}">{{$category_child->title}}</a>
                </li>
                @endif
              @endforeach
            </ul>
          @endif
        </li>
      @endif
    @endforeach
    <li class="menu-item">
      <a href="{{route('service.index', ['brand'=>$parent->slug])}}">其他問題</a>
    </li>
  </ul>
@endif
