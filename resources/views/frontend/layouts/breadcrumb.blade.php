<ol class="breadcrumb" data-wow-delay="0.4s">
  <li><a href="{{route('home')}}" title="{{$setting->name}}">台中當舖日鑫</a></li>
  @foreach ($breadcrumb as $key => $path)
    @if ($key == count($breadcrumb)-1)
      <li class="active" aria-current="page">{{$path[0]}}</li>
    @else
      <li><a title="{{$path[2]}}" href="{{$path[1]}}">{{$path[0]}}</a></li>
    @endif
  @endforeach
</ol>


<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "BreadcrumbList",
  "itemListElement":
  [
    {
      "@type": "ListItem",
      "position": 1,
      "item":
      {
        "@id": "{{route('home')}}",
        "name": "{{$setting->name}}"
      }
    },
    @php
      $last = last($breadcrumb);
    @endphp
    @foreach ($breadcrumb as $key=>$path)
    {
      "@type": "ListItem",
      "position": {{$key+2}},
      "item":
      {
        "@id": "{{$path[1]}}",
        "name": "{{$path[0]}}"
      }
    }
    {{$last!=$path ? ',' : ''}}
    @endforeach
  ]
}
</script>
