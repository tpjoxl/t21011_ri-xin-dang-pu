@component('mail::layout')
  @slot('header')

  @endslot

  <div class="table-inform">
    {{ config('app.name') }} 網站管理員 您好：<br>
    使用者填寫了<b>「立即諮詢」</b>表單，內容如下表：
    <table>
      <tr>
        <th>@lang('validation.attributes.name')</th>
        <td>{{$contact->name}}</td>
      </tr>
      <tr>
        <th>@lang('validation.attributes.sex_title')</th>
        <td>{{!is_null($contact->sex)?$contact->present()->sex_title($contact->sex):''}}</td>
      </tr>
      <tr>
        <th>@lang('validation.attributes.service_category')</th>
        <td>{{$contact->service_category}}</td>
      </tr>
      <tr>
        <th>@lang('validation.attributes.phone')</th>
        <td>{{$contact->phone}}</td>
      </tr>
      <tr>
        <th>@lang('validation.attributes.line_id')</th>
        <td>{{$contact->line_id}}</td>
      </tr>
      <tr>
        <th>@lang('validation.attributes.city')</th>
        <td>{{$contact->city}}</td>
      </tr>
      <tr>
        <th>@lang('validation.attributes.message')</th>
        <td>{!! nl2br($contact->message) !!}</td>
      </tr>
      <tr>
        <th>@lang('validation.attributes.filled_url')</th>
        <td>{{$contact->url}}</td>
      </tr>
    </table>
    <div class="filled-date">
      填寫日期：<span>{{$contact->created_at}}</span>
    </div>
  </div>

  @slot('footer')

  @endslot
@endcomponent
