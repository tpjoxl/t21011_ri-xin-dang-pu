@extends('frontend.layouts.master')

@section('content')
<div class="breadcrumb-area shadow dark bg-fixed text-center text-light" style="background-image: url(assets/img/2440x1578.png);">
  <div class="container">
      <div class="row">
          <div class="col-lg-12 col-md-12">
              <h1>404 找不到此頁面</h1>
              @php
                $breadcrumb = [
                  ['404', '', '404'],
                ];
              @endphp
              @include('frontend.layouts.breadcrumb')
          </div>
      </div>
  </div>
</div>
<div class="error-page-area text-center default-padding">
  <div class="container">
      <div class="row">
          <div class="col-lg-8 col-md-8 col-lg-offset-2 col-md-offset-2">
              <div class="error-box">
                  <h1>404</h1>
                  <h2>Sorry Page Was Not Found!</h2>
                  <p>找不到此頁面</p>
                  <a class="btn btn-theme effect btn-md" href="{{route('home')}}">回首頁</a>
              </div>
          </div>
      </div>
  </div>
</div>
@endsection
