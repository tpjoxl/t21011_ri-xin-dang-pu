<?php echo '<?xml version="1.0" encoding="UTF-8"?>'; ?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    <url>
        <loc>{{ route('home') }}</loc>
        <changefreq>weekly</changefreq>
        <priority>1.0</priority>
    </url>
    @foreach ($posts as $post)
        <url>
            <loc>{{ $post->loc }}</loc>
            @if (!empty($post->lastmod))
            <lastmod>{{ $post->lastmod }}</lastmod>
            @endif
            @if (!empty($post->changefreq))
            <changefreq>{{ $post->changefreq }}</changefreq>
            @endif
            @if (!empty($post->priority))
            <priority>{{ $post->priority }}</priority>
            @endif
        </url>
    @endforeach
</urlset>
