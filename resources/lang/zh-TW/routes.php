<?php

return [
  'blog' => '日鑫文章',
  'about' => '關於日鑫',
  'contact' => '聯絡我們',
  'list' => '列表',
  'category' => '分類',
  'service' => '服務項目',
  'search' => '搜尋',
];

