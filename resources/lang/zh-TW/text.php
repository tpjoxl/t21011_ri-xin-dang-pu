<?php

return [
  'toggle_nav' => '切換選單',
  'home' => '首頁',
  'blog' => '日鑫文章',
  'about' => '關於日鑫',
  'contact' => '聯絡我們',
  'no_data' => '尚無資料',
  'back_home' => '回首頁',
  'back_list' => '回列表頁',
  'reset' => '清除重填',
  'submit' => '確定送出',
  'ok' => '確定',
  'close' => '關閉',
  'all_blog' => '全部文章',
  'search' => '搜尋',
  'search_keyword' => '關鍵字',
  'search_placeholder' => '請輸入關鍵字',
];

