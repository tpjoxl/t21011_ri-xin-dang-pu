<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | such as the size rules. Feel free to tweak each of these messages.
    |
    */

    'accepted'             => '必須接受 :attribute。',
    'active_url'           => ':attribute 並非一個有效的網址。',
    'after'                => ':attribute 必須要晚於 :date。',
    'after_or_equal'       => ':attribute 必須要等於 :date 或更晚',
    'alpha'                => ':attribute 只能以字母組成。',
    'alpha_dash'           => ':attribute 只能以字母、數字及斜線組成。',
    'alpha_num'            => ':attribute 只能以字母及數字組成。',
    'array'                => ':attribute 必須為陣列。',
    'before'               => ':attribute 必須要早於 :date。',
    'before_or_equal'      => ':attribute 必須要等於 :date 或更早。',
    'between'              => [
        'numeric' => ':attribute 必須介於 :min 至 :max 之間。',
        'file'    => ':attribute 必須介於 :min 至 :max kb 之間。 ',
        'string'  => ':attribute 必須介於 :min 至 :max 個字元之間。',
        'array'   => ':attribute: 必須有 :min - :max 個元素。',
    ],
    'boolean'              => ':attribute 必須為bool值。',
    'confirmed'            => ':attribute 確認欄位的輸入不一致。',
    'date'                 => ':attribute 並非一個有效的日期。',
    'date_format'          => ':attribute 不符合 :format 的格式。',
    'different'            => ':attribute 與 :other 必須不同。',
    'digits'               => ':attribute 必須是 :digits 位數字。',
    'digits_between'       => ':attribute 必須介於 :min 至 :max 位數字。',
    'dimensions'           => ':attribute 圖片尺寸不正確。',
    'distinct'             => ':attribute 已經存在。',
    'email'                => ':attribute 必須是有效的電子郵件地址。',
    'emails'                => ':attribute 必須是有效的電子郵件地址。',
    'exists'               => '所選擇的 :attribute 選項無效。',
    'file'                 => ':attribute 必須是一個檔案。',
    'filled'               => ':attribute 不能留空。',
    'image'                => ':attribute 必須是一張圖片。',
    'in'                   => '所選擇的 :attribute 選項無效。',
    'in_array'             => ':attribute 沒有在 :other 中。',
    'integer'              => ':attribute 必須是一個整數。',
    'ip'                   => ':attribute 必須是一個有效的 IP 位址。',
    'json'                 => ':attribute 必須是正確的 JSON 字串。',
    'max'                  => [
        'numeric' => ':attribute 不能大於 :max。',
        'file'    => ':attribute 不能大於 :max kb。',
        'string'  => ':attribute 不能多於 :max 個字元。',
        'array'   => ':attribute 最多有 :max 個元素。',
    ],
    'mimes'                => ':attribute 必須為 :values 的檔案。',
    'mimetypes'            => ':attribute 必須為 :values 的檔案。',
    'min'                  => [
        'numeric' => ':attribute 不能小於 :min。',
        'file'    => ':attribute 不能小於 :min kb。',
        'string'  => ':attribute 不能小於 :min 個字元。',
        'array'   => ':attribute 至少有 :min 個元素。',
    ],
    'not_in'               => '所揀選的 :attribute 選項無效。',
    'numeric'              => ':attribute 必須為一個數字。',
    'present'              => ':attribute 必須存在。',
    'regex'                => ':attribute 的格式錯誤。',
    'required'             => ':attribute 為必填。',
    'required_if'          => '當 :other 是 :value 時 :attribute 為必填。',
    'required_unless'      => '當 :other 不是 :value 時 :attribute 為必填。',
    'required_with'        => '當 :values 出現時 :attribute 為必填。',
    'required_with_all'    => '當 :values 出現時 :attribute 為必填。',
    'required_without'     => '當 :values 留空時 :attribute field 為必填。',
    'required_without_all' => '當 :values 都不出現時 :attribute 為必填。',
    'same'                 => ':attribute 與 :other 必須相同。',
    'size'                 => [
        'numeric' => ':attribute 的大小必須是 :size。',
        'file'    => ':attribute 的大小必須是 :size kb。',
        'string'  => ':attribute 必須是 :size 個字元。',
        'array'   => ':attribute 必須是 :size 個元素。',
    ],
    'string'               => ':attribute 必須是一個字串。',
    'timezone'             => ':attribute 必須是一個正確的時區值。',
    'unique'               => ':attribute 已經存在。',
    'uploaded'             => ':attribute 上傳失敗。',
    'url'                  => ':attribute 的格式錯誤。',
    'captcha'              => ':attribute 輸入錯誤。',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention 'attribute.rule' to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom'               => [
        'g-recaptcha-response' => [
            'required' => '請勾選驗證欄位',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of 'email'. This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [
        'title'                 => '標題',
        'description'           => '描述',
        'name'                  => '姓名',
        'first_name'            => '名',
        'last_name'             => '姓',
        'gender'                => '性別',
        'sex'                   => '稱謂',
        'sex_title'             => '稱謂',
        'email'                 => '信箱',
        'company'               => '公司',
        'phone'                 => '電話',
        'phone2'                => '手機',
        'fax'                   => '傳真',
        'address'               => '地址',
        'business_hours'        => '服務時間',
        'qrcode'                => 'QR CODE',
        'line_qrcode'           => 'LINE QR CODE',
        'status'                => '狀態',
        'home_status'           => '首頁顯示',
        'pic'                   => '圖片',
        'pic_pc'                => '圖片(電腦)',
        'pic_m'                 => '圖片(行動裝置)',
        'list_pic'              => '列表圖片',
        'date_range'            => '上架時間區間',
        'created_range'         => '建立時間區間',
        'created_at'            => '建立時間',
        'tag_title'             => '圖片title屬性',
        'tag_alt'               => '圖片alt屬性',
        'url'                   => '連結網址',
        'date_on'               => '起始時間',
        'date_off'              => '結束時間',
        'slug'                  => 'URL KEY',
        'power_group'           => '權限群組',
        'power_option'          => '權限選項',
        'group_name'            => '權限群組名稱',
        'admin_account'         => '管理者帳號',
        'admin_password'        => '管理者密碼',
        'account'               => '帳號',
        'password'              => '密碼',
        'password_confirmation' => '確認密碼',
        'old_password'          => '舊密碼',
        'new_password'          => '新密碼',
        'passable'              => '直接修改密碼',
        'username'              => '使用者名稱',
        'birthday'              => '生日',
        'idcard'                => '身分證字號',
        'epaper'                => '訂閱電子報',
        'category_title'        => '分類名稱',
        'seo_title'             => '標題(TITLE)',
        'seo_description'       => '描述(DESCRIPTION)',
        'seo_keyword'           => '關鍵字(KEYWORDS)',
        'og_title'              => '社群連結標題(og:title)',
        'og_description'        => '社群連結描述(og:description)',
        'og_image'              => '社群連結縮圖(og:image)',
        'meta_robots'           => 'META(robots)',
        'site_name'             => '網站名稱',
        'site_backend_name'     => '網站後台簡稱',
        'site_mail'             => '客服收件信箱',
        'site_url'              => '網站網址',
        'fb_app'                => 'FB APP ID',
        'head_code'             => 'Head嵌入碼',
        'body_code'             => 'Body嵌入碼',
        'smtp'                  => 'SMTP',
        'smtp_port'             => 'SMTP PORT',
        'smtp_account'          => 'SMTP帳號',
        'smtp_password'         => 'SMTP密碼',
        'smtp_ssl'              => 'SMTP SSL',
        'backupmail'            => '備份資料庫收件信箱',
        'parent_id'             => '所屬階層',
        'category_id'           => '所屬分類',
        'categories'            => '所屬分類',
        'categories.*'          => '所屬分類',
        'login_at'              => '上次登入時間',
        'login_range'           => '登入時間區間',
        'ip'                    => '登入位址',
        'captcha'               => '驗證欄位',
        'text'                  => '內容',
        'note'                  => '備註',
        'set_top'               => '置頂顯示',
        'file'                  => '檔案',
        'file_path'             => '檔案路徑',
        'file_title'            => '檔案名稱',
        'import_file'           => '匯入的檔案',
        'google_map'            => 'Google map網址',
        'blog_tag_title'        => '標籤名稱',
        'blog_tag'              => '關鍵字標籤',
        'blog_recommend'        => '推薦文章',
        'blog_service'          => '關聯的服務項目',
        'question'              => '問題',
        'answer'                => '答覆',
        'message'               => '詢問內容',
        'h1'                    => 'H1 標題',
        'h2'                    => 'H2 標題',
        'banner'                => 'Banner圖片',
        'service_category_id'   => '服務項目',
        'service_area_id'       => '地區',
        'service_id'            => '服務頁面',
        'line_id'               => 'LINE ID',
        'service_category'      => '詢問類型',
        'city'                  => '居住縣市',
        'filled_at'             => '填表時間',
        'filled_url'            => '填表網址',
        'recommends'            => '案例',
    ],

];
