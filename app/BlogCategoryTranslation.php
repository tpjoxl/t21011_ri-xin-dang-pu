<?php

namespace App;

class BlogCategoryTranslation extends Model
{
  public $timestamps = false;
  protected $fillable = [
    'title',
    'slug',
    'h1',
    'h2',
    'description',
    'text',
    'seo_title',
    'seo_description',
    'seo_keyword',
    'og_title',
    'og_description',
  ];
}
