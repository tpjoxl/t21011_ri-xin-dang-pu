<?php

namespace App;

use Dimsav\Translatable\Translatable;

class ServiceFaq extends Model
{
  use Translatable;
  protected $with = ['translations'];
  protected $table = 'service_faq';
  public $translatedAttributes = [
    'question',
    'answer',
  ];
  protected $fillable = [
    'status',
    'question',
    'answer',
    'date_on',
    'date_off',
    'rank',
    'service_category_id',
    'service_id',
  ];

  public function scopeOrdered($query, $rank = 'rank')
  {
    return $query->orderBy($rank, 'asc')
      ->orderBy('created_at', 'asc');
  }
  public function category() {
    return $this->belongsTo(ServiceCategory::class, 'service_category_id');
  }
}
