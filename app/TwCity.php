<?php

namespace App;

class TwCity extends Model
{
  public $timestamps = false;
  protected $table = 'tw_city';

  public function scopeOrdered($query, $rank = 'rank')
  {
    return $query->orderBy($rank, 'asc')
      ->orderBy('id', 'asc');
  }

  public function regions() {
    return $this->hasMany(TwRegion::class, 'city_id');
  }
}
