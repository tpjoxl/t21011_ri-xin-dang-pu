<?php

namespace App;

class Contact extends Model
{
    protected $table = 'contact';
    protected $presenter = 'App\Presenters\ContactPresenter';
    protected $fillable = [
      'name',
      'sex',
      'service_category',
      'phone',
      'line_id',
      'city',
      'message',
      'status',
      'rank',
      'note',
      'device',
      'url',
    ];
}
