<?php

namespace App;

use Dimsav\Translatable\Translatable;

class Blog extends Model
{
  use Translatable;
  protected $with = ['translations', 'categories'];
  protected $table = 'blog';
  protected $appends = ['picPath'];
  protected $setTop = true;
  protected $fillable = [
    'status',
    'home_status',
    'h1',
    'title',
    'slug',
    'url',
    'description',
    'text',
    'date_on',
    'date_off',
    'pic',
    'pic_m',
    'list_pic',
    'seo_title',
    'seo_description',
    'seo_keyword',
    'og_title',
    'og_description',
    'og_image',
    'meta_robots',
    'rank',
    'created_at',
  ];

  public function categories() {
    return $this->belongsToMany(BlogCategory::class, 'blog_category_relation', 'blog_id', 'category_id');
  }
  public function services() {
    return $this->belongsToMany(Service::class, 'service_blog_relation', 'blog_id', 'service_id');
  }
}
