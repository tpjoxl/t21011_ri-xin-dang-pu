<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Foundation\Application;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;

class Language {
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
      if (count(config('translatable.locales'))>1) {
        // Make sure current locale exists.
        $locale = $request->segment(1);

        if (is_null($locale)) {
          $segments = $request->segments();
          $segments[0] = strtolower(config('app.fallback_locale'));
          return redirect()->to(implode('/', $segments));
        }
        
        // $locale_code_arr = explode('-', $locale);
        // if (count($locale_code_arr)>1) {
        //   $locale_code_arr[count($locale_code_arr)-1] = strtoupper($locale_code_arr[count($locale_code_arr)-1]);
        //   $locale_code = implode('-', $locale_code_arr);
        // } else {
        //   $locale_code = $locale;
        // }
        // if (array_key_exists($locale_code, config('translatable.locales'))) {
        //   app()->setLocale($locale_code);
        //
        //   return $next($request);
        // } else {
        //   $segments = $request->segments();
        //   array_unshift($segments, strtolower(config('app.fallback_locale')));
        //   return redirect()->to(implode('/', $segments));
        // }
      }

      return $next($request);
    }

}
