<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\Power;

class AdminPowerCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        $pagePower = Power::where('route_name', 'like' ,'%'.request()->route()->getName().'%')->first();
        $powers = Auth::guard('admin')->user()->group->powers->pluck('id')->toArray();
        // return dd(Auth::guard('admin')->user()->group->powers->pluck('id')->toArray());
        // return dd(request()->route()->getName(), Auth::guard('admin')->user()->getGroup, $pagePowerId);
        if ( Auth::guard('admin')->check() && in_array($pagePower->id, $powers) )
        {
            return $next($request);
        }

        return redirect()->route('backend.home')->withError('無權限使用此功能');
        // return abort(401);
    }
}
