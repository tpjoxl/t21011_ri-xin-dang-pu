<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Lang;
use App\Setting;
use App\Blog;
use App\BlogCategory;
use App\Service;
use Carbon\Carbon;

class BlogController extends Controller
{
  protected $model, $model_table, $cate, $cate_table, $perPage;

  public function __construct(Blog $Blog, BlogCategory $BlogCategory, Service $Service)
  {
    $this->model = $Blog;
    $this->model_table = $Blog->getTable();
    $this->service = $Service;
    $this->cate = $BlogCategory;
    $this->cate_table = $BlogCategory->getTable();
    $this->prefix = 'blog';
    $this->perPage = 6;
  }
  public function index()
  {
    $prefix = $this->prefix;
    $data = Setting::getData($this->prefix);
    $items = $this->model
      ->display()
      ->ordered()
      ->paginate($this->perPage);

    $custom_page_title = (isset($data->seo_title)?$data->seo_title:Lang::get('text.'.$this->prefix));
    $breadcrumb = [
      [__('text.'.$prefix), route($prefix.'.index'), __('text.'.$prefix)],
    ];

    return view('frontend.'.$this->prefix.'.index', compact('prefix', 'data', 'items', 'custom_page_title', 'breadcrumb'));
  }

  public function category($category)
  {
    $prefix = $this->prefix;
    $data = $this->cate
      ->display()
      ->whereTranslation('slug', $category)
      ->firstOrFail();
    $data->h1 = $data->blog_h1;
    $data->h2 = $data->blog_h2;
    $data->pic = $data->blog_pic;
    $data->description = $data->blog_description;
    $data->text = $data->blog_text;
    $data->seo_title = $data->blog_seo_title;
    $data->seo_description = $data->blog_seo_description;
    $data->seo_keyword = $data->blog_seo_keyword;
    $data->og_title = $data->blog_og_title;
    $data->og_description = $data->blog_og_description;
    $data->og_image = $data->blog_og_image;
    $data->meta_robots = $data->blog_meta_robots;
    $items = $data->blogs()
      ->display()
      ->ordered('pivot_rank')
      ->paginate($this->perPage);

    $custom_page_title = ($data->seo_title?$data->seo_title:$data->title).' - '.Lang::get('text.'.$this->prefix);
    $breadcrumb = [
      [__('text.'.$prefix), route($prefix.'.index'), __('text.'.$prefix)],
      [$data->title, route($prefix.'.category', $category), $data->title],
    ];
    return view('frontend.'.$this->prefix.'.index', compact('prefix', 'data', 'items', 'custom_page_title', 'breadcrumb'));
  }
  public function service($service)
  {
    $prefix = $this->prefix;
    $main = Setting::getData($this->prefix);
    $data = $this->service
      ->display()
      ->whereTranslation('slug', $service)
      ->firstOrFail();
    $items = $data->blogs()
      ->display()
      ->ordered()
      ->paginate($this->perPage);

    $custom_page_title = ($data->seo_title?$data->seo_title:$data->title).' - '.Lang::get('text.'.$this->prefix);
    $breadcrumb = [
      [__('text.'.$prefix), route($prefix.'.index'), __('text.'.$prefix)],
      [$data->title, route($prefix.'.service', $service), $data->title],
    ];
    return view('frontend.'.$this->prefix.'.service', compact('prefix', 'main', 'data', 'items', 'custom_page_title', 'breadcrumb'));
  }
  public function detail($slug)
  {
    $prefix = $this->prefix;
    $main = Setting::getData($this->prefix);
    $data = $this->model
      ->display()
      ->with([
        'categories' => function($q) {
          $q->without('children');
        },
        // 'recommends' => function($q) {
        //   $q->display()->ordered();
        // },
      ])
      ->whereTranslation('slug', $slug)
      ->firstOrFail();
    // 確認現在的URL KEY的語系是否正確，不是的話要跳到相對語系的URL KEY
    if (is_null($data->translate()->where('slug', $slug)->where('locale', app()->getLocale())->first())) {
      return redirect()->route($this->prefix.'.detail', $data->translate()->slug);
    }
    $all = $this->model
        ->display()
        ->ordered()
        ->get();
    $currPos = array_search($data->id, $all->pluck('id')->toArray());
    $prev = $all->get($currPos - 1);
    $next = $all->get($currPos + 1);
    $custom_page_title = ($data->seo_title?$data->seo_title:$data->title).' - '.Lang::get('text.'.$this->prefix);
    $breadcrumb = [
      [__('text.'.$prefix), route($prefix.'.index'), __('text.'.$prefix)],
      [$data->title, route($prefix.'.detail', $slug), $data->title],
    ];
    return view('frontend.'.$this->prefix.'.detail', compact('prefix', 'data', 'custom_page_title', 'breadcrumb', 'main', 'prev', 'next'));
  }
  /**
   * 搜尋
   */
  public function search(Request $request)
  {
    if (is_null($request->q)) {
      return redirect()->route($this->prefix.'.index');
    }
    $prefix = $this->prefix;
    $setting = Setting::getData($this->prefix);
    $data = $this->model;
    $list_title = Lang::get('text.search_keyword').'：'.$request->q;
    // 搜尋的字串如果有空格就把它分成兩個搜尋條件
    $searchArr = preg_split("/\s+/", $request->q);
    $data->meta_robots = 'noindex,follow';
    $query = $this->model
      ->display()
      ->where(function ($q) use ($searchArr) {
        $q->whereTranslationLike('title', '%'.$searchArr[0].'%')
        ->orWhereTranslationLike('description', '%'.$searchArr[0].'%')
        ->orWhereTranslationLike('text', '%'.$searchArr[0].'%');
        if (count($searchArr)>1) {
          for ($i=1; $i < count($searchArr); $i++) {
            $q->orWhereTranslationLike('title', '%'.$searchArr[$i].'%')
              ->orWhereTranslationLike('description', '%'.$searchArr[$i].'%')
              ->orWhereTranslationLike('text', '%'.$searchArr[$i].'%');
          }
        }
      });
    $data->banner = $setting->banner;
    $items = $query->ordered()
      ->paginate($this->perPage);
    $custom_page_title = Lang::get('text.search').' - '.Lang::get('text.'.$this->prefix);
    $breadcrumb = [
      [__('text.'.$prefix), route($prefix.'.index'), __('text.'.$prefix)],
    ];
    return view('frontend.'.$this->prefix.'.index', compact('prefix', 'data', 'items', 'custom_page_title', 'list_title', 'breadcrumb'));
  }
  /**
   * 標籤
   */
  public function tag(Request $request)
  {
    $prefix = $this->prefix;
    $list_title = Lang::get('text.tag').'：'.$request->tag;
    $data = $this->tag
      ->whereTranslation('title', $request->tag)
      ->firstOrFail();
    $items = $this->model
      ->whereHas('tags', function($q) use ($data) {
        $q->where('id', $data->id);
      })
      ->display()
      ->ordered()
      ->paginate($this->perPage);
    // dd($data, $items);

    $data->meta_robots = 'noindex,follow';
    $custom_page_title = Lang::get('text.tag').' - '.Lang::get('text.'.$this->prefix);

    return view('frontend.'.$this->prefix.'.index', compact('prefix', 'data', 'items', 'custom_page_title', 'list_title'));
  }
}

