<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Lang;
use App\Setting;
use App\Contact;
use Carbon\Carbon;
use Validator;
use App\Mail\ContactInform;
use ReCaptcha\ReCaptcha;

class ContactController extends Controller
{
  public function __construct(Contact $Contact)
  {
    $this->model = $Contact;
    $this->prefix = 'contact';
  }
  public function index(Request $request)
  {
    $prefix = $this->prefix;
    $data = Setting::getData($this->prefix);
    $contact = $this->model;
    $custom_page_title = (isset($data->seo_title) ? $data->seo_title : Lang::get('text.' . $this->prefix));
    $breadcrumb = [
      [__('text.' . $prefix), route($prefix . '.index'), __('text.' . $prefix)],
    ];
    return view('frontend.' . $this->prefix . '.index', compact('data', 'custom_page_title', 'prefix', 'contact', 'breadcrumb'));
  }
  /**
   * 送出表單
   */
  public function formSubmit(Request $request)
  {
    $validator = Validator::make($request->all(), [
      'name' => 'required',
      'sex' => 'required',
      'service_category' => 'required',
      'phone' => 'required|regex:/\d$/',
      'line_id' => 'nullable',
      'message' => 'required',
      'g-recaptcha-response' => 'required',
    ], [], []);
    if ($validator->fails()) {
      return back()->withErrors($validator)->withInput();
    }
    // dd($request->all());

    $gRecaptchaResponse = $request->input('g-recaptcha-response');
    $remoteIp = \Request::ip();
    $secret = env('RE_CAP_SECRET');

    $recaptcha = new ReCaptcha($secret);
    $resp = $recaptcha->verify($gRecaptchaResponse, $remoteIp);
    if ($resp->isSuccess()) {
      // 儲存表單內容
      $dataSave = $request->except('g-recaptcha-response');

      $data = new Contact();
      $data->fill($dataSave);
      // dd($data);

      if ($data->save()) {
        $setting = Setting::getData('site');
        if (array_key_exists('site_mail', $setting) && !empty($setting->site_mail)) {
          $emails = explode(',', preg_replace('/\s(?=)/', '', $setting->site_mail));
          $bccEmails = array_slice($emails, 1);
          \Mail::to($emails[0])->bcc($bccEmails)->send(new ContactInform($data));

          if (\Mail::failures()) {
            return redirect()->back()->with('error', '送出失敗')->withInput();
          } else {
            return redirect()->back()->with('success', '送出成功')->with('successText', '感謝您的填寫，我們會盡速與您聯絡。');
          }
        }
        return redirect()->back()->with('success', '送出成功')->with('successText', '感謝您的填寫，我們會盡速與您聯絡。');
      } else {
        return redirect()->back()->with('error', '送出失敗')->withInput();
      }
    } else {
      $errors = $resp->getErrorCodes();
      return redirect()->back()->withInput()->withErrors($errors);
    }
  }
  /**
   * 表單送出結果頁
   */
  public function result()
  {
    $prefix = $this->prefix;
    $custom_page_title = Lang::get('text.' . $this->prefix);
    $breadcrumb = [
      [__('text.' . $prefix), route($prefix . '.result'), __('text.' . $prefix)],
    ];

    return view('frontend.' . $this->prefix . '.result', compact('custom_page_title', 'prefix', 'breadcrumb'));
    // if(\Session::has('success') || \Session::has('error')) {
    //   return view('frontend.'.$this->prefix.'.result');
    // } else {
    //   return redirect()->route($this->prefix.'.index');
    // }
  }
}
