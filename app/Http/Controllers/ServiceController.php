<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Lang;
use App\ServiceCategory;
use App\Service;
use Carbon\Carbon;
use Validator;
use App\Setting;
use Session;
use Route;

class ServiceController extends Controller
{
  protected $model, $model_table;

  public function __construct(Service $Service, ServiceCategory $ServiceCategory)
  {
    $this->model = $Service;
    $this->cate = $ServiceCategory;
    $this->prefix = 'service';
  }
  public function index($slug)
  {
    $prefix = $this->prefix;
    $data = $this->cate
      ->with([
        'faqs' => function($q) {
          $q->display()->ordered();
        },
        'recommends' => function($q) {
          $q->display()->ordered();
        },
        'blog_category' => function($q) {
          $q->display();
        },
        'blogs' => function($q) {
          $q->display()->ordered();
        },
      ])
      ->display()
      ->whereTranslation('slug', $slug)
      ->firstOrFail();

    $custom_page_title = ($data->seo_title?$data->seo_title:$data->title);
    $breadcrumb = [
      [$data->title, route($prefix.'.index', $slug), $data->title],
    ];
    return view('frontend.'.$this->prefix.'.index', compact('prefix', 'breadcrumb', 'data', 'custom_page_title'));
  }

  public function detail($category, $slug)
  {
    $prefix = $this->prefix;
    $data = $this->model
      ->display()
      ->with([
        'recommends' => function($q) {
          $q->display()->ordered();
        },
        'blogs' => function($q) {
          $q->display()->ordered();
        },
        'category' => function($q) {
          $q->display()->ordered();
        },
      ])
      ->whereHas('category', function ($q) use ($category) {
        $q->whereTranslation('slug', $category);
      })
      ->whereTranslation('slug', $slug)
      ->firstOrFail();

    $custom_page_title = ($data->seo_title?$data->seo_title:$data->title).' - '.$data->category->title;
    $breadcrumb = [
      [$data->category->title, route($prefix.'.index', $category), $data->category->title],
      [$data->title, route($prefix.'.detail', ['category'=>$category, 'slug'=>$slug]), $data->title],
    ];
    return view('frontend.'.$this->prefix.'.detail', compact('prefix', 'breadcrumb', 'data', 'custom_page_title'));
  }
}