<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Lang;
use App\Setting;
use Carbon\Carbon;

class AboutController extends Controller
{
  public function __construct()
  {
    $this->prefix = 'about';
  }
  public function index(Request $request)
  {
    $prefix = $this->prefix;
    $data = Setting::getData($this->prefix);
    $custom_page_title = (isset($data->seo_title)?$data->seo_title:Lang::get('text.'.$this->prefix));
    $breadcrumb = [
      [__('text.'.$prefix), route($prefix.'.index'), __('text.'.$prefix)],
    ];
    return view('frontend.'.$this->prefix.'.index', compact('data', 'custom_page_title', 'prefix', 'breadcrumb'));
  }
}
