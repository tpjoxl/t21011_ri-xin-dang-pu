<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Lang;
use App\ServiceCategory;
use App\Service;
use App\ServiceCase;
use Carbon\Carbon;
use Validator;
use App\Setting;
use Session;
use Route;

class ServiceCaseController extends Controller
{
  protected $model, $model_table;

  public function __construct(ServiceCase $ServiceCase, Service $Service, ServiceCategory $ServiceCategory)
  {
    $this->model = $ServiceCase;
    $this->cate = $ServiceCategory;
    $this->service = $Service;
    $this->prefix = 'service.case';
    $this->perPage = 12;
  }
  public function index($category, $service)
  {
    $prefix = $this->prefix;
    $data = $this->service
      ->display()
      ->whereHas('category', function ($q) use ($category) {
        $q->whereTranslation('slug', $category);
      })
      ->whereTranslation('slug', $service)
      ->firstOrFail();
    $items = $data->cases()
      ->with([
        'category',
        'service',
      ])
      ->display()
      ->ordered()
      ->paginate($this->perPage);

    $custom_page_title = $data->title.'案例 - '.($data->seo_title?$data->seo_title:$data->title).' - '.$data->category->title;
    $breadcrumb = [
      [$data->category->title, route('service.index', $category), $data->category->title],
      [$data->title, route('service.detail', ['category'=>$category, 'slug'=>$service]), $data->title],
      [$data->title.'案例', route($prefix.'.index', ['category'=>$category, 'service'=>$service]), $data->title.'案例'],
    ];
    return view('frontend.'.$this->prefix.'.index', compact('prefix', 'breadcrumb', 'data', 'items', 'custom_page_title'));
  }

  public function category($category)
  {
    $prefix = $this->prefix;
    $data = $this->cate
      ->display()
      ->whereTranslation('slug', $category)
      ->firstOrFail();

    $items = $data->cases()
      ->with([
        'category',
        'service',
      ])
      ->display()
      ->ordered()
      ->paginate($this->perPage);

    $custom_page_title = $data->title.'案例 - '.$data->title;
    $breadcrumb = [
      [$data->title, route('service.index', $category), $data->title],
      [$data->title.'案例', route($prefix.'.category', ['category'=>$category]), $data->title.'案例'],
    ];
    return view('frontend.'.$this->prefix.'.index', compact('prefix', 'breadcrumb', 'data', 'items', 'custom_page_title'));
  }
  public function detail($category, $service, $slug)
  {
    $prefix = $this->prefix;
    $data = $this->model
      ->display()
      ->with([
        'category' => function($q) {
          $q->with([
            'cases' => function($q) {
              $q->with(['category', 'service'])->display()->ordered();
            },
          ]);
        },
        'service' => function($q) {
          $q->with([
            'cases' => function($q) {
              $q->with(['category', 'service'])->display()->ordered();
            },
          ]);
        },
      ])
      ->whereHas('category', function ($q) use ($category) {
        $q->whereTranslation('slug', $category);
      })
      ->whereHas('service', function ($q) use ($service) {
        $q->whereTranslation('slug', $service);
      })
      ->whereTranslation('slug', $slug)
      ->firstOrFail();

    $custom_page_title = ($data->seo_title?$data->seo_title:$data->title).' - '.$data->category->title;
    $breadcrumb = [
      [$data->category->title, route('service.index', $category), $data->category->title],
      [$data->service->title, route('service.detail', ['category'=>$category, 'slug'=>$service]), $data->service->title],
      [$data->service->title.'案例', route($prefix.'.index', ['category'=>$category, 'service'=>$service]), $data->service->title.'案例'],
      [$data->title, route($prefix.'.detail', ['category'=>$category, 'service'=>$service, 'slug'=>$slug]), $data->title],
    ];
    return view('frontend.'.$this->prefix.'.detail', compact('prefix', 'breadcrumb', 'data', 'custom_page_title'));
  }
  public function detail2($category, $slug)
  {
    $prefix = $this->prefix;
    $data = $this->model
      ->display()
      ->with([
        'category' => function($q) {
          $q->with([
            'cases' => function($q) {
              $q->with(['category', 'service'])->display()->ordered();
            },
          ]);
        },
        'service' => function($q) {
          $q->with([
            'cases' => function($q) {
              $q->with(['category', 'service'])->display()->ordered();
            },
          ]);
        },
      ])
      ->whereHas('category', function ($q) use ($category) {
        $q->whereTranslation('slug', $category);
      })
      ->whereTranslation('slug', $slug)
      ->firstOrFail();
    if (!is_null($data->service_id)) {
      return redirect()->route($prefix.'.detail', ['category'=>$category, 'service'=>$data->service->slug, 'slug'=>$slug]);
    }

    $custom_page_title = ($data->seo_title?$data->seo_title:$data->title).' - '.$data->category->title;
    $breadcrumb = [
      [$data->category->title, route('service.index', $category), $data->category->title],
      [$data->category->title.'案例', route($prefix.'.category', ['category'=>$category]), $data->category->title.'案例'],
      [$data->title, route($prefix.'.detail2', ['category'=>$category, 'slug'=>$slug]), $data->title],
    ];
    return view('frontend.'.$this->prefix.'.detail', compact('prefix', 'breadcrumb', 'data', 'custom_page_title'));
  }
}