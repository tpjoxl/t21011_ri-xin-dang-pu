<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Banner;
use App\Blog;
use App\ServiceFaq;
use Carbon\Carbon;
use DB;

class HomeController extends Controller
{
  public function index(Request $request)
  {
    $banners = Banner::display()
      ->ordered()
      ->get();
    $blogs = Blog::where('home_status', 1)
      ->display()
      ->orderBy('created_at', 'desc')
      ->limit(7)
      ->get();
    $bloggs =  $blogs->slice(3);
    $faqs = ServiceFaq::where('home_status', 1)->display()->ordered()->limit(8)->get();

    // dd($articles);
    return view('frontend.home', compact('banners', 'blogs', 'bloggs', 'faqs'));
  }
}
