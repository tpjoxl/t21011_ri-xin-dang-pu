<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Lang;
use App\Setting;
use App\Faq;
use App\Course;
use App\BlogCategory;
use App\Blog;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;

class GeneratedController extends Controller
{
  protected $tags;

  public function __construct()
  {
  }
  public function siteMap()
  {
      $view = Cache()->remember('generated.sitemap', 0, function () {

          $posts = \App\Blog::display()->orderBy('updated_at', 'desc')->first();
          $this->addTag(route('blog.index'), (!is_null($posts))?$posts->updated_at:Carbon::now(), 'daily', '0.5');

          $posts = \App\BlogCategory::display()->orderBy('updated_at', 'desc')->get();
          if ($posts->count()>0) {
            foreach ($posts as $post) {
              $this->addTag(route('blog.category', ['category'=>$post->slug]), $post->updated_at, 'daily', '0.5');
            }
          }

          $posts = \App\Blog::display()->ordered()->get();
          if ($posts->count()>0) {
            foreach ($posts as $post) {
              if (is_null($post->url)) {
                $this->addTag(route('blog.detail', $post->slug), $post->updated_at, 'daily', '0.8');
              }
            }
          }

          $post = Setting::getData('about');
          $this->addTag(route('about.index'), $post->updated_at, 'yearly', '0.3');

          $post = Setting::getData('contact');
          $this->addTag(route('contact.index'), $post->updated_at, 'yearly', '0.3');

          $posts = \App\ServiceCategory::with([
              'services'=>function($q) {
                $q->with([
                  'cases' => function($q) {
                    $q->display()->ordered();
                  },
                ])->display()->ordered();
              },
              'cases' => function($q) {
                $q->display()->ordered();
              },
            ])->display()->ordered()->get();
          foreach ($posts as $post) {
            if (is_null($post->url)) {
              $this->addTag(route('service.index', ['slug'=>$post->slug]), $post->updated_at, 'weekly', '0.64');
            }
            foreach ($post->services as $service) {
              $this->addTag(route('service.detail', ['category'=>$post->slug, 'slug'=>$service->slug]), $service->updated_at, 'weekly', '0.8');
            }
          }

          $posts = $this->tags;

          // return generated xml (string) , cache whole file
          return view('generated.sitemap', compact('posts'))->render();
      });
      return response($view)->header('Content-Type', 'text/xml');
  }

  /**
   * 標準條件式
   * @param object $obj
   * @param array $mode
   * @param bool $orderDefault
   */

  public function condition($obj, $mode, $orderDefault='')
  {
    if (in_array('categories',$mode)) {
      $obj->whereHas('categories', function ($query) {
        $query->where('status', 1);
      });
    }

    if (in_array('status',$mode)) {
      $obj->where('status', 1);
    }

    if (in_array('date_on',$mode)) {
      $obj->where(function ($query) {
          $query->whereNull('date_on')
                ->orWhere('date_on', '<=', Carbon::today()->toDateString());
      })
      ->where(function ($query) {
          $query->whereNull('date_off')
                ->orWhere('date_off', '>=', Carbon::today()->toDateString());
      });
    }

    if ($orderDefault) {
      $obj = $this->orderDefault($obj);
    }

    return $obj;
  }

  /**
   * 預設排序
   * @param object $obj
   */
  public function orderDefault($obj)
  {
    $obj->orderBy('rank', 'desc')
        ->orderBy('created_at', 'desc');

    return $obj;
  }

  /**
   * 加入標籤
   * @param string $loc
   * @param bool $lastmod
   * @param string $changefreq
   * @param float $priority
   */
  public function addTag($loc, $lastmod=fales, $changefreq='yearly', $priority=0.5)
  {
    $post = new static();
    $post->loc = urldecode($loc);
    if($lastmod){
      $post->lastmod = (new Carbon($lastmod))->tz('UTC')->toAtomString();
    }
    $post->changefreq = $changefreq;
    $post->priority = $priority;
    $this->tags[] = $post;
  }
}