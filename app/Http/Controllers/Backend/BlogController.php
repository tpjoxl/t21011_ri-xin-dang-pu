<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Lang;
use App\BlogCategory;
use App\Blog;
use App\Service;
use Validator;
use Illuminate\Validation\Rule;

class BlogController extends BasicController
{
  public function __construct(Blog $Blog, BlogCategory $BlogCategory, Service $Service)
  {
    parent::__construct($Blog, $BlogCategory);
    $this->prefix = 'blog';
    $this->rank_all = true;
    $this->service = $Service;
    $this->relationItems = 'blogs';

    $this->valid_attrs = [
      'services' => Lang::get('validation.attributes.'.$this->prefix.'_service', [], env('BACKEND_LOCALE')),
      'recommends' => Lang::get('validation.attributes.'.$this->prefix.'_recommend', [], env('BACKEND_LOCALE')),
    ];

    if (method_exists($this->model, 'categories')) {
      $this->valid_rules = $this->arrayInsertAfter($this->valid_rules, [
        'categories' => 'required|array',
        'categories.*' => 'required',
      ]);
      $this->valid_attrs = $this->arrayInsertAfter($this->valid_attrs, [
        'categories.*' => Lang::get('validation.attributes.categories', [], env('BACKEND_LOCALE')),
      ]);
    }
  }
  protected function getEditValidRules()
  {
    $rules = $this->getValidRules();
    $rules['created_at'] = 'required';
    return $rules;
  }
  protected function getFormFields()
  {
    // $recommends = $this->model->ordered()->get();
    $services = $this->service->ordered()->get();
    $fields = [
      'services' => [
        'type' => 'multi_select',
        'name' => 'services',
        'required' => false,
        'options' => $services,
        'live_search' => true,
      ],
      'status' => [
        'type' => 'radio',
        'name' => 'status',
        'required' => true,
        'options' => $this->model->present()->status(),
        'default' => 1,
      ],
      'home_status' => [
        'type' => 'radio',
        'name' => 'home_status',
        'required' => false,
        'options' => $this->model->present()->status(),
        'default' => 1,
      ],
      'set_top' => [
        'type' => 'set_top',
        'name' => 'rank',
        'required' => false,
      ],
      'title' => [
        'type' => 'text_input',
        'name' => 'title',
        'required' => true,
      ],
      'slug' => [
        'type' => 'text_input',
        'name' => 'slug',
        'required' => false,
      ],
      'url' => [
        'type' => 'text_input',
        'name' => 'url',
        'required' => false,
      ],
      'description' => [
        'type' => 'text_area',
        'name' => 'description',
        'required' => false,
        'rows' => 3,
      ],
      'text' => [
        'type' => 'text_editor',
        'name' => 'text',
        'required' => false,
      ],
      'date_range' => [
        'type' => 'date_range',
        'name' => ['date_on', 'date_off'],
        'placeholder' => [Lang::get('validation.attributes.date_on', [], env('BACKEND_LOCALE')), Lang::get('validation.attributes.date_off', [], env('BACKEND_LOCALE'))],
        'required' => false,
      ],
      // 'recommends' => [
      //   'type' => 'multi_select',
      //   'name' => 'recommends',
      //   'required' => false,
      //   'options' => $recommends,
      //   'live_search' => true,
      // ],
      // 'list_pic' => [
      //   'type' => 'img',
      //   'name' => 'list_pic',
      //   'required' => false,
      //   'w' => null,
      //   'h' => null,
      //   'folder' => $this->table
      // ],
      'pic' => [
        'type' => 'img',
        'name' => 'pic',
        'required' => false,
        'w' => 800,
        'h' => 600,
        'folder' => $this->table
      ],
      'seo_title' => [
        'type' => 'text_input',
        'name' => 'seo_title',
        'required' => false,
      ],
      'seo_description' => [
        'type' => 'text_area',
        'name' => 'seo_description',
        'required' => false,
        'rows' => 3,
      ],
      'seo_keyword' => [
        'type' => 'text_input',
        'name' => 'seo_keyword',
        'required' => false,
      ],
      'og_title' => [
        'type' => 'text_input',
        'name' => 'og_title',
        'required' => false,
      ],
      'og_description' => [
        'type' => 'text_area',
        'name' => 'og_description',
        'required' => false,
        'rows' => 3,
      ],
      'og_image' => [
        'type' => 'img',
        'name' => 'og_image',
        'required' => false,
        'w' => null,
        'h' => null,
        'folder' => $this->table
      ],
      'meta_robots' => [
        'type' => 'text_input',
        'name' => 'meta_robots',
        'required' => false,
        'default' => 'index, follow',
      ],
    ];
    if (method_exists($this->model, 'categories')) {
      $fields = $this->arrayInsertAfter($fields, [
        'categories' => [
          'type' => 'multi_select',
          'name' => 'categories',
          'required' => true,
          'options' => $this->category->getTree(),
          'add_first' => [
            'url' => route('backend.service.category.create'),
            'title' => Lang::get('backend.category', [], env('BACKEND_LOCALE')),
          ]
        ]
      ]);
      if ($this->category->getLimitLevel()==1) {
        $fields['categories']['max'] = 1;
      }
    }
    return $fields;
  }
  protected function getEditFormFields()
  {
    $fields = $this->getFormFields();
    $fields = $this->arrayInsertAfter($fields, [
      'created_at' => [
        'type' => 'text_input',
        'name' => 'created_at',
        'required' => true,
        'hint' => Lang::get('backend.created_at_hint'),
      ]
    ], 'text');
    return $fields;
  }
  protected function getSearchFields()
  {
    $fields = [
      'status' => [
        'type' => 'radio',
        'name' => 'status',
        'options' => $this->model->present()->status(),
        'search' => 'equal',
      ],
      'title' => [
        'type' => 'text_input',
        'name' => 'title',
        'search' => 'like',
      ],
      'date_range' => [
        'type' => 'date_range',
        'name' => ['date_on', 'date_off'],
        'placeholder' => [Lang::get('validation.attributes.date_on', [], env('BACKEND_LOCALE')), Lang::get('validation.attributes.date_off', [], env('BACKEND_LOCALE'))],
        'search' => 'range',
        'compare_fields' => ['date_on', 'date_off']
      ],
    ];
    if (method_exists($this->model, 'categories')) {
      if ($this->rank_all) {
        $fields = $this->arrayInsertAfter($fields, [
          'categories' => [
            'type' => 'select',
            'name' => 'categories',
            'options' => $this->category->getTree(),
            'search' => 'category',
          ],
        ], 'status');
      } else {
        $fields = $this->arrayInsertAfter($fields, [
          'categories' => [
            'type' => 'hidden',
            'name' => 'categories',
            'search' => 'category',
          ],
        ]);
      }
    }
    return $fields;
  }
  /**
   * 產生列表資料結構
   */
  public function generateListData($datas)
  {
    $list_datas = [];
    foreach ($datas as $data) {
      $list_datas[$data->id] = [
        'title' => [
          'align' => 'left',
          'type' => 'link',
          'data' => $data->title,
          'url' => route('backend.'.$this->prefix.'.edit', array_merge(request()->all(), ['id' => $data->id])),
        ],
        'date_range' => [
          'align' => 'left',
          'type' => 'text',
          'data' => ($data->date_on || $data->date_off)?$data->date_on.'～'.$data->date_off:'',
        ],
        'set_top' => [
          'align' => 'center',
          'type' => 'html',
          'data' => $data->present()->checkSetTop(),
        ],
        'status' => [
          'align' => 'center',
          'type' => 'html',
          'options' => $this->model->present()->status(),
          'data' => $this->model->present()->status($data->status),
        ],
      ];
      if (method_exists($this->model, 'categories')) {
        $list_datas[$data->id] = $this->arrayInsertAfter($list_datas[$data->id], [
          'categories' => [
            'align' => 'left',
            'type' => 'text',
            'data' => $data->categories->implode('title', ', '),
          ],
        ], 'title');
      }
      if (auth()->guard('admin')->user()->group->checkPowerByName($this->prefix.'.edit')) {
        $list_datas[$data->id]['edit'] = [
          'label' => Lang::get('backend.edit', [], env('BACKEND_LOCALE')),
          'align' => 'center',
          'type' => 'btn',
          'data' => '<span class="glyphicon glyphicon-pencil"></span>',
          'url' => route('backend.'.$this->prefix.'.edit', array_merge(request()->all(), ['id' => $data->id])),
        ];
      }
      if (auth()->guard('admin')->user()->group->checkPowerByName($this->prefix.'.destroy')) {
        $list_datas[$data->id]['delete'] = [
          'label' => Lang::get('backend.delete', [], env('BACKEND_LOCALE')),
          'align' => 'center',
          'type' => 'btn',
          'data' => '<span class="glyphicon glyphicon-trash"></span>',
          'url' => route('backend.'.$this->prefix.'.destroy', ['id' => $data->id]),
          'class' => 'btn-default btn-delete-row',
        ];
      }
    }
    return $list_datas;
  }
  /**
   * 顯示修改表單
   */
  public function edit($id)
  {
    $rank_all = $this->rank_all;
    $data = $this->model->find($id);
    if (is_null($data)) {
      return redirect()->route('backend.'.$this->prefix.'.index')->with('error', Lang::get('backend.data_not_found', [], env('BACKEND_LOCALE')));
    }
    if (!is_null($this->category)) {
      $categories = $this->category->getTree();
      $category = $this->category;
    }
    $prefix = $this->prefix;
    $form_fields = $this->getEditFormFields();
    // $form_fields['recommends']['ignore'] = $id;
    $valid_attrs = $this->valid_attrs;

    $view = 'backend.'.$this->prefix.'.edit';
    if (!view()->exists($view)) {
      $view = 'backend.common.edit';
    }
    return view($view, compact('category', 'categories', 'data', 'prefix', 'rank_all', 'form_fields', 'valid_attrs'));
  }
}
