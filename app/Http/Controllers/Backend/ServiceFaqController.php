<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Lang;
use App\ServiceCategory;
use App\ServiceFaq;
use Validator;
use Illuminate\Validation\Rule;

class ServiceFaqController extends BasicController
{
  public function __construct(ServiceFaq $ServiceFaq, ServiceCategory $ServiceCategory)
  {
    parent::__construct($ServiceFaq, $ServiceCategory);
    $this->prefix = 'service.faq';
    $this->relationItems = 'faqs';
    $this->relationCategory = 'category';
    $this->relationCategoryKey = 'service_category_id';
    $this->rank_all = false;
  }
  /**
   * 表單驗證的規則
   */
  protected function getValidRules()
  {
    $rules = [
      'question' => ['required', Rule::unique($this->table)],
      'answer' => 'required',
    ];
    return $rules;
  }
  /**
   * 表單欄位
   */
  protected function getFormFields()
  {
    $fields = [
      'status' => [
        'type' => 'radio',
        'name' => 'status',
        'required' => true,
        'options' => $this->model->present()->status(),
        'default' => 1,
      ],
      'service_category_id' => [
        'type' => 'hidden',
        'name' => 'service_category_id',
        'required' => true,
        'options' => $this->category->getTree(),
        'live_search' => true,
        'default' => request()->service_category_id,
      ],
      'question' => [
        'type' => 'text_input',
        'name' => 'question',
        'required' => true,
      ],
      'answer' => [
        'type' => 'text_editor',
        'name' => 'answer',
        'required' => true,
      ],
    ];
    return $fields;
  }
  /**
   * 搜尋欄位
   */
  protected function getSearchFields()
  {
    $fields = [
      'service_category_id' => [
        'type' => 'hidden',
        'name' => 'service_category_id',
        'default' => request()->service_category_id,
        'search' => 'equal',
      ],
      'status' => [
        'type' => 'radio',
        'name' => 'status',
        'options' => $this->model->present()->status(),
        'search' => 'equal',
      ],
      'question' => [
        'type' => 'text_input',
        'name' => 'question',
        'search' => 'like',
      ],
      'created_range' => [
        'type' => 'date_range',
        'name' => ['created_at1', 'created_at2'],
        'placeholder' => [Lang::get('validation.attributes.date_on'), Lang::get('validation.attributes.date_off')],
        'search' => 'range',
        'compare_fields' => ['created_at', 'created_at']
      ],
    ];
    return $fields;
  }
  /**
   * 產生列表資料結構
   */
  public function generateListData($datas)
  {
    $list_datas = [];
    foreach ($datas as $data) {
      $list_datas[$data->id] = [
        'question' => [
          'align' => 'left',
          'type' => 'link',
          'data' => $data->question,
          'url' => route('backend.'.$this->prefix.'.edit', ['id' => $data->id]),
        ],
        'created_at' => [
          'align' => 'left',
          'type' => 'text',
          'data' => $data->created_at,
        ],
        'status' => [
          'align' => 'center',
          'type' => 'html',
          'options' => $this->model->present()->status(),
          'data' => $this->model->present()->status($data->status),
        ],
      ];
      if (method_exists($this->model, 'categories')) {
        $list_datas[$data->id] = $this->arrayInsertAfter($list_datas[$data->id], [
          'categories' => [
            'align' => 'left',
            'type' => 'text',
            'data' => $data->categories->implode('title', ', '),
          ],
        ], 'question');
      }
      $list_datas[$data->id]['edit'] = [
        'label' => Lang::get('backend.edit'),
        'align' => 'center',
        'type' => 'btn',
        'data' => '<span class="glyphicon glyphicon-pencil"></span>',
        'url' => route('backend.'.$this->prefix.'.edit', ['id' => $data->id]),
      ];
      $list_datas[$data->id]['delete'] = [
        'label' => Lang::get('backend.delete'),
        'align' => 'center',
        'type' => 'btn',
        'data' => '<span class="glyphicon glyphicon-trash"></span>',
        'url' => route('backend.'.$this->prefix.'.destroy', ['id' => $data->id]),
        'class' => 'btn-default btn-delete-row',
      ];
    }
    return $list_datas;
  }

  /**
   * 排序頁顯示的欄位資料
   */
  protected function getRankFields() {
    $fields = [
      'question' => [
        'type' => 'text',
        'name' => 'question',
      ],
    ];
    return $fields;
  }
}
