<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Lang;
use App\ServiceCategory;
use App\ServiceArea;
use App\Service;
use Validator;
use Illuminate\Validation\Rule;

class ServiceController extends BasicController
{
  public function __construct(Service $Service, ServiceCategory $ServiceCategory, ServiceArea $ServiceArea)
  {
    parent::__construct($Service, $ServiceCategory);
    $this->prefix = 'service';
    $this->area = $ServiceArea;
    $this->rank_all = true;
    $this->valid_msgs = [
      'service_category_id.unique' => '此:attribute 與 地區 的頁面已經存在。',
    ];
    $this->select_areas = $this->area->ordered()->get();
    $this->select_categories = $this->category->ordered()->get();
  }
  protected function getValidRules()
  {
    $rules = [
      'title' => ['required', Rule::unique($this->table)],
      'slug' => ['nullable', Rule::unique($this->table)],
      'service_area_id' => 'nullable',
      'service_category_id' => ['required', Rule::unique($this->table)->where(function ($query) {
        return $query
          ->where('service_category_id', request()->service_category_id)
          ->where('service_area_id', request()->service_area_id);
      })],
    ];
    return $rules;
  }
  protected function getFormFields()
  {
    $fields = [
      'status' => [
        'type' => 'radio',
        'name' => 'status',
        'required' => true,
        'options' => $this->model->present()->status(),
        'default' => 1,
      ],
      'title' => [
        'type' => 'text_input',
        'name' => 'title',
        'required' => true,
      ],
      'slug' => [
        'type' => 'text_input',
        'name' => 'slug',
        'required' => false,
      ],
      'h1' => [
        'type' => 'text_input',
        'name' => 'h1',
        'required' => false,
      ],
      'service_area_id' => [
        'type' => 'select',
        'name' => 'service_area_id',
        'required' => false,
        'options' => $this->select_areas,
        'live_search' => true,
      ],
      'service_category_id' => [
        'type' => 'select',
        'name' => 'service_category_id',
        'required' => true,
        'options' => $this->select_categories,
        'live_search' => true,
      ],
      'text' => [
        'type' => 'text_editor',
        'name' => 'text',
        'required' => false,
      ],
      'date_range' => [
        'type' => 'date_range',
        'name' => ['date_on', 'date_off'],
        'placeholder' => [Lang::get('validation.attributes.date_on'), Lang::get('validation.attributes.date_off')],
        'required' => false,
      ],
      'banner' => [
        'type' => 'img',
        'name' => 'banner',
        'required' => false,
        'w' => 1920,
        'h' => 300,
        'folder' => $this->table
      ],
      'recommends' => [
        'type' => 'multi_select',
        'name' => 'recommends',
        'required' => false,
        'options' => $this->model->recommends()->getRelated()->when($this->data->id && $this->data->category, function ($q) {
          $q->whereHas('categories', function ($q) {
            $q->where('id', $this->data->category->id);
          });
        })->ordered()->get()->transform(function ($item, $key) {
          $item->title = ($item->categories->count() ? '[' . $item->categories->first()->title . '] ' : '') . $item->title;
          return $item;
        }),
        'live_search' => true,
        'actions' => true,
      ],
      'seo_title' => [
        'type' => 'text_input',
        'name' => 'seo_title',
        'required' => false,
      ],
      'seo_description' => [
        'type' => 'text_area',
        'name' => 'seo_description',
        'required' => false,
        'rows' => 3,
      ],
      'seo_keyword' => [
        'type' => 'text_input',
        'name' => 'seo_keyword',
        'required' => false,
      ],
      'og_title' => [
        'type' => 'text_input',
        'name' => 'og_title',
        'required' => false,
      ],
      'og_description' => [
        'type' => 'text_area',
        'name' => 'og_description',
        'required' => false,
        'rows' => 3,
      ],
      'og_image' => [
        'type' => 'img',
        'name' => 'og_image',
        'required' => false,
        'w' => null,
        'h' => null,
        'folder' => $this->table
      ],
      'meta_robots' => [
        'type' => 'text_input',
        'name' => 'meta_robots',
        'required' => false,
        'default' => 'index, follow',
      ],
    ];
    if (method_exists($this->model, 'categories')) {
      $fields = $this->arrayInsertAfter($fields, [
        'categories' => [
          'type' => 'multi_select',
          'name' => 'categories',
          'required' => true,
          'options' => $this->category->getTree(),
          'add_first' => [
            'url' => route('backend.' . $this->model->getTable() . '.category.create'),
            'title' => Lang::get('backend.category'),
          ]
        ]
      ]);
      if ($this->category->getLimitLevel() == 1) {
        $fields['categories']['max'] = 1;
      }
    }
    return $fields;
  }
  /**
   * 搜尋欄位
   */
  protected function getSearchFields()
  {
    $fields = [
      'status' => [
        'type' => 'radio',
        'name' => 'status',
        'options' => $this->model->present()->status(),
        'search' => 'equal',
      ],
      'title' => [
        'type' => 'text_input',
        'name' => 'title',
        'search' => 'like',
      ],
      'service_area_id' => [
        'type' => 'select',
        'name' => 'service_area_id',
        'options' => $this->select_areas,
        'search' => 'equal',
      ],
      'service_category_id' => [
        'type' => 'select',
        'name' => 'service_category_id',
        'options' => $this->select_categories,
        'search' => 'equal',
      ],
      'date_range' => [
        'type' => 'date_range',
        'name' => ['date_on', 'date_off'],
        'placeholder' => [Lang::get('validation.attributes.date_on'), Lang::get('validation.attributes.date_off')],
        'search' => 'range',
        'compare_fields' => ['date_on', 'date_off']
      ],
    ];
    if (method_exists($this->model, 'categories')) {
      if ($this->rank_all) {
        $fields = $this->arrayInsertAfter($fields, [
          'categories' => [
            'type' => 'select',
            'name' => 'categories',
            'options' => $this->category->getTree(),
            'search' => 'category',
          ],
        ], 'status');
      } else {
        $fields = $this->arrayInsertAfter($fields, [
          'categories' => [
            'type' => 'hidden',
            'name' => 'categories',
            'search' => 'category',
          ],
        ]);
      }
    }
    return $fields;
  }
  /**
   * 產生列表資料結構
   */
  public function generateListData($datas)
  {
    $list_datas = [];
    foreach ($datas as $data) {
      $list_datas[$data->id] = [
        'title' => [
          'align' => 'left',
          'type' => 'link',
          'data' => $data->title,
          'url' => route('backend.' . $this->prefix . '.edit', array_merge(request()->all(), ['id' => $data->id])),
        ],
        'service_area_id' => [
          'align' => 'left',
          'type' => 'text',
          'data' => $data->service_area_id ? $data->area->title : '',
        ],
        'service_category_id' => [
          'align' => 'left',
          'type' => 'text',
          'data' => $data->category->title,
        ],
        'date_range' => [
          'align' => 'left',
          'type' => 'text',
          'data' => ($data->date_on || $data->date_off) ? $data->date_on . '～' . $data->date_off : '',
        ],
        'status' => [
          'align' => 'center',
          'type' => 'html',
          'options' => $this->model->present()->status(),
          'data' => $this->model->present()->status($data->status),
        ],
      ];
      if (method_exists($this->model, 'categories')) {
        $list_datas[$data->id] = $this->arrayInsertAfter($list_datas[$data->id], [
          'categories' => [
            'align' => 'left',
            'type' => 'text',
            'data' => $data->categories->implode('title', ', '),
          ],
        ], 'title');
      }
      if (auth()->guard('admin')->user()->group->checkPowerByName($this->prefix . '.edit')) {
        $list_datas[$data->id]['edit'] = [
          'label' => Lang::get('backend.edit', [], env('BACKEND_LOCALE')),
          'align' => 'center',
          'type' => 'btn',
          'data' => '<span class="glyphicon glyphicon-pencil"></span>',
          'url' => route('backend.' . $this->prefix . '.edit', array_merge(request()->all(), ['id' => $data->id])),
        ];
      }
      if (auth()->guard('admin')->user()->group->checkPowerByName($this->prefix . '.destroy')) {
        $list_datas[$data->id]['delete'] = [
          'label' => Lang::get('backend.delete', [], env('BACKEND_LOCALE')),
          'align' => 'center',
          'type' => 'btn',
          'data' => '<span class="glyphicon glyphicon-trash"></span>',
          'url' => route('backend.' . $this->prefix . '.destroy', ['id' => $data->id]),
          'class' => 'btn-default btn-delete-row',
        ];
      }
    }
    return $list_datas;
  }
}
