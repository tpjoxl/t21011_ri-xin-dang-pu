<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Lang;
use App\Contact;
use Validator;
use Illuminate\Validation\Rule;

class ContactController extends BasicController
{
  public function __construct(Contact $Contact)
  {
    parent::__construct($Contact);
    $this->prefix = 'contact';
    $this->valid_attrs = [
      'created_at' => Lang::get('validation.attributes.filled_at'),
      'device' => '裝置',
    ];
  }
  protected function getEditValidRules()
  {
    $rules = [];
    return $rules;
  }
  protected function getEditFormFields()
  {
    $fields = [
      'status' => [
        'type' => 'radio',
        'name' => 'status',
        'required' => true,
        'options' => $this->model->present()->status(),
        'default' => 0,
      ],
      'created_at' => [
        'type' => 'text',
        'name' => 'created_at',
        'required' => false,
      ],
      'url' => [
        'type' => 'text',
        'name' => 'url',
        'required' => false,
      ],
      'device' => [
        'type' => 'text',
        'name' => 'device',
        'required' => false,
      ],
      'name' => [
        'type' => 'text',
        'name' => 'name',
        'required' => false,
      ],
      'sex' => [
        'type' => 'text_option',
        'name' => 'sex',
        'required' => false,
        'options' => $this->model->present()->sex(),
      ],
      'phone' => [
        'type' => 'text',
        'name' => 'phone',
        'required' => false,
      ],
      'service_category' => [
        'type' => 'text',
        'name' => 'service_category',
        'required' => false,
      ],
      'line_id' => [
        'type' => 'text',
        'name' => 'line_id',
        'required' => false,
      ],
      'city' => [
        'type' => 'text',
        'name' => 'city',
        'required' => false,
      ],
      'message' => [
        'type' => 'text_nl2br',
        'name' => 'message',
        'required' => false,
      ],
      'note' => [
        'type' => 'text_area',
        'name' => 'note',
        'required' => false,
      ],
    ];
    return $fields;
  }
  protected function getSearchFields()
  {
    $fields = [
      'status' => [
        'type' => 'radio',
        'name' => 'status',
        'options' => $this->model->present()->status(),
        'search' => 'equal',
      ],
      'name' => [
        'type' => 'text_input',
        'name' => 'name',
        'search' => 'like',
      ],
      'phone' => [
        'type' => 'text_input',
        'name' => 'phone',
        'search' => 'like',
      ],
      // 'email' => [
      //   'type' => 'text_input',
      //   'name' => 'email',
      //   'search' => 'like',
      // ],
      'created_range' => [
        'type' => 'date_range',
        'name' => ['created_at1', 'created_at2'],
        'placeholder' => [Lang::get('validation.attributes.date_on'), Lang::get('validation.attributes.date_off')],
        'search' => 'range',
        'compare_fields' => ['created_at', 'created_at']
      ],
    ];
    return $fields;
  }
  /**
   * 產生列表資料結構
   */
  public function generateListData($datas)
  {
    $list_datas = [];
    foreach ($datas as $data) {
      $list_datas[$data->id] = [
        'name' => [
          'align' => 'left',
          'type' => 'link',
          'data' => $data->name,
          'url' => route('backend.'.$this->prefix.'.edit', ['id' => $data->id]),
        ],
        'sex' => [
          'align' => 'left',
          'type' => 'html',
          'options' => $this->model->present()->sex(),
          'data' => !is_null($data->sex)?$this->model->present()->sex($data->sex):'',
        ],
        'phone' => [
          'align' => 'left',
          'type' => 'text',
          'data' => $data->phone,
        ],
        // 'email' => [
        //   'align' => 'left',
        //   'type' => 'text',
        //   'data' => $data->email,
        // ],
        'created_at' => [
          'align' => 'left',
          'type' => 'text',
          'data' => $data->created_at,
        ],
        'note' => [
          'align' => 'left',
          'type' => 'html',
          'data' => nl2br($data->note),
        ],
        'status' => [
          'align' => 'center',
          'type' => 'html',
          'options' => $this->model->present()->status(),
          'data' => $this->model->present()->status($data->status),
        ],
      ];
      if (method_exists($this->model, 'categories')) {
        $list_datas[$data->id] = $this->arrayInsertAfter($list_datas[$data->id], [
          'categories' => [
            'align' => 'left',
            'type' => 'text',
            'data' => $data->categories->implode('title', ', '),
          ],
        ], 'title');
      }
      if (auth()->guard('admin')->user()->group->checkPowerByName($this->prefix.'.edit')) {
        $list_datas[$data->id]['edit'] = [
          'label' => Lang::get('backend.edit'),
          'align' => 'center',
          'type' => 'btn',
          'data' => '<span class="glyphicon glyphicon-pencil"></span>',
          'url' => route('backend.'.$this->prefix.'.edit', ['id' => $data->id]),
        ];
      }
      if (auth()->guard('admin')->user()->group->checkPowerByName($this->prefix.'.destroy')) {
        $list_datas[$data->id]['delete'] = [
          'label' => Lang::get('backend.delete'),
          'align' => 'center',
          'type' => 'btn',
          'data' => '<span class="glyphicon glyphicon-trash"></span>',
          'url' => route('backend.'.$this->prefix.'.destroy', ['id' => $data->id]),
          'class' => 'btn-default btn-delete-row',
        ];
      }
    }
    return $list_datas;
  }
}
