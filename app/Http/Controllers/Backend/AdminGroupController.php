<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Lang;
use App\Power;
use App\AdminGroup;
use Validator;
use Illuminate\Validation\Rule;

class AdminGroupController extends CategoryController
{
  protected $power;

  public function __construct(AdminGroup $AdminGroup, Power $Power)
  {
      parent::__construct($AdminGroup);
      $this->power = $Power;
      $this->prefix = 'admin.group';
      $this->valid_rules = [
        'title' => ['required', Rule::unique($this->table)],
        'power_list' => 'required',
      ];
      $this->valid_msgs = [];
      $this->valid_attrs = [
        'title' => Lang::get('validation.attributes.group_name'),
        'power_list' => Lang::get('validation.attributes.power_option'),
      ];
  }
  /**
   * 顯示新增表單頁
   */
  public function create(Request $request)
  {
    $parents = $this->model->getTree();
    $data = $this->model;
    $admin_group = auth()->guard('admin')->user()->group;

    // if ($admin_group->title == env('MASTER_GROUP')) {
    //   $all_powers = $this->power->ordered()->get();
    // } else {
    //   $all_powers = $this->power->ordered()->where('active', 1)->get();
    // }
    $all_powers = $this->power->ordered()->where('active', 1)->get();

    $powers = $this->power->getTree($all_powers);
    // dd($all_powers, $powers);
    $prefix = $this->prefix;
    return view('backend.'.$this->prefix.'.create', compact('parents', 'data', 'powers', 'prefix'));
  }
  /**
   * 儲存
   */
  public function store(Request $request)
  {
    // 表單驗證規則
    $validator = $this->validator($request->all(), $this->valid_rules, $this->valid_msgs, $this->valid_attrs);
    if ($validator->fails()) {
      return back()->withErrors($validator)->withInput();
    }
    $main_data = $this->model;
    $data = $request->except('parent', 'power_list');
    $result = $main_data->fillAndSave($data);

    if ($result && $main_data->powers()->sync($request->power_list)) {
      return redirect()->route('backend.'.$this->prefix.'.index')->with('success', Lang::get('backend.create_success'));
    } else {
      return redirect()->back()->with('error', Lang::get('backend.create_error'));
    }
  }
  /**
   * 顯示修改表單
   */
  public function edit($id)
  {
    $data = $this->model->find($id);
    $parents = $this->model->getTree();
    $admin_group = auth()->guard('admin')->user()->group;

    if ($data->title == env('MASTER_GROUP')) {
      if ($admin_group->title == env('MASTER_GROUP')) {
        $all_powers = $this->power->ordered()->get();
      } else {
        return redirect()->route('backend.'.$this->prefix.'.index')->with('error', Lang::get('backend.no_power'));
      }
    } else {
      $all_powers = $this->power->ordered()->where('active', 1)->get();
    }
    $powers = $this->power->getTree($all_powers);
    // return dd($powers);
    $prefix = $this->prefix;
    return view('backend.'.$this->prefix.'.edit', compact('parents', 'data', 'powers', 'prefix'));
  }
  /**
   * 更新
   */
  public function update(Request $request, $id)
  {
    // 表單驗證規則
    $validator = $this->validator($request->all(), $this->valid_rules, $this->valid_msgs, $this->valid_attrs, $id);
    if ($validator->fails()) {
      return back()->withErrors($validator)->withInput();
    }

    $data = $request->except('_token', 'power_list');
    $t = $this->model->findOrFail($id);
    $result = $t->update($data);

    if ($result && $t->powers()->sync($request->power_list)) {
      return redirect()->route('backend.'.$this->prefix.'.index')->with('success', Lang::get('backend.save_success'));
    } else {
      return redirect()->route('backend.'.$this->prefix.'.index')->with('error', Lang::get('backend.save_error'));
    }
  }
  /**
   * 刪除
   */
  public function destroy(Request $request)
  {
    $ids = explode(",",$request->id);

    $masterGroupId = $this->model->where('title', env('MASTER_GROUP'))->get()->pluck('id')->toArray();

    $result = $this->model->destroy(array_diff($ids, $masterGroupId));
    if($result) {
      return redirect()->route('backend.'.$this->prefix.'.index')->with('success', Lang::get('backend.delete_success'));
    } else {
      return redirect()->back()->with('error', Lang::get('backend.delete_error'));
    }
  }
}
