<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\ServiceCategory;
use Lang;
use Validator;
use Illuminate\Validation\Rule;

class ServiceCategoryController extends CategoryController
{
  public function __construct(ServiceCategory $ServiceCategory)
  {
    parent::__construct($ServiceCategory);
    $this->prefix = 'service.category';
    $this->valid_attrs = [
      'menu_status' => '在主選單顯示',
      'pic' => '首頁列表' . Lang::get('validation.attributes.pic'),
      'icon' => '首頁列表icon',
      'description' => '首頁列表' . Lang::get('validation.attributes.description'),
      'text' => Lang::get('validation.attributes.text') . '1 文字',
      'text_pic' => Lang::get('validation.attributes.text') . '1 圖片',
      'text2' => Lang::get('validation.attributes.text') . '2',
      'text3' => Lang::get('validation.attributes.text') . '3',
      'text3_bg' => Lang::get('validation.attributes.text') . '3 背景圖',
      'text4' => Lang::get('validation.attributes.text') . '4',
      'text5' => Lang::get('validation.attributes.text') . '5',
    ];
  }
  protected function getFormFields()
  {
    $fields = [
      'status' => [
        'type' => 'radio',
        'name' => 'status',
        'required' => true,
        'options' => $this->model->present()->status(),
        'default' => 1,
      ],
      'menu_status' => [
        'type' => 'radio',
        'name' => 'menu_status',
        'required' => true,
        'options' => $this->model->present()->yes_or_no(),
        'default' => 1,
        'hint' => '否則顯示在其他服務下拉內',
      ],
      'title' => [
        'type' => 'text_input',
        'name' => 'title',
        'required' => true,
      ],
      'slug' => [
        'type' => 'text_input',
        'name' => 'slug',
        'required' => false,
      ],
      'h1' => [
        'type' => 'text_input',
        'name' => 'h1',
        'required' => false,
      ],
      // 'h2' => [
      //   'type' => 'text_area',
      //   'name' => 'h2',
      //   'required' => false,
      //   'rows' => 3,
      // ],
      'banner' => [
        'type' => 'img',
        'name' => 'banner',
        'required' => false,
        'w' => 1920,
        'h' => 300,
        'folder' => $this->table
      ],
      'pic' => [
        'type' => 'img',
        'name' => 'pic',
        'required' => false,
        'w' => 360,
        'h' => 240,
        'folder' => $this->table
      ],
      // 'icon' => [
      //   'type' => 'img',
      //   'name' => 'icon',
      //   'required' => false,
      //   'w' => null,
      //   'h' => null,
      //   'folder' => $this->table
      // ],
      'description' => [
        'type' => 'text_area',
        'name' => 'description',
        'required' => false,
        'rows' => 3,
      ],
      // 'text_pic' => [
      //   'type' => 'img',
      //   'name' => 'text_pic',
      //   'required' => false,
      //   'w' => null,
      //   'h' => null,
      //   'folder' => $this->table,
      //   'hint' => '建議寬度480px',
      // ],
      'text' => [
        'type' => 'text_editor',
        'name' => 'text',
        'required' => false,
      ],
      'text2' => [
        'type' => 'text_editor',
        'name' => 'text2',
        'required' => false,
        'rows' => 3,
      ],
      'text3' => [
        'type' => 'text_area',
        'name' => 'text3',
        'required' => false,
      ],
      // 'text3_bg' => [
      //   'type' => 'img',
      //   'name' => 'text3_bg',
      //   'required' => false,
      //   'w' => 1920,
      //   'h' => 350,
      //   'folder' => $this->table
      // ],
      'text4' => [
        'type' => 'text_editor',
        'name' => 'text4',
        'required' => false,
      ],
      'text5' => [
        'type' => 'text_editor',
        'name' => 'text5',
        'required' => false,
      ],
      'recommends' => [
        'type' => 'multi_select',
        'name' => 'recommends',
        'required' => false,
        'options' => $this->model->recommends()->getRelated()->when($this->data->id, function ($q) {
          $q->whereHas('categories', function ($q) {
            $q->where('service_category_id', $this->data->id);
          });
        })->ordered()->get()->transform(function ($item, $key) {
          $item->title = ($item->categories->count() ? '[' . $item->categories->first()->title . '] ' : '') . $item->title;
          return $item;
        }),
        'live_search' => true,
        'actions' => true,
      ],
      'seo_title' => [
        'type' => 'text_input',
        'name' => 'seo_title',
        'required' => false,
      ],
      'seo_description' => [
        'type' => 'text_area',
        'name' => 'seo_description',
        'required' => false,
        'rows' => 3,
      ],
      'seo_keyword' => [
        'type' => 'text_input',
        'name' => 'seo_keyword',
        'required' => false,
      ],
      'og_title' => [
        'type' => 'text_input',
        'name' => 'og_title',
        'required' => false,
      ],
      'og_description' => [
        'type' => 'text_area',
        'name' => 'og_description',
        'required' => false,
        'rows' => 3,
      ],
      'og_image' => [
        'type' => 'img',
        'name' => 'og_image',
        'required' => false,
        'w' => null,
        'h' => null,
        'folder' => $this->table
      ],
      'meta_robots' => [
        'type' => 'text_input',
        'name' => 'meta_robots',
        'required' => false,
        'default' => 'index, follow',
      ],
    ];
    if ($this->limit_level > 1) {
      $fields = $this->arrayInsertAfter($fields, [
        'parent_id' => [
          'type' => 'select',
          'name' => 'parent_id',
          'required' => true,
          'options' => $this->model->getTree(null, null, 1),
          'placeholder' => '最上層'
        ],
      ]);
    }
    return $fields;
  }
}
