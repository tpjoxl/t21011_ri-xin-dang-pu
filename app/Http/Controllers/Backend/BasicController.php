<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Lang;
use Validator;
use Illuminate\Validation\Rule;
use Carbon\Carbon;

class BasicController extends Controller
{
  public function __construct($Model, $Category = null)
  {
    parent::__construct($Model);
    $this->category = $Category;
    // 前台是否有全部項目顯示的頁面，有的話後台就需要全部項目(不分類))的排序
    $this->rank_all = true;
    $this->relationItems = 'items';
    $this->relationCategory = 'categories';
    $this->relationCategoryKey = 'categories';
    $this->data = $this->model->make();

    // dd($this->category->getTree());
    // 如果有分類的話
    if (method_exists($this->model, 'categories')) {
      $this->valid_rules = $this->arrayInsertAfter($this->valid_rules, [
        'categories' => 'required|array',
        'categories.*' => 'required',
      ]);
      $this->valid_attrs = $this->arrayInsertAfter($this->valid_attrs, [
        'categories.*' => Lang::get('validation.attributes.categories'),
      ]);
    }
  }
  /**
   * 表單驗證的規則
   */
  protected function getCreateValidRules()
  {
    return $this->getValidRules();
  }
  protected function getEditValidRules()
  {
    return $this->getValidRules();
  }
  protected function getValidRules()
  {
    $rules = [
      'title' => ['required', Rule::unique($this->table), 'max:191'],
      'slug' => ['nullable', Rule::unique($this->table), 'max:191'],
      'description' => 'nullable|max:3000',
      'seo_title' => 'nullable|max:191',
      'seo_description' => 'nullable|max:3000',
      'seo_keyword' => 'nullable|max:3000',
      'og_title' => 'nullable|max:191',
      'og_description' => 'nullable|max:3000',
      'url' => 'nullable|url',
    ];
    if (method_exists($this->model, 'categories')) {
      $rules = $this->arrayInsertAfter($rules, [
        'categories' => 'required|array',
        'categories.*' => 'required',
      ]);
    }
    return $rules;
  }
  /**
   * 表單欄位
   */
  protected function getCreateFormFields() {
    return $this->getFormFields();
  }
  protected function getEditFormFields() {
    return $this->getFormFields();
  }
  protected function getFormFields()
  {
    $fields = [
      'status' => [
        'type' => 'radio',
        'name' => 'status',
        'required' => true,
        'options' => $this->model->present()->status(),
        'default' => 1,
      ],
      'home_status' => [
        'type' => 'radio',
        'name' => 'home_status',
        'required' => false,
        'options' => $this->model->present()->status(),
        'default' => 1,
      ],
      'set_top' => [
        'type' => 'set_top',
        'name' => 'rank',
        'required' => false,
      ],
      'title' => [
        'type' => 'text_input',
        'name' => 'title',
        'required' => true,
      ],
      'slug' => [
        'type' => 'text_input',
        'name' => 'slug',
        'required' => false,
      ],
      'url' => [
        'type' => 'text_input',
        'name' => 'url',
        'required' => false,
      ],
      'description' => [
        'type' => 'text_area',
        'name' => 'description',
        'required' => false,
        'rows' => 3,
      ],
      'text' => [
        'type' => 'text_editor',
        'name' => 'text',
        'required' => false,
      ],
      'date_range' => [
        'type' => 'date_range',
        'name' => ['date_on', 'date_off'],
        'placeholder' => [Lang::get('validation.attributes.date_on'), Lang::get('validation.attributes.date_off')],
        'required' => false,
      ],
      'pic' => [
        'type' => 'img',
        'name' => 'pic',
        'required' => false,
        'w' => null,
        'h' => null,
        'folder' => $this->table
      ],
      'seo_title' => [
        'type' => 'text_input',
        'name' => 'seo_title',
        'required' => false,
      ],
      'seo_description' => [
        'type' => 'text_area',
        'name' => 'seo_description',
        'required' => false,
        'rows' => 3,
      ],
      'seo_keyword' => [
        'type' => 'text_input',
        'name' => 'seo_keyword',
        'required' => false,
      ],
      'og_title' => [
        'type' => 'text_input',
        'name' => 'og_title',
        'required' => false,
      ],
      'og_description' => [
        'type' => 'text_area',
        'name' => 'og_description',
        'required' => false,
        'rows' => 3,
      ],
      'og_image' => [
        'type' => 'img',
        'name' => 'og_image',
        'required' => false,
        'w' => null,
        'h' => null,
        'folder' => $this->table
      ],
      'meta_robots' => [
        'type' => 'text_input',
        'name' => 'meta_robots',
        'required' => false,
        'default' => 'index, follow',
      ],
    ];
    if (method_exists($this->model, 'categories')) {
      $fields = $this->arrayInsertAfter($fields, [
        'categories' => [
          'type' => 'multi_select',
          'name' => 'categories',
          'required' => true,
          'options' => $this->category->getTree(),
          'add_first' => [
            'url' => route('backend.'.$this->model->getTable().'.category.create'),
            'title' => Lang::get('backend.category'),
          ]
        ]
      ]);
      if ($this->category->getLimitLevel()==1) {
        $fields['categories']['max'] = 1;
      }
    }
    return $fields;
  }
  /**
   * 搜尋欄位
   */
  protected function getSearchFields()
  {
    $fields = [
      'status' => [
        'type' => 'radio',
        'name' => 'status',
        'options' => $this->model->present()->status(),
        'search' => 'equal',
      ],
      'title' => [
        'type' => 'text_input',
        'name' => 'title',
        'search' => 'like',
      ],
      'date_range' => [
        'type' => 'date_range',
        'name' => ['date_on', 'date_off'],
        'placeholder' => [Lang::get('validation.attributes.date_on'), Lang::get('validation.attributes.date_off')],
        'search' => 'range',
        'compare_fields' => ['date_on', 'date_off']
      ],
      'created_range' => [
        'type' => 'date_range',
        'name' => ['created_at1', 'created_at2'],
        'placeholder' => [Lang::get('validation.attributes.date_on'), Lang::get('validation.attributes.date_off')],
        'search' => 'range',
        'compare_fields' => ['created_at', 'created_at']
      ],
    ];
    if (method_exists($this->model, 'categories')) {
      if ($this->rank_all) {
        $fields = $this->arrayInsertAfter($fields, [
          'categories' => [
            'type' => 'select',
            'name' => 'categories',
            'options' => $this->category->getTree(),
            'search' => 'category',
          ],
        ], 'status');
      } else {
        $fields = $this->arrayInsertAfter($fields, [
          'categories' => [
            'type' => 'hidden',
            'name' => 'categories',
            'search' => 'category',
          ],
        ]);
      }
    }
    return $fields;
  }
  protected function getSearchBasicDatas() {
    if (request()->has('categories') && method_exists($this->model, 'categories') && $this->getSearchFields()['categories']['search'] == 'category') {
      $datas = $this->category
        ->find(request()->input('categories'))
        ->{$this->relationItems}()
        ->withPivot('rank')
        ->orderBy('pivot_rank', 'desc')
        ->orderBy('created_at', 'desc');
    } elseif (request()->has($this->relationCategoryKey) && method_exists($this->model, $this->relationCategory) && $this->getSearchFields()[$this->relationCategoryKey]['search'] == 'equal') {
      $datas = $this->category
        ->find(request()->input($this->relationCategoryKey))
        ->{$this->relationItems}()
        ->ordered();
    } else {
      $datas = $this->model
        ->ordered();
    }
    return $datas;
  }
  /**
   * 搜尋
   */
  public function search()
  {
    $request = request();
    $datas = $this->getSearchBasicDatas();

    foreach ($this->getSearchFields() as $search_field) {
      if ($search_field['search'] == 'equal') {
        if ($request->has($search_field['name'])) {
          if (method_exists($this->model, 'translations') && in_array($search_field['name'], $this->model->translatedAttributes)) {
            $datas->whereTranslation($search_field['name'], $request->input($search_field['name']));
          } else {
            $datas->where($search_field['name'], $request->input($search_field['name']));
          }
        }
      } elseif ($search_field['search'] == 'like') {
        if ($request->has($search_field['name'])) {
          if (method_exists($this->model, 'translations') && in_array($search_field['name'], $this->model->translatedAttributes)) {
            $datas->whereTranslationLike($search_field['name'], '%'.$request->input($search_field['name']).'%');
          } else {
            $datas->where($search_field['name'], 'like','%'.$request->input($search_field['name']).'%');
          }
        }
      } elseif ($search_field['search'] == 'range') {
        if ($search_field['compare_fields'][0] == $search_field['compare_fields'][1]) {
          if ($request->has($search_field['name'][0])) {
            $datas->where($search_field['compare_fields'][0], '>=', $request->input($search_field['name'][0]));
          }
          if ($request->has($search_field['name'][1])) {
            $datas->where($search_field['compare_fields'][1], '<=', (new Carbon($request->input($search_field['name'][1])))->addDay());
          }
        } else {
          if ($request->has($search_field['name'][0])) {
            $datas->where(function ($q) use ($search_field, $request) {
                $q->whereNull($search_field['compare_fields'][0])
                      ->orWhere($search_field['compare_fields'][0], '>=', $request->input($search_field['name'][0]));
            })->where(function ($q) use ($search_field, $request) {
                $q->whereNull($search_field['compare_fields'][1])
                      ->orWhere($search_field['compare_fields'][1], '>=', $request->input($search_field['name'][0]));
            });
          }
          if ($request->has($search_field['name'][1])) {
            $datas->where(function ($q) use ($search_field, $request) {
                $q->whereNull($search_field['compare_fields'][0])
                      ->orWhere($search_field['compare_fields'][0], '<=', $request->input($search_field['name'][1]));
            })->where(function ($q) use ($search_field, $request) {
                $q->whereNull($search_field['compare_fields'][1])
                      ->orWhere($search_field['compare_fields'][1], '<=', $request->input($search_field['name'][1]));
            });
          }
        }
      } elseif ($search_field['search'] == 'relation') {
        if ($request->has($search_field['name'])) {
          $datas->whereHas($search_field['name'], function ($query) use ($search_field, $request) {
            $query->where($search_field['compare_fields'], $request->input($search_field['name']));
          });
        }
      } elseif ($search_field['search'] == 'relation_like') {
        if ($request->has($search_field['name'])) {
          $datas->whereHas($search_field['relation'], function ($query) use ($search_field, $request) {
            $query->where($search_field['name'], 'like', '%'.$request->input($search_field['name']).'%');
          });
        }
      }
    }
    return $datas;
  }
  /**
   * 列表
   */
  public function index(Request $request)
  {
    if (!is_null($this->category) && !$this->rank_all && !$request->has($this->relationCategoryKey) && count($this->category->all())>0) {
      return redirect()->route('backend.'.$this->prefix.'.index', [$this->relationCategoryKey=>$this->category->all()->first()->id]);
    }
    $model = $this->model;
    $datas = $this->search()->paginate($this->perPage);
    if (is_null($datas)) {
      return redirect()->route('backend.'.$this->prefix.'.index');
    }

    if (!is_null($this->category)) {
      $categories = $this->category->getTree();
    }
    $prefix = $this->prefix;
    $rank_all = $this->rank_all;
    $relationItems = $this->relationItems;
    $relationCategory = $this->relationCategory;
    $relationCategoryKey = $this->relationCategoryKey;

    $search_fields = $this->getSearchFields();
    $list_datas = $this->generateListData($datas);
    $list_pager = [
      'first'=>$datas->firstItem(),
      'last'=>$datas->lastItem(),
      'total'=>$datas->total(),
      'render'=>$datas->appends(request()->input())->render()
    ];
    $valid_attrs = $this->valid_attrs;

    $view = 'backend.'.$this->prefix.'.index';
    if (!view()->exists($view)) {
      $view = 'backend.common.index';
    }
    return view($view, compact('prefix', 'rank_all', 'model', 'categories', 'search_fields', 'list_fields', 'list_pager', 'list_datas', 'valid_attrs', 'relationItems', 'relationCategory', 'relationCategoryKey'));
  }
  /**
   * 產生列表資料結構
   */
  public function generateListData($datas)
  {
    $list_datas = [];
    foreach ($datas as $data) {
      $list_datas[$data->id] = [
        'title' => [
          'align' => 'left',
          'type' => 'link',
          'data' => $data->title,
          'url' => route('backend.'.$this->prefix.'.edit', array_merge(request()->all(), ['id' => $data->id])),
        ],
        'date_range' => [
          'align' => 'left',
          'type' => 'text',
          'data' => ($data->date_on || $data->date_off)?$data->date_on.'～'.$data->date_off:'',
        ],
        'created_at' => [
          'align' => 'left',
          'type' => 'text',
          'data' => $data->created_at,
        ],
        'status' => [
          'align' => 'center',
          'type' => 'html',
          'options' => $this->model->present()->status(),
          'data' => $this->model->present()->status($data->status),
        ],
      ];
      if (method_exists($this->model, 'categories')) {
        $list_datas[$data->id] = $this->arrayInsertAfter($list_datas[$data->id], [
          'categories' => [
            'align' => 'left',
            'type' => 'text',
            'data' => $data->categories->implode('title', ', '),
          ],
        ], 'title');
      }
      if (auth()->guard('admin')->user()->group->checkPowerByName($this->prefix.'.edit')) {
        $list_datas[$data->id]['edit'] = [
          'label' => Lang::get('backend.edit'),
          'align' => 'center',
          'type' => 'btn',
          'data' => '<span class="glyphicon glyphicon-pencil"></span>',
          'url' => route('backend.'.$this->prefix.'.edit', array_merge(request()->all(), ['id' => $data->id])),
        ];
      }
      if (auth()->guard('admin')->user()->group->checkPowerByName($this->prefix.'.destroy')) {
        $list_datas[$data->id]['delete'] = [
          'label' => Lang::get('backend.delete'),
          'align' => 'center',
          'type' => 'btn',
          'data' => '<span class="glyphicon glyphicon-trash"></span>',
          'url' => route('backend.'.$this->prefix.'.destroy', ['id' => $data->id]),
          'class' => 'btn-default btn-delete-row',
        ];
      }
    }
    return $list_datas;
  }
  /**
   * 新增頁面
   */
  public function create(Request $request)
  {
    $rank_all = $this->rank_all;
    $this->data = $data = $this->model->make();
    $prefix = $this->prefix;
    $form_fields = $this->getCreateFormFields();
    if (!is_null($this->category)) {
      $category = $this->category;
      $categories = $this->category->getTree();
    }
    $valid_attrs = $this->valid_attrs;

    $view = 'backend.'.$this->prefix.'.create';
    if (!view()->exists($view)) {
      $view = 'backend.common.create';
    }
    return view($view, compact('categories', 'data', 'prefix', 'rank_all', 'form_fields', 'valid_attrs'));
  }
  /**
   * 儲存
   */
  public function store(Request $request)
  {
    $requestData = $request->except('search');

    $validator = $this->validator($requestData, $this->getCreateValidRules(), $this->valid_msgs, $this->valid_attrs);
    if ($validator->fails()) {
      return back()->withErrors($validator)->withInput();
    }
    $this->data = $main_data = $this->model->make();
    $result1 = $main_data->fillAndSave($requestData);

    if ($result1) {
      if ($this->rank_all) {
        return redirect()->route('backend.'.$this->prefix.'.index')->with('success', Lang::get('backend.create_success'));
      } else {
        return redirect()->route('backend.'.$this->prefix.'.index', [$this->relationCategoryKey=>(is_a($this->relationCategory, 'Illuminate\Database\Eloquent\Collection')?$main_data->{$this->relationCategory}->first()->id:$main_data->{$this->relationCategory}->id)])->with('success', Lang::get('backend.save_success'));
      }
    } else {
      return redirect()->back()->with('error', Lang::get('backend.create_error'));
    }
  }
  /**
   * 顯示修改表單
   */
  public function edit($id)
  {
    $rank_all = $this->rank_all;
    $this->data = $data = $this->model->find($id);
    if (is_null($data)) {
      return redirect()->route('backend.'.$this->prefix.'.index')->with('error', Lang::get('backend.data_not_found'));
    }
    if (!is_null($this->category)) {
      $categories = $this->category->getTree();
      $category = $this->category;
    }
    $prefix = $this->prefix;
    $form_fields = $this->getEditFormFields();
    $valid_attrs = $this->valid_attrs;
    // return dd($data, $categories, $category);
    $view = 'backend.'.$this->prefix.'.edit';
    if (!view()->exists($view)) {
      $view = 'backend.common.edit';
    }
    return view($view, compact('category', 'categories', 'data', 'prefix', 'rank_all', 'form_fields', 'valid_attrs'));
  }
  /**
   * 更新
   */
  public function update(Request $request, $id)
  {
    $requestData = $request->except('search');

    $validator = $this->validator($requestData, $this->getEditValidRules(), $this->valid_msgs, $this->valid_attrs, $id);
    if ($validator->fails()) {
      return back()->withErrors($validator)->withInput();
    }
    // dd($requestData, $dataToSave);
    $this->data = $main_data = $this->model->find($id);
    $result1 = $main_data->fillAndSave($requestData);

    if ($result1) {
      if ($this->rank_all) {
        return redirect()->back()->with('success', Lang::get('backend.save_success'));
      } else {
        return redirect()->route('backend.'.$this->prefix.'.edit', ['id'=>$id, $this->relationCategoryKey=>(is_a($this->relationCategory, 'Illuminate\Database\Eloquent\Collection')?$main_data->{$this->relationCategory}->first()->id:$main_data->{$this->relationCategory}->id)])->with('success', Lang::get('backend.save_success'));
      }
    } else {
      return redirect()->back()->with('error', Lang::get('backend.save_error'));
    }
  }
  /**
   * 刪除
   */
  public function destroy(Request $request)
  {
    $ids = explode(",",$request->id);

    $result = $this->model->destroy($ids);

    if($result) {
      return redirect()->route('backend.'.$this->prefix.'.index')->with('success', Lang::get('backend.delete_success'));
    } else {
      return redirect()->back()->with('error', Lang::get('backend.delete_error'));
    }
  }
  /**
   * 修改狀態
   */
  public function setStatus(Request $request, $val)
  {
    $ids = explode(",",$request->id);
    $result = $this->model->whereIn('id', $ids)->update(['status' => $val]);
    if ($result) {
      return redirect()->back()->with('success', Lang::get('backend.status_success'));
    } else {
      return redirect()->back()->with('error', Lang::get('backend.status_error'));
    }
  }
  /**
   * 排序頁顯示的欄位資料
   */
  protected function getRankFields() {
    $fields = [
      // 'pic' => [
      //   'type' => 'img',
      //   'name' => 'pic',
      // ],
      'title' => [
        'type' => 'text',
        'name' => 'title',
      ],
    ];
    return $fields;
  }
  /**
   * 排序列表頁
   */
  public function rank($category_id = null)
  {
    $prefix = $this->prefix;
    $rank_all = $this->rank_all;
    $rank_fields = $this->getRankFields();
    $valid_attrs = $this->valid_attrs;
    $relationItems = $this->relationItems;
    $relationCategory = $this->relationCategory;
    $relationCategoryKey = $this->relationCategoryKey;
    $view = 'backend.'.$this->prefix.'.rank';
    if (!view()->exists($view)) {
      $view = 'backend.common.rank';
    }
    if (!is_null($this->category)) {
      $categories = $this->category->getTree();
      if (is_null($category_id)) {
        if ($rank_all) {
          $category = null;
          $datas = $this->model->ordered()->get();
          return view($view, compact('categories', 'datas', 'category', 'prefix', 'rank_all', 'rank_fields', 'valid_attrs', 'relationItems', 'relationCategory', 'relationCategoryKey'));
        } else {
          $category = $this->category->first();
          if ($category) {
            return  redirect()->route('backend.'.$this->prefix.'.rank.index', ['id'=>$this->category->first()->id]);
          } else {
            $category = $categories = $datas = null;
            return view($view, compact('categories', 'datas', 'category', 'prefix', 'rank_all', 'rank_fields', 'valid_attrs', 'relationItems', 'relationCategory', 'relationCategoryKey'));
          }
        }
      } else {
        $category = $this->category->find($category_id);
        $datas = $category->{$this->relationItems}()->ordered($rank_all?'pivot_rank':'rank')->get();
        return view($view, compact('categories', 'datas', 'category', 'prefix', 'rank_all', 'rank_fields', 'valid_attrs', 'relationItems', 'relationCategory', 'relationCategoryKey'));
        // $category = $this->category->find($category_id);
        // $datas = $category->{$this->relationItems}()->orderBy('rank', 'desc')->orderBy('created_at', 'desc')->get();
        // return view($view, compact('categories', 'datas', 'category', 'prefix'));
      }
    } else {
      $datas = $this->model->ordered()->get();
      return view($view, compact('datas', 'prefix', 'rank_all', 'rank_fields', 'valid_attrs', 'relationItems', 'relationCategory', 'relationCategoryKey'));
    }
  }
  /**
   * 排序更新
   */
  public function rankUpdate(Request $request, $category_id = null)
  {
    $ids = explode(",",$request->input('sortNum'));

    $result = 0;
    if (is_null($category_id) || !$this->rank_all) {
      foreach ($ids as $key => $id) {
        $data = $this->model->find($id);
        $result += $data->update(['rank' => $key+1]);
      }
    } else {
      foreach ($ids as $key => $id) {
        $data = $this->category->find($category_id);
        $result += $data->{$this->relationItems}()->updateExistingPivot($id, ['rank' => $key+1]);
      }
    }

    if ($result) {
      return back()->with('success', Lang::get('backend.rank_success', [], env('BACKEND_LOCALE')));
    } else {
      return back()->with('error', Lang::get('backend.rank_error', [], env('BACKEND_LOCALE')));
    }
  }

}
