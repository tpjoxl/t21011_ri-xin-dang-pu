<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\BlogCategory;
use Lang;
use Validator;
use Illuminate\Validation\Rule;

class BlogCategoryController extends CategoryController
{
  public function __construct(BlogCategory $BlogCategory)
  {
    parent::__construct($BlogCategory);
    $this->prefix = 'blog.category';
    $this->limit_level = $BlogCategory->getLimitLevel();
    $this->valid_attrs = [
      'service_category_id' => '關聯的服務項目'
    ];
  }
  protected function getValidRules()
  {
    $rules = [
      'seo_title' => 'nullable|max:191',
      'seo_description' => 'nullable|max:3000',
      'seo_keyword' => 'nullable|max:3000',
      'og_title' => 'nullable|max:191',
      'og_description' => 'nullable|max:3000',
    ];
    if (method_exists($this->model, 'categories')) {
      $rules = $this->arrayInsertAfter($rules, [
        'categories' => 'required|array',
        'categories.*' => 'required',
      ]);
    }
    return $rules;
  }
  protected function getFormFields()
  {
    $fields = [
      'status' => [
        'type' => 'radio',
        'name' => 'status',
        'required' => true,
        'options' => $this->model->present()->status(),
        'default' => 1,
      ],
      'title' => [
        'type' => 'text_input',
        'name' => 'title',
        'required' => true,
      ],
      'slug' => [
        'type' => 'text_input',
        'name' => 'slug',
        'required' => false,
      ],
      'service_category_id' => [
        'type' => 'select',
        'name' => 'service_category_id',
        'required' => false,
        'options' => $this->model->service_category()->getRelated()->ordered()->get(),
        'live_search' => true,
      ],
      'banner' => [
        'type' => 'img',
        'name' => 'banner',
        'required' => false,
        'w' => 1920,
        'h' => 300,
        'folder' => $this->table
      ],
      'h1' => [
        'type' => 'text_input',
        'name' => 'h1',
        'required' => false,
      ],
      'h2' => [
        'type' => 'text_input',
        'name' => 'h2',
        'required' => false,
      ],
      // 'pic' => [
      //   'type' => 'img',
      //   'name' => 'pic',
      //   'required' => false,
      //   'w' => 1920,
      //   'h' => 600,
      //   'folder' => $this->table
      // ],
      // 'description' => [
      //   'type' => 'text_area',
      //   'name' => 'description',
      //   'required' => false,
      //   'rows' => 3,
      // ],
      'text' => [
        'type' => 'text_editor',
        'name' => 'text',
        'required' => false,
      ],
      'seo_title' => [
        'type' => 'text_input',
        'name' => 'seo_title',
        'required' => false,
      ],
      'seo_description' => [
        'type' => 'text_area',
        'name' => 'seo_description',
        'required' => false,
        'rows' => 3,
      ],
      'seo_keyword' => [
        'type' => 'text_input',
        'name' => 'seo_keyword',
        'required' => false,
      ],
      'og_title' => [
        'type' => 'text_input',
        'name' => 'og_title',
        'required' => false,
      ],
      'og_description' => [
        'type' => 'text_area',
        'name' => 'og_description',
        'required' => false,
        'rows' => 3,
      ],
      'og_image' => [
        'type' => 'img',
        'name' => 'og_image',
        'required' => false,
        'w' => null,
        'h' => null,
        'folder' => $this->table
      ],
      'meta_robots' => [
        'type' => 'text_input',
        'name' => 'meta_robots',
        'required' => false,
        'default' => 'index, follow',
      ],
    ];
    if ($this->limit_level > 1) {
      $fields = $this->arrayInsertAfter($fields, [
        'parent_id' => [
          'type' => 'select',
          'name' => 'parent_id',
          'required' => true,
          'options' => $this->model->getTree(null, null, 1),
          'placeholder' => '最上層'
        ],
      ]);
    }
    return $fields;
  }
}
