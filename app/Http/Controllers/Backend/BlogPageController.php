<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Lang;
use App\Setting;
use Illuminate\Support\Facades\Route;

class BlogPageController extends SettingController
{
  public function __construct(Setting $Setting)
  {
    parent::__construct($Setting);
    $this->prefix = 'blog';
  }
  protected function getValidAttrs($routeName)
  {
    $validAttrs = [
      Route::currentRouteName() => [
        'pic' => 'Banner圖片(電腦版)',
        'pic_m' => 'Banner圖片(手機版)',
      ],
    ];
    return array_key_exists($routeName, $validAttrs) ? $validAttrs[$routeName] : [];
  }
  protected function getValidRules($routeName)
  {
    $validRules = [
      Route::currentRouteName() => [],
    ];
    return array_key_exists($routeName, $validRules) ? $validRules[$routeName] : [];
  }
  protected function getFormFields($routeName)
  {
    $formFields = [
      Route::currentRouteName() => [
        'banner' => [
          'type' => 'img',
          'name' => 'banner',
          'required' => false,
          'w' => 1920,
          'h' => 300,
          'folder' => str_replace('.', '_', $this->prefix),
          'common' => true,
        ],
        'h1' => [
          'type' => 'text_input',
          'name' => 'h1',
          'required' => false,
          'common' => false,
        ],
        // 'h2' => [
        //   'type' => 'text_input',
        //   'name' => 'h2',
        //   'required' => false,
        //   'common' => false,
        // ],
        // 'text' => [
        //   'type' => 'text_editor',
        //   'name' => 'text',
        //   'required' => false,
        //   'common' => false,
        // ],
        'seo_title' => [
          'type' => 'text_input',
          'name' => 'seo_title',
          'required' => false,
          'common' => false,
        ],
        'seo_description' => [
          'type' => 'text_area',
          'name' => 'seo_description',
          'required' => false,
          'rows' => 3,
          'common' => false,
        ],
        'seo_keyword' => [
          'type' => 'text_input',
          'name' => 'seo_keyword',
          'required' => false,
          'common' => false,
        ],
        'og_title' => [
          'type' => 'text_input',
          'name' => 'og_title',
          'required' => false,
          'common' => false,
        ],
        'og_description' => [
          'type' => 'text_area',
          'name' => 'og_description',
          'required' => false,
          'rows' => 3,
          'common' => false,
        ],
        'og_image' => [
          'type' => 'img',
          'name' => 'og_image',
          'required' => false,
          'w' => null,
          'h' => null,
          'folder' => str_replace('.', '_', $this->prefix),
          'common' => true,
        ],
        'meta_robots' => [
          'type' => 'text_input',
          'name' => 'meta_robots',
          'required' => false,
          'default' => 'index, follow',
          'common' => true,
        ],
      ],
    ];
    return array_key_exists($routeName, $formFields) ? $formFields[$routeName] : $this->defaultFields;
  }
}
