<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Lang;
use App\Power;
use App\AdminGroup;
use Validator;
use Illuminate\Validation\Rule;

class PowerController extends CategoryController
{
  public function __construct(Power $Power)
  {
    parent::__construct($Power);
    $this->prefix = 'power';
    $this->prefix_after = '';
    $this->valid_attrs = [
      'title' => '權限名稱',
      'active' => '客戶啟用狀態',
      'icon' => 'ICON',
      'divider_after' => '後面加分隔線',
    ];
  }
  /**
   * 分類列表頁的狀態要代入的參數
   */
  protected function getIndexStatus($param = null)
  {
    $fields = [
      'name' => 'active',
      'options' => $this->model->present()->active(),
    ];
    if (is_null($param)) {
      return $fields;
    } else {
      return array_key_exists($param, $fields)?$fields[$param]:null;
    }
  }
  protected function getEditValidRules()
  {
    $rules = [
      'title' => ['required', Rule::unique($this->table)],
    ];
    return $rules;
  }
  protected function getEditFormFields()
  {
    $fields = [
      'active' => [
        'type' => 'radio',
        'name' => 'active',
        'required' => true,
        'options' => $this->model->present()->active(),
        'default' => 1,
      ],
      'title' => [
        'type' => 'text_input',
        'name' => 'title',
        'required' => true,
      ],
    ];
    if ($this->limit_level>1) {
      $fields = $this->arrayInsertAfter($fields, [
        'parent_id' => [
          'type' => 'select',
          'name' => 'parent_id',
          'required' => true,
          'options' => $this->model->getTree(null, null, 1),
        ],
      ]);
    }
    return $fields;
  }
  /**
   * 顯示修改表單
   */
  public function edit($id)
  {
    $parents =$this->model->getTree();
    $data = $this->model->find($id);
    $prefix = $this->prefix;
    $form_fields = $this->getEditFormFields();
    if (is_null($data->parent_id)) {
      $form_fields['icon'] = [
        'type' => 'text_input',
        'name' => 'icon',
        'required' => false,
      ];
      $form_fields['divider_after'] = [
        'type' => 'radio',
        'name' => 'divider_after',
        'required' => false,
        'options' => $this->model->present()->yes_or_no(),
        'default' => 0,
      ];
    }
    $valid_attrs = $this->valid_attrs;
    // dd($data);

    return view('backend.common.category.edit', compact('parents', 'data', 'prefix', 'form_fields', 'valid_attrs'));
  }
  /**
   * 更新
   */
  public function update(Request $request, $id)
  {
    $requestData = $request->all();

    $validator = $this->validator($requestData, $this->getEditValidRules(), $this->valid_msgs, $this->valid_attrs, $id);

    if ($validator->fails()) {
      return back()->withErrors($validator)->withInput();
    }

    $main_data = $this->model->find($id);

    $result1 = $main_data->fillAndSave($requestData);
    if ($result1) {
      return redirect()->route('backend.'.$this->prefix.'.index')->with('success', Lang::get('backend.save_success'));
    } else {
      return redirect()->back()->with('error', Lang::get('backend.save_error'));
    }
  }
}
