<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Lang;
use App\Setting;
use App\ServiceFaq;
use App\ServiceCategory;
use Validator;
use Carbon\Carbon;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Route;

class SiteSettingController extends SettingController
{
  public function __construct(Setting $Setting, ServiceFaq $ServiceFaq, ServiceCategory $ServiceCategory)
  {
    parent::__construct($Setting);
    $this->prefix = 'site';
    $this->faq = $ServiceFaq;
    $this->faqCategory = $ServiceCategory;
  }
  protected function getValidAttrs($routeName)
  {
    $validAttrs = [
      'backend.' . $this->prefix . '.info' => [
        'name' => Lang::get('validation.attributes.site_name'),
        'address_url' => '地址 Google map 連結',
        'fb' => 'Facebook 連結',
        'line' => 'LINE 連結',
        'line_id' => 'LINE ID',
      ],
      'backend.' . $this->prefix . '.editor' => [
        'home_editor1' => '服務項目',
        'home_editor2' => '借款流程',
        'home_editor2_pic' => '借款流程背景圖',
        'home_editor3' => '我們的優勢',
        'home_editor4' => '精選常見問答(描述文字)',
        'home_faqs' => '精選常見問答',
        'home_editor5' => '精選文章(描述文字)',
      ],
      'backend.' . $this->prefix . '.setting' => [
        'backend_name' => Lang::get('validation.attributes.site_backend_name'),
        'site_mail' => Lang::get('validation.attributes.site_mail'),
        'site_url' => Lang::get('validation.attributes.site_url'),
      ],
    ];
    return array_key_exists($routeName, $validAttrs) ? $validAttrs[$routeName] : [];
  }
  protected function getValidRules($routeName)
  {
    $validRules = [
      'backend.' . $this->prefix . '.info' => [
        'name' => 'required',
        'phone' => 'nullable|regex:/\d$/',
        // 'phone' => 'nullable|regex:/0\d{1,2}-?\d{3,4}-?\d{3,4}/',
        // 'address' => 'nullable|string',
        // 'fax' => 'nullable|regex:/\d$/',
        'email' => 'nullable|email',
        'business_hours' => 'nullable|string',
      ],
      'backend.' . $this->prefix . '.editor' => [],
      'backend.' . $this->prefix . '.setting' => [
        'site_mail' => 'nullable|emails',
        'site_url' => 'required',
        'head_code' => 'nullable',
        'body_code' => 'nullable',
      ],
      'backend.' . $this->prefix . '.smtp' => [],
    ];
    return array_key_exists($routeName, $validRules) ? $validRules[$routeName] : [];
  }
  protected function getFormFields($routeName)
  {
    $faqCategories = $this->faqCategory->where('parent_id', null)->has('faqs')->with([
      'faqs' => function ($q) {
        $q->display()->ordered();
      }
    ])->ordered()->get();
    $home_faqs = $this->faq->where('home_status', 1)->ordered()->display()->get()->pluck('id')->toArray();
    $formFields = [
      'backend.' . $this->prefix . '.info' => [
        'name' => [
          'type' => 'text_input',
          'name' => 'name',
          'required' => true,
          'common' => false,
        ],
        'phone' => [
          'type' => 'text_input',
          'name' => 'phone',
          'required' => false,
          'common' => false,
        ],
        'business_hours' => [
          'type' => 'text_input',
          'name' => 'business_hours',
          'required' => false,
          'common' => false,
        ],
        'email' => [
          'type' => 'text_input',
          'name' => 'email',
          'required' => false,
          'common' => true,
        ],
        'address' => [
          'type' => 'text_input',
          'name' => 'address',
          'required' => false,
          'common' => false,
        ],
        'address_url' => [
          'type' => 'text_input',
          'name' => 'address_url',
          'required' => false,
          'common' => false,
        ],
        'line_id' => [
          'type' => 'text_input',
          'name' => 'line_id',
          'required' => false,
          'common' => true,
        ],
        'line' => [
          'type' => 'text_input',
          'name' => 'line',
          'required' => false,
          'common' => true,
        ],
        'fb' => [
          'type' => 'text_input',
          'name' => 'fb',
          'required' => false,
          'common' => true,
        ],
      ],
      'backend.' . $this->prefix . '.editor' => [
        'home_editor1' => [
          'type' => 'text_editor',
          'name' => 'home_editor1',
          'required' => false,
          'common' => false,
        ],
        'home_editor2' => [
          'type' => 'text_editor',
          'name' => 'home_editor2',
          'required' => false,
          'common' => false,
        ],
        'home_editor2_pic' => [
          'type' => 'img',
          'name' => 'home_editor2_pic',
          'required' => false,
          'w' => 1920,
          'h' => 650,
          'folder' => $this->table,
          'common' => true,
        ],
        'home_editor3' => [
          'type' => 'text_editor',
          'name' => 'home_editor3',
          'required' => false,
          'common' => false,
        ],
        'home_editor4' => [
          'type' => 'text_editor',
          'name' => 'home_editor4',
          'required' => false,
          'common' => false,
        ],
        'home_faqs' => [
          'type' => 'include',
          'name' => 'home_faqs',
          'required' => true,
          'options' => $faqCategories,
          'selected' => $home_faqs,
          'add_first' => [
            'url' => route('backend.service.faq.create'),
          ],
          'view' => 'backend.site._form_faqs',
          'common' => true,
        ],
        'home_editor5' => [
          'type' => 'text_editor',
          'name' => 'home_editor5',
          'required' => false,
          'common' => false,
        ],
        'seo_title' => [
          'type' => 'text_input',
          'name' => 'seo_title',
          'required' => false,
          'common' => false,
        ],
        'seo_description' => [
          'type' => 'text_area',
          'name' => 'seo_description',
          'required' => false,
          'rows' => 3,
          'common' => false,
        ],
        'seo_keyword' => [
          'type' => 'text_input',
          'name' => 'seo_keyword',
          'required' => false,
          'common' => false,
        ],
        'og_title' => [
          'type' => 'text_input',
          'name' => 'og_title',
          'required' => false,
          'common' => false,
        ],
        'og_description' => [
          'type' => 'text_area',
          'name' => 'og_description',
          'required' => false,
          'rows' => 3,
          'common' => false,
        ],
        'og_image' => [
          'type' => 'img',
          'name' => 'og_image',
          'required' => false,
          'w' => null,
          'h' => null,
          'folder' => $this->table,
          'common' => true,
        ],
        'meta_robots' => [
          'type' => 'text_input',
          'name' => 'meta_robots',
          'required' => false,
          'default' => 'index, follow',
          'common' => true,
        ],
      ],
      'backend.' . $this->prefix . '.setting' => [
        'backend_name' => [
          'type' => 'text_input',
          'name' => 'backend_name',
          'required' => false,
          'common' => true,
        ],
        'site_mail' => [
          'type' => 'text_input',
          'name' => 'site_mail',
          'required' => false,
          'common' => true,
        ],
        'site_url' => [
          'type' => 'text_input',
          'name' => 'site_url',
          'required' => false,
          'common' => true,
        ],
        'head_code' => [
          'type' => 'text_area',
          'name' => 'head_code',
          'required' => false,
          'rows' => 3,
          'common' => true,
        ],
        'body_code' => [
          'type' => 'text_area',
          'name' => 'body_code',
          'required' => false,
          'rows' => 3,
          'common' => true,
        ],
      ],
      // 'backend.'.$this->prefix.'.smtp' => [
      //   'smtp' => [
      //     'type' => 'text_input',
      //     'name' => 'smtp',
      //     'required' => false,
      //     'common' => true,
      //   ],
      //   'smtp_port' => [
      //     'type' => 'text_input',
      //     'name' => 'smtp_port',
      //     'required' => false,
      //     'common' => true,
      //   ],
      //   'smtp_account' => [
      //     'type' => 'text_input',
      //     'name' => 'smtp_account',
      //     'required' => false,
      //     'common' => true,
      //   ],
      //   'smtp_password' => [
      //     'type' => 'password_input',
      //     'name' => 'smtp_password',
      //     'required' => false,
      //     'common' => true,
      //   ],
      // ],
    ];
    return array_key_exists($routeName, $formFields) ? $formFields[$routeName] : $this->defaultFields;
  }
  public function page(Request $request)
  {
    $page = Route::currentRouteName();
    $valid_rules = $this->getValidRules($page);
    $valid_msgs = $this->getValidMsgs($page);
    $valid_attrs = $this->getValidAttrs($page);
    $form_fields = $this->getFormFields($page);
    $settings = $this->model->where('page', $page)->get();
    $transAttrs = [];
    $realTransAttrs = [];
    foreach ($form_fields as $key => $form_field) {
      if (!$form_field['common']) {
        if (count($this->lang_datas) > 1) {
          $transAttrs[] = $key;
        }
        $realTransAttrs[] = $key;
      }
    }

    if (count($transAttrs)) {
      $data = $settings->groupBy('locale')->transform(function ($item, $key) {
        return $item->pluck('setting_value', 'setting_name');
      })->toArray();
    } else {
      $data['common'] = $settings->whereIn('locale', ['common', config('app.locale')])->pluck('setting_value', 'setting_name')->toArray();
    }

    if ($request->isMethod('POST')) {
      $requestData = $request->except(['home_faqs', 'home_discounts']);

      $validator = $this->validator($requestData, $valid_rules, $valid_msgs, $valid_attrs, $realTransAttrs);
      if ($validator->fails()) {
        return back()->withErrors($validator)->withInput();
      }

      // 首頁文案與SEO管理
      if ($page == 'backend.site.editor') {
        // 儲存首頁顯示的商品
        $allFaqsId = $this->faq->all()->pluck('id')->toArray();
        if ($request->has('home_faqs')) {
          $this->faq->whereIn('id', array_diff($allFaqsId, array_unique($request->home_faqs)))->update(['home_status' => 0]);
          $this->faq->whereIn('id', array_unique($request->home_faqs))->update(['home_status' => 1]);
        } else {
          $this->faq->whereIn('id', $allFaqsId)->update(['home_status' => 0]);
        }
      }

      $formFieldsLimit = collect(array_column($form_fields, 'name'))->flatten()->toArray();
      $deleteField = array_diff($settings->pluck('setting_name')->toArray(), $formFieldsLimit);
      $deleteIds = $this->model->where('page', $page)->whereIn('setting_name', $deleteField)->get()->pluck('id')->toArray();
      // dd($deleteField, $deleteIds);
      $this->model->destroy($deleteIds);

      $result = [];
      $requestData['updated_at'] = Carbon::now()->toDateTimeString();
      // dd($requestData, $form_fields, array_keys($form_fields));
      foreach ($requestData as $key => $value) {
        if (!empty($form_fields[$key]['except'])) {
          $this->except($request);
          continue;
        }
        if (in_array($key, $formFieldsLimit) || $key == 'updated_at') {
          $dataToSave = [
            'setting_value' => $value,
          ];
          if (!in_array($key, $realTransAttrs)) {
            $saveData = $this->model->where('page', $page)->where('setting_name', $key)->where('locale', 'common')->firstOrNew(['setting_name' => $key, 'page' => $page]);
          } else {
            $saveData = $this->model->where('page', $page)->where('setting_name', $key)->where('locale', config('app.locale'))->firstOrNew(['setting_name' => $key, 'locale' => config('app.locale'), 'page' => $page]);
          }

          $result[] = $saveData->fill($dataToSave)->save();
        }
        if ($key == 'lang') {
          foreach ($value as $langCode => $langData) {
            foreach ($langData as $k => $v) {
              if (in_array($k, $formFieldsLimit)) {
                $dataToSave = [
                  'setting_value' => $v,
                ];
                $saveData = $this->model->where('page', $page)->where('setting_name', $k)->where('locale', $langCode)->firstOrNew(['setting_name' => $k, 'locale' => $langCode, 'page' => $page]);
                $result[] = $saveData->fill($dataToSave)->save();
              }
            }
          }
        }
      }

      if ($result) {
        return back()->with('success', Lang::get('backend.save_success', [], env('BACKEND_LOCALE')));
      } else {
        return back()->with('error', Lang::get('backend.save_error', [], env('BACKEND_LOCALE')));
      }
    }
    // dd($data);

    return view('backend.common.setting', compact('transAttrs', 'settings', 'form_fields', 'data', 'prefix', 'valid_attrs'));
  }
}
