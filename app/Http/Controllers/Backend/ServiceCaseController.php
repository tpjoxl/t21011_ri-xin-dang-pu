<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Lang;
use App\ServiceCategory;
use App\ServiceCase;
use App\Service;
use Validator;
use Illuminate\Validation\Rule;

class ServiceCaseController extends BasicController
{
  public function __construct(ServiceCase $ServiceCase, ServiceCategory $ServiceCategory, Service $Service)
  {
    parent::__construct($ServiceCase, $ServiceCategory);
    $this->prefix = 'service.case';
    $this->rank_all = false;
    $this->relationItems = 'cases';
    $this->relationCategory = 'category';
    $this->relationCategoryKey = 'service_category_id';
  }
  protected function getEditValidRules()
  {
    $rules = $this->getValidRules();
    $rules['created_at'] = 'required';
    return $rules;
  }
  /**
   * 表單欄位
   */
  protected function getFormFields()
  {
    $fields = [
      'status' => [
        'type' => 'radio',
        'name' => 'status',
        'required' => true,
        'options' => $this->model->present()->status(),
        'default' => 1,
      ],
      'service_category_id' => [
        'type' => 'hidden',
        'name' => 'service_category_id',
        'options' => $this->category->getTree(),
        'default' => request()->service_category_id,
      ],
      'service_id' => [
        'type' => 'select',
        'name' => 'service_id',
        'required' => false,
        'options' => request()->service_category_id?$this->category->find(request()->service_category_id)->services:[],
        'live_search' => true,
      ],
      'title' => [
        'type' => 'text_input',
        'name' => 'title',
        'required' => true,
      ],
      'slug' => [
        'type' => 'text_input',
        'name' => 'slug',
        'required' => false,
      ],
      'h1' => [
        'type' => 'text_input',
        'name' => 'h1',
        'required' => false,
      ],
      'description' => [
        'type' => 'text_area',
        'name' => 'description',
        'required' => false,
        'rows' => 3,
      ],
      'text' => [
        'type' => 'text_editor',
        'name' => 'text',
        'required' => false,
      ],
      'date_range' => [
        'type' => 'date_range',
        'name' => ['date_on', 'date_off'],
        'placeholder' => [Lang::get('validation.attributes.date_on'), Lang::get('validation.attributes.date_off')],
        'required' => false,
      ],
      'banner' => [
        'type' => 'img',
        'name' => 'banner',
        'required' => false,
        'w' => 1351,
        'h' => 272,
        'folder' => $this->table
      ],
      'pic' => [
        'type' => 'img',
        'name' => 'pic',
        'required' => false,
        'w' => 360,
        'h' => 240,
        'folder' => $this->table
      ],
      'seo_title' => [
        'type' => 'text_input',
        'name' => 'seo_title',
        'required' => false,
      ],
      'seo_description' => [
        'type' => 'text_area',
        'name' => 'seo_description',
        'required' => false,
        'rows' => 3,
      ],
      'seo_keyword' => [
        'type' => 'text_input',
        'name' => 'seo_keyword',
        'required' => false,
      ],
      'og_title' => [
        'type' => 'text_input',
        'name' => 'og_title',
        'required' => false,
      ],
      'og_description' => [
        'type' => 'text_area',
        'name' => 'og_description',
        'required' => false,
        'rows' => 3,
      ],
      'og_image' => [
        'type' => 'img',
        'name' => 'og_image',
        'required' => false,
        'w' => null,
        'h' => null,
        'folder' => $this->table
      ],
      'meta_robots' => [
        'type' => 'text_input',
        'name' => 'meta_robots',
        'required' => false,
        'default' => 'index, follow',
      ],
    ];
    return $fields;
  }
  protected function getEditFormFields()
  {
    $fields = $this->getFormFields();
    $fields = $this->arrayInsertAfter($fields, [
      'created_at' => [
        'type' => 'text_input',
        'name' => 'created_at',
        'required' => true,
        'hint' => Lang::get('backend.created_at_hint'),
      ]
    ], 'text');
    return $fields;
  }
  /**
   * 搜尋欄位
   */
  protected function getSearchFields()
  {
    $fields = [
      'service_category_id' => [
        'type' => 'hidden',
        'name' => 'service_category_id',
        'default' => request()->service_category_id,
        'search' => 'equal',
      ],
      'status' => [
        'type' => 'radio',
        'name' => 'status',
        'options' => $this->model->present()->status(),
        'search' => 'equal',
      ],
      'title' => [
        'type' => 'text_input',
        'name' => 'title',
        'search' => 'like',
      ],
      'service_id' => [
        'type' => 'select',
        'name' => 'service_id',
        'options' => request()->service_category_id?$this->category->find(request()->service_category_id)->services:[],
        'search' => 'equal',
      ],
      'created_range' => [
        'type' => 'date_range',
        'name' => ['created_at1', 'created_at2'],
        'placeholder' => [Lang::get('validation.attributes.date_on'), Lang::get('validation.attributes.date_off')],
        'search' => 'range',
        'compare_fields' => ['created_at', 'created_at']
      ],
    ];
    return $fields;
  }
  /**
   * 產生列表資料結構
   */
  public function generateListData($datas)
  {
    $list_datas = [];
    foreach ($datas as $data) {
      $list_datas[$data->id] = [
        'title' => [
          'align' => 'left',
          'type' => 'link',
          'data' => $data->title,
          'url' => route('backend.'.$this->prefix.'.edit', ['id' => $data->id]),
        ],
        'service_id' => [
          'align' => 'left',
          'type' => 'text',
          'data' => $data->service_id?$data->service->title:'',
        ],
        'created_at' => [
          'align' => 'left',
          'type' => 'text',
          'data' => $data->created_at,
        ],
        'status' => [
          'align' => 'center',
          'type' => 'html',
          'options' => $this->model->present()->status(),
          'data' => $this->model->present()->status($data->status),
        ],
      ];
      if (method_exists($this->model, 'categories')) {
        $list_datas[$data->id] = $this->arrayInsertAfter($list_datas[$data->id], [
          'categories' => [
            'align' => 'left',
            'type' => 'text',
            'data' => $data->categories->implode('title', ', '),
          ],
        ], 'question');
      }
      $list_datas[$data->id]['edit'] = [
        'label' => Lang::get('backend.edit'),
        'align' => 'center',
        'type' => 'btn',
        'data' => '<span class="glyphicon glyphicon-pencil"></span>',
        'url' => route('backend.'.$this->prefix.'.edit', ['id' => $data->id]),
      ];
      $list_datas[$data->id]['delete'] = [
        'label' => Lang::get('backend.delete'),
        'align' => 'center',
        'type' => 'btn',
        'data' => '<span class="glyphicon glyphicon-trash"></span>',
        'url' => route('backend.'.$this->prefix.'.destroy', ['id' => $data->id]),
        'class' => 'btn-default btn-delete-row',
      ];
    }
    return $list_datas;
  }
  public function edit($id)
  {
    $rank_all = $this->rank_all;
    $data = $this->model->find($id);
    if (is_null($data)) {
      return redirect()->route('backend.'.$this->prefix.'.index')->with('error', Lang::get('backend.data_not_found'));
    }
    if (!is_null($this->category)) {
      $categories = $this->category->getTree();
      $category = $this->category;
    }
    $prefix = $this->prefix;
    $form_fields = $this->getEditFormFields();
    $form_fields['service_id']['options'] = $this->category->find($data->service_category_id)->services;
    $valid_attrs = $this->valid_attrs;
    // return dd($data, $categories, $category);
    $view = 'backend.'.$this->prefix.'.edit';
    if (!view()->exists($view)) {
      $view = 'backend.common.edit';
    }
    return view($view, compact('category', 'categories', 'data', 'prefix', 'rank_all', 'form_fields', 'valid_attrs'));
  }
}
