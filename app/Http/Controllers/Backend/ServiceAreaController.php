<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\ServiceArea;
use Validator;
use Illuminate\Validation\Rule;

class ServiceAreaController extends CategoryController
{
  public function __construct(ServiceArea $ServiceArea)
  {
    parent::__construct($ServiceArea);
    $this->prefix = 'service.area';
    $this->valid_attrs = [
      'title' => '地區名稱',
    ];
  }
  protected function getFormFields()
  {
    $fields = [
      'title' => [
        'type' => 'text_input',
        'name' => 'title',
        'required' => true,
      ],
    ];
    if ($this->limit_level>1) {
      $fields = $this->arrayInsertAfter($fields, [
        'parent_id' => [
          'type' => 'select',
          'name' => 'parent_id',
          'required' => true,
          'options' => $this->model->getTree(null, null, 1),
          'placeholder' => '最上層'
        ],
      ]);
    }
    return $fields;
  }
}
