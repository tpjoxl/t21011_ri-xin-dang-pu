<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Lang;
use App\Banner;
use Validator;
use Illuminate\Validation\Rule;

class BannerController extends BasicController
{
  public function __construct(Banner $Banner)
  {
    parent::__construct($Banner);
    $this->prefix = 'banner';
    $this->valid_attrs = [
      'pic1' => Lang::get('validation.attributes.pic_pc'),
      'pic2' => Lang::get('validation.attributes.pic_m'),
      'text' => '文字1',
      'text2' => '文字2',
      'text3' => '文字3',
      'text4' => '文字4',
      'btn_txt' => '連結按鈕文字',
    ];
  }
  protected function getValidRules()
  {
    $rules = [
      'title' => 'required',
      'pic1' => 'required',
      'pic2' => 'required',
      'url' => 'nullable|string',
    ];
    if (method_exists($this->model, 'categories')) {
      $rules = $this->arrayInsertAfter($rules, [
        'categories' => 'required|array',
        'categories.*' => 'required',
      ]);
    }
    return $rules;
  }
  protected function getFormFields()
  {
    $fields = [
      'status' => [
        'type' => 'radio',
        'name' => 'status',
        'required' => true,
        'options' => $this->model->present()->status(),
        'default' => 1,
      ],
      'title' => [
        'type' => 'text_input',
        'name' => 'title',
        'required' => true,
      ],
      'text' => [
        'type' => 'text_input',
        'name' => 'text',
        'required' => false,
      ],
      'text2' => [
        'type' => 'text_input',
        'name' => 'text2',
        'required' => false,
      ],
      // 'text3' => [
      //   'type' => 'text_input',
      //   'name' => 'text3',
      //   'required' => false,
      // ],
      // 'text4' => [
      //   'type' => 'text_area',
      //   'name' => 'text4',
      //   'required' => false,
      //   'rows' => 3,
      // ],
      'date_range' => [
        'type' => 'date_range',
        'name' => ['date_on', 'date_off'],
        'placeholder' => [Lang::get('validation.attributes.date_on'), Lang::get('validation.attributes.date_off')],
        'required' => false,
      ],
      'pic1' => [
        'type' => 'img',
        'name' => 'pic1',
        'required' => true,
        'w' => 1920,
        'h' => 800,
        'folder' => $this->table
      ],
      'pic2' => [
        'type' => 'img',
        'name' => 'pic2',
        'required' => true,
        'w' => 750,
        'h' => 1000,
        'folder' => $this->table
      ],
      'tag_title' => [
        'type' => 'text_input',
        'name' => 'tag_title',
        'required' => false,
      ],
      'tag_alt' => [
        'type' => 'text_input',
        'name' => 'tag_alt',
        'required' => false,
      ],
      'btn_txt' => [
        'type' => 'text_input',
        'name' => 'btn_txt',
        'required' => false,
      ],
      'url' => [
        'type' => 'text_input',
        'name' => 'url',
        'required' => false,
      ],
      'target' => [
        'type' => 'radio',
        'name' => 'target',
        'label' => '',
        'required' => false,
        'options' => $this->model->present()->target(),
        'default' => '',
        'hint' => Lang::get('backend.url_hint')
      ],
    ];
    if (method_exists($this->model, 'categories')) {
      $fields = $this->arrayInsertAfter($fields, [
        'categories' => [
          'type' => 'multi_select',
          'name' => 'categories',
          'required' => true,
          'options' => $this->category->getTree(),
          'add_first' => [
            'url' => route('backend.'.$this->model->getTable().'.category.create'),
            'title' => Lang::get('backend.category'),
          ]
        ]
      ]);
      if ($this->category->getLimitLevel()==1) {
        $fields['categories']['max'] = 1;
      }
    }
    return $fields;
  }
  protected function getSearchFields()
  {
    $fields = [
      'status' => [
        'type' => 'radio',
        'name' => 'status',
        'options' => $this->model->present()->status(),
        'search' => 'equal',
      ],
      'title' => [
        'type' => 'text_input',
        'name' => 'title',
        'search' => 'like',
      ],
      'created_range' => [
        'type' => 'date_range',
        'name' => ['created_at1', 'created_at2'],
        'placeholder' => [Lang::get('validation.attributes.date_on'), Lang::get('validation.attributes.date_off')],
        'search' => 'range',
        'compare_fields' => ['created_at', 'created_at']
      ],
    ];
    return $fields;
  }
  /**
   * 產生列表資料結構
   */
  public function generateListData($datas)
  {
    $list_datas = [];
    foreach ($datas as $data) {
      $list_datas[$data->id] = [
        'pic' => [
          'align' => 'left',
          'type' => 'link',
          'data' => '<img src="'.asset($data->pic1).'"  class="list-pic">',
          'url' => route('backend.'.$this->prefix.'.edit', ['id' => $data->id]),
        ],
        'title' => [
          'align' => 'left',
          'type' => 'link',
          'data' => $data->title,
          'url' => route('backend.'.$this->prefix.'.edit', ['id' => $data->id]),
        ],
        'created_at' => [
          'align' => 'left',
          'type' => 'text',
          'data' => $data->created_at,
        ],
        'status' => [
          'align' => 'center',
          'type' => 'html',
          'options' => $this->model->present()->status(),
          'data' => $this->model->present()->status($data->status),
        ],
      ];
      if (method_exists($this->model, 'categories')) {
        $list_datas[$data->id] = $this->arrayInsertAfter($list_datas[$data->id], [
          'categories' => [
            'align' => 'left',
            'type' => 'text',
            'data' => $data->categories->implode('title', ', '),
          ],
        ], 'title');
      }
      if (auth()->guard('admin')->user()->group->checkPowerByName($this->prefix.'.edit')) {
        $list_datas[$data->id]['edit'] = [
          'label' => Lang::get('backend.edit'),
          'align' => 'center',
          'type' => 'btn',
          'data' => '<span class="glyphicon glyphicon-pencil"></span>',
          'url' => route('backend.'.$this->prefix.'.edit', ['id' => $data->id]),
        ];
      }
      if (auth()->guard('admin')->user()->group->checkPowerByName($this->prefix.'.destroy')) {
        $list_datas[$data->id]['delete'] = [
          'label' => Lang::get('backend.delete'),
          'align' => 'center',
          'type' => 'btn',
          'data' => '<span class="glyphicon glyphicon-trash"></span>',
          'url' => route('backend.'.$this->prefix.'.destroy', ['id' => $data->id]),
          'class' => 'btn-default btn-delete-row',
        ];
      }
    }
    return $list_datas;
  }
  /**
   * 排序頁顯示的欄位資料
   */
  protected function getRankFields() {
    $fields = [
      'pic' => [
        'type' => 'img',
        'name' => 'pic1',
      ],
      'title' => [
        'type' => 'text',
        'name' => 'title',
      ],
    ];
    return $fields;
  }
}
