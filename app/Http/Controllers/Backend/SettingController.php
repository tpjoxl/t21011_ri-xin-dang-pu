<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Lang;
use App\Setting;
use Validator;
use Carbon\Carbon;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Route;

class SettingController extends Controller
{
  protected $model, $defaultFields;

  public function __construct(Setting $Setting)
  {
    $this->middleware(['auth:admin', 'auth.admin.power']);
    $this->model = $Setting;
    $this->prefix = 'setting';
    $this->lang_datas = config('translatable.locales');
    $this->defaultFields = [
      'banner' => [
        'type' => 'img',
        'name' => 'banner',
        'required' => false,
        'w' => 1920,
        'h' => 300,
        'folder' => str_replace('.', '_', $this->prefix),
        'common' => true,
      ],
      'h1' => [
        'type' => 'text_input',
        'name' => 'h1',
        'required' => false,
        'common' => false,
      ],
      'text' => [
        'type' => 'text_editor',
        'name' => 'text',
        'required' => false,
        'common' => false,
      ],
      'seo_title' => [
        'type' => 'text_input',
        'name' => 'seo_title',
        'required' => false,
        'common' => false,
      ],
      'seo_description' => [
        'type' => 'text_area',
        'name' => 'seo_description',
        'required' => false,
        'rows' => 3,
        'common' => false,
      ],
      'seo_keyword' => [
        'type' => 'text_input',
        'name' => 'seo_keyword',
        'required' => false,
        'common' => false,
      ],
      'og_title' => [
        'type' => 'text_input',
        'name' => 'og_title',
        'required' => false,
        'common' => false,
      ],
      'og_description' => [
        'type' => 'text_area',
        'name' => 'og_description',
        'required' => false,
        'rows' => 3,
        'common' => false,
      ],
      'og_image' => [
        'type' => 'img',
        'name' => 'og_image',
        'required' => false,
        'w' => null,
        'h' => null,
        'foleder' => str_replace('.', '_', $this->prefix),
        'common' => true,
      ],
      'meta_robots' => [
        'type' => 'text_input',
        'name' => 'meta_robots',
        'required' => false,
        'default' => 'index, follow',
        'common' => true,
      ],
    ];
  }

  /**
   * 表單驗證規則
   */
  public function validator($requestData, $rules, $msgs, $attrs, $realTransAttrs = null)
  {
    $valid_rules = [];
    foreach ($rules as $field => $valid_rule) {
      if (in_array($field, $realTransAttrs)) {
        foreach ($this->lang_datas as $lang_code => $lang_name) {
          if (count($this->lang_datas) > 1) {
            $valid_rules['lang.' . $lang_code . '.' . $field] = $valid_rule;
          } else {
            $valid_rules[$field] = $valid_rule;
          }
        }
      } else {
        $valid_rules[$field] = $valid_rule;
      }
    }
    $valid_msgs = $msgs;
    $valid_attrs = [];
    foreach ($rules as $field_name => $field_rule) {
      if (in_array($field_name, $realTransAttrs) && count($this->lang_datas) > 1) {
        foreach ($this->lang_datas as $lang_code => $lang_name) {
          if (array_key_exists($field_name, $attrs)) {
            $valid_attrs['lang.' . $lang_code . '.' . $field_name] = $attrs[$field_name] . '(' . $lang_name . ')';
          } else {
            $valid_attrs['lang.' . $lang_code . '.' . $field_name] = Lang::get('validation.attributes.' . $field_name) . '(' . $lang_name . ')';
          }
        }
      } else {
        if (array_key_exists($field_name, $attrs)) {
          $valid_attrs[$field_name] = $attrs[$field_name];
        } else {
          $valid_attrs[$field_name] = Lang::get('validation.attributes.' . $field_name);
        }
      }
    }
    return Validator::make($requestData, $valid_rules, $valid_msgs, $valid_attrs);
  }
  protected function getValidMsgs($routeName)
  {
    $validMsgs = [];
    return array_key_exists($routeName, $validMsgs) ? $validMsgs[$routeName] : [];
  }
  protected function getValidAttrs($routeName)
  {
    $validAttrs = [];
    return array_key_exists($routeName, $validAttrs) ? $validAttrs[$routeName] : [];
  }
  protected function getValidRules($routeName)
  {
    $validRules = [];
    return array_key_exists($routeName, $validRules) ? $validRules[$routeName] : [];
  }
  protected function getFormFields($routeName)
  {
    $formFields = [];
    return array_key_exists($routeName, $formFields) ? $formFields[$routeName] : $this->defaultFields;
  }
  public function page(Request $request)

  {
    $page = Route::currentRouteName();
    $valid_rules = $this->getValidRules($page);
    $valid_msgs = $this->getValidMsgs($page);
    $valid_attrs = $this->getValidAttrs($page);
    $form_fields = $this->getFormFields($page);
    $settings = $this->model->where('page', $page)->get();
    $transAttrs = [];
    $realTransAttrs = [];
    foreach ($form_fields as $key => $form_field) {
      if (!$form_field['common']) {
        if (count($this->lang_datas) > 1) {
          $transAttrs[] = $key;
        }
        $realTransAttrs[] = $key;
      }
    }

    if (count($transAttrs)) {
      $data = $settings->groupBy('locale')->transform(function ($item, $key) {
        return $item->pluck('setting_value', 'setting_name');
      })->toArray();
    } else {
      $data['common'] = $settings->whereIn('locale', ['common', config('app.locale')])->pluck('setting_value', 'setting_name')->toArray();
    }

    if ($request->isMethod('POST')) {
      $requestData = $request->all();

      $validator = $this->validator($requestData, $valid_rules, $valid_msgs, $valid_attrs, $realTransAttrs);
      if ($validator->fails()) {
        return back()->withErrors($validator)->withInput();
      }

      $formFieldsLimit = collect(array_column($form_fields, 'name'))->flatten()->toArray();
      $deleteField = array_diff($settings->pluck('setting_name')->toArray(), $formFieldsLimit);
      $deleteIds = $this->model->where('page', $page)->whereIn('setting_name', $deleteField)->get()->pluck('id')->toArray();
      // dd($deleteField, $deleteIds);
      $this->model->destroy($deleteIds);

      $result = [];
      $requestData['updated_at'] = Carbon::now()->toDateTimeString();
      // dd($requestData, $form_fields, array_keys($form_fields));
      foreach ($requestData as $key => $value) {
        if (in_array($key, $formFieldsLimit) || $key == 'updated_at') {
          $dataToSave = [
            'setting_value' => $value,
          ];
          if (!in_array($key, $realTransAttrs)) {
            $saveData = $this->model->where('page', $page)->where('setting_name', $key)->where('locale', 'common')->firstOrNew(['setting_name' => $key, 'page' => $page]);
          } else {
            $saveData = $this->model->where('page', $page)->where('setting_name', $key)->where('locale', config('app.locale'))->firstOrNew(['setting_name' => $key, 'locale' => config('app.locale'), 'page' => $page]);
          }

          $result[] = $saveData->fill($dataToSave)->save();
        }
        if ($key == 'lang') {
          foreach ($value as $langCode => $langData) {
            foreach ($langData as $k => $v) {
              if (in_array($k, $formFieldsLimit)) {
                $dataToSave = [
                  'setting_value' => $v,
                ];
                $saveData = $this->model->where('page', $page)->where('setting_name', $k)->where('locale', $langCode)->firstOrNew(['setting_name' => $k, 'locale' => $langCode, 'page' => $page]);
                $result[] = $saveData->fill($dataToSave)->save();
              }
            }
          }
        }
      }

      if ($result) {
        return back()->with('success', Lang::get('backend.save_success'));
      } else {
        return back()->with('error', Lang::get('backend.save_error'));
      }
    }
    // dd($data);

    return view('backend.common.setting', compact('transAttrs', 'settings', 'form_fields', 'data', 'prefix', 'valid_attrs'));
  }
}
