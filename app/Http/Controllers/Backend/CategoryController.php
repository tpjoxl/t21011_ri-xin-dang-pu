<?php
namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Lang;
use App\Category;
use Validator;
use Illuminate\Validation\Rule;

class CategoryController extends Controller
{
  protected $limit_level;

  public function __construct($Category)
  {
    parent::__construct($Category);
    $this->limit_level = $Category->getLimitLevel();
    $this->prefix = 'common.category';
    $this->valid_attrs = [
      'title' => Lang::get('validation.attributes.category_title')
    ];
    $this->data = $this->model->make();
  }
  /**
   * 表單驗證的規則
   */
  protected function getCreateValidRules()
  {
    return $this->getValidRules();
  }
  protected function getEditValidRules()
  {
    return $this->getValidRules();
  }
  protected function getValidRules()
  {
    $rules = [
      'title' => ['required', Rule::unique($this->table), 'max:191'],
      'slug' => ['nullable', Rule::unique($this->table), 'max:191'],
      'seo_title' => 'nullable|max:191',
      'seo_description' => 'nullable|max:3000',
      'seo_keyword' => 'nullable|max:3000',
      'og_title' => 'nullable|max:191',
      'og_description' => 'nullable|max:3000',
    ];
    return $rules;
  }
  /**
   * 表單欄位
   */
  protected function getCreateFormFields() {
    return $this->getFormFields();
  }
  protected function getEditFormFields() {
    return $this->getFormFields();
  }
  protected function getFormFields()
  {
    $fields = [
      'status' => [
        'type' => 'radio',
        'name' => 'status',
        'required' => true,
        'options' => $this->model->present()->status(),
        'default' => 1,
      ],
      'title' => [
        'type' => 'text_input',
        'name' => 'title',
        'required' => true,
      ],
      'slug' => [
        'type' => 'text_input',
        'name' => 'slug',
        'required' => false,
      ],
      // 'pic' => [
      //   'type' => 'img',
      //   'name' => 'pic',
      //   'required' => false,
      //   'w' => null,
      //   'h' => null,
      //   'folder' => $this->table
      // ],
      'seo_title' => [
        'type' => 'text_input',
        'name' => 'seo_title',
        'required' => false,
      ],
      'seo_description' => [
        'type' => 'text_area',
        'name' => 'seo_description',
        'required' => false,
        'rows' => 3,
      ],
      'seo_keyword' => [
        'type' => 'text_input',
        'name' => 'seo_keyword',
        'required' => false,
      ],
      'og_title' => [
        'type' => 'text_input',
        'name' => 'og_title',
        'required' => false,
      ],
      'og_description' => [
        'type' => 'text_area',
        'name' => 'og_description',
        'required' => false,
        'rows' => 3,
      ],
      'og_image' => [
        'type' => 'img',
        'name' => 'og_image',
        'required' => false,
        'w' => null,
        'h' => null,
        'folder' => $this->table
      ],
      'meta_robots' => [
        'type' => 'text_input',
        'name' => 'meta_robots',
        'required' => false,
        'default' => 'index, follow',
      ],
    ];
    if ($this->limit_level>1) {
      $fields = $this->arrayInsertAfter($fields, [
        'parent_id' => [
          'type' => 'select',
          'name' => 'parent_id',
          'required' => true,
          'options' => $this->model->getTree(null, null, 1),
          'placeholder' => '最上層'
        ],
      ]);
    }
    return $fields;
  }
  /**
   * 分類列表頁的狀態要代入的參數
   */
  protected function getIndexStatus($param = null)
  {
    $fields = [
      'name' => 'status',
      'options' => $this->model->present()->status(),
    ];
    if (is_null($param)) {
      return $fields;
    } else {
      return array_key_exists($param, $fields)?$fields[$param]:null;
    }
  }
  /**
   * 顯示列表
   */
  public function index()
  {
    $model = $this->model;
    $datas = $this->model->getTree();
    $prefix = $this->prefix;
    $valid_attrs = $this->valid_attrs;
    $menu_status = $this->getIndexStatus();

    $view = 'backend.'.$this->prefix.'.index';
    if (!view()->exists($view)) {
      $view = 'backend.common.category.index';
    }
    return view($view, compact('datas', 'model', 'prefix', 'menu_status', 'valid_attrs'));
  }
  /**
   * 顯示新增表單頁
   */
  public function create(Request $request)
  {
    if($request->has('parent_id')) {
      $lv= $this->model->find($request->input('parent_id'))->level;
      if ($lv>=$this->limit_level)
      return redirect()->route('backend.'.$this->prefix.'.create')->with('error', Lang::get('backend.level_error', ['limit'=>$this->limit_level]));
    }
    $parents =$this->model->getTree();

    $this->data = $data = $this->model->make();
    $prefix = $this->prefix;
    $form_fields = $this->getCreateFormFields();
    $valid_attrs = $this->valid_attrs;

    $view = 'backend.'.$this->prefix.'.create';
    if (!view()->exists($view)) {
      $view = 'backend.common.category.create';
    }
    return view($view, compact('parents', 'data', 'prefix', 'form_fields', 'valid_attrs'));
  }
  /**
   * 儲存
   */
  public function store(Request $request)
  {
    $requestData = $request->all();

    $validator = $this->validator($requestData, $this->getCreateValidRules(), $this->valid_msgs, $this->valid_attrs);
    if ($validator->fails()) {
      return back()->withErrors($validator)->withInput();
    }

    $this->data = $main_data = $this->model->make();
    $result1 = $main_data->fillAndSave($requestData);

    if ($result1) {
      return redirect()->route('backend.'.$this->prefix.'.index')->with('success', Lang::get('backend.create_success'));
    } else {
      return redirect()->back()->with('error', Lang::get('backend.create_error'));
    }
  }
  /**
   * 顯示修改表單
   */
  public function edit($id)
  {
    $parents =$this->model->getTree();
    $this->data = $data = $this->model->find($id);
    $prefix = $this->prefix;
    $form_fields = $this->getEditFormFields();
    $valid_attrs = $this->valid_attrs;

    $view = 'backend.'.$this->prefix.'.edit';
    if (!view()->exists($view)) {
      $view = 'backend.common.category.edit';
    }
    return view($view, compact('parents', 'data', 'prefix', 'form_fields', 'valid_attrs'));
  }
  /**
   * 更新
   */
  public function update(Request $request, $id)
  {
    $requestData = $request->all();

    $validator = $this->validator($requestData, $this->getEditValidRules(), $this->valid_msgs, $this->valid_attrs, $id);

    if ($validator->fails()) {
      return back()->withErrors($validator)->withInput();
    }

    $this->data = $main_data = $this->model->find($id);

    $result1 = $main_data->fillAndSave($requestData);
    if ($result1) {
      return redirect()->route('backend.'.$this->prefix.'.index')->with('success', Lang::get('backend.save_success'));
    } else {
      return redirect()->back()->with('error', Lang::get('backend.save_error'));
    }
  }
  /**
   * 刪除
   */
  public function destroy(Request $request)
  {
    $ids = explode(",",$request->id);

    $result = $this->model->destroy($ids);
    if($result) {
      return redirect()->route('backend.'.$this->prefix.'.index')->with('success', Lang::get('backend.delete_success'));
    } else {
      return redirect()->back()->with('error', Lang::get('backend.delete_error'));
    }
  }
  /**
   * 修改狀態
   */
  public function setStatus(Request $request, $val)
  {
    $ids = explode(",",$request->id);
    $result = $this->model->whereIn('id', $ids)->update([$this->getIndexStatus('name') => $val]);
    if ($result) {
      return redirect()->back()->with('success', $result.'筆資料狀態已更新');
    } else {
      return redirect()->back()->with('error', '無資料更新');
    }
  }
  /**
   * 更新排序&階層
   */
  private $msg = '';
  private $rankUpdate = 0;
  private $levelUpdate = 0;
  public function rankUpdate(Request $request)
  {
    $this->msg='';
    $msg = '';
    $this->rankUpdate=0;
    $this->levelUpdate=0;

    $tree = json_decode($request->sortNum);
    $rankTree = $this->array_flatten($tree);
    $levelTree = $this->update_level($tree);
    $this->levelUpdate += $this->model->whereIn('id', $levelTree)->update(['parent_id'=> NULL, 'level'=>1]);

    foreach ($rankTree as $key => $id) {
      $this->rankUpdate += $this->model->findOrFail($id)->update(['rank'=> $key+1]);
    }
    if ($this->rankUpdate && $this->levelUpdate) {
      $msg = Lang::get('backend.rank_level_success');
    } else {
      if ($this->rankUpdate) {
        $msg = Lang::get('backend.rank_success');
      }
      if ($this->levelUpdate) {
        $msg = Lang::get('backend.level_success');
      }
    }
    // return dd($tree, $rankTree, $levelTree, $msg, $this->msg);

    if ($this->rankUpdate || $this->levelUpdate) {
      if ($this->msg!='') {
        return back()->with('success', $msg)->with('error', $this->msg);
      } else {
        return back()->with('success', $msg);
      }
    } else {
      if ($this->msg!='') {
        return back()->with('error', $this->msg);
      } else {
        return back()->with('error', Lang::get('backend.rank_level_error'));
      }
    }
  }
  private function array_flatten($array) {
    if (!is_array($array)) {
      return false;
    }
    $result = array();
    foreach ($array as $value) {
      array_push($result,$value->id);
      if (property_exists($value, 'children')) {
        $result = array_merge($result, $this->array_flatten($value->children));
      }
    }
    return $result;
  }
  private function update_level($array, $lv = 0, $parent = 0) {
    $level = $lv;
    if (!is_array($array)) {
      return false;
    }
    $result = array();
    foreach ($array as $value) {
      array_push($result,$value->id);
      if (property_exists($value, 'children')) {
        $level = $lv + 1;
        if ($level >= $this->limit_level) {
          $parent_id = $parent;
          $this->msg = Lang::get('backend.level_max_error', ['limit'=>$this->limit_level]);
          $result = array_merge($result, $this->update_level($value->children, $level, $parent_id));
        } else {
          $parent_id = $value->id;
          $this->levelUpdate += $this->model->whereIn('id', $this->update_level($value->children, $level))->update(['parent_id'=> $parent_id, 'level' => $level+1]);
        }
      }
    }
    return $result;
  }
}
