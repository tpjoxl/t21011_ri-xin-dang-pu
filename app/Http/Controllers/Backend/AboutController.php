<?php
namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Lang;
use App\Setting;
use Illuminate\Support\Facades\Route;

class AboutController extends SettingController
{
  public function __construct(Setting $Setting)
  {
    parent::__construct($Setting);
    $this->prefix = 'about';
  }
}
