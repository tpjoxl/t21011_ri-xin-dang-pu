<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Lang;
use App\Admin;
use App\AdminGroup;
use Validator;
use Illuminate\Validation\Rule;
use Hash;
use Auth;

class AdminController extends BasicController
{
  public function __construct(Admin $Admin, AdminGroup $AdminGroup)
  {
    parent::__construct($Admin, $AdminGroup);
    $this->prefix = 'admin';
    $this->rank_all = true;
    $this->valid_attrs = [
      'group_id' => Lang::get('validation.attributes.power_group'),
      'name' => Lang::get('validation.attributes.username'),
      'passable' => Lang::get('validation.attributes.passable', [], env('BACKEND_LOCALE')),
    ];
  }
  /**
   * 表單驗證的規則
   */
  protected function getValidRules()
  {
    $rules = [
      'group_id' => 'required',
      'account' => 'required|max:150|unique:'.$this->table,
      'password' => 'required|min:6|confirmed',
      'name' => 'required|max:255',
    ];
    return $rules;
  }
  protected function getEditValidRules()
  {
    $rules = [
      'group_id' => 'required',
      'name' => 'required|max:255',
    ];
    return $rules;
  }
  /**
   * 表單欄位
   */
  protected function getFormFields()
  {
    $fields = [
      'status' => [
        'type' => 'radio',
        'name' => 'status',
        'required' => true,
        'options' => $this->model->present()->active(),
        'default' => 1,
      ],
      'group_id' => [
        'type' => 'include',
        'name' => 'group_id',
        'view' => 'backend.admin._form_group_select',
      ],
      'account' => [
        'type' => 'text_input',
        'name' => 'account',
        'required' => true,
      ],
      'password' => [
        'type' => 'password_input',
        'name' => 'password',
        'required' => true,
      ],
      'password_confirmation' => [
        'type' => 'password_input',
        'name' => 'password_confirmation',
        'required' => true,
        'error_name' => 'password',
      ],
      'name' => [
        'type' => 'text_input',
        'name' => 'name',
        'required' => true,
      ],
    ];
    return $fields;
  }
  /**
   * 搜尋欄位
   */
  protected function getSearchFields()
  {
    $fields = [
      'status' => [
        'type' => 'radio',
        'name' => 'status',
        'options' => $this->model->present()->active(),
        'search' => 'equal',
      ],
      'group_id' => [
        'type' => 'include',
        'name' => 'group_id',
        'view' => 'backend.admin._search_group_select',
        'search' => 'equal',
      ],
      'account' => [
        'type' => 'text_input',
        'name' => 'account',
        'search' => 'like',
      ],
      'name' => [
        'type' => 'text_input',
        'name' => 'name',
        'search' => 'like',
      ],
      'login_range' => [
        'type' => 'date_range',
        'name' => ['login_at1', 'login_at2'],
        'placeholder' => [Lang::get('validation.attributes.date_on'), Lang::get('validation.attributes.date_off')],
        'search' => 'range',
        'compare_fields' => ['login_at', 'login_at']
      ],
    ];
    return $fields;
  }
  protected function getSearchBasicDatas() {
    $masterGroupId = $this->category->where('title', env('MASTER_GROUP'))->pluck('id')->first();
    $datas = $this->model->ordered();
    if (auth()->guard('admin')->user()->group->title != env('MASTER_GROUP')) {
      $datas->where('group_id', '!=', $masterGroupId);
    }
    return $datas;
  }
  /**
   * 產生列表資料結構
   */
  public function generateListData($datas)
  {
    $list_datas = [];
    foreach ($datas as $data) {
      $list_datas[$data->id] = [
        'account' => [
          'align' => 'left',
          'type' => 'link',
          'data' => $data->account,
          'url' => route('backend.'.$this->prefix.'.edit', ['id' => $data->id]),
        ],
        'name' => [
          'align' => 'left',
          'type' => 'text',
          'data' => $data->name,
        ],
        'power_group' => [
          'align' => 'left',
          'type' => 'text',
          'data' => $data->group->title,
        ],
        'login_at' => [
          'align' => 'left',
          'type' => 'text',
          'data' => $data->login_at,
        ],
        'status' => [
          'align' => 'center',
          'type' => 'html',
          'options' => $this->model->present()->active(),
          'data' => $this->model->present()->active($data->status),
        ],
      ];
      if (auth()->guard('admin')->user()->group->checkPowerByName($this->prefix.'.edit')) {
        $list_datas[$data->id]['edit'] = [
          'label' => Lang::get('backend.edit'),
          'align' => 'center',
          'type' => 'btn',
          'data' => '<span class="glyphicon glyphicon-pencil"></span>',
          'url' => route('backend.'.$this->prefix.'.edit', ['id' => $data->id]),
        ];
      }
      if (auth()->guard('admin')->user()->group->checkPowerByName($this->prefix.'.destroy')) {
        $list_datas[$data->id]['delete'] = [
          'label' => Lang::get('backend.delete'),
          'align' => 'center',
          'type' => 'btn',
          'data' => '<span class="glyphicon glyphicon-trash"></span>',
          'url' => route('backend.'.$this->prefix.'.destroy', ['id' => $data->id]),
          'class' => 'btn-default btn-delete-row',
        ];
      }
    }
    return $list_datas;
  }
  /**
   * 顯示修改表單
   */
  public function edit($id)
  {
    $rank_all = $this->rank_all;
    $data = $this->model->find($id);
    if (is_null($data)) {
      return redirect()->route('backend.'.$this->prefix.'.index')->with('error', Lang::get('backend.data_not_found'));
    }
    // 非最大權限群組內的帳號，不能修改最大權限群組內的帳號資料
    if ($data->group->title == env('MASTER_GROUP') && auth()->guard('admin')->user()->group->title != env('MASTER_GROUP')) {
      return redirect()->route('backend.'.$this->prefix.'.index')->with('error', Lang::get('backend.no_power'));
    }
    if (!is_null($this->category)) {
      $categories = $this->category->getTree();
      $category = $this->category;
    }
    $prefix = $this->prefix;
    $form_fields = $this->getEditFormFields();
    $form_fields['account']['type'] = 'text';
    $form_fields = $this->arrayExcept($form_fields, ['password', 'password_confirmation']);
    $valid_attrs = $this->valid_attrs;
    // return dd($data, $categories, $category);
    $passableid = auth()->guard('admin')->user()->id;
    if ($passableid==1) {
      $form_fields['passable'] = [
        'type' => 'radio',
        'name' => 'passable',
        'required' => true,
        'options' => $this->model->present()->yes_or_no(),
        'default' => 0,
      ];
    }

    return view('backend.admin.edit', compact('category', 'categories', 'data', 'prefix', 'rank_all', 'form_fields', 'valid_attrs'));
  }
  /**
   * 密碼修改
   */
  public function password(Request $request, $id)
  {
    $auth_admin = auth()->guard('admin')->user();
    // 表單驗證規則
    $validator = Validator::make($request->all(), [
      'old_password' => $auth_admin->passable==0?'required':'nullable',
      'password' => 'required|min:6|confirmed',
    ], [], [
      'password' => Lang::get('validation.attributes.new_password'),
    ]);
    if ($validator->fails()) {
      return back()->withErrors($validator)->withInput();
    }
    $data = $this->model->find($id);
    if ($auth_admin->passable==0 && !Hash::check($request->old_password, $data->password)) {
      return redirect()->back()->with('error', Lang::get('backend.old_password_error'));
    }
    $data->password = bcrypt($request->input('password'));

    if ($data->save()) {
      return redirect()->route('backend.'.$this->prefix.'.index')->with('success', Lang::get('backend.password_success'));
    } else {
      return redirect()->route('backend.'.$this->prefix.'.index')->with('error', Lang::get('backend.password_error'));
    }
  }
}
