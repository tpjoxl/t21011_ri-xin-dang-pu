<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use Lang;
use Validator;
use Illuminate\Validation\Rule;

class Controller extends BaseController
{
  use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
  protected $model, $prefix, $table, $translatable, $lang_datas;
  protected $validData =[];
  protected $perPage = 50;

  public function __construct($Model)
  {
    $this->middleware(['auth:admin', 'auth.admin.power']);
    $this->model = $Model;
    $this->table = $Model->getTable();
    $this->trans_table = $this->table.'_translations';
    $this->lang_datas = config('translatable.locales');
    $this->valid_rules = [];
    $this->valid_msgs = [];
    $this->valid_attrs = [];
  }
  /**
   * 表單驗證規則
   */
  public function validator($requestData, $rules, $msgs, $attrs, $id = null)
  {
    $valid_rules = [];
    foreach ($rules as $field => $valid_rule) {
      if (method_exists($this->model, 'translations') && in_array($field, $this->model->translatedAttributes)) {
        foreach ($this->lang_datas as $lang_code => $lang_name) {
          if (is_array($valid_rule)) {
            $valid_rule[1] = Rule::unique($this->trans_table, $field)->where(function ($query) use ($lang_code) {
              return $query->where('locale', $lang_code);
            })->ignore($id, $this->table.'_id');
          }
          if (count($this->lang_datas)>1) {
            $valid_rules['lang.'.$lang_code.'.'.$field] = $valid_rule;
          } else {
            $valid_rules[$field] = $valid_rule;
          }
        }
      } else {
        if (is_array($valid_rule)) {
          $valid_rule[1] = $valid_rule[1]->ignore($id);
        }
        $valid_rules[$field] = $valid_rule;
      }
    }
    $valid_msgs = $msgs;
    $valid_attrs = [];
    foreach ($rules as $field_name => $field_rule) {
      if (method_exists($this->model, 'translations') && in_array($field_name, $this->model->translatedAttributes) && count($this->lang_datas)>1) {
        foreach ($this->lang_datas as $lang_code => $lang_name)  {
          if (array_key_exists($field_name, $attrs)) {
            $valid_attrs['lang.'.$lang_code.'.'.$field_name] = $attrs[$field_name].'('.$lang_name.')';
          } else {
            $valid_attrs['lang.'.$lang_code.'.'.$field_name] = Lang::get('validation.attributes.'.$field_name).'('.$lang_name.')';
          }
        }
      } else {
        if (array_key_exists($field_name, $attrs))  {
          $valid_attrs[$field_name] = $attrs[$field_name];
        } else {
          $valid_attrs[$field_name] = Lang::get('validation.attributes.'.$field_name);
        }
      }
    }
    return Validator::make($requestData, $valid_rules, $valid_msgs, $valid_attrs);
  }
  function arrayExcept($array, Array $excludeKeys){
    foreach($excludeKeys as $key){
      unset($array[$key]);
    }
    return $array;
  }
  function arrayInsertAfter($origin_fields, $insert_fields, $name=null)
  {
    $key_indices = array_flip(array_keys($origin_fields));
    if (is_null($name) || empty($name)) {
      $pos = 0;
    } else {
      $pos = $key_indices[$name]+1;
    }
    $newArr = array_slice($origin_fields, 0, $pos, true) +
              $insert_fields +
              array_slice($origin_fields, $pos, NULL, true);
    // array_splice($origin_fields, $pos, 0, $insert_fields);
    return $newArr;
  }
}
