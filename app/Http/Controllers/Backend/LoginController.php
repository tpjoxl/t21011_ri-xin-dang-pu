<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Lang;
use Auth;
use Validator;
use ReCaptcha\ReCaptcha;

class LoginController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest:admin', ['except' => 'logout']);
    }

    public function showLoginForm()
    {
        return view('backend.login');
    }

    public function login(Request $request)
    {
        // 驗證表單資料
        $validator = Validator::make($request->all(), [
            'account' => 'required',
            'password' => 'required|min:6',
            'g-recaptcha-response' => 'required'
        ], [], [
            'account' => Lang::get('validation.attributes.admin_account', [], env('BACKEND_LOCALE')),
            'password' => Lang::get('validation.attributes.admin_password', [], env('BACKEND_LOCALE')),
            'g-recaptcha-response' => Lang::get('validation.attributes.captcha', [], env('BACKEND_LOCALE'))
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($request->only('account'));
        }

        $gRecaptchaResponse = $request->input('g-recaptcha-response');
        $remoteIp = \Request::ip();
        $secret = env('RE_CAP_SECRET');

        $recaptcha = new ReCaptcha($secret);
        $resp = $recaptcha->verify($gRecaptchaResponse, $remoteIp);
        if ($resp->isSuccess()) {
            // 嘗試登入使用者
            if (Auth::guard('admin')->attempt(['account'=>$request->account, 'password'=>$request->password, 'status'=>1], $request->remember)) {
                Auth::guard('admin')->user()->update([
                    'login_at' => \Carbon\Carbon::now(),
                    'ip' => \Request::ip()
                ]);
                // 如果成功，就導向到後台首頁
                return redirect()->intended(route('backend.home'));
            }
            // 如果失敗，就回到登入頁面
            return redirect()->back()->withInput($request->only('account'))->withErrors(Lang::get('backend.login_error', [], env('BACKEND_LOCALE')));
        } else {
            $errors = $resp->getErrorCodes();
            return redirect()->back()->withInput($request->only('account'))->withErrors($errors);
        }

    }

    public function logout()
    {
        Auth::guard('admin')->logout();

        return redirect()->route('backend.login');
    }
}
