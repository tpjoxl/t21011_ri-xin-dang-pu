<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Contact;

class HomeController extends Controller
{
  public function __construct()
  {
    $this->middleware(['auth:admin']);
  }

  public function index(Request $request)
  {
    if (auth()->guard('admin')->user()->group->title = env('MASTER_GROUP')) {
      \App\AdminGroup::find(1)->powers()->sync(\App\Power::all()->pluck('id')->toArray());
    }
    $contacts = Contact::orderBy('created_at', 'desc')->get()->groupBy('status');
    $listLimit = 10;
    return view('backend.home', compact('contacts', 'listLimit'));
  }
}
