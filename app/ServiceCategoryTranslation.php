<?php

namespace App;

class ServiceCategoryTranslation extends Model
{
  public $timestamps = false;
  protected $fillable = [
    'h1',
    'h2',
    'title',
    'slug',
    'description',
    'text',
    'text2',
    'text3',
    'text4',
    'text5',
    'seo_title',
    'seo_description',
    'seo_keyword',
    'og_title',
    'og_description',
  ];
}
