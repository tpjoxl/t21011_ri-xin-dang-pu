<?php

namespace App;

class BannerTranslation extends Model
{
  public $timestamps = false;
  protected $fillable = [
    'title',
    'tag_title',
    'tag_alt',
    'url',
    'target',
    'text',
    'text2',
    'text3',
    'text4',
    'btn_txt',
  ];
}
