<?php



namespace App;



// use Illuminate\Database\Eloquent\Model as Eloquent;

use Illuminate\Support\Facades\Storage;



class ModelFile extends Model

{

    public $translatedAttributes = [
      'title',
      'description',
    ];
    protected $fillable = [
      'path',
      'icon',
      'pic',
      'status',
      'ext',
      'rank',
      'title',
      'description',
    ];
    protected $maxSize = 4; // MB
    protected $appends = ['fileName', 'picPath'];

    public function getMaxSize()
    {
      return $this->maxSize * 1024;
    }
    public function uploadFile() {
      ini_set('memory_limit','256M');
      $uploadFileArr = array();
      if (request()->hasFile('files')) {
        foreach (request()->file('files') as $image) {
          $originalName = $image->getClientOriginalName();
          $ext = $image->getClientOriginalExtension();
          // $type = $image->getClientMimeType();
          // $realPath = $image->getRealPath();
          $fileName = date('YmdHis').'-'.uniqid().'.'.$ext;
          $path = $image->storeAs($this->table, $fileName);
          // $path = $image->store('information', 'uploads');
          // return asset('uploads/'.$path);

          // 檢查資料庫是否有相同檔案名稱
          // $saveName = str_replace('.'.$ext,'',$originalName);

          array_push($uploadFileArr, ['path'=>'/uploads/'.$path, 'icon' =>$this->getFileIcon($ext), 'title' => str_replace('.'.$ext,'',$originalName), 'ext' => $ext]);
        }
      }
      // $files = Storage::files('information/'.$information_id);
      return $uploadFileArr;
    }
    public function getFileNameAttribute()
    {
      return $this->title.'.'.$this->ext;
    }
    public function deleteFile($ids) {
      $delete_files = $this->find($ids)->pluck('path')->toArray();
      foreach ($delete_files as $key => $delete_file) {
        if ($delete_file) {
          $delete_files[$key] = str_replace("uploads/","",$delete_file);
        }
      }
      $result = Storage::disk('uploads')->delete($delete_files);
      return $result;
    }

    private function getFileIcon($ext) {
      $file_icon_array = [
          'file' => 'fa fa-file-o',
          'txt'  => 'fa fa-file-text-o',
          'pdf'  => 'fa fa-file-pdf-o',
          'doc'  => 'fa fa-file-word-o',
          'docx' => 'fa fa-file-word-o',
          'xls'  => 'fa fa-file-excel-o',
          'xlsx' => 'fa fa-file-excel-o',
          'csv' => 'fa fa-file-excel-o',
          '7z'  => 'fa fa-file-archive-o',
          'rar'  => 'fa fa-file-archive-o',
          'zip'  => 'fa fa-file-archive-o',
          'gif'  => 'fa fa-file-image-o',
          'jpg'  => 'fa fa-file-image-o',
          'jpeg' => 'fa fa-file-image-o',
          'png'  => 'fa fa-file-image-o',
          'ppt'  => 'fa fa-file-powerpoint-o',
          'pptx' => 'fa fa-file-powerpoint-o',
      ];
      return array_key_exists($ext, $file_icon_array) ? $file_icon_array[$ext] : $file_icon_array['file'];
    }
}

