<?php

namespace App;

use Illuminate\Support\Collection;

class ModelCategory extends Model
{
    protected $limit_level = 1;
    protected $fillable = [
      'status',
      'home_status',
      'h1',
      'title',
      'slug',
      'description',
      'text',
      'text2',
      'pic',
      'banner',
      'list_pic',
      'seo_title',
      'seo_description',
      'seo_keyword',
      'og_title',
      'og_description',
      'og_image',
      'meta_robots',
      'rank',
      'parent_id',
      'level',
    ];
    // public $timestamps = false;
    public function scopeOrdered($query, $rank = 'rank')
    {
      if ($rank == 'rank') {
        $rank = $this->getTable() . "." . $rank;
      }
      return $query->orderBy($rank, 'asc');
    }
    public function scopeDisplay($query)
    {
      $result = $query->where($this->getTable().'.status', 1);
      if (method_exists($this, 'user_groups') && auth()->check()) {
        $result->whereHas('user_groups', function ($query) {
          $query->where('id', auth()->guard('web')->user()->group->id);
        });
      }
      return $result;
    }
    public function parent() {
      return $this->belongsTo(get_class($this), 'parent_id');
    }
    public function children() {
      return $this->hasMany(get_class($this), 'parent_id');
    }
    public function getLimitLevel() {
      return $this->limit_level;
    }
    public function fillAndSave($data) {
      $dataToSave = $this->arrayExcept($data, ['lang']);
      if (array_key_exists('parent_id',$data) && $data['parent_id']) {
        $parent = $this->find($data['parent_id']);
        if ($parent->level+1>$this->limit_level) {
          $dataToSave['parent_id'] = $parent->parent_id;
          $dataToSave['level'] = $parent->level;
        } else {
          $dataToSave['level'] = $parent->level+1;
        }
      } else {
        $dataToSave['level'] = 1;
      }
      if (count(config('translatable.locales'))>1 && method_exists($this, 'translations')) {
        if (array_key_exists('lang', $data) && array_key_exists('slug', reset($data['lang']))) {
          $data['lang'] = $this->convertToLangSlug($data['lang']);
        }

        $result = $this->fill($dataToSave)->save();
        if (array_key_exists('lang', $data)) {
          $this->saveTrans($data['lang']);
        }
      } else {
        if (array_key_exists('slug', $dataToSave)) {
          $dataToSave = $this->convertToSlug($dataToSave);
        }
        $result = $this->fill($dataToSave)->save();
      }
      
      if (array_key_exists('recommends', $data) && method_exists($this, 'recommends')) {
        $this->recommends()->sync(array_filter($data['recommends']));
      }
      return $result;
    }
    // 取得從自己開始往下的所有階層資料
    public function getTree($all_datas = null, $root = null, $limit = 0) {
      $rootId = $root;
      if (is_null($all_datas)) {
        $all = $this->ordered()->get();
      } else {
        $all = $all_datas;
      }
      if (count($all)<1) {
        return [];
      }
      $roots = $all->groupBy('parent_id');
      $tree = $this->getChildren($roots, $roots[$root], $limit);
      return $tree;
    }
    protected function getChildren($roots, $parents, $limit, $path='', $path_id = '', $path_divider = ' / ')
    {
      $arr = array();
      foreach ($parents as $key => $parent) {
        $arr[$key] = $parent->toArray();
        $arr[$key]['full_path'] = $path.$parent->title;
        $arr[$key]['full_path_id'] = $path_id.$parent->id;
        $arr[$key]['children_id'] = array($parent->id);
        if ($roots->has($parent->id) && count($children = $roots[$parent->id])>0) {
          if ($arr[$key]['level'] < $this->limit_level - $limit) {
            $arr[$key]['children'] = $this->getChildren($roots, $children, $limit, $arr[$key]['full_path'].$path_divider, $arr[$key]['full_path_id'].',');
          } else {
            $arr[$key]['children'] = array();
          }
          $arr[$key]['children_id'] = array_merge($arr[$key]['children_id'], $this->getChildrenIds($roots, $children));
          // $arr[$key]['children_id'] = array_merge($arr[$key]['children_id'], $children->pluck('id'));
        } else {
          $arr[$key]['children'] = array();
        }
        $arr[$key]['children_id'] = implode(',', $arr[$key]['children_id']);
      }
      return $arr;
    }
    // 取得從自己開始往下的所有階層ID
    // public function getTreeIds($rootId = null) {
    //   $all = $this->orderBy('rank', 'desc')->get();
    //   $roots = $all->groupBy('parent_id');
    //   $treeIds = array();
    //   if (!is_null($rootId)) {
    //     array_push($treeIds, $rootId);
    //   }
    //   if ($roots->has($rootId)) {
    //     $treeIds = array_merge($treeIds, $this->getChildrenIds($roots, $roots[$rootId]));
    //   }
    //
    //   return $treeIds;
    // }
    protected function getChildrenIds($roots, $parents)
    {
      $arr = array();
      foreach ($parents as $key => $parent) {
        array_push($arr, $parent->id);
        if ($roots->has($parent->id) && count($children = $roots[$parent->id])>0) {
          $arr = array_merge($arr, $this->getChildrenIds($roots, $children));
        }
      }
      return $arr;
    }
    // 取得從自己開始往上的所有階層ID
    // public function getPathIds($rootId = null) {
    //   $curr = $this->find($rootId);
    //   $all = $this->all();
    //   $roots = $all->keyBy('id');
    //   $treeIds = array($curr->id);
    //   // return dd($curr->parent_id);
    //   if ($curr->parent_id) {
    //     $treeIds = $this->getParentsIds($roots, $curr->parent_id);
    //     array_unshift($treeIds, $curr->id);
    //   } else {
    //     $treeIds = array($curr->id);
    //   }
    //   return $treeIds;
    // }
    // protected function getParentsIds($roots, $parent_id)
    // {
    //   $arr = array($parent_id);
    //   $parent = $roots[$parent_id];
    //   if ($parent->parent_id) {
    //     $arr = array_merge($arr, $this->getParentsIds($roots, $parent->parent_id));
    //   }
    //   return $arr;
    // }
}
