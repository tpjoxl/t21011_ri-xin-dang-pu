<?php

namespace App;

// use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;

class ModelImage extends Model
{
    protected $picW = 400;
    protected $picH = 400;
    protected $thumbW = 160;
    protected $thumbH = 160;
    protected $resize = false; // 是否等比縮放後補白邊
    protected $fillContent = false; // 當圖小於範圍時，是否等比放大
    protected $bgColor = 'rgba(0, 0, 0, 0)'; // 補白邊的背景色
    public $translatedAttributes = [
      'description',
    ];
    protected $fillable = [
      'path',
      'thumb_path',
      'parent_id',
      'is_cover',
      'rank',
      'description',
    ];
    public function uploadFile($request_files, $parent_id, $folder_name=null) {
      ini_set('memory_limit','256M');
      $folder_name = is_null($folder_name)?$this->table:$folder_name;
      $uploadFileArr = array();
      // $parent_id = request()->parent;
      foreach ($request_files as $image) {
        $originalName = $image->getClientOriginalName();
        $ext = $image->getClientOriginalExtension();
        // $type = $image->getClientMimeType();
        // $realPath = $image->getRealPath();
        $imgName = date('YmdHis').'-'.uniqid().'.'.$ext;
        if (is_null($parent_id)) {
          $path = 'uploads/'.$folder_name.'/';
          $thumbPath = 'uploads/'.$folder_name.'/thumbs/';
        } else {
          $path = 'uploads/'.$folder_name.'/'.$parent_id.'/';
          $thumbPath = 'uploads/'.$folder_name.'/'.$parent_id.'/thumbs/';
        }
        // $path = $image->store('album', 'uploads');
        // return asset('uploads/'.$path);
        if(!\File::isDirectory($path)) {
          Storage::disk('uploads')->makeDirectory(str_replace("uploads/","",$path));
        }
        if(!\File::isDirectory($thumbPath)) {
          Storage::disk('uploads')->makeDirectory(str_replace("uploads/","",$thumbPath));
        }
        $this->createThumbs($image, $path.$imgName, $this->picW, $this->picH);
        $this->createThumbs($image, $thumbPath.$imgName, $this->thumbW, $this->thumbH);

        array_push($uploadFileArr, ['path'=>$path.$imgName, 'thumb_path' =>$thumbPath.$imgName, 'description' => str_replace('.'.$ext,'',$originalName)]);
      }
      // $files = Storage::files('album/'.$parent_id);
      return $uploadFileArr;
    }

    // 另存縮圖
    public function createThumbs($image, $path, $coverW, $coverH) {
      $img = Image::make($image);
      $imgH = $img->height();
      $imgW = $img->width();
      if (is_null($coverW) && is_null($coverH)) {
        // 照原圖尺寸存檔
        $pic = Image::canvas($imgW, $imgH)->fill($this->bgColor);
        $pic->insert($img, 'center');
      } else if (is_null($coverH)) {
        // 等比縮小到不超過指定寬度
        $pic = $img->resize($coverW, null, function ($constraint) { $constraint->aspectRatio(); });
      } else {
        if ($imgW < $coverW && $imgH < $coverH) {
          // 補白邊
          $pic = Image::canvas($coverW, $coverH)->fill($this->bgColor);
          if ($this->fillContent) {
            if ($imgW - $coverW > $imgH - $coverH) {
              $resizeImg = $img->resize($coverW, null, function ($constraint) { $constraint->aspectRatio(); });
            } else {
              $resizeImg = $img->resize(null, $coverH, function ($constraint) { $constraint->aspectRatio(); });
            }
            if ($resizeImg->width() - $coverW > $resizeImg->height() - $coverH) {
              $resizeImg = $resizeImg->resize($coverW, null, function ($constraint) { $constraint->aspectRatio(); });
            } else {
              $resizeImg = $resizeImg->resize(null, $coverH, function ($constraint) { $constraint->aspectRatio(); });
            }
            $pic->insert($resizeImg, 'center');
          } else {
            $pic->insert($img, 'center');
          }
        } else {
          if ($this->resize) {
            if ($imgW - $coverW > $imgH - $coverH) {
              $resizeImg = $img->resize($coverW, null, function ($constraint) { $constraint->aspectRatio(); });
            } else {
              $resizeImg = $img->resize(null, $coverH, function ($constraint) { $constraint->aspectRatio(); });
            }
            if ($resizeImg->width() - $coverW > $resizeImg->height() - $coverH) {
              $resizeImg = $resizeImg->resize($coverW, null, function ($constraint) { $constraint->aspectRatio(); });
            } else {
              $resizeImg = $resizeImg->resize(null, $coverH, function ($constraint) { $constraint->aspectRatio(); });
            }
            $pic = Image::canvas($coverW, $coverH)->fill($this->bgColor);
            $pic->insert($resizeImg, 'center');
          } else {
            // 裁切填滿範圍
            $pic = $img->fit($coverW, $coverH);
          }
        }
      }
      $result = $pic->save($path);
      $pic->destroy();

      if ($result) {
        return true;
      } else {
        return false;
      }
    }

    public function deleteFile($ids) {
      $path = $this->find($ids)->pluck('path')->toArray();
      $thumb_path = $this->find($ids)->pluck('thumb_path')->toArray();
      $delete_files = array_merge($path, $thumb_path);
      foreach ($delete_files as $key => $delete_file) {
        $delete_files[$key] = str_replace("uploads/","",$delete_file);
      }
      $result = Storage::disk('uploads')->delete($delete_files);
      $this->destroy($ids);

      return $result;
    }
    public function setCover($id, $parent_id) {
      $img = $this->find($id);
      // $parent_id = $img->getAlbum->id;

      $result = $img->update(['is_cover' => 1]);
      $this->where('parent_id', $parent_id)
          ->where('id', '!=', $id)
          ->update(['is_cover' => 0]);
      return $result;
    }
    public function updateCover($parent_id) {
      $imgs = $this->where('parent_id', $parent_id)->get();
      $cover = $imgs->groupBy('is_cover');
      if ($imgs->count() > 0 && $cover->has(0) && $cover[0]->count() == $imgs->count()) {
        $imgs->first()->update(['is_cover' => 1]);
      }
    }
}
