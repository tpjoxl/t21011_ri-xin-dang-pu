<?php

namespace App;

use Illuminate\Database\Eloquent\Model as Eloquent;
use Laracasts\Presenter\PresentableTrait;
use Carbon\Carbon;

class Model extends Eloquent
{
  use PresentableTrait;
  protected $presenter = 'App\Presenters\Presenter';
  // protected $guarded = [];
  protected $fillable = [
    'status',
    'home_status',
    'h1',
    'title',
    'slug',
    'url',
    'description',
    'text',
    'text2',
    'date_on',
    'date_off',
    'banner',
    'pic',
    'banner',
    'list_pic',
    'seo_title',
    'seo_description',
    'seo_keyword',
    'og_title',
    'og_description',
    'og_image',
    'meta_robots',
    'rank',
    'created_at',
  ];
  public $translatedAttributes = [
    'h1',
    'title',
    'slug',
    'description',
    'text',
    'text2',
    'seo_title',
    'seo_description',
    'seo_keyword',
    'og_title',
    'og_description',
  ];
  public function getTransAttrs()
  {
    if (count(config('translatable.locales')) < 2 || !method_exists($this, 'translations')) {
      return array();
    } else {
      return $this->translatedAttributes;
    }
  }
  public function scopeOrdered($query, $rank = 'rank')
  {
    if ($rank == 'rank') {
      $rank = $this->getTable() . "." . $rank;
    }
    return $query->orderBy($rank, 'asc')
      ->orderBy('created_at', 'desc');
  }
  public function scopeDisplay($query)
  {
    if (method_exists($this, 'categories')) {
      $result = $query->whereHas('categories', function ($query) {
        $query->display();
      });
    } else {
      $result = $query;
    }

    return $result->where($this->getTable().'.status', 1)
      ->where(function ($q) {
          $q->whereNull('date_on')
                ->orWhere('date_on', '<=', Carbon::today()->toDateString());
      })
      ->where(function ($q) {
          $q->whereNull('date_off')
                ->orWhere('date_off', '>=', Carbon::today()->toDateString());
      });
  }
  public function getPicPathAttribute()
  {
    if ($this->pic) {
      return $this->pic;
    } else {
      return 'images/'.$this->table.'_default.png';
    }
  }

  public function fillAndSave($data)
  {
    $dataToSave = $this->arrayExcept($data, ['lang', 'categories']);

    // 如果置頂功能沒有勾選就恢復原始排序
    if ($this->setTop && !array_key_exists('rank', $dataToSave)) {
      $dataToSave['rank'] = 1;
    }

    // 如果有語系就儲存關聯
    if (count(config('translatable.locales'))>1 && method_exists($this, 'translations')) {
      if (array_key_exists('lang', $data) && array_key_exists('slug', reset($data['lang']))) {
        $data['lang'] = $this->convertToLangSlug($data['lang']);
      }

      $result = $this->fill($dataToSave)->save();
      if (array_key_exists('lang', $data)) {
        $this->saveTrans($data['lang']);
      }
    } else {
      if (array_key_exists('slug', $dataToSave)) {
        $dataToSave = $this->convertToSlug($dataToSave);
      }
      $result = $this->fill($dataToSave)->save();
    }
    // 如果有分類就儲存關聯
    if (array_key_exists('categories', $data) && method_exists($this, 'categories')) {
      if ($this->setTop && !array_key_exists('rank', $dataToSave)) {
        $cate = [];
        foreach (array_filter($data['categories']) as $key => $cate_arr) {
          $cate[$cate_arr] = ['rank' => $dataToSave['rank']];
        }
        $this->categories()->sync($cate);
      } else {
        $this->categories()->sync(array_filter($data['categories']));
      }
    }
    if (array_key_exists('recommends', $data) && method_exists($this, 'recommends')) {
      $this->recommends()->sync(array_filter($data['recommends']));
    }
    if (array_key_exists('services', $data) && method_exists($this, 'services')) {
      $this->services()->sync(array_filter($data['services']));
    }
    if (array_key_exists('tags', $data) && method_exists($this, 'tags')) {
      $this->tags()->sync(array_filter($data['tags']));
    }
    return $result;
  }
  public function updateRows($ids, $datas)
  {
    return $this->whereIn('id', $ids)->update($datas);
  }
  public function saveTrans($lang_datas)
  {
    foreach ($lang_datas as $lang_code => $lang_data) {
      $this->translateOrNew($lang_code)->fill($lang_data);
    }
    return $this->save();
  }
  protected function convertToUrlEncode($str)
  {
    // 把字串內的符號半形轉全形
    $nft = array(
      "(", ")", "[", "]", "{", "}", ",", ";", ":",
      "?", "!", "@", "#", "$", "%", "&", "|", "\\",
      "/", "+", "=", "*", "~", "`", "'", "\"", "<", ">",
      "^"
    );
    $wft = array(
      "（", "）", "〔", "〕", "｛", "｝", "，", "；", "：",
      "？", "！", "＠", "＃", "＄", "％", "＆", "｜", "＼",
      "／", "＋", "＝", "＊", "～", "、", "、", "＂", "＜", "＞",
      "︿"
    );
    $str = str_replace($nft, $wft, $str);

    // 把字串內的空格轉成-
    $str = preg_replace('/\s+/', '-', $str);
    // 把字串內的大寫轉小寫
    return strtolower($str);
    //把字串內的空格轉成-，?轉成？，把大寫轉小寫，然後再轉成URL編碼
    // return strtolower(preg_replace('/\?+/', '？', preg_replace('/\s+/', '-', $str)));
    // return rawurlencode(strtolower(preg_replace('/\s+/', '-', $str)));
  }

  /**
   * 自動抓資料產生slug
   */
  protected function convertToSlug($requestData, $field = 'title')
  {
    // 如果URL KEY沒有設定，就自動拿title轉，有的話一樣也要檢查是否有空白字元或英文都要小寫
    if (!isset($requestData['slug']) || empty($requestData['slug'])) {
      $requestData['slug'] = $this->convertToUrlEncode($requestData[$field]);
    } else {
      $requestData['slug'] = $this->convertToUrlEncode($requestData['slug']);
    }
    return $requestData;
  }
  protected function convertToLangSlug($langDatas, $field = 'title')
  {
    foreach ($langDatas as $lang_code => $lang_data) {
      if (!isset($lang_data['slug']) || empty($lang_data['slug'])) {
        $langDatas[$lang_code]['slug'] = $this->convertToUrlEncode($lang_data[$field]);
      } else {
        $langDatas[$lang_code]['slug'] = $this->convertToUrlEncode($lang_data['slug']);
      }
    }
    return $langDatas;
  }
  function arrayExcept($array, Array $excludeKeys){
    foreach($excludeKeys as $key){
      unset($array[$key]);
    }
    return $array;
  }
  // public function getRouteKeyName() {
  //   return 'slug';
  // }
}

