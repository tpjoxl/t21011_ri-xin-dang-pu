<?php

namespace App;

class Power extends ModelCategory
{
  public $timestamps = false;
  protected $table = 'power';
  protected $limit_level = 2;
  protected $fillable = [
    'title',
    'name',
    'parent_id',
    'status',
    'rank',
    'icon',
    'route_prefix',
    'route_name',
    'url_param',
    'active',
    'level',
    'divider_after',
  ];

  public function admin_groups() {
    return $this->belongsToMany(AdminGroup::class, 'admin_group_power_relation', 'power_id', 'admin_group_id');
  }
  public function scopeOrdered($query, $rank = 'rank')
  {
    return $query->orderBy($rank, 'asc');
  }
}
