<?php

namespace App;

use Dimsav\Translatable\Translatable;

class Banner extends Model
{
  use Translatable;
  protected $with = ['translations'];
  protected $table = 'banner';
  protected $fillable = [
    'status',
    'title',
    'pic1',
    'pic2',
    'date_on',
    'date_off',
    'tag_title',
    'tag_alt',
    'url',
    'target',
    'rank',
    'text',
    'text2',
    'text3',
    'text4',
    'btn_txt',
  ];
  public $translatedAttributes = [
    'title',
    'tag_title',
    'tag_alt',
    'url',
    'target',
    'text',
    'text2',
    'text3',
    'text4',
    'btn_txt',
  ];
  public function scopeOrdered($query, $rank = 'rank')
  {
    return $query->orderBy($rank, 'asc')
      ->orderBy('created_at', 'asc');
  }
}
