<?php



namespace App\Providers;



use Illuminate\Support\ServiceProvider;

use Illuminate\Support\Facades\Schema;

use Illuminate\Support\Facades\Validator;

use Illuminate\Http\Request;

use App\Power;

use App\Setting;

use Carbon\Carbon;


class BackendServiceProvider extends AppServiceProvider

{

    /**

     * Bootstrap any application services.

     *

     * @return void

     */

    public function boot()

    {

      // $this->siteinfo = SiteInfo::first();
      // $this->sitesetting = SiteSetting::first();
      $this->setting = Setting::getData('site');
      $this->lang_datas = config('translatable.locales');
      // $this->siteinfo = \Cache::rememberForever('siteinfo', function () {
      //     return SiteInfo::first();
      // });
      // $this->sitesetting = \Cache::rememberForever('sitesetting', function () {
      //     return SiteSetting::first();
      // });

      config([
        'app.name' => $this->setting->name,
        'app.url' => $this->setting->site_url,
        'mail.from.name' => $this->setting->name,
      ]);
      if ($this->setting->site_mail) {
        $emails = explode(',', preg_replace('/\s(?=)/', '', $this->setting->site_mail));
        config([
          'mail.from.address' => env('MAIL_FROM_ADDRESS', $emails[0])
        ]);
      }


      view()->composer('backend.*', function($view) {

        // $siteinfo = $this->siteinfo;

        // $sitesetting = $this->sitesetting;

        $setting = $this->setting;

        $auth_admin = auth()->guard('admin')->user();



        $view->with([

          // 'siteinfo' => $siteinfo,

          // 'sitesetting' => $sitesetting,

          'setting' => $setting,
          'auth_admin' => $auth_admin,

          'lang_datas' => $this->lang_datas
        ]);

      });



      view()->composer('backend.layouts.master', function($view) {

        $page_power = Power::where('route_name', 'like','%'.request()->route()->getName().'%')->first();

        $view->with('page_power', $page_power);

      });


      Validator::extend("emails", function($attribute, $value, $parameters, $validator) {
        $rules = [
            'email' => 'required|email',
        ];
        $email_arr = explode(',', preg_replace('/\s(?=)/', '', $value));
        foreach ($email_arr as $email) {
            $data = [
                'email' => $email
            ];
            $validator = Validator::make($data, $rules);
            if ($validator->fails()) {
                return false;
            }
        }
        return true;
      });
    }

}

