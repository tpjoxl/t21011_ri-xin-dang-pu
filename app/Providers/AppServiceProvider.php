<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Power;
use App\Setting;
use Carbon\Carbon;

class AppServiceProvider extends ServiceProvider
{
    protected $setting;
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
      Schema::defaultStringLength(191);
      // $this->siteinfo = SiteInfo::first();
      // $this->sitesetting = SiteSetting::first();
      if (request()->segment(1) == env('BACKEND_ROUTE_PREFIX')) {
        $this->app->register('App\Providers\BackendServiceProvider');
      } else {
        $this->app->register('App\Providers\FrontendServiceProvider');
      }
      // $this->menu_abouts = \App\About::where('status', 1)
      //   ->where(function ($query) {
      //       $query->whereNull('date_on')
      //             ->orWhere('date_on', '<=', Carbon::today()->toDateString());
      //   })
      //   ->where(function ($query) {
      //       $query->whereNull('date_off')
      //             ->orWhere('date_off', '>=', Carbon::today()->toDateString());
      //   })
      //   ->orderBy('rank', 'desc')
      //   ->orderBy('created_at', 'desc')
      //   ->get();
      // $this->menu_news_categories = \App\NewsCategory::where('status', 1)
      //   ->orderBy('rank', 'desc')
      //   ->get();
      // $this->menu_teacher_categories = \App\TeacherCategory::where('status', 1)
      //   ->orderBy('rank', 'desc')
      //   ->get();
      // $this->menu_course_categories = \App\CourseCategory::where('status', 1)
      //   ->orderBy('rank', 'desc')
      //   ->get();
      // $this->menu_information_categories = \App\InformationCategory::where('status', 1)
      //   ->orderBy('rank', 'desc')
      //   ->get();
      // $this->menu_admissions = \App\Admission::where('status', 1)
      //   ->where(function ($query) {
      //       $query->whereNull('date_on')
      //             ->orWhere('date_on', '<=', Carbon::today()->toDateString());
      //   })
      //   ->where(function ($query) {
      //       $query->whereNull('date_off')
      //             ->orWhere('date_off', '>=', Carbon::today()->toDateString());
      //   })
      //   ->orderBy('rank', 'desc')
      //   ->orderBy('created_at', 'desc')
      //   ->get();
      // $this->menu_link_categories = \App\LinkCategory::where('status', 1)
      //   ->orderBy('rank', 'desc')
      //   ->get();
      // $this->menu_environment_categories = \App\EnvironmentCategory::where('status', 1)
      //   ->orderBy('rank', 'desc')
      //   ->get();

      // config([
      //   'app.name' => $this->sitesetting->name,
      //   'app.url' => $this->sitesetting->url,
      // ]);

      // view()->composer('backend.*', function($view) {
      //   $siteinfo = $this->siteinfo;
      //   $sitesetting = $this->sitesetting;
      //   $auth_admin = auth()->guard('admin')->user();
      //
      //   $view->with([
      //     'siteinfo' => $siteinfo,
      //     'sitesetting' => $sitesetting,
      //     'auth_admin' => $auth_admin,
      //   ]);
      // });
      //
      // view()->composer('backend.layouts.master', function($view) {
      //   $page_power = Power::where('route_name', 'like','%'.request()->route()->getName().'%')->first();
      //   $view->with('page_power', $page_power);
      // });
      //
      // view()->composer('frontend.*', function($view) {
      //   $siteinfo = $this->siteinfo;
      //   $sitesetting = $this->sitesetting;
      //   $auth_web = auth()->guard('web')->user();
      //
      //   $view->with([
      //     'siteinfo' => $siteinfo,
      //     'sitesetting' => $sitesetting,
      //     'auth_web' => $auth_web,
      //     'menu_abouts' => $this->menu_abouts,
      //     'menu_news_categories' => $this->menu_news_categories,
      //     'menu_teacher_categories' => $this->menu_teacher_categories,
      //     'menu_course_categories' => $this->menu_course_categories,
      //     'menu_information_categories' => $this->menu_information_categories,
      //     'menu_admissions' => $this->menu_admissions,
      //     'menu_link_categories' => $this->menu_link_categories,
      //     'menu_environment_categories' => $this->menu_environment_categories,
      //   ]);
      // });
      //
      // Validator::extend("emails", function($attribute, $value, $parameters, $validator) {
      //   $rules = [
      //       'email' => 'required|email',
      //   ];
      //   $email_arr = explode(',', preg_replace('/\s(?=)/', '', $value));
      //   foreach ($email_arr as $email) {
      //       $data = [
      //           'email' => $email
      //       ];
      //       $validator = Validator::make($data, $rules);
      //       if ($validator->fails()) {
      //           return false;
      //       }
      //   }
      //   return true;
      // });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
      if ($this->app->environment() == 'local') {
        $this->app->register('Barryvdh\Debugbar\ServiceProvider');
      }
    }
}
