<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Power;
use App\Setting;
use Session;
use Carbon\Carbon;
use Jenssegers\Agent\Agent;

class FrontendServiceProvider extends AppServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
      $this->common = new Setting;
      $this->setting = Setting::getData('site');
      $this->lang_datas = config('translatable.locales');
      $this->agent = new Agent();

      config([
        'app.name' => $this->setting->name,
        'app.url' => $this->setting->site_url,
        'mail.from.name' => $this->setting->name,
      ]);
      if ($this->setting->site_mail) {
        $emails = explode(',', preg_replace('/\s(?=)/', '', $this->setting->site_mail));
        config([
          'mail.from.address' => env('MAIL_FROM_ADDRESS', $emails[0])
        ]);
      }

      $this->cities = \App\TwCity::ordered()->get();
      $this->contact_setting = Setting::getData('contact');
      $this->menu_service_categories = \App\ServiceCategory::with([
          'services' => function($q) {
            $q->display()->ordered();
          }
        ])
        ->display()->ordered()->get();

      view()->composer('frontend.*', function($view) {

        $common = $this->common;
        $setting = $this->setting;
        $agent = $this->agent;
        $auth_web = auth()->guard('web')->user();


        $view->with([

          'common' => $common,

          'setting' => $setting,
          'agent' => $agent,
          'auth_web' => $auth_web,

          'lang_datas' => $this->lang_datas,
          'cities' => $this->cities,
          'menu_service_categories' => $this->menu_service_categories,
        ]);

      });

      // view()->composer(['frontend.layouts.footer', 'frontend.store._list', 'frontend.layouts._form'], function($view) {
      //   $stores = $this->stores;
      //
      //   $view->with([
      //     'stores' => $stores,
      //   ]);
      // });
      view()->composer(['frontend.layouts.footer_form'], function($view) {
        $view->with([
          'contact_setting' => $this->contact_setting,
        ]);
      });
      view()->composer(['frontend.home', 'frontend.about.side', 'frontend.service.case.side'], function($view) {
        $latest_blogs = \App\Blog::display()->orderBy('created_at', 'desc')->limit(4)->get();

        $view->with([
          'latest_blogs' => $latest_blogs,
        ]);
      });
      view()->composer(['frontend.layouts.footer'], function($view) {
        $footer_latest_blogs = \App\Blog::display()->orderBy('created_at', 'desc')->limit(3)->get();
        // $footer_latest_cases = \App\ServiceCase::display()->orderBy('created_at', 'desc')->limit(2)->get();

        $view->with([
          'footer_latest_blogs' => $footer_latest_blogs,
          // 'footer_latest_cases' => $footer_latest_cases,
        ]);
      });
      view()->composer('frontend.blog.side', function($view) {
        $menu_blog_categories = \App\BlogCategory::withCount([
            'blogs' => function($q) {
              $q->display();
            }
          ])
          ->display()->ordered()->get();
        $latest_blogs = \App\Blog::display()->orderBy('created_at', 'desc')->limit(3)->get();
        $all_blogs = \App\Blog::display()->count();
        $view->with([
          'latest_blogs' => $latest_blogs,
          'all_blogs' => $all_blogs,
          'menu_blog_categories' => $menu_blog_categories,
        ]);
      });
      // view()->composer('frontend.news.side', function($view) {
      //   $all_news_categories = \App\NewsCategory::withCount('items')->display()->ordered()->get();
      //   $menu_news_categories = $all_news_categories->first()->getTree($all_news_categories);
      //   $latest_news = \App\News::display()->orderBy('created_at', 'desc')->limit(10)->get();
      //
      //   $view->with([
      //     'menu_news_categories' => $menu_news_categories,
      //     'latest_news' => $latest_news,
      //   ]);
      // });
      // view()->composer('frontend.news._form', function($view) {
      //   $newsForm = new \App\NewsForm;
      //   $newsFormData = Setting::getData('news');
      //   $newsForm->form_text = $newsFormData->form_text;
      //
      //   $view->with([
      //     'newsForm' => $newsForm,
      //   ]);
      // });
    }
}