<?php

namespace App;

use Dimsav\Translatable\Translatable;

class ServiceCategory extends ModelCategory
{
  use Translatable;
  protected $with = ['translations'];
  protected $table = 'service_category';
  protected $limit_level = 1;
  protected $appends = ['picPath'];
  protected $fillable = [
    'status',
    'menu_status',
    'home_status',
    'h1',
    'h2',
    'title',
    'slug',
    'description',
    'text',
    'text_pic',
    'text2',
    'text3',
    'text3_bg',
    'text4',
    'text5',
    'pic',
    'icon',
    'banner',
    'list_pic',
    'seo_title',
    'seo_description',
    'seo_keyword',
    'og_title',
    'og_description',
    'og_image',
    'meta_robots',
    'rank',
    'parent_id',
    'level',
  ];
  public $translatedAttributes = [
    'h1',
    'h2',
    'title',
    'slug',
    'description',
    'text',
    'text2',
    'text3',
    'text4',
    'text5',
    'seo_title',
    'seo_description',
    'seo_keyword',
    'og_title',
    'og_description',
  ];

  public function services() {
    return $this->hasMany(Service::class, 'service_category_id');
  }
  public function blog_category() {
    return $this->hasOne(BlogCategory::class);
  }
  public function blogs() {
    return $this->blog_category?$this->blog_category->blogs():$this->recommends();
  }
  public function recommends() {
    return $this->belongsToMany(Blog::class, 'service_category_blog_recommend', 'category_id', 'blog_id')->withPivot('rank');
  }
  public function faqs() {
    return $this->hasMany(ServiceFaq::class, 'service_category_id');
  }
  public function cases() {
    return $this->hasMany(ServiceCase::class, 'service_category_id');
  }
}
