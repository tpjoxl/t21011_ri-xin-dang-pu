<?php

namespace App;

use Dimsav\Translatable\Translatable;

class BlogCategory extends ModelCategory
{
  use Translatable;
  protected $with = ['translations'];
  protected $table = 'blog_category';
  protected $limit_level = 1;
  protected $appends = ['picPath'];
  protected $fillable = [
    'status',
    'menu_status',
    'home_status',
    'title',
    'slug',
    'list_pic',
    'rank',
    'parent_id',
    'level',
    'h1',
    'h2',
    'pic',
    'banner',
    'description',
    'text',
    'seo_title',
    'seo_description',
    'seo_keyword',
    'og_title',
    'og_description',
    'og_image',
    'meta_robots',
  ];
  public $translatedAttributes = [
    'h1',
    'h2',
    'title',
    'slug',
    'description',
    'text',
    'seo_title',
    'seo_description',
    'seo_keyword',
    'og_title',
    'og_description',
  ];

  public function blogs() {
    return $this->belongsToMany(Blog::class, 'blog_category_relation', 'category_id', 'blog_id')->withPivot('rank');
  }
  public function service_category() {
    return $this->belongsTo(ServiceCategory::class, 'service_category_id');
  }
}
