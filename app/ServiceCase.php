<?php

namespace App;

use Dimsav\Translatable\Translatable;

class ServiceCase extends Model
{
  use Translatable;
  protected $with = ['translations'];
  protected $table = 'service_case';
  protected $appends = ['picPath'];
  protected $setTop = true;
  protected $fillable = [
    'status',
    'home_status',
    'h1',
    'title',
    'slug',
    'url',
    'description',
    'text',
    'date_on',
    'date_off',
    'pic',
    'banner',
    'list_pic',
    'seo_title',
    'seo_description',
    'seo_keyword',
    'og_title',
    'og_description',
    'og_image',
    'meta_robots',
    'rank',
    'created_at',
    'service_category_id',
    'service_id',
  ];

  public function category() {
    return $this->belongsTo(ServiceCategory::class, 'service_category_id');
  }
  public function service() {
    return $this->belongsTo(Service::class, 'service_id');
  }
}
