<?php

namespace App;


class AdminGroup extends ModelCategory
{
    protected $limit_level = 1;
    protected $table = 'admin_group';
    public $timestamps = false;

    public function admins() {
      return $this->hasMany(Admin::class,'group_id','id');
    }
    public function powers() {
      return $this->belongsToMany(Power::class, 'admin_group_power_relation', 'admin_group_id', 'power_id');
    }
    public function checkPower($id) {
      $power = $this->powers->pluck('id')->toArray();
      return in_array($id, $power);
    }
    public function checkPowerByName($name) {
      $power = $this->powers->pluck('name')->toArray();
      return in_array($name, $power);
    }

    public function getTree($all_datas = null, $root = null, $limit = 0) {
      $rootId = $root;
      if (is_null($all_datas)) {
        if (auth()->guard('admin')->user()->group->title == env('MASTER_GROUP')) {
          $all = $this->orderBy('rank', 'desc')->get();
        } else {
          $all = $this->orderBy('rank', 'desc')->where('title', '!=', env('MASTER_GROUP'))->get();
        }
      } else {
        $all = $all_datas;
      }
      if (count($all)<1) {
        return null;
      }
      $roots = $all->groupBy('parent_id');
      $tree = $this->getChildren($roots, $roots[$root], $limit);
      return $tree;
    }
}
