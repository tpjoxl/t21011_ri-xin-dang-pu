<?php

namespace App;

use Dimsav\Translatable\Translatable;

class Service extends Model
{
  use Translatable;
  protected $with = ['translations'];
  protected $table = 'service';
  protected $appends = ['picPath'];
  protected $fillable = [
    'status',
    'home_status',
    'h1',
    'title',
    'slug',
    'url',
    'description',
    'text',
    'date_on',
    'date_off',
    'pic',
    'banner',
    'list_pic',
    'seo_title',
    'seo_description',
    'seo_keyword',
    'og_title',
    'og_description',
    'og_image',
    'meta_robots',
    'rank',
    'created_at',
    'service_area_id',
    'service_category_id',
  ];
  public $translatedAttributes = [
    'h1',
    'title',
    'slug',
    'description',
    'text',
    'text2',
    'text3',
    'text4',
    'seo_title',
    'seo_description',
    'seo_keyword',
    'og_title',
    'og_description',
  ];

  public function getPicPathAttribute()
  {
    if ($this->list_pic) {
      return $this->list_pic;
    } else {
      return 'images/'.$this->table.'_default.png';
    }
  }
  public function scopeOrdered($query, $rank = 'rank')
  {
    return $query->orderBy($rank, 'asc')
      ->orderBy('created_at', 'asc');
  }

  public function category() {
    return $this->belongsTo(ServiceCategory::class, 'service_category_id');
  }
  public function area() {
    return $this->belongsTo(ServiceArea::class, 'service_area_id');
  }
  public function cases() {
    return $this->hasMany(ServiceCase::class, 'service_id');
  }
  public function blogs() {
    return $this->belongsToMany(Blog::class, 'service_blog_relation', 'service_id', 'blog_id');
  }
  public function recommends() {
    return $this->belongsToMany(Blog::class, 'service_blog_recommend', 'service_id', 'blog_id');
  }
}
