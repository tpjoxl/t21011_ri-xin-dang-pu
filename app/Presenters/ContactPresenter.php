<?php

namespace App\Presenters;
use Lang;

class ContactPresenter extends Presenter
{
  public function __construct($entity)
  {
    parent::__construct($entity);

    $this->status_arr = [
      1=>'<span class="label label-success">'.Lang::get("backend.processed").'</span>',
      0=>'<span class="label label-danger">'.Lang::get("backend.pending").'</span>',
    ];
  }
}
