<?php

namespace App\Presenters;
use Lang;

class Presenter extends \Laracasts\Presenter\Presenter
{
  protected $status_arr,$target_arr,$active_arr,$sex_arr;

  public function __construct($entity)
  {
    parent::__construct($entity);

    $this->status_arr = [
      1=>'<span class="label label-success">'.Lang::get("backend.enable").'</span>',
      0=>'<span class="label label-danger">'.Lang::get("backend.disable").'</span>',
    ];
    $this->target_arr = [
      '' => Lang::get("backend.target_self"),
      '_blank' => Lang::get("backend.target_blank"),
    ];
    $this->active_arr = [
      1 => '<span class="label label-success">'.Lang::get("backend.active").'</span>',
      0 => '<span class="label label-danger">'.Lang::get("backend.inactive").'</span>',
    ];
    $this->sex_arr = [
      0 => Lang::get("backend.sex_m"),
      1 => Lang::get("backend.sex_f"),
    ];
    $this->sex_title_arr = [
      0 => Lang::get("backend.sex_title_m"),
      1 => Lang::get("backend.sex_title_f"),
    ];
    $this->yes_or_no_arr = [
      1 => Lang::get("backend.yes"),
      0 => Lang::get("backend.no"),
    ];
    $this->time_slot_arr = [
      '11:00-12:00' => '11:00-12:00',
      '12:00-13:00' => '12:00-13:00',
      '13:00-14:00' => '13:00-14:00',
      '14:00-15:00' => '14:00-15:00',
      '15:00-16:00' => '15:00-16:00',
      '16:00-17:00' => '16:00-17:00',
      '17:00-18:00' => '17:00-18:00',
      '18:00-19:00' => '18:00-19:00',
      '19:00-20:00' => '19:00-20:00',
      '20:00-21:00' => '20:00-21:00',
    ];
  }

  // 顯示狀態
  public function status($val = null) {
    if (!is_null($val)) {
      return array_key_exists($val, $this->status_arr) ? $this->status_arr[$val] : $this->status_arr[0];
    }
    return $this->status_arr;
  }

  // 連結開啟方式
  public function target($val = '') {
  	if ($val !== '') {
  		return array_key_exists($val, $this->target_arr) ? $this->target_arr[$val] : $this->target_arr[''];
  	}

    return $this->target_arr;
  }

  // 啟用狀態
  public function active($val = null) {
    if (!is_null($val)) {
      return array_key_exists($val, $this->active_arr) ? $this->active_arr[$val] : $this->active_arr[0];
    }

    return $this->active_arr;
  }
  // 性別
  public function sex($val = null) {
    if (!is_null($val)) {
      return array_key_exists($val, $this->sex_arr) ? $this->sex_arr[$val] : $this->sex_arr[0];
    }

    return $this->sex_arr;
  }
  // 稱謂
  public function sex_title($val = null) {
    if (!is_null($val)) {
      return array_key_exists($val, $this->sex_title_arr) ? $this->sex_title_arr[$val] : $this->sex_title_arr[0];
    }

    return $this->sex_title_arr;
  }
  public function time_slot($val = null) {
    if (!is_null($val)) {
      return array_key_exists($val, $this->time_slot_arr) ? $this->time_slot_arr[$val] : $this->time_slot_arr[0];
    }

    return $this->time_slot_arr;
  }
  public function yes_or_no($val = null) {
    if (!is_null($val)) {
      return array_key_exists($val, $this->yes_or_no_arr) ? $this->yes_or_no_arr[$val] : $this->yes_or_no_arr[0];
    }

    return $this->yes_or_no_arr;
  }
  public function checkSetTop()
  {
    return $this->rank==0?'<i class="fa fa-check-square"></i>':'';
  }
  public function langUrl($data=null, $lang=null)
  {
    $langUrl = [];
    $langDatas = config('translatable.locales');
    $segments = request()->segments();
    if (is_null(request()->route())) {
      return [];
    }
    $uriArr = explode('/', request()->route()->uri());

    foreach ($langDatas as $langCode => $langName) {
      $url = [];
      if (!empty($uriArr)) {
        foreach ($uriArr as $key => $uri) {
          if ($key == 0) {
            $url[$key] = strtolower($langCode);
          } else {
            if (preg_match_all('/\{(\w*)\}/', $uri, $match)) {
              if(isset($data) && !is_null($data) && $match[1]=='slug') {
                $url[$key] = $data->translate($langCode)->slug;
              } else {
                $url[$key] = $segments[$key];
              }
            } else {
              $route_key = array_search($uri, Lang::get('routes'));
              if ($route_key && Lang::has('routes.' . $route_key, $langCode)) {
                $url[$key] = Lang::get('routes.' . $route_key, [], $langCode);
              } else {
                $url[$key] = $segments[$key];
              }
            }
          }
        }
      }

      $langUrl[$langCode] = url()->to('/') .'/'. implode('/', $url);
    }

    if (!is_null($lang)) {
      return array_key_exists($lang, $langUrl) ? $langUrl[$lang] : $langUrl[0];
    } else {
      return $langUrl;
    }
  }
  // 文字去除HTML並刪節
  public function ellipsis($str, $length) {
    return mb_substr(strip_tags($str), 0, $length, 'utf-8').(mb_strlen(strip_tags($str))>$length?'...':'');
  }
}
