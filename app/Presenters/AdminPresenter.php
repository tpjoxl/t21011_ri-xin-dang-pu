<?php

namespace App\Presenters;

class AdminPresenter extends Presenter
{
  public function group_name() {
    return $this->group->title;
  }
}
