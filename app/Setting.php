<?php

namespace App;

class Setting extends Model
{
  public $timestamps = false;
  protected $table = 'setting';
  protected $fillable = [
    'setting_name',
    'setting_value',
    'locale',
    'page',
  ];

  public static function getData($page)
  {
    if (is_array($page)) {
      $query = static::query();
      foreach ($page as $key => $p) {
        if ($key==0) {
          $query->where('page', 'like', 'backend.'.$p.'%');
        } else {
          $query->orWhere('page', 'like', 'backend.'.$p.'%');
        }
      }
    } else {
      $query = static::where('page', 'like', 'backend.'.$page.'%');
    }
    $datas = $query->get();
    $result = $datas->whereIn('locale', ['common', config('app.locale')])->pluck('setting_value', 'setting_name')->toArray();
    foreach (config('translatable.locales') as $langCode => $langName) {
      $result[$langCode] = $datas->whereIn('locale', ['common', $langCode])->pluck('setting_value', 'setting_name')->toArray();
    }
    return (object) $result;
  }
}
