<?php

namespace App;

class TwRegion extends Model
{
  public $timestamps = false;
  protected $table = 'tw_region';
  protected $fillable = [
    'name',
    'code',
    'city_id',
    'rank',
    'remoted',
  ];

  public function scopeOrdered($query, $rank = 'rank')
  {
    return $query->orderBy($rank, 'asc')
      ->orderBy('id', 'asc');
  }
  
  public function city() {
    return $this->belongsTo(TwCity::class, 'city_id');
  }
}
