<?php

namespace App;

use Dimsav\Translatable\Translatable;

class ServiceArea extends ModelCategory
{
  use Translatable;
  protected $with = ['translations'];
  protected $table = 'service_area';
  protected $limit_level = 1;

  public function services() {
    return $this->hasMany(Service::class, 'service_area_id');
  }
}
