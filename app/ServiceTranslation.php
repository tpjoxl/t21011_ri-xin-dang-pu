<?php

namespace App;

class ServiceTranslation extends Model
{
  public $timestamps = false;
  protected $fillable = [
    'title',
    'slug',
    'description',
    'text',
    'text2',
    'text3',
    'text4',
    'seo_title',
    'seo_description',
    'seo_keyword',
    'og_title',
    'og_description',
  ];
}
