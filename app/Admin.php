<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laracasts\Presenter\PresentableTrait;

class Admin extends Authenticatable
{
    use Notifiable;
    use PresentableTrait;

    protected $table = 'admin';
    protected $guard = 'admin';
    protected $presenter = 'App\Presenters\AdminPresenter';
    public $translatedAttributes = [];
    public function scopeOrdered($query, $rank = 'rank')
    {
      return $query->orderBy('created_at', 'asc');
    }
    public function group() {
      return $this->belongsTo(AdminGroup::class, 'group_id');
    }
    public function fillAndSave($data) {
      $dataToSave = $data;
      if (array_key_exists('password', $dataToSave)) {
        $dataToSave['password'] = bcrypt($dataToSave['password']);
      }
      $result = $this->fill($dataToSave)->save();
      return $result;
    }
    public function getTransAttrs() {
      if (count(config('translatable.locales')) < 2 || !method_exists($this, 'translations')) {
        return array();
      } else {
        return $this->translatedAttributes;
      }
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'account', 'password', 'status', 'login_at', 'ip', 'group_id', 'passable'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}
