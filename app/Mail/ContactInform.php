<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Contact;
use App\Setting;

class ContactInform extends Mailable
{
    use Queueable, SerializesModels;
    public $contact;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Contact $contact)
    {
      $this->contact = $contact;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
      $setting = Setting::getData('site');
      $emails = explode(',', preg_replace('/\s(?=)/', '', $setting->site_mail));
      $subject = $setting->name.' - 立即諮詢表單通知';
      // $from = '37@37design.com.tw';
      return $this->markdown('emails.contact_inform')
                  ->subject($subject);
    }
}
