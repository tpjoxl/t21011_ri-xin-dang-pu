<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');

Route::get('sitemap.xml', 'GeneratedController@siteMap');

Route::group(['prefix' => (count(config('translatable.locales'))>1)?strtolower(app()->getLocale()):''], function () {
  Route::get('/', 'HomeController@index')->name('home');
  Route::any('lang/{locale}', 'LanguageController@switchLang')->name('lang.switch');

  Route::group(['prefix' => __('routes.blog')], function () {
    Route::get('/', 'BlogController@index')->name('blog.index');
    Route::get('/'.__('routes.search'), 'BlogController@search')->name('blog.search');
    Route::get('/'.__('routes.category').'/{category}', 'BlogController@category')->name('blog.category');
    Route::get('/'.__('routes.service').'/{service}', 'BlogController@service')->name('blog.service');
    Route::get('/{slug}', 'BlogController@detail')->name('blog.detail');
  });

  Route::group(['prefix' => __('routes.about')], function () {
    Route::get('/', 'AboutController@index')->name('about.index');
  });

  Route::group(['prefix' => __('routes.contact')], function () {
    Route::get('/', 'ContactController@index')->name('contact.index');
    Route::post('/', 'ContactController@formSubmit')->name('contact.submit');
    // Route::get('result', 'ContactController@result')->name('contact.result');
  });

  Route::get('/{slug}', 'ServiceController@index')->name('service.index');
  // Route::get('/{category}/case-list', 'ServiceCaseController@category')->name('service.case.category');
  Route::get('/{category}/{slug}', 'ServiceController@detail')->name('service.detail');
  // Route::get('/{category}/{service}/case-list', 'ServiceCaseController@index')->name('service.case.index');
  // Route::get('/{category}/case/{slug}', 'ServiceCaseController@detail2')->name('service.case.detail2');
  // Route::get('/{category}/{service}/case/{slug}', 'ServiceCaseController@detail')->name('service.case.detail');
});
