<?php



/*

|--------------------------------------------------------------------------

| Backend Routes

|--------------------------------------------------------------------------

*/



Route::get('/', 'HomeController@index')->name('backend.home');

Route::get('/login', 'LoginController@showLoginForm')->name('backend.login');

Route::post('/login', 'LoginController@login')->name('backend.login.submit');

Route::get('/logout', 'LoginController@logout')->name('backend.logout');



// 權限功能管理

Route::group(['prefix' => 'power'], function () {

  Route::get('/', 'PowerController@index')->name('backend.power.index');

  Route::get('/{id}/edit', 'PowerController@edit')->where('id', '[0-9]+')->name('backend.power.edit');

  Route::patch('/{id}/edit', 'PowerController@update')->where('id', '[0-9]+')->name('backend.power.update');

  Route::any('/status/{val}', 'PowerController@setStatus')->where('val', '[0-9]+')->name('backend.power.status');

  Route::any('/rank/update', 'PowerController@rankUpdate')->name('backend.power.rank.update');

});



// 網站設定

Route::group(['prefix' => 'site'], function () {

  Route::any('/info', 'SiteSettingController@page')->name('backend.site.info');

  Route::any('/setting', 'SiteSettingController@page')->name('backend.site.setting');

  Route::any('/editor', 'SiteSettingController@page')->name('backend.site.editor');
});



// 管理員管理

Route::group(['prefix' => 'admin'], function () {

  Route::get('/', 'AdminController@index')->name('backend.admin.index');

  Route::any('/create', 'AdminController@create')->name('backend.admin.create');

  Route::post('/create', 'AdminController@store')->name('backend.admin.store');

  Route::get('/{id}/edit', 'AdminController@edit')->where('id', '[0-9]+')->name('backend.admin.edit');

  Route::patch('/{id}/edit', 'AdminController@update')->where('id', '[0-9]+')->name('backend.admin.update');

  Route::post('/{id}/edit/password', 'AdminController@password')->where('id', '[0-9]+')->name('backend.admin.password');

  Route::any('/destroy', 'AdminController@destroy')->name('backend.admin.destroy');

  Route::any('/status/{val}', 'AdminController@setStatus')->where('val', '[0-9]+')->name('backend.admin.status');



  //管理員權限群組分類

  Route::group(['prefix' => 'group'], function () {

    Route::get('', 'AdminGroupController@index')->name('backend.admin.group.index');

    Route::get('create', 'AdminGroupController@create')->name('backend.admin.group.create');

    Route::post('create', 'AdminGroupController@store')->name('backend.admin.group.store');

    Route::get('{id}/edit', 'AdminGroupController@edit')->where('id', '[0-9]+')->name('backend.admin.group.edit');

    Route::patch('{id}/edit', 'AdminGroupController@update')->where('id', '[0-9]+')->name('backend.admin.group.update');

    Route::any('destroy', 'AdminGroupController@destroy')->name('backend.admin.group.destroy');

    Route::any('/status/{val}', 'AdminGroupController@setStatus')->where('val', '[0-9]+')->name('backend.admin.group.status');

    Route::any('/rank/update', 'AdminGroupController@rankUpdate')->name('backend.admin.group.rank.update');

  });

});


Route::group(['prefix' => 'banner'], function () {
  Route::get('/', 'BannerController@index')->name('backend.banner.index');
  Route::get('/create', 'BannerController@create')->name('backend.banner.create');
  Route::post('/create', 'BannerController@store')->name('backend.banner.store');
  Route::get('/{id}/edit', 'BannerController@edit')->where('id', '[0-9]+')->name('backend.banner.edit');
  Route::patch('/{id}/edit', 'BannerController@update')->where('id', '[0-9]+')->name('backend.banner.update');
  Route::any('/destroy', 'BannerController@destroy')->name('backend.banner.destroy');
  Route::get('/rank/{id?}', 'BannerController@rank')->where('id', '[0-9]+')->name('backend.banner.rank.index');
  Route::post('/rank/{id?}', 'BannerController@rankUpdate')->where('id', '[0-9]+')->name('backend.banner.rank.update');
  Route::any('/status/{val}', 'BannerController@setStatus')->where('val', '[0-9]+')->name('backend.banner.status');
});

Route::group(['prefix' => 'service'], function () {
  Route::any('/', 'ServiceController@index')->name('backend.service.index');
  Route::get('/create', 'ServiceController@create')->name('backend.service.create');
  Route::post('/create', 'ServiceController@store')->name('backend.service.store');
  Route::get('/{id}/edit', 'ServiceController@edit')->where('id', '[0-9]+')->name('backend.service.edit');
  Route::patch('/{id}/edit', 'ServiceController@update')->where('id', '[0-9]+')->name('backend.service.update');
  Route::any('/destroy', 'ServiceController@destroy')->name('backend.service.destroy');
  Route::get('/rank/{id?}', 'ServiceController@rank')->where('id', '[0-9]+')->name('backend.service.rank.index');
  Route::post('/rank/{id?}', 'ServiceController@rankUpdate')->where('id', '[0-9]+')->name('backend.service.rank.update');
  Route::any('/status/{val}', 'ServiceController@setStatus')->where('val', '[0-9]+')->name('backend.service.status');
  Route::any('/page', 'ServicePageController@page')->name('backend.service.page');

  // 分類
  Route::group(['prefix' => 'category'], function () {
    Route::get('/', 'ServiceCategoryController@index')->name('backend.service.category.index');
    Route::get('/create', 'ServiceCategoryController@create')->name('backend.service.category.create');
    Route::post('/create', 'ServiceCategoryController@store')->name('backend.service.category.store');
    Route::get('/{id}/edit', 'ServiceCategoryController@edit')->where('id', '[0-9]+')->name('backend.service.category.edit');
    Route::patch('/{id}/edit', 'ServiceCategoryController@update')->where('id', '[0-9]+')->name('backend.service.category.update');
    Route::any('/destroy', 'ServiceCategoryController@destroy')->name('backend.service.category.destroy');
    Route::any('/status/{val}', 'ServiceCategoryController@setStatus')->where('val', '[0-9]+')->name('backend.service.category.status');
    Route::any('/rank/update', 'ServiceCategoryController@rankUpdate')->name('backend.service.category.rank.update');
  });

  Route::group(['prefix' => 'area'], function () {
    Route::get('/', 'ServiceAreaController@index')->name('backend.service.area.index');
    Route::get('/create', 'ServiceAreaController@create')->name('backend.service.area.create');
    Route::post('/create', 'ServiceAreaController@store')->name('backend.service.area.store');
    Route::get('/{id}/edit', 'ServiceAreaController@edit')->where('id', '[0-9]+')->name('backend.service.area.edit');
    Route::patch('/{id}/edit', 'ServiceAreaController@update')->where('id', '[0-9]+')->name('backend.service.area.update');
    Route::any('/destroy', 'ServiceAreaController@destroy')->name('backend.service.area.destroy');
    Route::any('/status/{val}', 'ServiceAreaController@setStatus')->where('val', '[0-9]+')->name('backend.service.area.status');
    Route::any('/rank/update', 'ServiceAreaController@rankUpdate')->name('backend.service.area.rank.update');
  });

  Route::group(['prefix' => 'faq'], function () {
    Route::any('/', 'ServiceFaqController@index')->name('backend.service.faq.index');
    Route::get('/create', 'ServiceFaqController@create')->name('backend.service.faq.create');
    Route::post('/create', 'ServiceFaqController@store')->name('backend.service.faq.store');
    Route::get('/{id}/edit', 'ServiceFaqController@edit')->where('id', '[0-9]+')->name('backend.service.faq.edit');
    Route::patch('/{id}/edit', 'ServiceFaqController@update')->where('id', '[0-9]+')->name('backend.service.faq.update');
    Route::any('/destroy', 'ServiceFaqController@destroy')->name('backend.service.faq.destroy');
    Route::get('/rank/{id?}', 'ServiceFaqController@rank')->where('id', '[0-9]+')->name('backend.service.faq.rank.index');
    Route::post('/rank/{id?}', 'ServiceFaqController@rankUpdate')->where('id', '[0-9]+')->name('backend.service.faq.rank.update');
    Route::any('/status/{val}', 'ServiceFaqController@setStatus')->where('val', '[0-9]+')->name('backend.service.faq.status');
  });
});

Route::group(['prefix' => 'about'], function () {
  Route::any('/page', 'AboutController@page')->name('backend.about.page');
});

Route::group(['prefix' => 'blog'], function () {
  Route::get('/', 'BlogController@index')->name('backend.blog.index');
  Route::get('/create', 'BlogController@create')->name('backend.blog.create');
  Route::post('/create', 'BlogController@store')->name('backend.blog.store');
  Route::get('/{id}/edit', 'BlogController@edit')->where('id', '[0-9]+')->name('backend.blog.edit');
  Route::patch('/{id}/edit', 'BlogController@update')->where('id', '[0-9]+')->name('backend.blog.update');
  Route::any('/destroy', 'BlogController@destroy')->name('backend.blog.destroy');
  Route::get('/rank/{id?}', 'BlogController@rank')->where('id', '[0-9]+')->name('backend.blog.rank.index');
  Route::post('/rank/{id?}', 'BlogController@rankUpdate')->where('id', '[0-9]+')->name('backend.blog.rank.update');
  Route::any('/status/{val}', 'BlogController@setStatus')->where('val', '[0-9]+')->name('backend.blog.status');
  Route::any('/page', 'BlogPageController@page')->name('backend.blog.page');

  // 分類
  Route::group(['prefix' => 'category'], function () {
    Route::get('/', 'BlogCategoryController@index')->name('backend.blog.category.index');
    Route::get('/create', 'BlogCategoryController@create')->name('backend.blog.category.create');
    Route::post('/create', 'BlogCategoryController@store')->name('backend.blog.category.store');
    Route::get('/{id}/edit', 'BlogCategoryController@edit')->where('id', '[0-9]+')->name('backend.blog.category.edit');
    Route::patch('/{id}/edit', 'BlogCategoryController@update')->where('id', '[0-9]+')->name('backend.blog.category.update');
    Route::any('/destroy', 'BlogCategoryController@destroy')->name('backend.blog.category.destroy');
    Route::any('/status/{val}', 'BlogCategoryController@setStatus')->where('val', '[0-9]+')->name('backend.blog.category.status');
    Route::any('/rank/update', 'BlogCategoryController@rankUpdate')->name('backend.blog.category.rank.update');
  });
});

// 聯絡我們
Route::group(['prefix' => 'contact'], function () {
  Route::get('/', 'ContactController@index')->name('backend.contact.index');
  Route::get('/create', 'ContactController@create')->name('backend.contact.create');
  Route::post('/create', 'ContactController@store')->name('backend.contact.store');
  Route::get('/{id}/edit', 'ContactController@edit')->where('id', '[0-9]+')->name('backend.contact.edit');
  Route::patch('/{id}/edit', 'ContactController@update')->where('id', '[0-9]+')->name('backend.contact.update');
  Route::any('/destroy', 'ContactController@destroy')->name('backend.contact.destroy');
  Route::any('/status/{val}', 'ContactController@setStatus')->where('val', '[0-9]+')->name('backend.contact.status');
  Route::any('/page', 'ContactPageController@page')->name('backend.contact.page');
});
