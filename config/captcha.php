<?php

return [

    // 'characters' => '2346789abcdefghjmnpqrtuxyzABCDEFGHJMNPQRTUXYZ',
    'characters' => '2346789',

    'default'   => [
        'length'    => 5,
        'width'     => 120,
        'height'    => 36,
        'quality'   => 90,
    ],

    'flat'   => [
      'length'    => 4,
      'width'     => 82,
      'height'    => 30,
      'quality'   => 90,
      'lines'     => 6,
      'bgImage'   => false,
      'bgColor'   => '#ecf2f4',
      'fontColors'=> ['#2c3e50', '#303f9f', '#795548', '#c00000', '#019701'],
      'contrast'  => 0,
    ],

    'mini'   => [
        'length'    => 3,
        'width'     => 60,
        'height'    => 32,
    ],

    'inverse'   => [
        'length'    => 5,
        'width'     => 120,
        'height'    => 36,
        'quality'   => 90,
        'sensitive' => true,
        'angle'     => 12,
        'sharpen'   => 10,
        'blur'      => 2,
        'invert'    => true,
        'contrast'  => -5,
    ]

];
