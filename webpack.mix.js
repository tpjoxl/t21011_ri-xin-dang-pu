let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

// mix.js('resources/assets/js/main.js', 'public/js')
//     .sass('resources/assets/sass/main.scss', 'public/css')
mix.sass('resources/assets/sass/editor.scss', 'public/css')
    .sass('resources/assets/sass/main.scss', 'public/css')
    .options({
        postCss: [
          require('autoprefixer')({
              browsers: [
                'Android 2.3',
                'Android >= 4',
                'Chrome >= 20',
                'Firefox >= 24',
                'Explorer >= 8',
                'iOS >= 6',
                'Opera >= 12',
                'Safari >= 6'
              ],
              cascade: false
          })
        ]
    }).sourceMaps();
if (mix.inProduction()) {
  mix.version();
}
