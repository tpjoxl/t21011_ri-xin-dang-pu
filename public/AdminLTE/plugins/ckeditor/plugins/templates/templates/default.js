﻿/*
Copyright (c) 2003-2011, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
*/

CKEDITOR.addTemplates('default', {
    imagesPath: CKEDITOR.getUrl(CKEDITOR.plugins.getPath('templates') + 'templates/images/'), templates: [
    {
        title: '導覽區範本1',
        image: 'fast_tp1.png',
        description: '導覽區樣式1',
        html: '<div class="editor-fast-nav1"><h2 class="nav-heading">導讀區</h2><ul class="nav-list"><li><a href="#article_1" title="導讀連結文字1">導讀連結文字1</a></li><li><a href="#article_2" title="導讀連結文字2">導讀連結文字2</a></li><li><a href="#article_3" title="導讀連結文字3">導讀連結文字3</a></li><li><a href="#article_4" title="導讀連結文字4">導讀連結文字4</a></li><li><a href="#article_5" title="導讀連結文字5">導讀連結文字5</a></li></ul></div><div id="article_1" class="editor-fast-sect"><h3 class="sect-heading">標題1</h3><p>內文1</p></div><div id="article_2" class="editor-fast-sect"><h3 class="sect-heading">標題2</h3><p>內文2</p></div><div id="article_3" class="editor-fast-sect"><h3 class="sect-heading">標題3</h3><p>內文3</p></div><div id="article_4" class="editor-fast-sect"><h3 class="sect-heading">標題4</h3><p>內文4</p></div><div id="article_5" class="editor-fast-sect"><h3 class="sect-heading">標題5</h3><p>內文5</p></div>'
    },
    {
        title: '導覽區範本2',
        image: 'fast_tp2.png',
        description: '導覽區樣式2',
        html: '<div class="editor-fast-nav2"><h2 class="nav-heading">導讀區</h2><ul class="nav-list"><li><a href="#article_1" title="導讀連結文字1">導讀連結文字1</a></li><li><a href="#article_2" title="導讀連結文字2">導讀連結文字2</a></li><li><a href="#article_3" title="導讀連結文字3">導讀連結文字3</a></li><li><a href="#article_4" title="導讀連結文字4">導讀連結文字4</a></li><li><a href="#article_5" title="導讀連結文字5">導讀連結文字5</a></li></ul></div><div id="article_1" class="editor-fast-sect"><h3 class="sect-heading">標題1</h3><p>內文1</p></div><div id="article_2" class="editor-fast-sect"><h3 class="sect-heading">標題2</h3><p>內文2</p></div><div id="article_3" class="editor-fast-sect"><h3 class="sect-heading">標題3</h3><p>內文3</p></div><div id="article_4" class="editor-fast-sect"><h3 class="sect-heading">標題4</h3><p>內文4</p></div><div id="article_5" class="editor-fast-sect"><h3 class="sect-heading">標題5</h3><p>內文5</p></div>'
    },
    {
        title: '導覽區範本3',
        image: 'fast_tp3.png',
        description: '導覽區樣式3',
        html: '<div class="editor-fast-nav3"><h2 class="nav-heading">導讀區</h2><ul class="nav-list"><li><a href="#article_1" title="導讀連結文字1">導讀連結文字1</a></li><li><a href="#article_2" title="導讀連結文字2">導讀連結文字2</a></li><li><a href="#article_3" title="導讀連結文字3">導讀連結文字3</a></li><li><a href="#article_4" title="導讀連結文字4">導讀連結文字4</a></li><li><a href="#article_5" title="導讀連結文字5">導讀連結文字5</a></li></ul></div><div id="article_1" class="editor-fast-sect"><h3 class="sect-heading">標題1</h3><p>內文1</p></div><div id="article_2" class="editor-fast-sect"><h3 class="sect-heading">標題2</h3><p>內文2</p></div><div id="article_3" class="editor-fast-sect"><h3 class="sect-heading">標題3</h3><p>內文3</p></div><div id="article_4" class="editor-fast-sect"><h3 class="sect-heading">標題4</h3><p>內文4</p></div><div id="article_5" class="editor-fast-sect"><h3 class="sect-heading">標題5</h3><p>內文5</p></div>'
    },
    {
        title: '電話號碼連結',
        image: 'link_phone.png',
        description: '電話號碼連結',
        html: '<a href="tel:0966-423-333" title="免費諮詢">0966-423333</a>'
    },
    {
        title: 'Line 連結',
        image: 'link_line.png',
        description: 'Line 連結',
        html: '<a href="https://line.me/ti/p/i-QJyYCurd" target="_blank" title="線上諮詢">0966423333</a>'
    },
    {
        title: '條列式內容1',
        image: 'list_tp1.png',
        description: '條列式內容1',
        html: '<div class="editor-list1"><ul><li>第一項</li><li>第二項</li><li>第三項</li><li>第四項</li><li>第五項</li></ul></div>'
    },
    {
        title: '條列式內容2',
        image: 'list_tp2.png',
        description: '條列式內容2',
        html: '<div class="editor-list2"><ul><li>▶<b>標題1：</b>內容1</li><li>▶<b>標題2：</b>內容2</li><li>▶<b>標題3：</b>內容3</li></ul></div>'
    },
    {
        title: '自訂表格範本1',
        image: 'table_tp1.png',
        description: '自訂表格範本1',
        html: '<table class="editor-tmp-table1"><tbody><tr><th>申辦條件</th><td>名下自有汽車（有原車貸亦可申辦）</td></tr><tr><th>申辦利息</th><td>最低優惠利率0.85%起</td></tr><tr><th>貸款額度</th><td>只要您名下自有房屋土地還有殘值皆可申辦貸款，貸款額度無上限。</td></tr><tr><th>貸款期限</th><td>全程無綁約，隨借隨還，最長可達7年。</td></tr><tr><th>準備文件</th><td>房屋所有權狀正本正本、物件所有權人身份證件正本、影本一份、物件所有權人印鑑證明（有效期需3個月內，如本人在場可不提供）、本人印鑑章</td></tr><tr><th>審核時間</th><td>最慢3個工作天即可撥款</td></tr></tbody></table>'
    },
    {
        title: '自訂表格範本2',
        image: 'table_tp2.png',
        description: '自訂表格範本2',
        html: '<table class="editor-tmp-table2"><tbody><tr><th>申辦條件</th><td>只要名下有房屋或土地，不需要審核個人條件即可申辦</td></tr><tr><th>申辦利息</th><td>利率最低0.85%起</td></tr><tr><th>貸款額度</th><td>只要您名下自有房屋土地還有殘值皆可申辦貸款，貸款額度無上限。</td></tr><tr><th>貸款期限</th><td>全程無綁約，隨借隨還，最長可達7年。</td></tr><tr><th>準備文件</th><td>房屋所有權狀正本正本、物件所有權人身份證件正本、影本一份、物件所有權人印鑑證明（有效期需3個月內，如本人在場可不提供）、本人印鑑章</td></tr><tr><th>審核時間</th><td>最慢3個工作天即可撥款</td></tr></tbody></table>'
    },
    {
        title: '自訂表格範本3',
        image: 'table_tp3.png',
        description: '自訂表格範本3',
        html: '<table class="editor-tmp-table3"><tbody><tr><th>申辦條件</th><td>只要名下有房屋或土地，不需要審核個人條件即可申辦</td></tr><tr><th>申辦利息</th><td>利率最低0.85%起</td></tr><tr><th>貸款額度</th><td>只要您名下自有房屋土地還有殘值皆可申辦貸款，貸款額度無上限。</td></tr><tr><th>貸款期限</th><td>全程無綁約，隨借隨還，最長可達7年。</td></tr><tr><th>準備文件</th><td>房屋所有權狀正本正本、物件所有權人身份證件正本、影本一份、物件所有權人印鑑證明（有效期需3個月內，如本人在場可不提供）、本人印鑑章</td></tr><tr><th>審核時間</th><td>最慢3個工作天即可撥款</td></tr></tbody></table>'
    },
    {
        title: '自訂表格範本4',
        image: 'table_tp4.png',
        description: '自訂表格範本4',
        html: '<div class="editor-tmp-table-wrap"><table class="editor-tmp-table4"><thead><tr><th>標題</th><th>行標題1</th><th>行標題2</th></tr></thead><tbody><tr><th>列標題1</th><td>內容內容內容1-1</td><td>內容內容內容1-2</td></tr><tr><th>列標題2</th><td>內容內容內容2-1</td><td>內容內容內容2-2</td></tr></tbody></table></div>'
    },
    {
        title: '自訂表格範本5',
        image: 'table_tp5.png',
        description: '自訂表格範本5',
        html: '<div class="editor-tmp-table-wrap"><table class="editor-tmp-table5"><thead><tr><th>標題1</th><th>標題2</th><th>標題3</th></tr></thead><tbody><tr><td>內容內容內容內容</td><td>內容內容內容內容</td><td>內容內容內容內容</td></tr><tr><td>內容內容內容內容</td><td>內容內容內容內容</td><td>內容內容內容內容</td></tr><tr><td>內容內容內容內容</td><td>內容內容內容內容</td><td>內容內容內容內容</td></tr></tbody></table></div>'
    },
     {
        title: '表格範本1',
        image: 'tp1.png',
        description: '條列式表格，只有水平框線。',
        html: '<div class="table-responsive"> <table class="editor-table table-hover"> <thead> <tr> <th>表格標題</th> <th>表格標題</th> <th>表格標題</th> </tr> </thead> <tbody> <tr> <td>文字</td> <td>文字</td> <td>文字</td> </tr> <tr> <td>文字</td> <td>文字</td> <td>文字</td> </tr> <tr> <td>文字</td> <td>文字</td> <td>文字</td> </tr> </tbody> </table> </div>'
    },
     {
        title: '表格範本2',
        image: 'tp2.png',
        description: '條列式表格，欄位四周皆有框線。',
        html: '<div class="table-responsive"> <table class="editor-table table-bordered table-hover"> <thead> <tr> <th>表格標題</th> <th>表格標題</th> <th>表格標題</th> </tr> </thead> <tbody> <tr> <td>文字</td> <td>文字</td> <td>文字</td> </tr> <tr> <td>文字</td> <td>文字</td> <td>文字</td> </tr> <tr> <td>文字</td> <td>文字</td> <td>文字</td> </tr> </tbody> </table> </div>'
    },
     {
        title: '表格範本3',
        image: 'tp3.png',
        description: '條列式表格，欄位間隔以灰階背景區分，只有水平框線。',
        html: '<div class="table-responsive"> <table class="editor-table table-striped table-hover"> <thead> <tr> <th>表格標題</th> <th>表格標題</th> <th>表格標題</th> </tr> </thead> <tbody> <tr> <td>文字</td> <td>文字</td> <td>文字</td> </tr> <tr> <td>文字</td> <td>文字</td> <td>文字</td> </tr> <tr> <td>文字</td> <td>文字</td> <td>文字</td> </tr> </tbody> </table> </div>'
    },
     {
        title: '表格範本4',
        image: 'tp4.png',
        description: '條列式表格，欄位間隔以灰階背景區分，欄位四周皆有框線。',
        html: '<div class="table-responsive"> <table class="editor-table table-bordered table-striped table-hover"> <thead> <tr> <th>表格標題</th> <th>表格標題</th> <th>表格標題</th> </tr> </thead> <tbody> <tr> <td>文字</td> <td>文字</td> <td>文字</td> </tr> <tr> <td>文字</td> <td>文字</td> <td>文字</td> </tr> <tr> <td>文字</td> <td>文字</td> <td>文字</td> </tr> </tbody> </table> </div>'
    },
     {
        title: '表格範本5',
        image: 'tp5.png',
        description: '雙標題條列式表格，欄位間隔以灰階背景區分，只有水平框線。',
        html: '<div class="table-responsive"> <table class="editor-table table-striped table-hover"> <thead> <tr> <th>#</th> <th>表格標題</th> <th>表格標題</th> <th>表格標題</th> </tr> </thead> <tbody> <tr> <th scope="row">1</th> <td>文字</td> <td>文字</td> <td>文字</td> </tr> <tr> <th scope="row">2</th> <td>文字</td> <td>文字</td> <td>文字</td> </tr> <tr> <th scope="row">3</th> <td>文字</td> <td>文字</td> <td>文字</td> </tr> </tbody> </table> </div>'
    },
	 {
        title: '表格範本6',
        image: 'tp6.png',
        description: '雙標題條列式表格，欄位間隔以灰階背景區分，欄位四周皆有框線。',
        html: '<div class="table-responsive"> <table class="editor-table table-striped table-bordered table-hover"> <thead> <tr> <th>#</th> <th>表格標題</th> <th>表格標題</th> <th>表格標題</th> </tr> </thead> <tbody> <tr> <th scope="row">1</th> <td>文字</td> <td>文字</td> <td>文字</td> </tr> <tr> <th scope="row">2</th> <td>文字</td> <td>文字</td> <td>文字</td> </tr> <tr> <th scope="row">3</th> <td>文字</td> <td>文字</td> <td>文字</td> </tr> </tbody> </table> </div>'
    },
	 {
        title: '段落範本1',
        image: 'tp7.png',
        description: 'RWD 2欄排版，768px以下為1欄排版',
        html: '<div class="row editor-row"> <div class="col-sm-6"> <div style="text-align:center;"><img src="http://fakeimg.pl/600x400/?text=NO IMAGE" alt="連按兩下置入圖片"></div> <h3 class="title">標題文字</h3> <p>內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字</p> </div> <div class="col-sm-6"> <div style="text-align:center;"><img src="http://fakeimg.pl/600x400/?text=NO IMAGE" alt="連按兩下置入圖片"></div> <h3 class="title">標題文字</h3> <p>內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字</p> </div> </div>'
    },
     {
        title: '段落範本2',
        image: 'tp8.png',
        description: 'RWD 3欄排版，768px以下為1欄排版',
        html: '<div class="row editor-row"> <div class="col-sm-4"> <div style="text-align:center;"><img src="http://fakeimg.pl/600x400/?text=NO IMAGE" alt="連按兩下置入圖片"></div> <h3 class="title">標題文字</h3> <p>內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字</p> </div> <div class="col-sm-4"> <div style="text-align:center;"><img src="http://fakeimg.pl/600x400/?text=NO IMAGE" alt="連按兩下置入圖片"></div> <h3 class="title">標題文字</h3> <p>內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字</p> </div> <div class="col-sm-4"> <div style="text-align:center;"><img src="http://fakeimg.pl/600x400/?text=NO IMAGE" alt="連按兩下置入圖片"></div> <h3 class="title">標題文字</h3> <p>內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字</p> </div> </div>'
    },
	 {
        title: '段落範本3',
        image: 'tp9.png',
        description: 'RWD 4欄排版，992px以下為2欄排版，768px以下為1欄排版',
        html: '<div class="row editor-row"> <div class="col-md-3 col-sm-6"> <div style="text-align:center;"><img src="http://fakeimg.pl/600x400/?text=NO IMAGE" alt="連按兩下置入圖片"></div> <h3 class="title">標題文字</h3> <p>內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字</p> </div> <div class="col-md-3 col-sm-6"> <div style="text-align:center;"><img src="http://fakeimg.pl/600x400/?text=NO IMAGE" alt="連按兩下置入圖片"></div> <h3 class="title">標題文字</h3> <p>內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字</p> </div> <div class="col-md-3 col-sm-6"> <div style="text-align:center;"><img src="http://fakeimg.pl/600x400/?text=NO IMAGE" alt="連按兩下置入圖片"></div> <h3 class="title">標題文字</h3> <p>內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字</p> </div> <div class="col-md-3 col-sm-6"> <div style="text-align:center;"><img src="http://fakeimg.pl/600x400/?text=NO IMAGE" alt="連按兩下置入圖片"></div> <h3 class="title">標題文字</h3> <p>內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字</p> </div> </div>'
    },
     {
        title: '段落範本4',
        image: 'tp10.png',
        description: 'RWD 6欄排版，1200px以下為3欄排版，992px以下為2欄排版，768px以下為1欄排版',
        html: '<div class="row editor-row"> <div class="col-lg-2 col-md-3 col-sm-4 col-xs-6"> <div style="text-align:center;"><img src="http://fakeimg.pl/600x400/?text=NO IMAGE" alt="連按兩下置入圖片"></div> <h3 class="title">標題文字</h3> <p>內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字</p> </div> <div class="col-lg-2 col-md-3 col-sm-4 col-xs-6"> <div style="text-align:center;"><img src="http://fakeimg.pl/600x400/?text=NO IMAGE" alt="連按兩下置入圖片"></div> <h3 class="title">標題文字</h3> <p>內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字</p> </div> <div class="col-lg-2 col-md-3 col-sm-4 col-xs-6"> <div style="text-align:center;"><img src="http://fakeimg.pl/600x400/?text=NO IMAGE" alt="連按兩下置入圖片"></div> <h3 class="title">標題文字</h3> <p>內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字</p> </div> <div class="col-lg-2 col-md-3 col-sm-4 col-xs-6"> <div style="text-align:center;"><img src="http://fakeimg.pl/600x400/?text=NO IMAGE" alt="連按兩下置入圖片"></div> <h3 class="title">標題文字</h3> <p>內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字</p> </div> <div class="col-lg-2 col-md-3 col-sm-4 col-xs-6"> <div style="text-align:center;"><img src="http://fakeimg.pl/600x400/?text=NO IMAGE" alt="連按兩下置入圖片"></div> <h3 class="title">標題文字</h3> <p>內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字</p> </div> <div class="col-lg-2 col-md-3 col-sm-4 col-xs-6"> <div style="text-align:center;"><img src="http://fakeimg.pl/600x400/?text=NO IMAGE" alt="連按兩下置入圖片"></div> <h3 class="title">標題文字</h3> <p>內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字</p> </div> </div>'
    },
    {
       title: '段落範本5',
       image: 'tp5.gif',
       description: '圖片置左，文字資訊跟隨圖片右側，有標題及內容',
       html: '<div class="row editor-row"> <div class="col-sm-6"> <div style="text-align:center;"><img src="http://fakeimg.pl/600x400/?text=NO IMAGE" alt="連按兩下置入圖片"></div></div> <div class="col-sm-6"> <h3 class="title">標題文字</h3> <p>內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字</p> </div> </div>'
   },
    {
       title: '段落範本6',
       image: 'tp6.gif',
       description: '圖片置左，文字資訊跟隨圖片右側，有標題及內容',
       html: '<div class="row editor-row"> <div class="col-sm-6"> <h3 class="title">標題文字</h3> <p>內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字</p> </div> <div class="col-sm-6"> <div style="text-align:center;"><img src="http://fakeimg.pl/600x400/?text=NO IMAGE" alt="連按兩下置入圖片"></div> </div> </div>'
   },
    {
       title: '段落範本7',
       image: 'tp11.png',
       description: '圖片置左，文字資訊跟隨圖片右側，有標題及內容',
       html: '<div class="editor-media-box"> <div class="media-left"><img alt="64x64" class="media-object" data-src="holder.js/64x64" style="width: 64px; height: 64px;" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIHZpZXdCb3g9IjAgMCA2NCA2NCIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+PCEtLQpTb3VyY2UgVVJMOiBob2xkZXIuanMvNjR4NjQKQ3JlYXRlZCB3aXRoIEhvbGRlci5qcyAyLjYuMC4KTGVhcm4gbW9yZSBhdCBodHRwOi8vaG9sZGVyanMuY29tCihjKSAyMDEyLTIwMTUgSXZhbiBNYWxvcGluc2t5IC0gaHR0cDovL2ltc2t5LmNvCi0tPjxkZWZzPjxzdHlsZSB0eXBlPSJ0ZXh0L2NzcyI+PCFbQ0RBVEFbI2hvbGRlcl8xNWI5ZjFhNWFlZCB0ZXh0IHsgZmlsbDojQUFBQUFBO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1mYW1pbHk6QXJpYWwsIEhlbHZldGljYSwgT3BlbiBTYW5zLCBzYW5zLXNlcmlmLCBtb25vc3BhY2U7Zm9udC1zaXplOjEwcHQgfSBdXT48L3N0eWxlPjwvZGVmcz48ZyBpZD0iaG9sZGVyXzE1YjlmMWE1YWVkIj48cmVjdCB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIGZpbGw9IiNFRUVFRUUiLz48Zz48dGV4dCB4PSIxNCIgeT0iMzYuNSI+NjR4NjQ8L3RleHQ+PC9nPjwvZz48L3N2Zz4=" data-holder-rendered="true"></div> <div class="media-body"> <h4 class="media-heading">標題文字</h4> 內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字 </div> </div>'
   },
  {
       title: '段落範本8',
       image: 'tp12.png',
       description: '文字資訊置左，圖片跟隨右側，有標題及內容',
       html: '<div class="editor-media-box"> <div class="media-body"> <h4 class="media-heading">標題文字</h4> 內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字 </div> <div class="media-right"><img alt="64x64" class="media-object" data-src="holder.js/64x64" style="width: 64px; height: 64px;" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIHZpZXdCb3g9IjAgMCA2NCA2NCIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+PCEtLQpTb3VyY2UgVVJMOiBob2xkZXIuanMvNjR4NjQKQ3JlYXRlZCB3aXRoIEhvbGRlci5qcyAyLjYuMC4KTGVhcm4gbW9yZSBhdCBodHRwOi8vaG9sZGVyanMuY29tCihjKSAyMDEyLTIwMTUgSXZhbiBNYWxvcGluc2t5IC0gaHR0cDovL2ltc2t5LmNvCi0tPjxkZWZzPjxzdHlsZSB0eXBlPSJ0ZXh0L2NzcyI+PCFbQ0RBVEFbI2hvbGRlcl8xNWI5ZjFhNWFlZCB0ZXh0IHsgZmlsbDojQUFBQUFBO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1mYW1pbHk6QXJpYWwsIEhlbHZldGljYSwgT3BlbiBTYW5zLCBzYW5zLXNlcmlmLCBtb25vc3BhY2U7Zm9udC1zaXplOjEwcHQgfSBdXT48L3N0eWxlPjwvZGVmcz48ZyBpZD0iaG9sZGVyXzE1YjlmMWE1YWVkIj48cmVjdCB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIGZpbGw9IiNFRUVFRUUiLz48Zz48dGV4dCB4PSIxNCIgeT0iMzYuNSI+NjR4NjQ8L3RleHQ+PC9nPjwvZz48L3N2Zz4=" data-holder-rendered="true"></div> </div>'
   },
    ]
});
