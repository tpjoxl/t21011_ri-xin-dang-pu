/**
 * @license Copyright (c) 2003-2017, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	config.language = 'zh';  //語系中文
	// config.language = 'en';  //語系英文
	// config.uiColor = '#AADC6E';
	config.resize_enabled = false;  //設定不能resize TEXTAREA
    config.height = 300;  //設定高度
    config.enterMode = CKEDITOR.ENTER_BR;     //換行使用br，不使用p tag
    config.allowedContent = true;
    config.extraPlugins = 'tliyoutube,googlemap,qrc,lineheight,confighelper,codemirror';

	//為加入額外字體
    config.font_names = 'Arial/Arial, Helvetica, sans-serif;Comic Sans MS/Comic Sans MS, cursive;Courier New/Courier New, Courier, monospace;Georgia/Georgia, serif;Lucida Sans Unicode/Lucida Sans Unicode, Lucida Grande, sans-serif;Tahoma/Tahoma, Geneva, sans-serif;Times New Roman/Times New Roman, Times, serif;Trebuchet MS/Trebuchet MS, Helvetica, sans-serif;Verdana/Verdana, Geneva, sans-serif;新細明體;標楷體;微軟正黑體';

	config.toolbar = 'Default';

    config.toolbar_Default =
	[
		['Source', '-', 'Templates'],
		['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo'],
		['Find', 'Replace', '-', 'SelectAll'],
		['Maximize', 'ShowBlocks', '-', 'About'],
		['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat'],
		['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'],
        ['Link', 'Unlink', 'Anchor'],
		// ['Image',  'tliyoutube', 'googlemap', 'qrc', 'Table', 'CreateDiv', 'Iframe', 'HorizontalRule', 'Smiley', 'SpecialChar'], ['TextColor', 'BGColor'],
		['Image',  'tliyoutube', 'googlemap', 'qrc', 'Table', 'CreateDiv', 'Iframe', 'HorizontalRule', 'SpecialChar'], ['TextColor', 'BGColor'],
		['Styles',  'Font', 'FontSize', 'LineHeight']
	];

    config.toolbar_Full =
	[
		['Source', '-', 'Save', 'NewPage', 'DocProps', 'Preview', 'Print', '-', 'Templates'],
		['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo'],
		['Find', 'Replace', '-', 'SelectAll', '-', 'SpellChecker', 'Scayt'],
		['Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField'],
		['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat'],
		['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', 'Iframe', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl'],
		['Link', 'Unlink', 'Anchor'],
		['Image',  'tliyoutube', 'googlemap', 'qrc', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak'],
		['Styles', 'Font', 'FontSize', 'LineHeight'],
		['TextColor', 'BGColor'],
		['Maximize', 'ShowBlocks', '-', 'About']
	];

    config.toolbar_Basic =
	[

    ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat'],
    ['Maximize', 'ShowBlocks', 'Smiley', '-', 'About'],
		['Cut', '-', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo'],
    ['Styles', 'Font', 'FontSize'],
		['TextColor', 'BGColor']
	];

	config.toolbar_simple =
	[
		['Source'],
    ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat'],
		['NumberedList', 'BulletedList'],
    ['Styles', 'Font', 'FontSize'],
		['TextColor', 'BGColor']
	];
	// 範本"替代實際內容"預設不勾選
	config.templates_replaceContent = false;

	// 空的div不補&nbsp;
	config.fillEmptyBlocks = false;

	// 不過慮編輯器的空標籤
	$.each(CKEDITOR.dtd.$removeEmpty, function (i, value) {
		CKEDITOR.dtd.$removeEmpty[i] = false;
    });

    // 原始碼模式的highlight套件設定
    config.codemirror = {
        // theme: 'material',
		theme: 'monokai',  // 主題DEMO頁：https://codemirror.net/demo/theme.html
    }

	// config.toolbarGroups = [
	//     { name: 'document',    groups: [ 'mode', 'document', 'doctools' ] },
	//     { name: 'clipboard',   groups: [ 'clipboard', 'undo' ] },
	//     { name: 'editing',     groups: [ 'find', 'selection', 'spellchecker' ] },
	//     { name: 'insert' },
	//     { name: 'links' },
	//     { name: 'tools' },
	//     '/',
	//     { name: 'styles' },
	//     { name: 'colors' },
	//     { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
	//     { name: 'paragraph',   groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ] },
	// ];
	// config.removeButtons= 'Save,NewPage,Preview,Print,Language';


    //檔案管理器串接(不使用將設定註解掉即可)
 //    config.filebrowserImageBrowseUrl = '../filemanager/index.php';   //圖片用
	// config.filebrowserFlashBrowseUrl = '../filemanager/index.php';	 //Flash用
	// config.filebrowserBrowseUrl = '../filemanager/index.php';		 //超連結用

	// config.filebrowserBrowseUrl = '/AdminLTE/plugins/ckfinder/ckfinder.html';
	// config.filebrowserImageBrowseUrl = '/AdminLTE/plugins/ckfinder/ckfinder.html?Type=Images';
	// config.filebrowserFlashBrowseUrl = '/AdminLTE/plugins/ckfinder/ckfinder.html?Type=Flash';
	// config.filebrowserUploadUrl = '/AdminLTE/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files'; //可上傳一般檔案
	// config.filebrowserImageUploadUrl = '/AdminLTE/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images';//可上傳圖檔
	// config.filebrowserFlashUploadUrl = '/AdminLTE/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash';//可上傳Flash檔案
};
