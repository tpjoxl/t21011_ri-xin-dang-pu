$(document).ready(function() {
  var field_index = $('.prop-val-row').length-1;
  function displayCheck() {
    if ($('input[name=display]:checked').val() == 3) {
      $('.pic-row').show();
    } else {
      $('.pic-row').hide();
    }
  }
  displayCheck();
  $('input[name=display]').change(displayCheck);
  $('.btn-add-prop-val').click(function(event) {
    field_index += 1;
    var clone = $(this).parents('.form-group').find('.prop-val-row').eq(0).clone();
    clone.find('[name^=prop_val]').val('');
    $.each(clone.find('[name^=prop_val]'), function( index, value ) {
      var filed_name = $(this).attr('name');
      var filed_name_before = filed_name.slice(0, (filed_name.indexOf("[")+1));
      var filed_name_after = filed_name.slice(filed_name.indexOf("]"), filed_name.length);
      var filed_name_full = filed_name_before + field_index + filed_name_after;
      $(this).attr('name', filed_name_full);
      if ($(this).hasClass('pic-control')) {
        $(this).attr('id', 'thumbnail'+field_index)
        $(this).siblings('.input-group-btn').children('.btn').data('input', 'thumbnail'+field_index)
        $(this).siblings('.input-group-btn').children('.btn').data('preview', 'holder'+field_index)
        $(this).parents('.col-sm-8').next('.col-sm-2').children('img').attr('id', 'holder'+field_index)
        $(this).parents('.col-sm-8').next('.col-sm-2').children('img').attr('src', '')
      }
    });
    $('.prop-val-holder').append(clone);
    $('.lfm-img').filemanager('image');
    // console.log(clone);
    // $(this).before(clone);
    return false;
  });
  $(document).on('click', '.btn-del-prop-val', function(event) {
    if($('.prop-val-row').length>1) {
      $(this).parents('.prop-val-row').remove();
    } else {
      alert('選項值至少需要一個')
    }
    return false;
  });
});
