$(document).ready(function() {
  if ($('input[name^=products]:checked').length == $('input[name^=products]').length ){
      $('#checkAll').prop('checked', true);
  }
  $.each($('.item-child'), function(index, el) {
    if ($(this).find('input[name^=products]:checked').length > 0) {
      $(this).siblings('.item-root').find('input').prop('checked', true);
    }
  });
  $('#checkAll').on('change', function(){  //'select all' change
      $('#checkAll').prop('checked', $(this).prop('checked'));
      $('input[name^=products]').prop('checked', $(this).prop('checked')).trigger('change'); //change all '.checkbox' checked status
  });
  //'.checkbox' change
  $('input[name^=products]').on('change', function(){
      //uncheck 'select all', if one of the listed checkbox item is unchecked
      if(false == $(this).prop('checked')) { //if this item is unchecked
          $('#checkAll').prop('checked', false); //change 'select all' checked status to false
      }
      //check 'select all' if all checkbox items are checked
      if ($('input[name^=products]:checked').length == $('input[name^=products]').length ){
          $('#checkAll').prop('checked', true);
      }
  });
});
