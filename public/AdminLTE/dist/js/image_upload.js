$(document).ready(function() {
  if (location.hash) {
    $('html,body').stop().animate({
      scrollTop: $(location.hash).offset().top
    }, 400);
  }
  var langDatas;
  $('.btn-file-trigger').click(function(event) {
    $('#imageUpload input[type=file]').click();
    return false;
  });
  $('#imageUpload input[type=file]').change(function(event) {
    var maxSize = $(this).data('max-size');
    var msg = '';
    var maxSizeMsg = maxSize+'KB';
    if (maxSize/1024 >= 1) {
      maxSizeMsg = (maxSize/1024)+'MB';
    }

    $.each(this.files, function( index, value ) {
      if (maxSize*1024<=value.size) { // maxSize單位是KB，要跟bytes比較時須乘1024
        msg += value.name + '\n';
      }
    });
    if (msg) {
      alert(msg+'以上檔案大小超過'+maxSizeMsg+'限制，請重新選擇檔案');
    } else {
      $("form#imageUpload").submit();
    }
  });
  function updateList(data) {
    var list = '';
    $.each(data, function(index, img) {
      // console.log(index, img.path)
      list += '<div class="col-lg-2 col-md-3 col-sm-4 col-xs-6" data-id="'+img.id+'">'
      list += '<div class="img-item">'
      list += '<img src="/'+img.thumb_path+'" alt="">'
      $.each(langDatas, function(langCode, langName) {
        list += '<div class="form-group">'
        list += '<label class="control-label">圖片簡述('+langName+')</label>'
        list += '<input class="form-control" type="text" name="lang['+langCode+'][imgdesc][]" data-id="'+img.id+'" data-lang="'+langCode+'"  placeholder="請輸入圖片簡述('+langName+')" value="'+(img.translations[langCode]['description']?img.translations[langCode]['description']:'')+'">'
        list += '</div>'
      });
      // list += '<div class="form-group">'
      // list += '<label class="control-label">圖片簡述</label>'
      // list += '<input class="form-control" type="text" name="imgdesc" data-id="'+img.id+'"  placeholder="請輸入圖片簡述" value="'+(img.description?img.description:'')+'">'
      // list += '</div>'
      list += '<label class="delete-checkbox">'
      list += '<input class="selectedItem" type="checkbox" name="selectedItem[]" value="'+img.id+'">'
      list += '</label>'
      list += ((img.is_cover==1)?'<div class="cover-tag">封面</div>':'')
      list += '</div>'
      list += '</div>';
    });
    $('#img-list').html(list);
    $("form#imageUpload")[0].reset();
    $('#imgCounter').text($('#img-list .img-item').length);
  }
  $(document).on('change', 'input[name*=imgdesc]', function(event) {
    event.preventDefault();
    var $id = $(this).data('id');
    var $lang = $(this).data('lang');
    var $desc = $(this).val();
    $.ajax({
        type: 'POST',
        url: imgDescUpdateUrl,
        data: {id: $id, lang: $lang, description: $desc, _token: $('input[name="_token"]').val()},
        success: function(data){
          // console.log(data.message);
          // console.log(data.lang_datas);
          langDatas = data.lang_datas;
          updateList(data.datas)
        },
        error: function(data){
          var errors = data.responseJSON;
          alert(errors.message);
        },
    });
  });
  $('.set_cover').click(function(e){
      var atLeastOneIsChecked = $('input[name="selectedItem[]"]:checked').length;
      var href = $(this).attr('href');
      if (atLeastOneIsChecked <= 0) {
          alert("您尚未勾選任何資料");
      } else if (atLeastOneIsChecked > 1) {
          alert("設為封面的只能選一張");
      } else {
        var selectedIds = new Array();
        $('input:checkbox:checked[name="selectedItem[]"]').each(function(i) { selectedIds[i] = this.value; });
        var selectedId = $('input:checkbox:checked[name="selectedItem[]"]').val();
        window.location = href + "?id=" + selectedId;
      }
      return false;
  });
  $('#img-list').sortable({
      handle: "img",
      update: function (event, ui) {
          var data = $(this).sortable("toArray", {attribute : 'data-id'});
          console.log(data)
          $('#sortNum').val(data);
      },
  }).disableSelection();
  $('#sortNum').val($('#img-list').sortable("toArray", {attribute : 'data-id'}));
  $('.btn-save-rank').click(function(event) {
    $('#sortNum').val($('#img-list').sortable("toArray", {attribute : 'data-id'}));
    $("form#rankUpdate").submit();

    return false;
  });
});
