$(document).ready(function() {
  $('.ui-sortable').sortable({
      update: function (event, ui) {
          var data = $(this).sortable("toArray", {attribute : 'data-id'});
          // console.log(data)
          $('#sortNum').val(data);
      },
  }).disableSelection();
  $('#sortNum').val($('.ui-sortable').sortable("toArray", {attribute : 'data-id'}));
  $('.btn-save-rank').click(function(event) {
    $("form#rankUpdate").submit();
    return false;
  });
});
