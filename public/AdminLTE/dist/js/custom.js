$(document).ready(function() {
    $('.lfm-img').filemanager('image', {prefix:App.lfmPrefix});
    $('.lfm-file').filemanager('file', {prefix:App.lfmPrefix});

    $('.btn-deletePic').click(function(e) {
        $(this).siblings('.form-control').val('');
        $(this).parent().siblings('img').attr('src', '');
        return false;
    });
    $('.btn-deleteFile').click(function(e) {
        $(this).siblings('.form-control').val('');
        return false;
    });
    $('.treeview-menu li.active').parents('.treeview').addClass('active');
    $('.tree-menu li.active').parents('.tree-menu li').addClass('open');
    $('.tree-menu li').has('ul').addClass('has-child').append('<span class="toggle-submenu"></span>');

    var active_category = $('.category-menu .dropdown-menu .active').children('a').eq(0).text();
    // console.log(active_category)
    $('.category-menu .name').text(active_category);
    // 匯入excel
    $('.btn-import-trigger').click(function(event) {
      $('#importForm input[type=file]').click();
      return false;
    });
    $('input[name="import_file"]').change(function(event) {
      $("form#importForm").submit();
    });

    $('.item-root input:checkbox').change(function(event) {
        if ($(this).is(':checked'))
            $(this).parents('.item-group').find('.item-child input:checkbox').prop('checked', true).trigger('change');
        else
            $(this).parents('.item-group').find('.item-child input:checkbox').prop('checked', false).trigger('change');
    });
    $('.item-child input:checkbox').change(function(event) {
        if ($(this).parents('.item-group').find('.item-child input:checkbox:checked').length > 0)
            $(this).parents('.item-group').find('.item-root input:checkbox').prop('checked', true);
        else
            $(this).parents('.item-group').find('.item-root input:checkbox').prop('checked', false);
    });
    // $('.toggle-submenu').click(function() {
    //     $(this).siblings('ul').stop().slideToggle();
    //     $(this).parent().toggleClass('open');
    //     return false;
    // });
    // $(".is-colorpicker").colorpicker();

    // 縣市切換就更新區域下拉
    $('select[name=city]').on('change', function(){
        if ($(this).val() != '') {
            var cityId = $(this).val();
            var regions = '';
            $.getJSON("/api/city/"+cityId+"/region", function(result){
                for (var i in result) {
                    regions += '<option value="'+result[i].id+'">'+result[i].name+'</option>';
                }
                $('select[name=region]').html(regions);
            });
        } else {
            $('select[name=region]').html('<option value="">請先選擇縣市</option>');
        }
    });

    $('.hash-tabs a[data-toggle="tab"]').on('click', function(event) {
      event.preventDefault();
      var t = $(this).attr('href');
      location.hash = t.slice(1);
    });
    if (location.hash) {
      // console.log(location.hash);
      $('.hash-tabs a[href="'+location.hash+'"]').trigger('click');
    }

    // $(".select2").select2();
    $('select').selectpicker({
      selectAllText: '全選',
      deselectAllText: '全部取消',
    });
    $('.select-level').on('changed.bs.select', function (e, clickedIndex, newValue, oldValue) {
      // console.log(e.currentTarget, clickedIndex, newValue, oldValue);
      if ($(this).data('max-options')!=1) {
        var arr = [];
        var curr = e.currentTarget;
        var curr_val = $(this).val();
        isSelected = newValue;
        var curr_option = $(curr).find('option').eq(clickedIndex);
        if (isSelected) {
          if (curr_option.data('parents')) {
            var curr_parents = curr_option.data('parents').toString().split(",");
            $.each(curr_parents, function(index, el) {
              if (curr_val.indexOf(el) == -1) {
                curr_val.push(el)
              }
            });
          }
        } else {
          if (curr_option.data('children')) {
            var curr_children = curr_option.data('children').toString().split(",");
            $.each(curr_children, function(index, el) {
              if (curr_val.indexOf(el) > -1) {
                curr_val.splice(curr_val.indexOf(el), 1)
              }
            });
          }
        }
        // console.log(curr_val);
        $(this).val(curr_val);
        $('.select-level').selectpicker('refresh');
      }
    });
    $('.datepicker').datetimepicker({
      format: 'YYYY-MM-DD',
    });
    $(".datetimepicker").datetimepicker({
      format : 'YYYY-MM-DD HH:mm',
    });

    $('#search-form').submit(function () {
        $(this)
            .find('input[name], select[name]')
            .filter(function () {
                return !this.value;
            })
            .prop('name', '');
    });
    $('.btn-search-reset').click(function(event) {
        $('#search-form').find('.form-control').val('');
        $('#search-form').find('select').selectpicker('val', '');
        $('#search-form').find('[type=radio], [type=checkbox]').prop('checked', false).removeProp('checked');
        return false;
    });

    $('.chkAll').on('change', function(){  //'select all' change
        $('.chkAll').prop('checked', $(this).prop('checked'));
        $('.selectedItem').prop('checked', $(this).prop('checked')); //change all '.checkbox' checked status
    });
    //'.checkbox' change
    $('.selectedItem').on('change', function(){
        //uncheck 'select all', if one of the listed checkbox item is unchecked
        if(false == $(this).prop('checked')) { //if this item is unchecked
            $('.chkAll').prop('checked', false); //change 'select all' checked status to false
        }
        //check 'select all' if all checkbox items are checked
        if ($('.selectedItem:checked').length == $('.selectedItem').length ){
            $('.chkAll').prop('checked', true);
        }
    });
    $('.btn-delete-row').click(function(e) {
      if($(this).hasClass('del-all')) {
        if(confirm(alertTxt.delete_all_confirm)==false) return false;
      } else if($(this).hasClass('del-cate')) {
        if(confirm(alertTxt.delete_category_confirm)==false) return false;
      } else {
        if(confirm(alertTxt.delete_item_confirm)==false) return false;
      }
    });
    $('.btn-mail-verify, .btn-mail-enable').click(function(e) {
      if(confirm(alertTxt.sure_to_send)==false) return false;
    });
    $('#del_select_all, .del_select_all').click(function(e){
        var atLeastOneIsChecked = $('input[name="selectedItem[]"]:checked').length > 0;
        var href = $(this).attr('href');
        if (atLeastOneIsChecked <= 0) {
            alert(alertTxt.no_data_selected);
        } else {
            if (confirm(alertTxt.delete_selected_item_confirm)) {
                var selectedIds = new Array();
                $('input:checkbox:checked[name="selectedItem[]"]').each(function(i) { selectedIds[i] = this.value; });
                // console.log(href + "?id=" + selectedIds);
                window.location = href + "?id=" + selectedIds;
            }
        }
        return false;
    });
    $('.setstatus_select_all, #show_select_all, #hide_select_all').click(function(e){
        var atLeastOneIsChecked = $('input[name="selectedItem[]"]:checked').length > 0;
        var href = $(this).attr('href');
        if (atLeastOneIsChecked <= 0) {
            alert(alertTxt.no_data_selected);
        } else {
            var selectedIds = new Array();
            $('input:checkbox:checked[name="selectedItem[]"]').each(function(i) { selectedIds[i] = this.value; });
            // console.log(href + "?id=" + selectedIds);
            window.location = href + "?id=" + selectedIds;
        }
        return false;
    });
});

function PrintTagData(id) {
    if (isFirefox = navigator.userAgent.indexOf("Firefox") > 0) {
        window.print();
    } else {
        var Item = document.getElementById(id);
        var printdetail = window.open("", "TextareaDetail");
        printdetail.document.open();
        printdetail.document.write("<HTML><head>\n");
        printdetail.document.write('<link href="/AdminLTE/bootstrap/css/bootstrap.min.css" rel="stylesheet" />\n');
        printdetail.document.write('<link href="/AdminLTE/plugins/datepicker/datepicker3.css" rel="stylesheet" />\n');

        printdetail.document.write("<style type=\"text/css\">.hidden-print{display:none;} </style>");
        printdetail.document.write("</head>");
        printdetail.document.write("<BODY onload=\"window.print();\">");
        printdetail.document.write(Item.outerHTML);
        printdetail.document.write("</BODY></HTML>");
        printdetail.document.close();
    }
}
