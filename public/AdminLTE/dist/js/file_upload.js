$(document).ready(function() {
  var langDatas;
  $('.btn-file-trigger').click(function(event) {
    $('#fileUpload input[type=file]').click();
    return false;
  });
  $('#fileUpload input[type=file]').change(function(event) {
    var maxSize = $(this).data('max-size');
    var msg = '';
    var maxSizeMsg = maxSize+'KB';
    if (maxSize/1024 >= 1) {
      maxSizeMsg = (maxSize/1024)+'MB';
    }

    $.each(this.files, function( index, value ) {
      if (maxSize*1024<=value.size) { // maxSize單位是KB，要跟bytes比較時須乘1024
        msg += value.name + '\n';
      }
    });
    if (msg) {
      alert(msg+'以上檔案大小超過'+maxSizeMsg+'限制，請重新選擇檔案');
    } else {
      $("form#fileUpload").submit();
    }
  });
});
